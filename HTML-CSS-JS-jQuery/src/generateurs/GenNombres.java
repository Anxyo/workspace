package generateurs;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet générant dynamiquement (à travers le chainage avec nombres.jsp, via un RequestDispatcher)
 * un contenu JSON avec une couleur de 'foreground' (fg), de background (bg), une taille de 
 * police (fontSize), ainsi qu'un tableau de 3 doubles aléatoires.
 * On aurait pu faire + simple...
 * 
 */

@WebServlet("/nombres")
public class GenNombres extends HttpServlet {

	private static final long serialVersionUID = -6412797745537936504L;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setHeader("Cache-Control", "no-cache");	// pas de mise en cache
		response.setHeader("Pragma", "no-cache");
		
		String fg = GenerateurCouleurs.couleurAleatoire();	// génération d'une couleur fg aléatoire
		request.setAttribute("fg", fg);
		String bg = GenerateurCouleurs.couleurAleatoire(); // génération d'une couleur bg aléatoire
		request.setAttribute("bg", bg);
		String fontSize = "" + (10 + GenAleatoire.entierAleatoire(30));
		request.setAttribute("fontSize", fontSize);
		double[] nombres = { Math.random(), Math.random(), Math.random() };
		request.setAttribute("nombres", nombres);
		response.setContentType("application/json");
		RequestDispatcher dispatcher = request.getRequestDispatcher("ajax3/nombres.jsp");
		dispatcher.include(request, response);	// reviens ici après l'appel de include()
	}
}

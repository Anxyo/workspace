package generateurs;

/** Générateur simple de couleurs aléatoires.
 */

public abstract class GenerateurCouleurs {
	
  // Les noms de couleur HTML 'officielles'
  private static String[] couleurs = {
    "aqua", "black", "blue", "fuchsia", "gray", "green", "lime", "maroon",
    "navy", "olive", "purple", "red", "silver", "teal", "white", "yellow" };

  /** une couleur HTML aléatoire */
  
  public static String couleurAleatoire() {
    return(GenAleatoire.elementAleatoire(couleurs));
  }
  
  // pour tester
  public static void main(String[] args) {
    for(int i=0; i<10; i++) {
      System.out.println("Couleur aléatoire : " + couleurAleatoire());
    }
  }
}

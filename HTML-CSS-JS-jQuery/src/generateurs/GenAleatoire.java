package generateurs;

import java.util.Random;
/**
 * Classe utilitaire retournant des valeurs aléatoires, sous forme :
 * - d'un entier plus petit qu'une borne.
 * - d'un indice de tableau aléatoire.
 * - d'un élément aléatoire de tableau.
 *
 */
public class GenAleatoire {
  private static Random r = new Random();
  
  public static int entierAleatoire(int borne) {
    return(r.nextInt(borne));
  }
  
  public static int indiceAleatoire(Object[] tableau) {
    return(entierAleatoire(tableau.length));
  }
  
  public static <T> T elementAleatoire(T[] tableau) {
    return(tableau[indiceAleatoire(tableau)]);
  }
}

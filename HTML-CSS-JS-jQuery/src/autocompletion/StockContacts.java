package autocompletion;

/** Quelques membres de l'équipe jQuery
 *  voir http://jquery.org/team, emails fantaisistes par contre */

public abstract class StockContacts {
  private static Contact[] equipeJQuery = {
    new Contact("John", "Resig", "jresig@exemple.fr",
                "http://static.jquery.com/org/images/team/john.jpg"),
    
      new Contact("Brandon", "Aaron", "brandon@exemple.fr",
                "http://static.jquery.com/org/images/team/brandon.jpg"),
    
    new Contact("Jorn", "Zaefferer", "jz@exemple.fr",
                "http://static.jquery.com/org/images/team/joern.jpg"),
    
    new Contact("Yehuda", "Katz", "ykatz@exemple.fr",
                "http://static.jquery.com/org/images/team/yehuda.jpg"),
    
    new Contact("Dave", "Methvin", "dave_methvin@exemple.fr",
                "http://static.jquery.com/org/images/team/dave.jpg")
  };
  
  public static Contact[] equipeJQuery() {
    return equipeJQuery;
  }

}

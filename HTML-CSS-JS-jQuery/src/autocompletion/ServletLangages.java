package autocompletion;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;



@WebServlet("/langages")
public class ServletLangages extends HttpServlet {
  // langages les plus populaires suivant TIOBE
  // http://www.tiobe.com/index.php/content/paperinfo/tpci/index.html
    
  private static final String langages = 
    "Java,C,C++,PHP,C#,Python,Visual Basic,Objective-C,Perl,Ruby,JavaScript,Delphi," +
    "Lisp,SQL,Pascal,Ada,Object Pascal,Fortran,COBOL,Logo,ActionScript,Haskell,Prolog," + 
    "Erlang,Smalltalk,Forth,Awk,Scala, Bash,Clipper,Curl,Eiffel,F#, Groovy,Modula-2," +
    "Postscript,PowerShell,REALbasic,Verilog,VHDL,XBase,XSLT";
  
  private static final String[] langagesTab = langages.split(",");
  
  @Override
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    String debut = request.getParameter("term"); // noter que 'term' est précablé par autocomplete()
    List<String> langages = retrouveLangages(debut);
    response.setContentType("application/json");
    PrintWriter out = response.getWriter();
    out.print(new JSONArray(langages));
  }
  
  private List<String> retrouveLangages(String debut) {
    debut = debut.toUpperCase();
    List<String> langages = new ArrayList<>();
    for(String langage: langagesTab) {
      if(langage.toUpperCase().startsWith(debut)) {
        langages.add(langage);
      }
    }
    return(langages);
  }
}
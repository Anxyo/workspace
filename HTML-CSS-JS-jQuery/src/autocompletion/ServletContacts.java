package autocompletion;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;


@WebServlet("/contacts")
public class ServletContacts extends HttpServlet {
  @Override
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {
    String debutMot = request.getParameter("term"); // noter que 'term' est pr�cabl� par autocomplete()
    List<Contact> contacts = retrouveContacts(debutMot);
    response.setContentType("application/json");
    PrintWriter out = response.getWriter();
    JSONArray json = new JSONArray(contacts);
    String chaine = json.toString();
    out.print(chaine);
  }
  
  private List<Contact> retrouveContacts(String debutMot) {
    debutMot = debutMot.toUpperCase();
    List<Contact> contacts = new ArrayList<>();
    Contact[] jQueryContacts = StockContacts.equipeJQuery();
    for(Contact contact: jQueryContacts) {
      String prenom = 
        contact.getPrenom().toUpperCase();
      String nom = 
        contact.getNom().toUpperCase();
      if(prenom.startsWith(debutMot) ||
         nom.startsWith(debutMot)) {
        contacts.add(contact);
      }
    }
    return(contacts);
  }
}
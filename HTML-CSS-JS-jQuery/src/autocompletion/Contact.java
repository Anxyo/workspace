package autocompletion;

public class Contact {
  private final String prenom, nom, 
                       email, image;

  public Contact(String prenom, String nom, 
                 String email, String image) {
    this.prenom = prenom;
    this.nom = nom;
    this.email = email;
    this.image = image;
  }

  public String getPrenom() {
    return prenom;
  }

  public String getNom() {
    return nom;
  }

  public String getEmail() {
    return email;
  }

  public String getImage() {
    return image;
  }
  // label et value sont des propriétés aux noms 'magiques' 
  // pour l'auto-complétion
  public String getLabel() {    // ce qui apparait dans le menu
    return prenom + " " + nom;
  }
  
  public String getValue() {   // la valeur insérée en cas de choix
    return(String.format("%s %s, <email : %s>",
                         prenom, nom, email));
  }
}

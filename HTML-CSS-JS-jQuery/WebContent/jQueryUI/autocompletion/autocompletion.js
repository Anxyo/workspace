$(function() {
   $("#zoneLang1").autocomplete({ source: autocompletion.tabLangages });
   $("#btnRechLang1").click(function() { 
       autocompletion.rechercheGoogle("#zoneLang1"); 
     });
   $("#zoneLang2").autocomplete({ source: "../langages" });   // servlet
   $("#btnRechLang2").click(function() { 
       autocompletion.rechercheGoogle("#zoneLang2"); 
     });
   $("#zoneLang3").autocomplete({ source: autocompletion.comparaisonsLangages });
   $("#btnRechLang3").click(function() { 
       autocompletion.rechercheGoogle("#zoneLang3"); 
     });
   $("#zoneLang4").autocomplete({ source: autocompletion.tabLangages });
   $("#btnRechLang4").click(function() { 
       autocompletion.rechercheGoogle("#zoneLang4"); 
     });
   $("#btnToggleStyle").click(function() { 
     $(".ui-autocomplete").toggleClass("menu-transparent"); 
   });
   
   $("#zoneEmail1").autocomplete({ source: "../../contacts" }); // servlet
   $("#zoneEmail2").autocomplete({ 
       source: "../../contacts",
       select: function(event, ui) {
         $("#zoneEmail2").val(ui.item.value);
         $("#email-image").attr("src", ui.item.image);
       }
     });
   
   $(":button").button();
   $("#accordion-panel").accordion({ 
       collapsible: true, active: false, autoHeight: false
     });
});

var autocompletion = {}; // pour éviter les conflits de nommage

    autocompletion.strLangages =
    "Java,C,C++,PHP,C#,Python,Visual Basic,Objective-C,Perl,Ruby,JavaScript,Delphi," +
    "Lisp,SQL,Pascal,Ada,Object Pascal,Fortran,COBOL,Logo,ActionScript,Haskell,Prolog," + 
    "Erlang,Smalltalk,Forth,Awk,Scala, Bash,Clipper,Curl,Eiffel,F#, Groovy,Modula-2," +
    "Postscript,PowerShell,REALbasic,Verilog,VHDL,XBase,XSLT";

autocompletion.tabLangages = autocompletion.strLangages.split(",");

autocompletion.rechercheGoogle = function(selector) {
  var langage = escape($(selector).val());
  window.location.href = "https://www.google.com/#q=" + langage;
};

autocompletion.comparaisonsLangages = function(requete, callback) {
  // Comparaison 'case-insensitive' au début de mot.
  // Suppose aussi que l'utilisateur ne saisit pas de caractères de regexp 
  // dans le champ de recherche. Si on veut se prémunir contre ça, on devra
  // utiliser '$.ui.autocomplete.escapeRegex(requete.term)'.
  var regex = new RegExp("^" + requete.term, "i");
  var comparaisonsOK = $.grep(autocompletion.tabLangages, 
                                function(elemt) { 
                                return(regex.test(elemt)); 
                            });
  callback(comparaisonsOK);
};




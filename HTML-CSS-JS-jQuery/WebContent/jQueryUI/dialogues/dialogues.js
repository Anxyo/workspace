

$(function() {
    $("#btnDialogueSimple")
       .button()
       .click(function() { $("#dialogue-simple").dialog(); });
       
    $("#btnDialogueAnimation")
        .button()
        .click(function() {
          $("#dialogue-animation")
            .dialog({
              show: { effect: "blind",
                      duration: 1000 },
              hide: { effect: "explode",
                      duration: 1000 } });
         });
    
    $("#btnDialogueModal1")
        .button()
        .click(function() {
          $("#dialog-modal1")
            .dialog({ modal: true });
         });
    
    $("#btnDialogueModal2")
        .button()
        .click(function() {
           $("#dialog-modal2")
             .dialog({ 
                modal: true,
                width: 500,
                buttons: { "On s'en fiche !" : demoDialogues.onSenFiche,
                           "Oui, c'est vrai !": demoDialogues.ouiCestVrai }
             });
        });
    $("#btnDialogueAjax").button().click(demoDialogues.afficheNombre);
});

var demoDialogues = {}; // pour éviter des conflits de nommage

demoDialogues.onSenFiche = function() {
  console.log("On s'en fiche vraiment !");
  $(this).dialog("close");
}

demoDialogues.ouiCestVrai = function() {
  console.log("Oui c'est vrai !");
  $(this).dialog("close");
}

demoDialogues.afficheNombre = function(nbre) {
  $("#nombre-resultat").load("aleatoire.jsp");
  $("#dialogue-ajax").dialog();
}
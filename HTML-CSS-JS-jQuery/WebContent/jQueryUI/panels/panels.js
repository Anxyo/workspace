$(function () {
    $("#panels-onglets-statiques").tabs();
    $("#panels-onglets-ajax").tabs({collapsible: true});
    $("#panels-onglets-animes")
            .tabs({
                show: {effect: "blind",
                    duration: 2000},
                hide: {effect: "bounce",
                    duration: 1000}});

    $("#panels-accordeon").accordion();
});
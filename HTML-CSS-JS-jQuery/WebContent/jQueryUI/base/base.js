$(function() {
  $("#btnStyler").click(base.styleBoutons);
  
  $("#btnMaj").button({ 
      icons: { primary: "ui-icon-refresh" } 
  });
  
  $("#btnAccueil").button({ 
      text: false,
      icons: { primary: "ui-icon-home" } 
  });
  
  $("#dateDepart").datepicker();
  $("#dateRetour").datepicker({ changeMonth: true, 
                              numberOfMonths: 2 });
  $("#tableau-vols button").button();
  
  $("#pcs").spinner();
  $("#macs").spinner( { min: 0, max: 5 });
  $("#chromebooks").spinner( { min: 2, step: 2 });
  $("#tableau-stock button").button();
  
  $("#slider-div").slider({ min: 32, max: 212,
                            slide: base.affTemp });
  $("#table-divers button").button();
  
  $("#progressbar").progressbar();
  $("#btnProgress").button()
                          .click(base.majProgress);
});


var base = {}; // Pour éviter des conflits de noms

base.styleBoutons = function() {
  $("#boutons .style-test").button().click(base.affDialogue);
};

base.affDialogue = function() {
  $("#dialogues").dialog();
};

base.affTemp = function(event, ui) {
  $("#temperature").html(ui.value);
};

base.majProgress = function() {
  var idBarre = "#progressbar";
  var valCourante = $(idBarre).progressbar("option", "value");
  if (valCourante < 100) {
    valCourante += 10;
    $(idBarre).progressbar("option", "value", valCourante);
  }
};


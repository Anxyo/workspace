$(function() {
   demoAnimations.ajouteBoutonsEffets("#effets");
   $("#themes input").click(demoAnimations.appliqueThemeSelectionne);
   
   $("#effets :button").button();
   $("#panel-accordeon").accordion({ 
       collapsible: true, active: false, autoHeight: false
     });
});

var demoAnimations = {}; // pour éviter les conflits de noms

demoAnimations.ajouteBoutonsEffets = function(selecteur) {
  var effets = 
   ["blind", "bounce", "clip", "drop", "explode", "fade", "fold", 
    "highlight", "puff", "pulsate", "scale", "shake", "slide"];
  for(var i=0; i < effets.length; i++) {
    var effet = effets[i];
    var attributs = { type: "button", value: effet };
    var bouton = $("<input>", attributs);
    $(selecteur).append(bouton);    // injecte un nouveau bouton
  }
  $(selecteur + " :button").click(function(event) {
      demoAnimations.appliqueEffet(event.currentTarget.value, "#test-effets");
  });
};

demoAnimations.appliqueEffet = function(intituleBouton, selecteur) {
  $(selecteur).toggle(intituleBouton);
};

// construit dynamiquement le lien vers la feuille de style
demoAnimations.appliqueTheme = function(nomTheme) {
  var attributs = { 
    rel: "stylesheet",
    href: "../css/animations-themes/" + nomTheme + "/jquery-ui.css"
  };
  var lienFeuilleStyles = $("<link>", attributs);
  $("head").append(lienFeuilleStyles);
};

demoAnimations.appliqueThemeSelectionne = function() {
  var theme = $(this).val();
  demoAnimations.appliqueTheme(theme);
};


$(function() {
    $("#bouton-lent").click(afficheHeure);
    $("#bouton-createurs").click(afficheCreateurs);
    $("#bouton-liste").click(afficheListe);
});

/* ================================================= */
function afficheHeure() {
  var zoneCommentaire = "#enCours";
  var resultat = "#resultat-lent";
  $(resultat).html(""); 			// efface les affichages précédents
  $(zoneCommentaire).show();
  $.ajax({ 
    url: "heureServeurLente.jsp",
    success: function(text) {
      $(resultat).html(text); 
    },
    complete: function() {
      $(zoneCommentaire).hide(); 
    }
  });
}
/* ================================================= */
function afficheCreateurs() {
  $.ajax({ url: "createurs.json",
           dataType: "json",
           success: afficheCreateursHTML });
}

function afficheCreateursHTML(createurs) {
  $("#resultat-createurs").html("<h2>Le cr&eacute;ateur de JavaScript est " +
                        createurs.JavaScript + "</h2>");
}
/* ================================================= */
function afficheListe() {
  $.ajax({ url: "../nombres",
           dataType: "json",
           success: afficheListeHTML });
}

function afficheListeHTML(jsonData) {
    var codeHTML = "<div style='color:" + jsonData.fg + ";" + "background-color:" + jsonData.bg + ";" +
  					"font-size:" + jsonData.fontSize + "px'>\n" + "<ul>\n" +
  					listeNombres(jsonData.nombres) +
  					"</ul></div>";
  $("#resultat-liste").html(codeHTML);
}


function listeNombres(elemts) {
  var resultat = "";
  for(var i=0; i < elemts.length; i++) {
    resultat = resultat + "<li>" + elemts[i] + "</li>\n";
  }
  return resultat;
}


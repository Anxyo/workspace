$(function() {
    $("#bouton1").click(showParams1);
    $("#bouton2").click(showParams2);
    $("#bouton3").click(showParams3);
    $("#bouton4").click(insertParams);
});

function afficheAlerte(text, status) {
  alert(text);
}

function showParams1() {
  var rqt =
    "param1=" + escape($("#champ1").val()) +
    "&param2=" + escape($("#champ2").val());    // utilisation de escape()
  $.ajax({ url: "afficheParams.jsp",            // pour encoder les données.
           data: rqt,
           success: afficheAlerte });
}

function showParams2() {
  var params =
    { param1: $("#champ3").val(),
      param2: $("#champ4").val() };
  $.ajax({ url: "afficheParams.jsp",
           data: params,
           success: afficheAlerte });
}

function showParams3() {
  $.ajax({ url: "afficheParams.jsp",
           data: $("#data-form").serialize(),
           success: afficheAlerte });
}

function insertParams() {
  console.log("Appel d'afficheParams.jsp");
    $("#resultat").load("afficheParams.jsp", 
    $("#formulaire-load").serialize());
}


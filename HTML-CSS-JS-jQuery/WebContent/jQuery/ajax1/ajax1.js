$(function() {
    $("#bouton-heure-1").click(afficheHeure1);
    $("#bouton-heure-2").click(afficheHeure2);
    $("#bouton-heure-3").click(afficheHeure3);
});


function afficheHeure1() {
  $.ajax({ url: "heureServeur.jsp",
           success: afficheAlerte,
           cache: false });
}

function afficheAlerte(texte, status) {
  alert(texte);
}

function afficheHeure2() {
  insereResultat("heureServeur.jsp", 
                   "#resultat1");
}

function insereResultat(adresse, ou) {
  $.ajax({ 
      url: adresse,
      success: 
        function(texte) {
          insertText(texte, ou); 
        }
  });
}

function insertText(texte, ou) {
  $(ou).html(texte);
}

function afficheHeure3() {
  $("#resultat2").load("heureServeur.jsp");
}
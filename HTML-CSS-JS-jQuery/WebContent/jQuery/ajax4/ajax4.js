/* plus d'infos, voir https://fr.wikipedia.org/wiki/Futures_(informatique)
 * 
 * "En programmation, les notions de futurs (« futures »), promesses (« promises ») ou délais (« delay ») 
 *  font référence à des techniques de synchronisation pour certains langages concurrents. Il s'agit 
 *  d'abstractions qui servent de proxy pour un résultat non-connu au moment où il est référencé pour 
 *  la première fois, car son calcul ou son obtention se feront « plus tard » à l'exécution."
 *  
 *  En JavaScript permet principalement de simplifier le chainage des callbacks (le "callback hell")
 *  en obtenant une promesse et en y chainant de nouveaux handlers.
 *  
 *  Dis autrement : 
 *  
 *  "La promesse est un moyen de dire que certaines fonctions, bien que non bloquantes et asynchrones, sont liées 
 *   entre elles, forment une chaine de traitement, et doivent donc s’exécuter les unes à la suite des autres. Cela 
 *   permet de garantir un ordre d’exécution prévisible, et que chacune puisse accéder au résultat de la fonction 
 *   précédente." 
 *  
 * */


/* 				Enregistrement des handlers			 */

$(function() {
    $("#bouton-basique").click(handlerBasique);
    $("#bouton-surlignage").click(handlerSurlignage);
    $("#bouton-surlignage2").click(handlerSurlignage2);
    $("#bouton-messages").click(handlerMessagesTemp);    
    $("#bouton-basique-diff").click(handlerBasiqueDiff);
    $("#bouton-surlignage-diff").click(handlerSurlignageDiff);
    $("#bouton-messages-diff").click(handlerMessagesTempDiff);
    $("#bouton-when").click(affHeureNombres);
    $("#bouton-custom").click(affNombres);
});

/* ================================================= */
/* 			fonction utilitaire de surlignage		 */
function surligne(selecteur) {
  $(selecteur).hide().show("highlight");
}


/* ================================================= */
/*             Approche standard basique             */

function handlerBasique() {
  insereResultatAjax("heureServeur.jsp", 
                   "#resultat-basique");
}

function insereResultatAjax(adresse, zoneResultat) {
  $.ajax({ 
      url: adresse,
      success: 
        function(texte) {
          insereTexte(texte, zoneResultat); 
        }
  });
}

function insereTexte(texte, zoneResultat) {
  $(zoneResultat).html(texte);
}

/* =========================================================== */
/* Approche standard : surlignage par modification du callback */

function handlerSurlignage() {
	insereResultatAjax2("heureServeur.jsp", 
                    "#resultat-surlignage");
}

function insereResultatAjax2(adresse, zoneResultat) {
  $.ajax({ 
      url: adresse,
      success: 
        function(texte) {
          insereTexte2(texte, zoneResultat); 
        }
  });
}

function insereTexte2(texte, zoneResultat) {
  $(zoneResultat).html(texte);
  surligne(zoneResultat);
}
/* ===================================================== */
/* Approche standard : surlignage lors de l'appel $.ajax */
function handlerSurlignage2() {
	insereResultatAjax3("heureServeur.jsp", 
	                    "#resultat-surlignage2");
	}



function insereResultatAjax3(adresse, zoneResultat) {
  $.ajax({ 
      url: adresse,
      success: 
        function(texte) {
          insereTexte(texte, zoneResultat); 
          surligne(zoneResultat);
        }
  });
}

/* ================================================= */
/* 		Approche standard : messages d'attente 		 */

function handlerMessagesTemp() {
  var adresse = "heureServeurLente.jsp";
  var zoneResultat = "#resultat-msgs";
  var zoneAttente = "#resultat-msgs-encours";
  $(zoneResultat).html(""); // efface les messages précédents
  $(zoneAttente).show();
  $.ajax({ 
      url: adresse,
      success: function(texte) { 
        insereTexte(texte, zoneResultat); 
        surligne(zoneResultat);
      },
      complete: function(texte) { $(zoneAttente).hide(); }
  });
}

/* ================================================= */
/* 				Approche basique différée 			 */

function handlerBasiqueDiff() {
  var adresse = "heureServeur.jsp";
  var zoneResultat = "#resultat-basique-diff";
  var prom = $.ajax({ url: adresse });	// appel plus simple, retourne une 'promesse'
  prom.done(function(texte) { insereTexte(texte, zoneResultat); });
}

/* ================================================= */
/* Chainage d'appel de surlignage() via une promesse */

function handlerSurlignageDiff() {
  var adresse = "heureServeur.jsp";
  var zoneResultat = "#resultat-surlignage-diff";
  var prom = $.ajax({ url: adresse });
  prom.done(function(texte) { insereTexte(texte, zoneResultat); });	// chainage de callbacks
  prom.done(function(texte) { surligne(zoneResultat); });
}

/* ================================================= */
/* Gestion des messages d'attente via une promesse   */

function handlerMessagesTempDiff() {
  var adresse = "heureServeurLente.jsp";
  var zoneResultat = "#resultat-msgs-diff";
  var zoneAttente = "#resultat-msgs-encours-diff";
  $(zoneResultat).html(""); 
  $(zoneAttente).show();
  var prom = $.ajax({ url: adresse });
  prom.done(function(texte) { insereTexte(texte, zoneResultat); });
  prom.done(function(texte) { surligne(zoneResultat); });
  prom.complete(function(texte) { $(zoneAttente).hide(); });
}

/* ================================================= */
/*                Utilisation de when                */

function affHeureNombres() {
  var adresse1 = "heureServeurLente.jsp";
  var zoneResultat1 = "#resultat-when-resultat-heure";
  var adresse2 = "nombresLent.jsp";
  var zoneResultat2 = "#resultat-when-resultat-nombres";
  var zoneAttente = "#resultat-when-encours";
  $(zoneResultat1).html("");
  $(zoneResultat2).html("");
  $(zoneAttente).show();
  var prom1 = $.ajax({ url: adresse1 });
  prom1.done(function(texte) { insereTexte(texte, zoneResultat1); });
  prom1.done(function(texte) { surligne(zoneResultat1); });
  var prom2 = $.ajax({ url: adresse2 });
  prom2.done(function(texte) { insereTexte(texte, zoneResultat2); });
  prom2.done(function(texte) { surligne(zoneResultat2); });
  $.when(prom1, prom2).always(function(texte) { $(zoneAttente).hide(); });
}

/* ================================================= */
/*         Utilisation de when custom                */

function affNombres() {
  var zoneAttente = "#resultat-custom-encours";
  var zoneResultat1 = "#resultat-custom1";
  var zoneResultat2 = "#resultat-custom2";
  $(zoneResultat1).hide();
  $(zoneResultat2).hide();
  $(zoneAttente).show();
  $.when( affNum(zoneResultat1, 6000), 
		  affNum(zoneResultat2, 3000))
		  .done( function(texte) { $(zoneAttente).hide(); }
		  		);
}

function affNum(zoneResultat, millis) {
	  var dfd = $.Deferred();
	  $(zoneResultat).html("Nombre : " + Math.random());
	  $(zoneResultat).fadeIn(millis, dfd.resolve);
	  return dfd.promise();
}

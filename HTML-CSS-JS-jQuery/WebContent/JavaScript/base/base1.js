
/**
 * Types, variables, portées.
 * 
 */
function main(){
   nombresSimples();
   comparaisons();
   datesSimples();
   objetsTemporaires();
   compareChaines();
   compareObjets();
   testPortees();
   testHoist();
}



/**
 * Les nombres ont une représentation interne de doubles.
 * On peut les représenter en décimal, hexa ou octal 
 * (non standard).
 * Leur comparaison peut conduire à des résultats surprenants.
 * A noter quelques nombres "paradoxaux" : Infinity, et Nan.
 * 
 */
function nombresSimples(){
    console.log("\n=====nombresSimples()==============");
    
    var i = 0x45;   // 69 décimal
    var j = 045;    // 37 décimal, octal mais non standard !
    console.log("i = " + i + ", j = " + j);
    // arrondis
    var a = 0.3 - 0.2;
    var b = 0.2 - 0.1;
    if (a === b)
        console.log("a == b");
    else
        console.log("a != b"); // erreur d'arrondi !
    // Math contient de nombreuses méthodes de manipulation de nombres
    console.log("e^a vaut : " + Math.exp(a));
    // Nombres paradoxaux
    console.log("1/0 = " + (1/0) + ", ou " + (-1/0)); 
    console.log("mais le + étrange suit : " + 0/0);     // Nan
}

/**
 * Les booléens littéraux sont true et false, mais dans une 
 * évaluation booléenne :
 * - 0,"", null et undefined valent false
 * - tout le reste vaut true !
 * @returns
 */
function comparaisons(){
    var a = 14;
    console.log("\n=====comparaisons()==============");
    if (a == 14)
        console.log("a vaut bin 14");
    if (a = 14)
        console.log("une erreur classique !");
    if (a)
        console.log("a est 'vrai'...");
}
/**
 * Usage de base des dates.
 * @returns {undefined}
 */
function datesSimples(){
     console.log("\n===== datesSimples()==============");
    var uneDate1 = new Date(2011,05,27);
    var uneDate2 = new Date(1852,12,2,8,0,0);
    var aujourdhui = new Date();
    var tempsEcoule = (aujourdhui - uneDate2)/(1000 * 3600 * 24);
    console.log("Nbre de jours écoulés : " + tempsEcoule);
    console.log("Nous sommes le : " + aujourdhui.toString());   // très complet !
    console.log("Nous sommes le : " + aujourdhui.toDateString());
    console.log("Jour (dimanche == 0) : " + aujourdhui.getDay() + 
                ", mois : " + (aujourdhui.getMonth() + 1) + 
                ", année : " + aujourdhui.getFullYear() );
    
}

/**
 * "Wrapper objects" créés pour évaluer des expressions.
 * @returns {undefined}
 */
function objetsTemporaires(){
    console.log("\n=====objetsTemporaires()==============");
    var t = "un test";
    t.test = 5;          // objet temporaire créé à cette occasion (wrapper object)
    var x = t.test;      // objet tempo supprimé, x vaut undefined
    console.log("valeur de x : " + x);
}

/**
 * Les types primitifs (nombres littéraux, chaines, booléens, undefined et null)
 * sont immuables (constants) : on ne peut pas les modifier. 
 * Toute tentative de le faire génère un nouvel objet (pour les chaines et les 
 * nombres).
 * Leur comparaison se fait par valeur, contrairement aux objets.
 */
function compareChaines(){
    console.log("\n=====compareChaines()==============");
    var s = "bonjour";
    
    var t = "bonjour";
    console.log("la comparaison vaut : " + (s === t));      // true
}
/**
 * Inversement les objets sont référencés et les comparaisons directes
 * posent la question "ces deux références pointent elles vers le même
 * objet ?".
 * @returns {Boolean}
 */
function compareObjets(){
    console.log("\n=====compareObjets()==============");
    var o1 = {x:15}, o2 = {x:15};
 
    console.log("les deux objets sont égaux (réf) ? " + (o1 === o2));           // false
    console.log("les deux objets sont bien égaux (val) ? " + (o1.x === o2.x));  // true 
}

maGlobale = 10;                  // une premiere globale
var maGlobale2 = 10;             // une seconde globale
/**
 * Il existe une portée globale et une locale. 
 * Une variable locale peut masquer une globale.
 * Une fonction peut en contenir une autre qui "voit" les locales de son 
 * "container" et ne peut être appelée que depuis son container.
 * Bonnes pratiques : limiter les globales au maximum, utiliser toujours var
 * pour déclarer les locales.
 * Pas de portée "bloc" en Javascript.
 * 
 */
function testPortees(){
    console.log("\n=====testPortees()==============");
    maGlobal = 25;   // faute de frappe, cree implicitement une autre globale !
    maGlobale++;             // incrémente la globale
    var maGlobale = 20;     // locale qui masque la globale
    maGlobale++;            // incrémente la locale
    imbriquee();
    // appelable uniquement depuis testPortees()
    function imbriquee(){
        
        // affiche la locale qui masque la globale...
        console.log("MaGlobale : " + maGlobale);
    }
}
/**
 * "Hoisting" : toutes les locales sont considérées implicitement
 * comme étant déclarées (mais non forcément initialisée) en début
 * de fonction, même si leur déclaration réelle apparait plus tard.
 * Ainsi dans l'exemple suivant la globale testH est masquée tout
 * au long de la fonction, même AVANT sa déclaration formelle.
 * Bonne pratique : déclarer toutes les locales en début de fonction...
 */
var testH = "test Hoist";
function testHoist(){
    console.log("\n=====testHoist()==============");
    // ici, implicitement 'var testH;'
    console.log("valeur de testHoist : " + testH); // "undefined"
    var testH;
    testH = 10;
}
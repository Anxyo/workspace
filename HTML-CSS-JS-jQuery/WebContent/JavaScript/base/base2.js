/**
 * Opérateurs et ruptures de séquence.
 * 
 */

function main(){
    initialiseurs();
    operateursUnaire();
    operateursBinaires();
    operateurTernaire();
    operateursRelationnels();
    operateurEval();
    tests();
    boucles(5);
    operateurDelete();  // utilise une boucle for...in
    tryCatch();
    
    // ajouter : "uses strict", .
}
/**
 * Syntaxe utilisée pour initialiser un objet ou un tableau.
 * 
 */
function initialiseurs(){
    console.log("\n=====initialiseurs()==============");
    // définition d'objets
    // -------------------
    // rectangle a deux propriétés : longueur et largeur
    var rectangle = {longueur:15,largeur:20};
    // aucune propriété encore pour carre
    var carre = {};
    carre.longueur = 7; // ajout de la prop. longueur lors de l'accès
    carre.largeur = 7;  // ajout de la propriété largeur
    // objets imbriqués
    var pageHtml = {head: "<head><title>Le titre</title></head>",
                    body: {h1:"Titre du body",
                            div1:"Le div1",
                            div2:"Le div2"}};
            
    // noter les 2 syntaxes d'accès
    console.log("Accès à carre.longeur : " + carre.longueur);
    console.log("Accès à carre['longeur'] : " + carre['longueur']);
    console.log("Accès à div1 (v1) : " + pageHtml.body.div1);
    console.log("Accès à div1 (v2) : " + pageHtml["body"]["div1"]);
    // 
 // on choisira l'accès par [] si :
   // - on doit calculer le nom de la propriété à laquelle on veut accéder.
   // - ce nom est un mot réservé (à éviter !)
   // - si, bien sûr, cet objet est un tableau !
  
   // définition de tableaux
   // ----------------------
   var tab1 = [1,2,3,4,5,6];
   var tab2 = [];
   console.log("longeur de tab2 : " + tab2.length); // 0
   tab2[0] = 15;    // ajout d'un élément
   console.log("longeur de tab2 : " + tab2.length); // 1
   tab2[5] = 12;    // ajout d'un 2d élément en position 5
   console.log("longeur de tab2 : " + tab2.length); // 6
   var tab3 = [0,,,,,6];    // tableaux avec des "trous"
   console.log("longeur de tab3 : " + tab3.length); // 6
   // tableau "embarquant" un autre tableau comme élément
   tab3[3] = tab1;
   console.log("Valeur de tab3[3][2] : " + tab3[3][2]); // 3
   // tableau à deux dimensions
   var tab2D = [[1,2,3],[4,5,6],[7,8,9]];
   console.log("Valeur de tab2D[2][2] : " + tab2D[2][2]); // 9
}

function operateursUnaire(){
    console.log("\n=====operateursUnaire()==============");
    var a = 10;
    var b = a++;        // 11
    b = ++a;            // a et b valent 12
    var c = a++ * 3;        // 36, a vaut 13
    var d = ++b * 3;        // 39, b vaut 13
    console.log("c = " + c + ", a = " + a);
    console.log("d = " + d + ", b = " + b);
    a = b = c = d;          // tous à 39
    a--;                    // idem pour --
    --a;
}

function operateursBinaires(){
    console.log("\n=====operateursBinaires()==============");
    // + additionne deux nombres ou concatène deux chaines
    // si un des opérandes est une chaine l'expression est convertie en chaine
    var a = 12;
    var b = "coucou";
    console.log("somme a + b = " + a + b);  // 12coucou
     
    // si des opérandes sont de type objet, règles + complexes :
    // - s'il s'agit de dates, elles sont converties en chaine
    var c = new Date();
    console.log("somme 12 + c = " + (12 + c));  // 12.......(chaine date)
    
    // - s'il s'agit d'un autre type, encore 2 cas (!) :
    //    - s'il existe une méthode valueOf() convertissant en type primitif -> appelée
    var v1 = 10 +  true;        // conv. true-> 1 = 11
    var v2 = 10 + null;         // conv. null -> 0 = 10
    var v3 = 10 + undefined;    // conv. undefined -> Nan = Nan
    console.log("v1 = " + v1);  
    console.log("v2 = " + v2);  
    console.log("v3 = " + v3);  
        
    //    - sinon, converti en chaine via toString().
     var o = {x:12,y:15};       
     console.log("somme 12 + o = " + (12 + o));  // 12[object Object]
}

function operateurTernaire(){
    console.log("\n=====operateurTernaire()==============");
    var v1 = 12, v2 = 15, v3 = "ok", v4 = "pb";
    console.log("Résultat : " + (v1 < v2 ? v3 : v4));
}

function operateursRelationnels(){
    console.log("\n=====operateursRelationnels()==============");
    /**
     * Les opérateurs de comparaisons == et === sont différents :
     * 
     * === n'effectue aucune conversion de type. Si les types diffèrent 
     * le résultat est faux. Cet opérateur est la "comparaison stricte".
     * Si des chaines sont comparées tous les caractères doivent être les
     * mêmes (sans "normalisation unicode").
     * 
     * == tente de comparer strictement d'abord, comme ci-dessus. Si cela 
     * ne fonctionne pas il tente de convertir les opérandes et de comparer
     * les résultats des conversions (en utilisant valueOf() ou toString()).
     * 
     */
    var v1 = "toto";
    if ("toto" === v1){
        console.log("v1 vaut bien 'toto'");     // ok
    }
    var v2 = "12";
    if (v2 === 12)
        console.log("v2 === 12");     
     else
        console.log("v2 !== l'entier 12 !");  // conv. nécessaire !
    if (v2 == 12)
        console.log("v2 == 12");              // ok, conv effectuée !
     else
        console.log("v2 != l'entier 12 !");   
    /**
     * Les opérateurs <, >, <=, >= comparent tous les types après
     * les avoir convertis vers des chaines ou des nombres (comme
     * décrit précédemment) et un seul des deux est numérique, l'autre
     * est également converti en nombre. Ils sont ensuite comparés par 
     * valeur ou ordre alphabétique.
     */
     var u1 = "12" < 2;     // "12" converti en 12, résultat false
     var u2 = "12" < "2";   // comparaison alphabétique, résultat true
     console.log("u1 = " + u1);   
     console.log("u2 = " + u2);   
     /**
      * L'opérateur in retourne vrai si l'objet ou le tableau auquel il
      * s'applique a bien une propriété de ce nom.
      * 
      */
     var rectangle = {longueur:12, largeur:15};
     var tab = [1,2,3];
     if ("longueur" in rectangle){
         console.log("longueur est bien une prop. de rectangle");
     }
     if ("0" in tab){
         console.log('"0" une prop. de tab');
     }
     if (0 in tab){
         console.log('0 une prop. de tab (après conversion)');
     }
     if (3 in tab)
         console.log('3 une prop. de tab (après conversion)');
     else   // le tableau n'a que 3 éléments (props "0","1","2")
         console.log('3 n\'est pas une prop. de tab (après conversion)');
    /**
     * L'opérateur instanceof retourne true si l'opérande de gauche
     * est bien du type de la classe de droite.
     */
    var d = new Date();
    if (d instanceof Date){
        console.log('d est bien une instance de Date');
    }
    
    // Les opérateurs && et || effectuent des court-circuits d'évaluation.
    var a = 12, b = 15;
    if ((a > b) && aDroite()){   // aDroite() n'est pas appelée
    } 
    /**
     * Comme dans les autres langages de la même famille (les dérivés du C
     * comme C++, Java, C#) les opérateurs +=, -=, *= etc sont également
     * présents en JavaScript.
     */    
}

function aDroite(){
    console.log('Je suis la fonction ADroite() !');
    return true;
}
/**
 * eval() évalue interprète dynamiquement une chaine de caractères.
 * L'expression qui lui est passée n'est pas le seul élément à prendre
 * en compte : les variables définies dans son contexte d'appel sont
 * également vues par elle.
 */
function operateurEval(){
    console.log("\n=====operateurEval()==============");
    var x = 15;
    eval("x = x + 1;");
    console.log("valeur de x : " + x);  // 16, x est modifié
}
/**
 * delete peut supprimer des propriétés d'un objet.
 * La boucle for...in permet d'énumérer toutes les propriétés
 * énumérables d'un objet. 
 */

function operateurDelete(){
    console.log("\n=====operateurDelete()==============");
    var o = {x:1,y:2,z:3};
    for (var v in o){
        console.log("propriété courante : " + v);
    }
    delete o.x;
    console.log("Après delete, il reste : ");
    for (v in o){
        console.log("propriété courante : " + v);
    }
}


function tests(){
      console.log("\n=====tests()==============");
 /**
  * Le très classique if ... else if ... else...
  * 
  */
 var alea, i = 0;
 while(i < 5){
    alea = Math.random() * 10;  // alea compris entre 0 et 10
    if (alea < 3){
        console.log("alea est petit : " + alea);
    } else if ((alea >= 3)&&(alea < 6)){
        console.log("alea est moyen : " + alea);
    } else {
        console.log("alea est surement grand : " + alea);
    }
    i++;
 }
 
 /**
  * construction switch / case également bien connue...
  */
 var v1 = 12, v2 = "test", v3 = true, v4 = {}, v5 = [], v6 = undefined, v7 = null;
 var w = v4;
 //var w = tests;   // fonction
 switch (typeof w){
     case "number":
         console.log("nombre !");
        break;
     case "undefined":
        console.log("undefined !");
        break;
     case "object":
        console.log("object !");
        break;
     case "boolean":
        console.log("booléen !");
        break;
     case "function":
         console.log("fonction !");
         break;
     default:
         console.log("inconnu !");
 }
 
}

function boucles(valeur){
      console.log("\n=====boucles()==============");
      var r = 1;
      /**
       * version avec for
       */
      for (var i = 2; i <= valeur ; i++){
          r *= i;
      }
      console.log("Avec for, factorielle de " + valeur + " = " + r);
      
      /**
       * version avec while
       */
      var i = 1;
      r = 1;
      while(1){
          //debugger;     // active le debugger, s'il y en a un...
          r *= i;
          i++;
          if (i > valeur)
              break;
          else
              continue;      // inutile
      }
      console.log("Avec while, factorielle de " + valeur + " = " + r);
      
      /**
       * version avec do...while
       */
      i = 1;
      r = 1;
      do {
          r *= i;
          i++;
      }while (i <= valeur);
      console.log("Avec do...while, factorielle de " + valeur + " = " + r);    
      
      // ajouter la boucle for ... in
      
      
}

function tryCatch(){
      console.log("\n=====tryCatch()==============");
      try {
          console.log("factorielle 5 : " + intermediaire(5));
          console.log("factorielle -3 : " + intermediaire(-3));
          console.log("factorielle 19 : " + intermediaire(19));
      } catch (exc){
          console.log("Exception interceptée, bloc catch, fonction tryCatch()");
          console.log("TypeOf de exc ==" + typeof exc);
          console.log("Détails : " + exc);
          
      } finally {   // exécuté dans tous les cas
          console.log("bloc finally");
      }
}

function intermediaire(param){
       try {
        return factorielle(param);
      } catch (exc){
          console.log("Exception interceptée, bloc catch, fonction intermediaire : " + exc);
          throw exc;    // redéclenche l'exception (pas obligatoire !)
      }
}

function factorielle(p){
    if ((typeof p === "undefined")||
        (typeof p === "null")||
        (typeof p !== "number")){
            console.log("factorielle, type de p : " + typeof p);    
            throw new TypeError("Type invalide !");
        }
    if (p <= 0){
        throw "Valeur de paramètre négative !";
    } else {
        var r = 1;
        for (var i = 2; i <= p; i++)
            r *= i;
        return r;
    }
}



function main(){
    utilisationBase();  
    ajoutSuppressionElements();
    utilJoinReverse();
    utilPushPop();
    utilUnshiftEtShift();
    utilSort();
    utilConcatSliceEtSplice();
    utilForEach();
    utilAutresES5();
}

/**
 * 
 * Déclaration, lecture/écriture
 */
function utilisationBase(){
     console.log("\n=====utilisationBase()==============");
    // création et déclaration de tableaux
    var tab1 = [1,2,3,4];   // crée un tableau de 4 élément, d'indices 0 à 3
    var tab2 = [1,2,tab1];  // crée un tableau de 3 éléments, dont le dernier est un tableau
    var tab3 = [1,,,3];     // crée un tableau de 4 éléments, dont ceux d'indices 1 et 2 
                            // valent undefined.
    var tab4 = [,,,,];      // crée un tableau de 4 éléments, la dernière virgule est ignorée
    console.log("taille de tab4 : " + tab4.length);     // 4
    var tab5 = [];          // crée un tableau vide
    var tab6 = new Array(5);    // crée un tableau préalloué de 5 éléments
    console.log("taille de tab6 : " + tab6.length);     // 5
    
    // lecture/écriture des éléments
    tab5[2] = "coco";
    console.log("taille de tab5 : " + tab5.length);     // 3
    tab5[0] = 1;tab5[1] = 2;
    
    // si une conversion en entier est possible...
    console.log("Element de tab5 en pos. 1 : " + tab5["1"]);    // convertible
    console.log("Element de tab5 en pos. 1.0 : " + tab5[1.0]);  // convertible
    
    // boucle traditionnelle
    var s = "";
    for (var i = 0; i < tab3.length; i++){
        if (!tab3[i]) continue; // pour ignored les valeurs undefined / null 
        s += tab3[i] + " ";
    }
    console.log("s = " + s);
    // boucle for / in
    s = "";
    for (var index in tab3){    // range dans index les noms des prop., donc ici les indices
        s += tab3[index] + " ";
    }
    console.log("s = " + s);
}

/**
 * 
 * Ajout/suppression d'éléments 
 */
function ajoutSuppressionElements(){
    console.log("\n=====ajoutSuppressionElements()==============");
    // pour ajouter des éléments, il suffit de les affecter à une certaine position
    // le tableau se redimensionne en conséquence
    var tab1 = [1,2,3,4]; 
    tab1[4] = 5;    // accroissement
    
    tab1[10] = 11;    // les éléments intermédiaires valent undefined
    console.log("Elements de tab1 : " + tab1);
    // pour supprimer des éléments, on utilise delete ou on affecte une valeur à la prop. length.
    // l'utilisation de delete ne diminue PAS la longueur d'un tableau, la valeur 
    // correspondante vaut simplement undefined. Les lignes suivantes n'ont pas d'effet :
    delete tab1[5]; delete tab1[6];delete tab1[7];delete tab1[8];
    console.log("Elements de tab1 : " + tab1);
    // par contre, celle ci en a un :
    delete tab1[10];
    console.log("Elements de tab1 : " + tab1);
    // pour tronquer le tableau :
    tab1.length = 5;
    console.log("Elements de tab1 : " + tab1);
}
 

function utilJoinReverse(){
    console.log("\n=====utilJoinReverse()==============");
    // join() concatène tous les élément en une chaine. Le 
    // séparateur des éléments est le param. passé en entrée.
    console.log('Valeur de [1,2,3,4].join("-") : ' + [1,2,3,4].join("-"));
    // reverse() inverse l'ordre des éléments d'un tableau. Le tableau
    // sur lequel l'appel est effectué est le tableau inversé.
    console.log('Valeur de [1,2,3,4].reverse() : ' + [1,2,3,4].reverse()); // appel de Array.toString() 
}

function utilPushPop(){
    console.log("\n=====utilPushPop()==============");
      var pile1 = [1,2,3,4]; 
      pile1.push(5,6,"coco");
      console.log("Valeur de pile1, après un push() : " + pile1);
      pile1.pop();
      console.log("Valeur de pile1, après un 1er pop : " + pile1);
      pile1.pop();
      console.log("Valeur de pile1, après un 2d pop : " + pile1);
}

function  utilUnshiftEtShift(){
    console.log("\n=====utilUnshiftEtShift()==============");
    var fifo = [1,2,3,4];
    fifo.unshift(0);
    fifo.unshift(-1);
    console.log("fifo, après 2 unshift() : " + fifo);
    fifo.shift();fifo.shift();
    console.log("fifo, après 2 shift() : " + fifo);
    // attention à l'ordre avec unshift(-1,0) : d'abord 0 est inséré, puis -1.
    // Donc unshift(-1,0) est équivalent à unshift(0);unshift(-1) :
    fifo.unshift(-1,0);
    console.log("fifo, après 'appel de .unshift(-1,0) : " + fifo);
}
function utilSort(){
    console.log("\n=====utilSort()==============");
    // les tris sont fait, par défaut sur des chaines de caractères.
    // Si nécessaire une conversion est donc réalisée, comme ci-dessous.
    console.log("Appel de [1,2,6,3].sort() : " + [1,2,6,3].sort()); 
    // pour avoir la preuve de cette conversion (07 reste ne premier) : 
    console.log("Appel de ['07',2,6,3].sort() : " + ['07',2,6,3].sort()); 
    
    // passage d'une fonction de compararaison à sort(). Elle doit recevoir
    // deux paramètres et retourner une val > 0 si le 1er par. est à placer
    // APRES le second et une valeur < 0 si le premier par. est à placer AVANT.
    // Noter que dans l'exemple suivant la comparaison est bien numérique : 
    console.log("Tri décroissant de ['07',2,6,3] : " + ['07',2,6,3].sort(function(a,b){ return a < b;})); 
}

function utilConcatSliceEtSplice(){
    console.log("\n=====utilConcatSliceEtSplice()==============");
    // slice() découpe une "tranche" d'un tableau : on lui précise les bornes 
    // de la découpe et un nouveau tableau est retourné. Le tableau d'origine
    // n'est pas modifié.
    console.log("Valeur de [1,2,3,4].slice(1,3) : " + [1,2,3,4].slice(1,3));
    // splice() est différente, elle effectue à la fois des suppressions et
    // des ajouts au tableau qu'elle modifie. 
     var tab = [1,2,3,4];
     // Elle reçoit :
     // - la position ou commence la suppression et l'éventuelle insertion
     // - le nombre d'éléments à supprimer
     // - une éventuelle liste d'éléments à ajouter à partir de la position passée
     //   comme premier argument.
     // Elle retourne un tableau contenant les éléments supprimés
     var tranche = tab.splice(1,2,17,17);   // supprimer 2 élément à partir de la position 1
                                            // et les remplacer par 17,17
     console.log("splice(), tableau après : " + tab + ", tranche : " + tranche);
}

function utilForEach(){
     console.log("\n=====utilForEach()==============");
     var tab = [1,2,3,4];
     var somme = 0, produit = 1;
     
     // Reçoit une fonction comme paramètre d'entrée, avec 3 paramètres : la valeur courante,
     // l'indice dans le tableau de la valeur courante et le tableau lui même. Un seul utilisé ici.
     // Les autres pourraient être omis.
     tab.forEach(function(valeur, position, tableau){somme += valeur;produit *= valeur;});
     console.log("Somme : " + somme + ", produit : " + produit);
     
     // Noter qu'il n'y a pas de moyen direct d'interrompre le parcourt du tableau.
     // Une solution indirecte serait de générer une exception dans la fonction appelée
     // et de placer le forEach() dans un try...catch ...
     
}

function utilAutresES5(){
    console.log("\n=====utilAutresES5()==============");
    var tab = [1,2,3,4,3,2,1];
    console.log(   "Appel de indexOf(2) : " + tab.indexOf(2) + 
                   " et de lastIndex(2) : " +  tab.lastIndexOf(2)); // 1 et 5
      
    // Utilisation de map() : retourne un tableau de boolééns indiquant 
    // si l'élément correspondant est pair ou non.
    var pairs = tab.map(function(elt){return (elt % 2) === 0;});
    console.log(   "Appel de map() sur tab : " + pairs);
    
    
    // Utilisation de filter() : filtrage suivant un prédicat
    // on veut seulement les nombres pairs de tab : 
    var pairs = tab.filter(function(elt){return (elt % 2) === 0;});
    console.log(   "Appel de filter() sur tab : " + pairs);
    
    // Utilisation de every() : retourne true si TOUS les éléments
    // d'un tableau sont bien conformes à un prédicat.
    console.log(   "Appel de every() sur tab : " + (tab.every(function(elt){return (elt % 2) === 0;}) ?
                                                    "tous les éléments sont pairs" : "au moins un élément impair"));
    // Utilisation de some() : retourne true si AU MOINS 1 élément
    // d'un tableau est bien conforme à un prédicat.
    console.log(   "Appel de some() sur tab : " + (tab.some(function(elt){return (elt % 2) === 0;}) ?
                                                    "au moins un élément pair" : "aucun élément n'est pair"));
                                                    
    // utilisation de reduce() : elle appelle répétitivement la fonction en lui transmettant
    // son résultat précédant (1er param) et l'élément courant (2d param). Au premier appel, la valeur
    // précédante vaudra le second param passé à reduce() (ici 0).
    var somme = tab.reduce(function(valPrec, valeur){return valPrec + valeur;}, 0);
    console.log(   "Appel de reduce : " + somme + ", sur tab : " + tab);
                                                    
}
function main(){
    creationObjets();
    heritage();
    retrouveProprietes();
    getterEtSetters();
    propertyDescriptors();
    retrouvePrototype();
    retrouveClasse();
    verrouillageObjets();
    serialisationJSonMinimale();
    heritageMinimal();
}

function creationObjets(){     
     console.log("\n=====creationObjets()==============");
    // définition d'un objet littéral
    var listePoints = [{x:0,y:0},{"x":17,"y":2},{"x":19,"y":15},{"x":21,"y":-3},{"x":45,"y":12},{x:0,y:0}];
    var polygone = {nom:"region",points:listePoints};
    console.log("Affichage d'une coordonnée : " + polygone.points[2].x);
    
    // utilisation de new
    // utilise les propriétés du prototype pour initialiser le nouvel objet
    var sorbonne = new Date(1968,5,3);   // utilise Date.prototype pour définir les prop.
    console.log("Une journée de mai, il y a presque 50 ans : " + sorbonne.toLocaleDateString());
    
    // utilisation de Object.create()
    // create() crée un nouvel objet à partir d'un autre considéré comme son prototype,
    //          passé en paramètre.
    var point = {x:10,y:20};
    var autrePoint = Object.create(point);  // point est le prototype de autrePoint,
                                            // ou autrePoint 'hérite' de point.
    // on retrouve les propriétés de 'point'
    /*
     * Noter que l'utilisation des [] est plus souple que celle du ., puisque
     * le nom de la propriété peut être une expression calculée.
     */
    for (var p in autrePoint)
        console.log("\tprop : " + p + ", valeur :" + autrePoint[p]);    
}

function heritage(){            
     console.log("\n=====heritage()==============");
     /*
     * Chaine d'héritage : p3 dérive de p2 qui lui même dérive de p1, qui dérive de Object.
     */
     var p1 = {};   // dérive de Object.prototype
     p1.x = 10;     // ajout de propriété
     p2 = Object.create(p1);    // p2 dérive de p1.prototype
     p2.y = 15;
     p3 = Object.create(p2);    // p3 dérive de p2.prototype
     p3.z = 20;
     p3.x = "coco"; // masque la propriété héritée
     for (var p in p3)
        console.log("\tDans p3, prop : " + p + ", valeur :" + p3[p]);    
     // lire une propriété qui n'existe pas retourne undefined
     // mais une propriété d'un objet qui n'existe pas est une erreur
     console.log("\tDans p3, la prop 'sigmund' vaut: " + p3["sigmund"]);    
     //console.log("\tDans p4, la prop 'sigmund' vaut: " + p4["sigmund"]);  // ERREUR        
    
     // de ce fait la ligne suivante provoque une erreur (undefined n'a pas de props !)
     //console.log("\tDans p3, la prop 'sigmund' vaut: " + p3["sigmund"].length);    
   
     // bizarrement, la ligne suivante ne produit aucune erreur 'tonitruante'
     // Bien sur, l'affectation n'est pas effectuée, mais le runtime ne proteste pas...
     Object.prototype = 0;  // erreur en mode strict, tout de même...
}

function retrouveProprietes(){      // 6.4
     console.log("\n=====retrouveProprietes()==============");
     // Pour tester si une propriété est bien présente, on peut utiliser l'operateur in
     // mais il existe également les deux méthodes :
     // - hasOwnProperty() : teste si une propriété est bien présente, et uniquement
     //                      parmis les propriétés ajoutées et non pas héritées.
     // - propertyIsEnumerable() : fait la même chose que la précédente, mais ne retourne 
     //                             vrai que si la prop. est en plus énumérable.
     var p1 = {};   // dérive de Object.prototype
     p1.x = 10;     // ajout de propriété
     p2 = Object.create(p1);    // p2 dérive de p1.prototype
     p2.y = 15;
     p3 = Object.create(p2);    // p3 dérive de p2.prototype
     p3.z = 20;
     console.log("p3 a une propriété z ? : " + p3.hasOwnProperty("z")); // true
     console.log("p3 a une propriété y ? : " + p3.hasOwnProperty("y")); // false, héritée
     
     console.log("p3 a une propriété z énum. ? : " + p3.propertyIsEnumerable("z")); // true
     console.log("p3 a une propriété y énum. ? : " + p3.propertyIsEnumerable("y")); // false
     console.log("p3 a une propriété toString énum. ? : " + p3.propertyIsEnumerable("toString")); // false
     // enfin, une autre technique est de tenter d'accéder à la propriété et comparer
     // le résultat à undefined pour savoir si elle existe :
     if (p3.sigmund === undefined)
         console.log("pas de prop. sigmund dans p3 !"); 
}

function getterEtSetters(){
     console.log("\n=====getterEtSetters()==============");
    // les propriétés avec des accesseurs sont présentes dans ES5
    // leur syntaxe ne s'invente pas... Noter l'usage de this.
    var monObjet = { x : 10, 
                     y : 10,
                     prop : 0,
                     get maProp() {return this.prop;},
                     set maProp(v){this.prop = v;}
    };
    monObjet.maProp = 20;   // comme d'habitude...
    console.log("valeur de monObjet.prop : " + monObjet.maProp);  // changé, 20
    // l'un des intérêt est une propriété en lecture seule
    var rectangle = {
        x : 10,y : 12,
        get surface(){return this.x * this.y;}
    };
    console.log("valeur de rectangle.surface : " + rectangle.surface);  // 120
}
/*
 * Une propriété a 4 attributs, en plus de son nom : sa valeur bien sur,
 * mais également :
 * - un drapeau indiquant si elle est modifiable ("writable")
 * - un autre indiquant si elle est énumérable ("enumerable")
 * - et un dernier indiquant si configurable (cad si on peut la
 *   supprimer avec delete et changer ses autres attributs).
 *  En ES5 il est possible de controler tout cela avec des
 *  "property descriptors", un objet décrivant une propriété.
 */
function propertyDescriptors(){
    console.log("\n=====propertyDescriptors()==============");
    var o = {x:10,y:20};
    // on peut obtenir un property descriptor à partir d'une propriété courante
    // (non héritée) avec getOwnPropertyDescriptor() :
    var pdx = Object.getOwnPropertyDescriptor(o,"x");   // ok
    var pdts = Object.getOwnPropertyDescriptor(o,"toString");   // undefined, héritée
   // mais on préfère généralement créer de nouvelles propriétés en controlant
   // les 4 attributs, via l'appel de Object.defineProperty() :
   var p = {y:20};
   p = Object.defineProperty(p,"x",pdx);                // sur le modèle de o.x
   o = Object.defineProperty(o,"nlleProp",{
                                value:25,
                                writable:true,
                                enumerable:false,
                                configurable:true});  // + de controle
    // bilan
    console.log("valeur de p.x = " + p.x);
    console.log("Après création : valeur de o.nlleProp = " + o.nlleProp);
    o.nlleProp = 30;    // writable
    for (var prop in o){    // non énumérable
        console.log("Enumération : valeur de o." + prop + " = " + o[prop]);
    }
    delete o.nlleProp;     // configurable
    console.log("Après suppression : valeur de o.nlleProp = " + o.nlleProp);    // undefined
  
    // pour définir de multiples propriétés d'un coup on utilisera defineProperties() :
    var obj = {}; // pas grand chose dedans...
    Object.defineProperties(obj,{   "x":{value:10,writable:false,enumerable:true, configurable:true},
                                    "y":{value:11,writable:true,enumerable:false, configurable:true},
                                    z:{value:12,writable:false,enumerable:false, configurable:false}});
    obj.z = 15;
    console.log("Valeur de z, après tent. de modif. : " + obj.z);
    delete obj.z;
    console.log("Valeur de z, après tent. de suppr. : " + obj.z);
    console.log("z est dans obj ? : " + (obj.z in obj));    // niet...
}
/**
 * La fonction Object.isPrototypeOf() retourne true si un objet est bien 
 * le prototype d'un autre.
 * Si l'héritage est effectué sur plusieurs "génération", isPrototypeOf()
 * définit si un objet est "membre de la chaine des prototypes" d'un autre.
 * Le prototype d'un objet créé par new est le prototype du constructeur.
 */
function retrouvePrototype(){
    console.log("\n=====retrouvePrototype()==============");
    var p1 = {};   // dérive de Object.prototype
     p1.x = 10;     // ajout de propriété
     p2 = Object.create(p1);    // p2 dérive de p1.prototype
     p2.y = 15;
     p3 = Object.create(p2);    // p3 dérive de p2.prototype
     p3.z = 20;
     if (p1.isPrototypeOf(p3)){
          console.log("p1 est bien membre de la chaine des prototypes de p3"); 
     }
     if (p2.isPrototypeOf(p3)){
          console.log("p2 est également membre de cette chaine."); 
     }
}
/*
 * La classe d'un objet est une simple chaine de caractères permettant de 
 * le catégoriser. Il n'est pas très facile de la retrouver. On utilise pour 
 * cela Object.toString() héritée par tout objet JavaScript.
 */
function retrouveClasse(){
    console.log("\n=====retrouveClasse()==============");
    var d = new Date();
    console.log("classe de d : " + litClasse(d));
}
/*
 * Implementation "classique" (mais barbare), qui n'appelle pas directement 
 * toString car cette méthode est souvent redéfinie. On doit l'appeler au 
 * niveau de Objet mais sur l'objet passé en paramètre (utilisation de 
 * Function.call() pour cela.
 * On ne garde que les derniers caractères (à partir du rang 8, décrivant la 
 * classe) de la chaine retournée.
 */
function litClasse(v){
    if (v===null)
        return "Null";
    if (v === undefined)
        return "Undefined";
    return Object.prototype.toString.call(v);
}
/* 
 *Par défaut, un objet est extensible : on peut lui ajouter de nouvelles 
 *propriétés. On a vu qu'il est possible d'y ajouter des propriétés(donc aussi 
 *des méthodes). On peut même en ajouter à son prototype, et par voie de
 *conséquence à Object lui-même. Elles se retrouvent ensuite dans CHAQUE objet
 *créé.
 *Il est également possible de "verrouiler" un objet afin qu'il ne soit plus 
 *extensible. Pour cela :
 *- on peut utiliser Object.preventExtension() pour cela, ou encore,
 *- Object.seal() qui en plus en rend les propiétés non configurables (impossible
 *  de les supprimer). L'objet est alors verrouillé...
 * - si on souhaite, en plus qu'il soit en lecture seule, on peut appeler 
 * Object.freeze().
 */
function verrouillageObjets(){
     console.log("\n=====verrouillageObjets()==============");
     var o = {x:10,y:12};
     Object.freeze(o);
     o.x = 20;      // sans effet
     o.z = 30;      // sans effet
     delete o.x;    // sans effet
     console.log("Après freeze(), o.x vaut : " + o.x);  // 10
     console.log("Après freeze(), o.z vaut : " + o.z);  // undefined
}
/*
 * JSON (voir http://fr.wikipedia.org/wiki/JavaScript_Object_Notation) et
 * http://json.org.
 * Exemple minimal.
 */
function serialisationJSonMinimale(){
    console.log("\n=====serialisationJSonMinimale()==============");
    var o = {nom:{nomFamille:"Duponx",prenom:"Raoul"}, age:47,adresse:"12 rue du Ponx"};
    var s = JSON.stringify(o);
    // sans surprise
    console.log("o = " + s);
    var t = JSON.parse(s);  // tente de reconstruire l'objet
    console.log("nom : " + t.nom.nomFamille + ", prénom : " + t.nom.prenom);
}


function heritageMinimal(){
     console.log("\n=====heritageMinimal()==============");
    function Personne(nom, prenom){
        this.nom = nom;
        this.prenom = prenom;
    }
   //var la_personne =  new Personne("Pierre", "Paul");
    Employe.prototype = new Personne("Pierre", "Paul");
    function Employe(nom, prenom, position){
        this.position = position;
        //this.prenom = prenom;
        //this.nom = nom;
        //Personne.call(this,nom,prenom);
        Personne.call(Employe.prototype,nom,prenom);
    }
    
    var emp1 = new Employe("Dupont","Max","Comptable");
    console.log("Nom de emp1 : " + emp1.nom);
  
}



// Expressions régulières, un tour d'horizon
// Nombreuses ressources web, par exemple https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp.

main();

function main(){
	UtilisationBase();
	testPatterns();
	methodesString();
	methodesRegExp();
}	// fin de main()



function UtilisationBase(){
console.log("");
	console.log("function UtilisationBase() : ");
	console.log("==========================");
	// les deux déclarations suivantes sont équivalentes
	// uniquement les chaines se terminant par 's'
	var exp1 = /s$/;		// expression régulière littérale
	var exp2 = new RegExp("s$");	// définie avec le constructeur

	var test1 = 'Solutions';
	var chaine;
	if ((chaine = exp2.exec(test1))!= null) {
		console.log("chaine trouvée !");
	}
	else {
		console.log("chaine non trouvée !");
	}
}


/*
*	Les principales règles des expressions régulières sont :
*
*  	1 - Dans une expression régulière les caractères 'ordinaires' (A-Z, a-z, 0-9) sont utilisés littéralement.
*  		Exemple : /abc/ trouve la chaine 'abc' dans 'abcdefg'.

*  	2 - Des caractères non imprimables peuvent également y figurer, encodé ainsi : \t , \n etc.

*  	3 -	Une 'classe de caractères' est une suite de caractères placés entre []. Ainsi [abc] 'matche' a, b ou c.
*	   	On peut inverser cette liste avec ^. Ainsi [^xyz] correspond à tout caractère sauf x, y ou z.	
*	   	Certaines classes sont abrégées ainsi :
*		\w : [a-zA-Z0-9_]
*		\W : [^a-zA-Z0-9_]
*		\d : [0-9]
*		\D : [^0-9]

*	4 - Les caractères de répétition permettent d'éviter les redondances :
*		{n} 	n répetitions du caractère précédent, exemple \d{2}, pour \d\d
*		{n,m}	de n à m répetitions du caractère précédent, exemple \d{2,3}, pour \d\d et \d\d\d
*		+ 		de 1 à plus de caractères
*		? 		de 0 à 1 caractère 	(attention, trompeur car matche rien du tout)
*		*		de 0 à n caractères (attention, trompeur car matche rien du tout)

*	5 - Dans le cas de répétition, par défaut JS est 'vorace', ('greedy') et il tente de
*		trouver un minimum de concordances. Si on veur retirer ce comportement, on suffixe
*		l règle de répétition par ?. Il tente de trouver alors un maximum de concordances.

*	6 - Drapeaux. On peut faire suive l'expression régulière d'un drapeau (flag) pouvant être :
*		i : matching indépendant de la casse.
*		g : recherche toutes les occurences, plutôt que de s'arrêter à la première.
*		m : mode multilignes. Dans ce cas, ^ matche le début de ligne et $ la fin de ligne.

*	7 - Alternatives
*		Le caractère | permet de préciser des alternatives dans une expression régulière.
*		Ex : /\d{3}|[x-z]{2}/	on cherche 3 chiffres ou 2 caractère minuscules compris entre x et z.

*	8 - Regroupements
		Les parenthèses ont plusieurs usages dans les RegExp. Le premier est de 
		regrouper des éléments séparés dans une sous expression. Ainsi :
		/(xy|12)?z/

*	9 - Réferences
		Les parenthèses peuvent aussi définir des sous-expressions dans l'expression régulière qui seront
		référencées plus tard. Pour les référencer on utilise un numéro d'ordre : \1, \2 etc.
		Ainsi "2013-02-17".match(/(-)(\d{2})\1/g) retourne '-02-' car le \1 fait référence à la chaine '-'
		définie dans la première sous-expression (-).
*/
function testPatterns(){
	console.log("");
	console.log("function testPatterns() : ");
	console.log("=========================");
	// utilisation littérale des caractères ordinaires
	var ch1 = "xyzabcdaefgh";
	console.info(ch1.match(/abc/g));		// [ 'abc' ]
	// classes de caractères
	console.info(ch1.match(/[ag]/g));		// [ 'a', 'a', 'g' ]
	console.info(ch1.match(/\d/g));			// null
	console.info(ch1.match(/\D/g));			// [ 'x', 'y', 'z', 'a', 'b', 'c', 'd', 'a', 'e', 'f', 'g', 'h' ]
	// répétitions
	console.info("2014XB17abcd".match(/\d\d/g));		// [ '20', '14', '17' ]
	console.info("2014XB17abcd".match(/\d{2}/g));		// [ '20', '14', '17' ]
	// greedy ou non greedy
	console.info("2014XB17abcd".match(/\d{2,3}/g));	// [ '201', '17' ]  		'greedy', avare
	console.info("2014XB17abcd".match(/\d{2,3}?/g));	// [ '20', '14', '17' ]  	non greedy
	// alternatives
	console.info("xyzabcdaefgh".match(/\d{3}|[x-z]{2}/g).toString());	// xy
	console.info("2014XB17abcd".match(/\d{3}|[x-z]{2}/g).toString());	// 201
	// regroupements
	console.info("xyzabcdaefgh".match(/(xy|12)+z/g));			// [ 'xyz' ]
	console.info("14512z8c".match(/(xy|12)+z/g));				// [ '12z' ]
	// références
	console.info("2013-02-17".match(/(-)(\d{2})\1/g));			// [ '-02-' ]
}



/*
* Les chaines JavaScript ont des méthodes de traitement de RegExp. Il s'agit :
*	- search(regExp) : retourne l'indice de la première sous chaine ok, ou -1 si pas ok
*	  Si le param. transmis n'est pas une RegExp, il est d'abord passé au constructeur RegExp.
*	  Ignore le drapeau g.	
*	- replace(RegExp, chRemplacement) : effectue une recherche et si OK, remplace la 1ere chaine trouvée
*	  par la chaine chRemplacement. En cas de drapeau g, effectue tous les remplacements possibles.
*	  Le second arg. peut aussi être une fonction calculant dynamiquement la chaine de remplacement.
*	- match(regExp) : retourne un tableau contenant toutes les sous chaines correctes par rapport 
*	  à regExp (si le drapeau g est défini) ou la première chaine seulement.
*	- split(separateur) : scinde la chaine en un tableau contenant les sous chaines séparées par le
*	  séparateur transmis. Ce séparateur peut être une expression régulière ou une simple chaine.
*/
function methodesString(){
	console.log("");
	console.log("function methodesString() : ");
	console.log("========================");
	// utilisation de search()
	console.log("Bonjour le monde".search(/le/));	// 8
	// utilisation de replace()
	console.log("Bonjour le monde".replace(/le/,"tout le"));	// Bonjour tout le monde
	// utilisation de match()
	console.log("Bonjour le monde".match(/le/));	// [ 'le', index: 8, input: 'Bonjour le monde' ]
	// utilisation de split()
	console.log("Bonjour le monde".split(/ /));		// [ 'Bonjour', 'le', 'monde' ]

}

/*
* Les objets construits par RegExp ont les 5 propriétés suivantes :
*	- source 	(lect.seule) : (string) le texte de l'expression régulière
*	- global 	(lect.seule) : (booléen) l'expression régulière a t'elle le 'g flag' à true ?
*       - ignoreCase 	(lect.seule) : (booléen) l'expression régulière a t'elle le 'i flag' à true ?
*	- multiline 	(lect.seule) : (booléen) l'expression régulière a t'elle le 'm flag' à true ?
*	- lastIndex	(lect/ecrit) : (int) position de la prochaine recherche (si le flag g est vrai). 
* Et ils ont les deux méthodes :
* - exec() : 	se comporte comme string.match(regExp), mais c'est plutôt regExp.exec(string),
				hormis qu'elle ne retourne que le premier match, que l'on utilise g ou pas.
* - test() : 	retourne true si la chaine qu'on lui passe matche avec la regExp.
*/
function methodesRegExp(){console.log("");
	console.log("")
	console.log("function methodesRegExp() : ");
	console.log("========================");

	// création des objets regexp
	var re1 = new RegExp('[A-Z]\\d{2}');	// nécessaire de doubler le \ dans le string littéral 
	var re2 = /[A-Z]\d{2}/;					// littéral regexp, plus simple !
	
	// utilisation de exec()
	console.log(re1.exec('2014XB17abcd'));	// [ 'B17', index: 5, input: '2014XB17abcd' ]
	console.log(re2.exec('2014XB17abcd'));	// [ 'B17', index: 5, input: '2014XB17abcd' ]
	
	// utilisation de test()
	console.log(re1.test('2014XB17abcd'));	// true
	console.log(re2.test('2014XB17abcd'));	// true
}











function main(){
    testParametres(12,15);      // nb et types des params ok
    testParametres(12,"coco");  // nb ok, types pb
    testParametres(47,48,19);   // param suppl. ignoré 
    testParametres(12);         // un param de moins, la fonction doit le gérer 
    console.log("\nSomme de 1 + 17 + 2.3 + 'coco' = " + sommeArgs(1,17,2.3,'coco'));
    fonctionsImbriquees();
    creationObjets();
    differentsTypesDeThis();
    exemplesMethodes();
    heritagePrototype();
    testClosure();
    simulClasse();
}

// passages de paramètres : + ou -
function testParametres(a,b){
    console.log("\n===== testParametres() ==============");
    var s = 0, nbParametres = 0;
    if (a && (typeof a === "number")){
        s += a;
        nbParametres++;
    }
    if (b && (typeof b === "number")){
        s += b;
        nbParametres++;
    }
    console.log("\somme de " + nbParametres + " valeur(s) = " + s);
    return s;
}

// utilisation de la propriété arguments
// pour retourner la somme d'un nombre quelconque de paramètres
function sommeArgs(){
    console.log("\n===== utilArguments() ==============");
    var somme = 0, nombre;
    for (var i = 0; i < arguments.length; i++){
        nombre = Number(arguments[i]);
        if (!isNaN(nombre))
            somme += arguments[i];
    }
        return somme;    
}

/**
 * 
 * Fonctions imbriquées et expressions fonctionnelles.
 *
 */
function fonctionsImbriquees(){
     console.log("\n===== fonctionsImbriquees() ==============");
     var v1 = 12;
    // pour une fonction définie globalement (ni dans une autre, ni dans
    // un objet, et en utilisant l'instruction function), this représente
    // l'objet global. Comme, le contexte du script est l'objet window
    // du navigateur, les propriétés de cet objet sont accessibles
    // directement. Bien sûr, dans ce cas l'usage de this est inutile...
    console.log("Url courante : " + this.location);
    
    // fabrication d'une fonction imbriquée
    // ------------------------------------
    function imbriquee1(p){
        console.log("Depuis imbriquee1(), les locales de fonctionsImbriquees() sont visibles. Ex, v1 vaut : " + v1); // 12
         // Attention, ici this a changé, il s'agit du contexte de la fonction imbriquée, plus de l'englobante
         console.log("Depuis imbriquee1(), this a changé. En utilisant le contexte v1 vaut : " + this.v1);  // undefined
         console.log("Depuis imbriquee1(), l'url vaut toujours (en utilisant this) : " + this.location);    // ok
    }
    
    // invocation de cette fonction
    imbriquee1();
    
    // déclaration et invocation de fonction en même temps
    // ---------------------------------------------------
    (function imbriquee2(p){
         console.log("Depuis imbriquee2(), accès à v1 en utilisat this : " + this.v1);
         console.log("Depuis imbriquee2(), l'url vaut,en utilisat this : " + this.location);
    })();
    
    // Ce comportement s'explique par le fait qu'une fonction est un objet 
    // et que que l'on peut récupérer sa référence dans une variable. Ex :
    // fabrication d'une fonction en utilisant une expression fonctionnelle
    var uneFonction1 = function exprFonction1(p){
         console.log("Depuis exprFonction1(), les locales de fonctionsImbriquees() sont visibles. Ex, v1 vaut : " + v1); // 12
         console.log("Depuis exprFonction1(), accès à v1 en utilisat this : " + this.v1);  // undefined
         console.log("Depuis exprFonction1(), l'url vaut : " + this.location); // ok
    };
    // Et exécution...
    uneFonction1();
  
    // Utiliser un nom est complètement inutile, dans ce cas, on peut passer
    // uniquement par la variable pour l'invoquer :
    // ---------------------------------------------------------------------
    var uneFonction2 = function (p){
         console.log("Depuis la fonction anonyme, les locales de fonctionsImbriquees() sont visibles. Ex, v1 vaut : " + v1); // 12
         console.log("Depuis la fonction anonyme, accès à v1 en utilisat this : " + this.v1);  // undefined
         console.log("Depuis la fonction anonyme, l'url vaut : " + this.location);
    };
    uneFonction2();
    
    // Et si on ne veut pas utiliser de variable (fonction anonyme pour 
    // invocation unique) :
    // ----------------------------------------------------------------
    (function (p){
         console.log("Depuis la seconde fonction anonyme, les locales de fonctionsImbriquees() sont toujours visibles. Ex, v1 vaut : " + v1);
         console.log("Depuis la seconde fonction anonyme, accès à v1 en utilisat this : " + this.v1);  // undefined
         console.log("Depuis la seconde fonction anonyme, l'url vaut : " + this.location);
    })();
    
}


/**
 * Création d'objets
 * En plus de la définition littérale et l'utilisation de Object.create()
 * on peut demander à des fonctions de créer des objets pour nous.
 * Voici comment :
 */
function creationObjets(){
    console.log("\n===== creationObjets() ==============");
    // utilisation d'une fonction de création d'objets (factory)
    // ---------------------------------------------------------
    var p1 = creePersonne("Dupont", "Marc");
    p1.afficheNom();
    
    var p2 = creePersonne("Dupond", "Alexandre");
    p2.afficheNom();
    // utilisation d'un constructeur
    // -----------------------------
    var p3 = new Personne1('Durand', 'Pierre');
    
    // méthode définie au niveau du prototype
    // -------------------------------------
    var p4 = new Personne2('Poe','Edgar');  
    
    p3.afficheNom();    // ok
    p4.afficheNom();    // ok
}


// 1 - création d'une ou plusieurs instances à travers une fonction
// cette approche n'est pas la meilleure, car duplication du code
function creePersonne(nom,prenom){
    var laPersonne = {};
    laPersonne.nom = nom;
    laPersonne.prenom = prenom;
     laPersonne.afficheNom = function(){
         console.log("Nom : " + this.nom + ", prénom : " + this.prenom);
     };
     return laPersonne;
}


// 2 - utilisation d'un constructeur
// Noter l'utilisation d'une majuscule
function Personne1(nom,prenom){
    this.nom = nom;
    this.prenom = prenom;
    this.afficheNom = function(){
         console.log("Nom : " + this.nom + ", prénom : " + this.prenom);
     };
     // return this;   // implicite
}

// 3 - constructeur avec ajout de la méthode au niveau du prototype
// mieux : plus de duplicata de code.
function Personne2(nom,prenom){
    this.nom = nom;
    this.prenom = prenom;
    Personne2.prototype.afficheNom = function(){
         console.log("Nom : " + this.nom + ", prénom : " + this.prenom);
     };
     // return this;   // implicite
}


// propriétés ajoutées à une fonction


// méthodes
function exemplesMethodes(){
     console.log("\n===== exemplesMethodes() ==============");
     
     // un objet minimal
     // ----------------
     var personne1 = {'nom':"Duponx", 'prenom':"Jean Marc"};
     personne1.afficheNom = function(){
         console.log("Nom : " + this.nom + ", prénom : " + this.prenom);
     };
     // exécution de la méthode
     personne1.afficheNom();
     
     
     // on peut faire la même chose, lors de la déclaration du littéral
     // ---------------------------------------------------------------
     var personne2 = { 'nom':"Duponx", 
                        'prenom':"Jean Marc",
                        'afficheNom':function(){
                                           console.log("Nom : " + this.nom 
                                                        + ", prénom : " 
                                                        + this.prenom);
                                      }
     };    
     // exécution de la méthode
     personne2.afficheNom();
 }

// les divers types de this
function differentsTypesDeThis(){
      console.log("\n===== differentsTypesDeThis() ==============");
    var obj1 = {   nom: "Dupont", 
                    toString: function() {
                        return this.nom;
                    } 
                };
     
    console.log("Valeur de obj1 : " + obj1.toString());  // Dupont
    var autre = obj1.toString;
    console.log("Valeur de autre : " + autre());         // undefined, pas de this
    console.log("Valeur de obj1 : " + autre.call(obj1)); // Dupont, idem à obj1.toString()
    var obj2 = { nom: "Durand", toString : autre };
    console.log("Valeur de obj2 : " + obj2.toString());  // Durand, this est à nouveau défini
}


/**
 *  Héritage par prototype
 */

// constructeur de Personne
function Personne(leNom, lePrenom, lAge){
    this.nom = leNom;
    this.prenom = lePrenom;
    this.age = lAge;   
    Personne.prototype.toString = function(){
          return "Personne, Nom : " + this.nom + ", prénom : " 
                     + this.prenom + ", age : " + this.age + "(Personne)";
    };
}

// Constructeur de Client
function Client(unNom, unPrenom, unAge, unCa){
    this.ca = unCa;
   Personne.call(this,unNom,unPrenom,unAge);
   Client.prototype.toString = function(){
        // appel de l'implémentation de Personne
         return Personne.prototype.toString.call(this) + ", ca : " + this.ca + "(Client)";
         };
}

// A définir en dehors de Client() !
Client.prototype = new Personne();          // "héritage par prototype"
Client.prototype.constructor = Client;      // bonne pratique

function heritagePrototype(){
     console.log("\n===== heritagePrototype() ==============");
     var p1 = new Personne('de Chateaubriand','Alphonse',35);
     var c1 = new Client('Rousseau','Jean Jacques', 43,1500);
     console.log(p1);   // appel toString()
     console.log(c1);   // appel toString()

     if (p1 instanceof Personne)
         console.log("p1 a bien le même prototype que la fonction Personne");
     
     if (c1 instanceof Client)
         console.log("c1 a bien le même prototype que la fonction Client");
}

// closures
function closureMini(){
    var tab = ["rouge", "vert", "bleu"];                    // variable privée
    var f = function(indice){
        if ((indice >= 0)&&(indice <= tab.length))
            return tab[indice];
        else
            return "indice invalide";
    };
    return f;
}

function testClosure(){
    console.log("\n===== testClosure() ==============");
    var c = closureMini();
    console.log(" c(0) = " + c(0));
    console.log(" c(2) = " + c(2));
    console.log(" c(17) = " + c(17));
    console.log(" closureMini()(0) = " + closureMini()(0));
    console.log(" closureMini()(2) = " + closureMini()(2));
    console.log(" closureMini()(17) = " + closureMini()(17));
    
}

// simuler des méthodes de classes et des membres de classe
function Compte(tit, depot){
    // attributs d'instance
    this.titulaire = tit;
    if (depot >= 0)
        this.solde = depot;
    else
        this.solde = 0;
    this.NoCompte = Compte.genNo();
    // méthodes
    Compte.prototype.depot = function(mt){
        if (mt > 0)
            this.solde += mt;
    };
    Compte.prototype.retrait = function(mt){
        if ((mt > 0) && (mt <= this.solde))
            this.solde -= mt;
    };
    Compte.prototype.toString = function(){
        return "Titulaire : " + this.titulaire + ", no : " + this.NoCompte
                + ", solde : " + this.solde;
    };
}

// les membres "static"
Compte.nbComptes = 0;               // propriété de la fonction Compte()
Compte.genNo = function(){          // méthode de la fonction Compte()
  return ++Compte.nbComptes;  
};

function simulClasse(){
     console.log("\n===== simulClasse() ==============");
    var c1 = new Compte("Dupont", 1000);
    c1.depot(500);
    c1.retrait(3000);
    c1.retrait(200);
    console.log(c1);
}

/* 
 Utilisation de la nouvelle fonction Object.create() de ES 5.
 */


function mainProto2(){
console.log("====================== prototype2.js =========================");

// Premiere version où un objet Parent sert de prototype pour les objets Enfant.
// 

function Parent1() {   // définition du constructeur
  this.x = 0;
  this.y = 0;
}
Parent1.prototype.z = 0;
Parent1.prototype.afficheInfos = function() {   // ajout d'une méthode
    console.info("valeur de x = " + this.x + ", valeur de y = " + this.y);
};
Parent1.prototype.accesCloture = function() {   // pour utiliser une cloture
   var variablePrivee = 10;
    return function(){
        return ++variablePrivee;
    };
};

var papa = new Parent1();


// constructeur enfant
function Enfant1() {
  Parent1.call(this); // appel du constructeur du parent qui définit les propriétés dans l'enfant
}

// cree un Enfant
var fiston_1 = Object.create(papa);  
console.info("valeur de fiston_1.x : " + fiston_1.x);
fiston_1.x = 10;                // duplicata de x
console.info("valeur de fiston_1.x : " + fiston_1.x); 

var fiston_2 = Object.create(papa);  
console.info("valeur de fiston_2.x : " + fiston_2.x); 
fiston_2.__proto__.x = 10;                // pas de duplicata de x, mais peu naturel !
console.info("valeur de fiston_2.x : " + fiston_2.x); 

papa.y = 15;
console.info("valeur de fiston_1.y : " + fiston_1.y); // 15
console.info("valeur de fiston_2.y : " + fiston_2.y); // 15

// L'instance parent est donc partagée entre les enfants
// et les propriétés de l'objet prototype sont accessibles 
// dans l'enfant, mais en lecture uniquement. Toute tentative 
// d' y écrire en produit le duplicata 


// Seconde version, où le constructeur du parent sert de prototype à l'enfant
// Evite les duplicata de propriétés car il n'y a pas besoin d'un objet parent
// pour créer un objet enfant. Ressemble plus à l'héritage traditionnel.
// 
// 1 - Construction du parent
function Parent2() {   // définition du constructeur
  this.x = 0;
  this.y = 0;
}

Parent2.prototype.z = 0;
Parent2.prototype.afficheInfos = function() {   // ajout d'une méthode
    console.info("valeur de x = " + this.x + ", valeur de y = " + this.y);
};
Parent2.prototype.accesCloture = function() {   // pour utiliser une cloture 
   var variablePrivee = 10;
    return function(){
        return ++variablePrivee;
    };
};

// constructeur enfant
function Enfant2() {
  Parent2.call(this); // appel du constructeur du parent qui définit les propriétés dans l'enfant
}
// les deux instructions qui font tout !
Enfant2.prototype = Object.create(Parent2.prototype);
Enfant2.prototype.constructor = Enfant2;
// cree un Enfant
var fiston2 = new Enfant2();

console.info("'fiston2 instanceof Enfant2' vaut : " + (fiston2 instanceof Enfant2)); // true.
console.info("'fiston2 instanceof Parent2' vaut : " + (fiston2 instanceof Parent2)); // true.
fiston2.afficheInfos(); 
// Utilisation de la cloture définie au niveau Parent
var acces2 = fiston2.accesCloture();
console.info("appel de acces() : " + acces2()); // 11
console.info("appel de acces() : " + acces2()); // 12 
}





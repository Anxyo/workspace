
function mainPoo1(){
    console.log("====================== poo1.js =========================")
    contexteData();
    contexteFonctions();
}
// si on ne précise pas de contexte, le contexte par défaut d'un objet est
// l'objet global, donc, dans un navigateur, l'objet window !
// d'où la nécessité d'utiliser explicitement this.
function contexteData(){
    var obj1 = {    // ne fonctionne pas !
        prop1:"valeur 1",
        prop2:"valeur 2",
        initProp1 : function(valeur){
            if (valeur !== undefined){
                prop1 = valeur;     // par défaut 
            }
        }
    };
    var obj2 = {    // fonctionne mais peu élégant !
        prop1:"valeur 1",
        prop2:"valeur 2",
        initProp1 : function(valeur){
            if (valeur !== undefined){
                obj2.prop1 = valeur;    // couplage avec le nom de l'objet
            }
        }       
    };
    var obj3 = {    // Cette fois-ci, OK !
        prop1:"valeur 1",
        prop2:"valeur 2",
        initProp1 : function(valeur){
            if (valeur !== undefined){
                this.prop1 = valeur;    // pas de couplage
            }
        }       
    };
 
    obj1.initProp1("test_prop1");
    console.log("Valeur de obj1.prop1 = " + obj1.prop1);
    console.log("Valeur de window.prop1 = " + window.prop1);    // elle est là !
    
    obj2.initProp1("test_prop1");
    console.log("\nValeur de obj2.prop1 = " + obj2.prop1);
    
    obj3.initProp1("test_prop1");
    console.log("Valeur de obj3.prop1 = " + obj3.prop1);   
}

function contexteFonctions(){
var compteur = {
    combien:0,
    incremente:function(){
        this.combien++;
        }
    }; 
    
    for (i=0;i < 1000; i++)
        compteur.incremente();
    console.log("\nValeur de 'combien' dans 'compteur' (1er test) : " + compteur.combien); // 1000
    compteur.combien = 0;       // raz
    
    var ic = compteur.incremente;  // pointeur vers fonction, hors contexte
    for (i=0;i < 1000; i++)
        ic();                       // this incorrect : objet global (window)
    console.log("\nValeur de 'combien' dans 'compteur' (2d test) : " + compteur.combien); // 0
    
     for (i=0;i < 1000; i++)
        ic.call(compteur);  // force compteur comme valeur pour this, à nouveau OK
    console.log("\nValeur de 'combien' dans 'compteur' (2d test) : " + compteur.combien); // 1000
    
    compteur.combien = 0;       // raz
    var methode = creeContexte(compteur,compteur.incremente);
     for (i=0;i < 1000; i++)
        methode();  // force compteur comme valeur pour this, à nouveau OK
    console.log("\nValeur de 'combien' dans 'compteur' (3eme test) : " + compteur.combien); // 1000
    
    compteur.combien = 0;       // raz
    var methode = compteur.incremente.bind(compteur);  // ECMAScript 5, bien plus simple !
     for (i=0;i < 1000; i++)
        methode();  // force compteur comme valeur pour this, à nouveau OK
    console.log("\nValeur de 'combien' dans 'compteur' (4eme test) : " + compteur.combien); // 1000
    
}
// automatise un peu la création de contexte pour une fonction
// (idée issue de l'article http://alistapart.com/article/getoutbindingsituations).
// Inutile dans ECMAScript 5
function creeContexte(objet,methode){
    return function(){
        return methode.call(objet);
    };
}






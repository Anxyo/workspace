/* 
 Montre l'usage du pattern module.
 Le principe est d'utiliser la cloture pour masquer des données et des méthodes, 
 en simulant ainsi l'encapsulation traditionnelle.
 Il n'est pas possible, hormis la cloture de masquer des membres d'un objet.
 Généralement, avec cette approche :
- on crée un objet unique, celui défini par le module.
- on ne met pas en oeuvre d'héritage
 */

/* 1 - Préliminaires
 * Une fonction est un objet, dans lequel on peut créer des variables et des méthodes. 
 * De même, une fonction représente un contexte d'exécution fermé, une "cloture".
 * Il existe deux situations dans lesquelles on peut rencontrer le mot function :
 * - les déclarations de fonctions qui sont des instructions. Il n'est pas possible de
 *   placer des parenthèses après, pour en lancer l'exécution.
 * - les expressions fonctionnelles, que l'on peut exécuter.
 * 
 */

function mainModule(){
     console.log("====================== module.js =========================");
    var compteur1 = creeCompteur();
    var compteur2 = creeCompteur();
    compteur1();compteur1();compteur1(); // 1, 2, 3
    compteur2();compteur2();compteur2(); // 1, 2, 3 : chaque i est indépendant
    
    // les deux lignes suivantes sont correctes
    // le mot 'function' sert à définir une 'expression fonctionnelle'.
    var pipo = function(){console.log("Pipo, j'écris ton nom");};
    pipo();
    // mais pas celle là (le mot function est interprété comme une instruction 
    // et non une expression) :
    //function(){console.log("Pipo, j'écris ton nom")}();
   // Pour le faire compiler, il faut utiliser une des deux formes suivantes :
   (function(){console.log("Pipo, j'écris ton nom");})(); // exécution immédiate
   (function(){console.log("Pipo, j'écris ton nom");}()); // idem, recommandée par Crockford
  
  
    /* Le pattern module est une expression fonctionnelle exécutée lors de sa 
    *  déclaration (une 'IIFE' ou 'Immediately Invoked Function Expression') et
    *  retournant un objet. Exemple minimal, pour en voir la structure :
    */
    var auMinimum = (function(){
        console.log("Au minimum !");
        return {};      // retourne un objet vide !
    }());
  
  
     // Utilisation du module un peu plus complet
   Marc.ajouteInterets("JavaScript", "Java", "Bicyclette");
   console.log("Nom : " + Marc.getNomCompose());
   var interets = "\nInterets : ";
   for(var i = 0; i < Marc.interets().length; i++)
        interets += Marc.interets()[i] + " ";
   console.log(interets);
   
   // Utilisation du module dans un contexte web
   bouton.masque();
   bouton.connecte();

}


function creeCompteur(){    // utilisation d'une cloture
    var i = 0;
    return function(){console.log("i=" + ++i);};
}




/* 
 * 3 - Un exemple un peu plus complet. On notera les points suivants :
 *     - on a retourné tantot directement une expression fonctionnelle (comme
 *       getNomCompose ou interets) ou une référence vers une sous-fonction (comme
 *       age). Pourquoi prendre l'un ou l'autre ? La seconde approche est plus sure 
 *       car si à l'extérieur on écrit : 
 *         Marc.getNomCompose = 10;
 *       on va écraser l'expression fonctionnelle et si cette expression est utilisée 
 *       à l'intérieur de l'objet le code va tomber...
 *       Dans le second cas, l'écrasement de age ne produira pas "d'effet interne" car
 *       le code interne peut continuer d'utiliser getAge(), qui n'est pas publiée et
 *       fonctionne donc toujours...
 *     - l'excellente encapsulation des données prenom, nom, age et interets.
 */
var Marc = (function(){
    var prenom = "Marc", nom = "Dupont", 
        age = 42, interets = [];
    
    function getAge(){
        return age;
    }
    
    return {    // l'objet retourné
        getNomCompose : function(){
            return prenom + " " + nom;
        },
        age:getAge,
        interets:function(){
            return interets;
        },
        ajouteInterets:function(){
            for(var i = 0; i < arguments.length;i++)
                interets.push(arguments[i]);
        }
    };
    
}());

/*
 * 4 - Utilisation dans un contexte Web (tooltip).
 * 
 */
var bouton = (function(){
    var btn = $("#bouton"), bulle = $("#bulle");
    
    function apparait(){
        bulle.fadeIn();
        return this;
    }
    function disparait(){
        bulle.fadeOut();
        return this;        
    }
    return {
        connecte:function(){
            btn.bind("mouseover", apparait)
                  .bind("mouseout", disparait);
          return this;
        },
        deconnecte:function(){
            btn.unbind("mouseover", apparait)
                  .unbind("mouseout", disparait);            
          return this;
        },
        affiche:function(){
            apparait();
            return this;
        },
        masque:function(){
            disparait();
            return this;
        }
    };
    
}());




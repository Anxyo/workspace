/* 
 Cette approche utilise l'héritage prototypal sans les nouveautés introduites par
 ES 5. Beaucoup de problèmes ici, (cette version est déconseillée) :
 - peu claire.
 - duplicata des champs
 */
function mainProto1(){
     console.log("====================== prototype1.js =========================");
    // utilisation sans héritage
    var Pierre = new Personne("Pierre", "Dupont", 35);
    Pierre.ajouteInterets("Golf", "Lecture", "Programmation");
    Pierre.age(39);
    console.log("Le nouvel age de Pierre :" + Pierre.age());
    // avec héritage
    var Paul = new Programmeur("Paul", "Paul", 37, "JavaScript");
    Paul.ajouteInterets("Coding", "Coding", "Coding"); // définie dans Personne
    console.log("Langage favori de Paul : " + Paul.infosLangage());
}

// la définition de la fonction constructeur Personne
function Personne (prenom, nom, age) {
    if (this === window) {  // pour éviter une invocation sans new
        return new Personne(prenom, nom, age);
    }
    this._prenom = prenom,
    this._nom  = nom,
    this._age       = age;
    this.interets  = [];
}
/*
 * On ajoute ces deux méthodes au prototype de Personne à l'extérieur de
 * la définition de la fonction constructeur.
 */
Personne.prototype.age = function (age) {
    if (age) {
        this._age = age;
    }
    return this._age;
};

Personne.prototype.ajouteInterets = function () {
    for (var i = 0; i < arguments.length; i++) {
        this.interets.push(arguments[i]);
    }
    return this;
};

// héritage
function Programmeur(prenom, nom, age, langagePrefere){
    if (this === window) {
        return new Programmeur(prenom, nom, age, langagePrefere);
    }
    this._prenom = prenom,
    this._nom  = nom,
    this._age  = age;
    this.interets  = [];
    this._langagePrefere = langagePrefere;
}

Programmeur.prototype = new Personne();
Programmeur.prototype.constructor = Programmeur;
Programmeur.prototype.infosLangage = function(){
  return "Mon langage favori est " + this._langagePrefere.toUpperCase();
};

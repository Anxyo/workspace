// en prévision de la syntaxe jQuery...
var $ = function(id){
    return document.getElementById(id);
};


function enrgtHandlers(){
    document.getElementById("btnInsererFrere").onclick = insertionFrere;
    $("btnInsererEnfant").onclick = insertionEnfant;
    $("btnModif").onclick = modification;
    $("btnSuppression").onclick = suppression;
    $("btnParcourt1").onclick = parcourt1;
    $("btnParcourt2").onclick = parcourt2;
}
/*
 * Retrouve le noeud ul d'id 'insertion' et ajoute un autre noeud 'ul' a 
 * coté de lui, avec deux enfants et un id de 'dynamique'.
 */
function insertionFrere(){
 var noeudDepart = $('insertion');
 var nouveau = creeNouvelUL();
 noeudDepart.parentNode.insertBefore(nouveau,noeudDepart.nextSibling);  
}

function creeNouvelUL(){
 var nouv = document.createElement("ul");
 nouv.id = 'dynamique';
 var  nouvEnfant1 = document.createElement("li");
 nouvEnfant1.innerHTML = "Premier élément dynamique";
 var nouvEnfant2 = document.createElement("li");
 nouvEnfant2.innerHTML = "Second élément dynamique";
 nouv.appendChild(nouvEnfant1);
 nouv.appendChild(nouvEnfant2);
 return nouv;
}
/*
 * Insertion d'un groupe de noeuds enfants sous le noeud d'id 'parent', 
 * rendu également visible à cette occasion...
 */
function insertionEnfant(){
     var noeudDepart = $('parent');
     // noter l'usage de la propriété style
     noeudDepart.style.visibility = "visible";
     var nouveau = creeNouvelUL();
     noeudDepart.appendChild(nouveau);
}
/*
 * Modifie le contenu HTML du noeud d'id 'parent'
 */
function modification(){
     var noeudDepart = $('parent');
     noeudDepart.innerHTML = "Je suis le parent, et j'ai supprimé mes enfants.";
}
/*
 * Supprime le noeud d'id 'parent'
 */
function suppression(){
     var noeudDepart = $('parent');
     noeudDepart.parentNode.removeChild(noeudDepart);
}

/*
 * Une première façon de le faire, en comptant le nombre d'enfants du noeud parent.
 */
function parcourt1(){
     var noeudDepart = $('insertion');
     var parent = noeudDepart.parentNode;
     var nbEltsEnfants = parent.children.length;
     var noeudCourant;
     for (var i = 0; i < nbEltsEnfants; i++){
         noeudCourant = parent.children[i];
         console.log("Noeud no " + (i + 1)  + ", : " + noeudCourant.nodeName);
     }       
}
/*
 * La propriété nextElementSibling vaut null lorsqu'il n'y a plus de suivant.
 * On peut donc le réécrire comme cela :
 */
function parcourt2(){
     var noeudDepart = $('insertion');
     var parent = noeudDepart.parentNode;
     var noeudCourant = parent.children[0], i = 0;
        while (noeudCourant) {
         console.log("Noeud no " + ++i + ", : " + noeudCourant.nodeName);
         noeudCourant = noeudCourant.nextElementSibling;
        }
}



// en prévision de la syntaxe jQuery...
var $ = function(id){
    return document.getElementById(id);
};


function enrgtHandlers(){
    $("btnRechId").onclick = parId;
    $("btnRechName").onclick = parNom;
    $("btnRechTag").onclick = parTag;
    $("btnRechClasse").onclick = parClasse;
    $("btnRechSelecteur").onclick = parSelecteur;
    $("btnStopSelec").onclick = stopSelection;
}

function parId(){
    var id = 'titre';
    var elt = $(id);
    selectionneMulti([elt]);
    alert(elt.innerHTML);
}
/**
 * Recherche en fonction de la valeur de l'attribut name.
 * Concerne avant tout les formulaires et les images.
 */
function parNom(){
    var nom = 'dom2';
    var elts = document.getElementsByName(nom);
    if (elts){
        alert("Source de l'image : " + elts[0].src);
    }
}

function parTag(){
    var nomTag = 'p';
    var elts = document.getElementsByTagName(nomTag);
    selectionneMultiStyle(elts);
}

function parClasse(){
    var nomClasse = "speciale";
    var elts = document.getElementsByClassName(nomClasse);
    if (elts)
        selectionneMultiStyle(elts);
}

function parSelecteur(){
    var selecteur = prompt("Sélecteur CSS :");
    var elts = document.querySelectorAll(selecteur);
    if (elts)
        selectionneMultiStyle(elts);
}
function stopSelection(){
   selectionneMulti();
   selectionneMultiStyle();
}

function selectionneMulti(elements){
    if (selectionneMulti.dernierElt) {
         for (var el1 = 0; el1 < selectionneMulti.dernierElt.length;el1++)
             selectionneMulti.dernierElt[el1].className = "deselection";
    }
    
    if (elements){
        for (var el2 = 0; el2 < elements.length;el2++){
            var v = elements[el2];
            v.className="selection";
            //elts[el2].style.background = "yellow";        
        }
        selectionneMulti.dernierElt = elements;
     }
     else
         selectionneMulti.dernierElt = undefined;
}
selectionneMulti.dernierElt = 0;

function selectionneMultiStyle(elements){
    if (selectionneMultiStyle.dernierElt) {
         for (var el1 = 0; el1 < selectionneMultiStyle.dernierElt.length; el1++)
             selectionneMultiStyle.dernierElt[el1].style.background="white";
    }
    
    if (elements){
        for (var el2 = 0 ;el2 < elements.length; el2++){
            elements[el2].style.background = "yellow";        
        }
        selectionneMultiStyle.dernierElt = elements;
     }
     else
         selectionneMultiStyle.dernierElt = undefined;
}
selectionneMultiStyle.dernierElt = 0;

function selectionne(elt){
    if (this.dernierElt)
         this.dernierElt.className="deselection";
    if (elt){
            elt.className="selection";
            this.dernierElt = elt;
     }
     else
         this.dernierElt = undefined;
}


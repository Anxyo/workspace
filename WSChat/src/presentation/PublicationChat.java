package presentation;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

// Point de publication du websocket
// Il y aura autant d'instances de cette classe (des 'endpoints') que d'utilisateurs connect�s
@ServerEndpoint(value = "/chat/{nomutil}")
public class PublicationChat {
	
	// pour ranger nos utilisateurs connect�s 
	private static Map<String, String> utilisateurs = new HashMap<>();
	
	 // un Set threadsafe (voir par exemple https://stackoverflow.com/questions/29249714/when-is-copyonwritearrayset-useful-to-achieve-thread-safe-hashset)
	private static Set<PublicationChat> endpoints = new CopyOnWriteArraySet<>();
	private Session laSession;
	
	// Appel�e lors de l'ouverture de la connexion
	@OnOpen
	public void onOpen(Session laSession, 
						@PathParam("nomutil") String nomUtil) throws IOException {
		this.laSession = laSession; // m�morise la session
        endpoints.add(this);		// ajoute ce nouveau point de publication � la collection
        utilisateurs.put(laSession.getId(), nomUtil); // et l'utilisateur dont le nom est transmis dans l'URL, � la liste des utilisateurs
        // construit un message
        this.envoie("connexion de " + nomUtil + " !");
	}

	// Appel�e � la fermeture de la connexion
	@OnClose
	public void onClose(Session session) throws IOException {
		endpoints.remove(this);
        envoie("Deconnexion de " + utilisateurs.get(laSession.getId()) + "...");
	}
	
	// Appel�e lors de la r�ception des messages
	@OnMessage
	public void onMessage(Session session, String msg) throws IOException {
		envoie(msg); // fait suivre
	}
	
	// Appel�e en cas d'erreur
	@OnError
	public void onError(Session laSession, Throwable erreur) {
		System.out.println("Erreur : " + erreur.getMessage());
	}
	
	// m�thode d'envoie des messages de texte
	synchronized private void envoie(String msg) throws IOException {
		for (PublicationChat pc : endpoints) {
			pc.laSession.getBasicRemote().sendText(msg);
		}
	}
	
}

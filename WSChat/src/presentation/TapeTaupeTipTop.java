package presentation;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/taupes")
public class TapeTaupeTipTop {
	private static Set<TapeTaupeTipTop> endpoints = new CopyOnWriteArraySet<TapeTaupeTipTop>();
	private Session laSession;
	
	@OnOpen
	public void onOpen(Session laSession) throws IOException {
		this.laSession = laSession; // m�morise la session
        endpoints.add(this);		// ajoute ce nouveau point de publication � la collection
        
        String role = "MT";
        if (endpoints.size() > 1)
        	role = "TT";
        // {"cmd":
        laSession.getBasicRemote().sendText("{\"cmd\":\"init\", \"valeur\":\"" + role + "\"}");
        // construit un message
        System.out.println("connexion !" + laSession.getId());
	}

	// Appel�e � la fermeture de la connexion
	@OnClose
	public void onClose(Session session) throws IOException {
		endpoints.remove(this);
		System.out.println("Deconnexion...");
	}
	
	// Appel�e lors de la r�ception des messages
	@OnMessage
	public void onMessage(Session session, String msg) throws IOException {
		envoie(msg); // fait suivre
	}
	
	// Appel�e en cas d'erreur
	@OnError
	public void onError(Session laSession, Throwable erreur) {
		System.out.println("Erreur : " + erreur.getMessage());
	}
	
	// m�thode d'envoie des messages de texte
	synchronized private void envoie(String msg) throws IOException {
		for (TapeTaupeTipTop tttt : endpoints) {
			if (tttt != this) {
				tttt.laSession.getBasicRemote().sendText(msg);
			}
		}
	}
}

package modele;

public class Message {
	String de, pour, contenu;
	public Message() {}
	public Message(String de, String pour, String contenu) {
		super();
		this.de = de;
		this.pour = pour;
		this.contenu = contenu;
	}
	public String getDe() {
		return de;
	}
	public void setDe(String de) {
		this.de = de;
	}
	public String getPour() {
		return pour;
	}
	public void setPour(String pour) {
		this.pour = pour;
	}
	public String getContenu() {
		return contenu;
	}
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	@Override
	public String toString() {
		return "Message [de=" + de + ", pour=" + pour + ", contenu=" + contenu + "]";
	}
	
}

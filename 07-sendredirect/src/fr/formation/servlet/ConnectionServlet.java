package fr.formation.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet("/connect")
public class ConnectionServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("Vérification de l'identité");
		System.out.println("Recherche en base de données...");
		
		request.getSession().setAttribute("login", request.getParameter("login"));
		
		response.sendRedirect("todolist.jsp");
		
		
//		RequestDispatcher rd = request.getRequestDispatcher("/todolist.jsp");
//		rd.forward(request, response);
		
		
		
	}

}

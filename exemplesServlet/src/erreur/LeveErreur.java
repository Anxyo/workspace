package erreur;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

@SuppressWarnings("serial")
public class LeveErreur extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		// commence par retrouver l'URL de cette servlet
		String strMonUrl = req.getRequestURL().toString();
		String rqt = req.getQueryString();
		if (rqt != null) {
			if (!rqt.equals(""))
			{
			// quelle est la valeur de val ?
			int val = Integer.parseInt(req.getParameter("val"));
			switch (val) {
				// si val = 1 lance une exception ArrayIndexOutOfBoundsException
				case 1 :
					int[] Montab = { 1, 2, 3 };
					Montab[4] = 25;   // badaboum
					break;

					// si val = 2 lance une exception StringIndexOutOfBoundsException
				case 2 :
					String s = "coco";
					s.charAt(10);  // re-badaboum
					break;

					// si val = 3 appelle sendError() avec un code mapp�(404)
					//            sans message
				case 3 :
					res.sendError(404);
					break;

					// si val = 4 appelle sendError() avec un code non mapp� (12256)
					//         avec un message
				case 4 :
					res.sendError(12256, "Ce code (12256) n'est pas mapp� !");
					break;

					// si val = 5 appelle sendError() avec un code non mapp� (12256)
					//         sans message
				case 5 :
					res.sendError(12256);
					break;

			}
			}
		}
		// pas de chaine de requete affiche les liens
		res.setContentType("text/html");
		PrintWriter sortie = res.getWriter();
		sortie.println(
			"<html><head><title>Gestions des erreurs</title></head><body>");
		sortie.println("<center><h1>Gestions des erreurs</h1></center>");
		sortie.println("<hr/>");
		// 1 - un lien pour proposer la lev�e de l'exception PbMappeException
		sortie.println(
			"<br/><a href='"
				+ strMonUrl
				+ "?val=1'>"
				+ "Lever une exception mapp�e avec une page de traitement.</a>");

		// 2 - un lien pour proposer la lev�e de l'exception PbNonMappeException
		sortie.println(
			"<br/><a href='"
				+ strMonUrl
				+ "?val=2'>"
				+ "Lever une exception NON mapp�e avec une page de traitement.</a>");

		// 3 - un lien pour proposer l'appel de sendError() avec un code mapp�
		sortie.println(
			"<br/><a href='"
				+ strMonUrl
				+ "?val=3'>"
				+ "D�clenchement d'une erreur avec un code d'erreur mapp� avec une page de traitement.</a>");

		// 4 - un lien pour proposer l'appel de sendError() avec un code non mapp�
		// mais avec un message
		sortie.println(
			"<br/><a href='"
				+ strMonUrl
				+ "?val=4'>"
				+ "D�clenchement d'une erreur avec un code non mapp�, mais avec un message d'erreur.</a>");

		// 5 - un lien pour proposer l'appel de sendError() avec un code non mapp�
		// sans message
		sortie.println(
			"<br/><a href='"
				+ strMonUrl
				+ "?val=5'>"
				+ "D�clenchement d'une erreur avec un code non mapp�, sans message d�fini.</a>");

		sortie.println("</body></html>");
	}
}


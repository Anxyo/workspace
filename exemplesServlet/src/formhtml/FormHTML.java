package formhtml;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
//import java.util.*;

@SuppressWarnings("serial")
public class FormHTML extends HttpServlet {


	public void doPost(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		String chaine;
		res.setContentType("text/html");
		PrintWriter sortie = res.getWriter();

		// Plut�t que d'�mettre les donn�es au fur et � mesure
		// on construit un buffer que l'on �met d'un coup
		// (cela permet de traiter les erreurs rencontr�es lors
		//  de la construction de la chaine de r�ponse puisque
		// les en-t�tes HTTP ne sont pas encore �mis).
		chaine = getChaineReponse(req);
		// �mission r�elle
		sortie.println(chaine);
		// fermeture
		sortie.close();
	}
	// �crit les donn�es transmises dans un fichier dont le nom est �galement transmis
	private boolean ecritFichier(String AEcrire, String chemin) {
		boolean bDrapeau = true;
		RandomAccessFile fichier;
		try {
			// tente d'ouvrir un nouvel objet RandomAccessFile
			fichier = new RandomAccessFile(chemin, "rw");
			// on va en fin de fichier
			fichier.seek(fichier.length());
			// on �crit les donn�es
			fichier.writeBytes(AEcrire);
			// on ajoute un s�parateur d'enregistrements
			fichier.writeBytes("========================================\n");
			// ferme le fichier
			fichier.close();

		} catch (Exception e) {
			bDrapeau = false;
		}
		// le code diagnostique
		return bDrapeau;
	}
	private String getChaineReponse(HttpServletRequest req)
		throws ServletException, IOException {
		boolean bSauve = false;
		String chaine1, chaine2, diag;
		// pour gagner du temps on utlise un StringBuffer
		// (�vite les objets temporaires lors des concat�nations de String)
		StringBuffer bufferHTML = new StringBuffer(1024); // pour navigateur
		StringBuffer buffer = new StringBuffer(1024);
		// pour sauvegarde fichier

		// Sauvegarde ou pas ?
		chaine1 = req.getParameter("radSauve");
		if (chaine1.equals("Oui"))
			bSauve = true; // demande de sauvegarde

		// on parcourt la liste des param�tres dont on connait le nom
		// (l'attribut NAME du formulaire HTML)

		// d'abord les nom/pr�nom
		buffer.append("De Mr/Mme : ");
		chaine1 = req.getParameter("txtPrenom");
		chaine2 = req.getParameter("txtNom");
		bufferHTML.append(chaine1 + "   " + chaine2 + "<BR>");
		if (bSauve)
			buffer.append(chaine1 + "   " + chaine2 + "\n");

		// le E-mail
		chaine1 = req.getParameter("txtEmail");
		bufferHTML.append("E-mail : " + chaine1 + "<BR>");
		if (bSauve)
			buffer.append("E-mail : " + chaine1 + "\n");

		// le t�l�phone
		chaine1 = req.getParameter("txtTelephone");
		bufferHTML.append("T�l�phone : " + chaine1 + "<BR>");
		if (bSauve)
			buffer.append("T�l�phone : " + chaine1 + "\n");

		// la situation
		chaine1 = req.getParameter("slctSituation");
		bufferHTML.append("Situation : " + chaine1 + "<BR>");
		if (bSauve)
			buffer.append("Situation : " + chaine1 + "\n");

		// les remarques
		chaine1 = req.getParameter("txtRemarques");
		bufferHTML.append("Remarques : " + chaine1 + "<BR>");
		if (bSauve)
			buffer.append("Remarques : " + chaine1 + "\n");

		if (bSauve) { // on sauvegarde
			if (ecritFichier(buffer.toString(), "c:\\sauve.txt")) {
				diag =
					"Suivant votre demande, la sauvegarde a �t� effectu�e avec succ�s. \n";
			} else
				diag = "Erreur lors de la tentative de sauvegarde !\n";
		} else
			diag = "Suivant votre demande, la sauvegarde n'a pas �t� faite.\n";

		// ajoute le r�sultat de la sauvegarde au bout de la chaine
		bufferHTML.append(diag);

		// c'est fini...
		return bufferHTML.toString();

	}
	public void doGet(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
		this.doPost(req,res);
	}
}

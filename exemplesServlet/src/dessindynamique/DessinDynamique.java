package dessindynamique;

import dessindynamique.Acme.JPM.Encoders.*;
import java.io.*;
import java.awt.*;
import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class DessinDynamique extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		// nous devons envoyer des donn�es binaires et donc
		// un Writer ne sera pas OK (texte seulement)
		ServletOutputStream sortie = res.getOutputStream();

		Frame frame = null;
		Graphics g = null;

		try {
			// Cr�ation d'une fen�tre invisible
			// n�cessaire � l'obtention d'un contexte graphique
			frame = new Frame();
			// cr�ation du composant natif correspondant
			// (il est normalement cr�� lors de l'affichage
			// mais nous avons besoin de son Device Context)
			frame.addNotify();

			// On cr�e un objet image associ� � la fen�tre "bidon"
			Image image = frame.createImage(500, 110);
			// et on obtient un contexte graphique sur elle
			g = image.getGraphics();
			// Tous les dessins r�alis�s maintenant sont trac�s
			// sur un contexte graphique "m�moire" invisible

			// on commence par fabriquer un petit camembert
			g.drawOval(0, 0, 100, 100);
			g.setColor(Color.blue);
			g.fillArc(0, 0, 100, 100, 0, 79);
			g.setColor(Color.cyan);
			g.fillArc(0, 0, 100, 100, 80, 90);
			g.setColor(Color.green);
			g.fillArc(0, 0, 100, 100, 171, 50);
			g.setColor(Color.orange);
			g.fillArc(0, 0, 100, 100, 221, 90);
			g.setColor(Color.yellow);
			g.fillArc(0, 0, 100, 100, 311, 48);
			// puis on affiche un texte avec une grande police
			g.setColor(Color.black);
			Font UnePolice = new Font("Serif", Font.PLAIN, 54);
			// on en fait police du contexte
			g.setFont(UnePolice);
			// et on affiche le texte avec aux coordonn�es 130,50
			g.drawString("Dessins", 115, 50);

			// enfin, on dessine un graphique � barres empil�es
			g.setColor(Color.blue);
			g.fillRect(300, 0, 24, 90);
			g.setColor(Color.cyan);
			g.fillRect(325, 0, 24, 30);
			g.setColor(Color.green);
			g.fillRect(350, 0, 24, 60);
			g.setColor(Color.orange);
			g.fillRect(375, 0, 24, 12);
			g.setColor(Color.yellow);
			g.fillRect(400, 0, 24, 80);
			g.setColor(Color.pink);
			g.fillRect(425, 0, 24, 80);

			// le plus d�licat reste � faire : encoder ce
			// graphique m�moire sous forme d'une image GIF
			// et � l'envoyer au client. On se sert des classes
			// de support de ACME
			// d�finition du type MIME
			res.setContentType("image/gif");
			// cr�ation d'un encodeur et encodage

			GifEncoder encoder = new GifEncoder(image, sortie);
			encoder.encode();
		} finally {
			// lib�ration m�moire des objets utilis�s
			if (g != null)
				g.dispose();
			if (frame != null)
				frame.removeNotify(); // plus de peer associ�
			// (m�thode de la classe Container)
		}
	}
}

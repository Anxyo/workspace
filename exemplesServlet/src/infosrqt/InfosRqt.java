package infosrqt;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class InfosRqt extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();

		out.println(
			"<HTML><HEAD><TITLE>Infos sur la requete cliente</TITLE></HEAD><BODY>");
		out.println(
			"<H1 align=\"center\">Infos sur la requete cliente</H1><HR><BR>");

		// la chaine de requ�te tout d'abord
		out.println("Requete : " + req.getQueryString() + "<BR>");
		// puis les param�tres transmis
		out.println("Param�tres : <BR>");
		// on obtient tous les noms des param�tres d'une coup
		Enumeration<String> e = req.getParameterNames();
		// puis on parcourt l'�num�ration
		while (e.hasMoreElements()) {
			// le nom du param�tre courant
			String nom = (String) e.nextElement();
			// toutes les valeurs associ�es : il est rare qu'il y en ait
			// plus d'une ...
			String listeValeurs[] = req.getParameterValues(nom);
			// s'il y en a au moins une
			if (listeValeurs != null) {
				// on parcourt le tableau...
				for (int nb = 0; nb < listeValeurs.length; nb++) {
					// ... et on affiche chaque valeur
					out.println("- " + nom + " = " + listeValeurs[nb] + "<BR>");
				} // fin du for
			} // fin du if

		} // fin du while

		// fin du document HTML
		out.println("<BR><HR></BODY> </HTML>");

	} // fin de doGet()
}

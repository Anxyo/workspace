package coucousecur;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// aucune annotation de s�curit� dans web.xml, dans ce cas...
@WebServlet("/CoucouAnnotations")
// les deux r�les autoris�s pour toutes les m�thodes et donc
// aussi bien pour le GET que pour le POST
@ServletSecurity(@HttpConstraint(rolesAllowed={"manager","stagiaire"}))
public class CoucouAnnotations extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CoucouAnnotations() {
        super();
    }

	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		out.println("<html>");
		out.println("<head><title>Premi�re servlet</title></head>");
		out.println("<body>");
		String strNom = req.getRemoteUser();
		out.println("<strong>Coucou, � : " + strNom + ", bienvenue dans cette servlet � annotations !.</strong><hr/>");
		// ce qui suit est utile pour la s�curit� "programmatique"
		out.println(AffInfoRole(req, strNom, "stagiaire"));
		out.println(AffInfoRole(req, strNom, "role1"));
		out.println(AffInfoRole(req, strNom, "manager"));
		out.println("</body></html>");
	}
	
	// routine de service
	private String AffInfoRole(HttpServletRequest req, String nom, String role) {
		return "<strong>" + nom
				+ (req.isUserInRole(role) ? " est" : " n'est pas")
				+ " dans le r�le de '" + role + "'.</strong><br/>";
	}

}


package coucousecur;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class Coucou extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		out.println("<html>");
		out.println("<head><title>Premi�re servlet</title></head>");
		out.println("<body>");
		String strNom = req.getRemoteUser();
		out.println("<strong>Coucou, � : " + strNom + ".</strong><hr/>");
		// ce qui suit est utile pour la s�curit� "programmatique"
		out.println(AffInfoRole(req, strNom, "stagiaire"));
		out.println(AffInfoRole(req, strNom, "role1"));
		out.println(AffInfoRole(req, strNom, "manager"));
		out.println("</body></html>");
	}

	// routine de service
	private String AffInfoRole(HttpServletRequest req, String nom, String role) {
		return "<strong>" + nom
				+ (req.isUserInRole(role) ? " est" : " n'est pas")
				+ " dans le r�le de '" + role + "'.</strong><br/>";
	}
}


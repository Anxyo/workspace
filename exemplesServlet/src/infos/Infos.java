package infos;

import java.io.*;
import javax.servlet.*;

@SuppressWarnings("serial")
public class Infos extends GenericServlet {

	private String getNomServeur(String infoServeur) {
		int posSlash = infoServeur.indexOf('/');
		if (posSlash == -1)
			return infoServeur;
		else
			return infoServeur.substring(0, posSlash);
	}
	private String getVersionServeur(String infoServeur) {
		int posSlash = infoServeur.indexOf('/');
		if (posSlash == -1)
			return null;
		else
			return infoServeur.substring(posSlash + 1);
	}

	public void service(ServletRequest req, ServletResponse res)
		throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		// pour changer l'apparence de l'affichage...
		String carRouges =
			"<font color=\"#FF0000\" face=\"Times New Roman, Times, serif\"><em>";
		String finCarRouges = "</em></font>";

		out.println(
			"<HTML><HEAD><TITLE>Infos sur le serveur et le client</TITLE></HEAD><BODY>");
		out.println("<H1 align=\"center\">Infos</H1><HR>");
		out.println("<H2 align=\"left\">Infos sur le serveur</H2>");
		out.println(
			"L'appel req.getServerName() retourne : "
				+ carRouges
				+ req.getServerName()
				+ finCarRouges
				+ "<BR>");
		out.println(
			"L'appel req.getServerPort() retourne : "
				+ carRouges
				+ req.getServerPort()
				+ finCarRouges
				+ "<BR>");
		out.println(
			"L'appel getServletContext().getServerInfo() retourne : "
				+ carRouges
				+ getServletContext().getServerInfo()
				+ finCarRouges
				+ "<BR>");
		out.println(
			"Le nom retourn� par getServerInfo() est : "
				+ carRouges
				+ getNomServeur(getServletContext().getServerInfo())
				+ finCarRouges
				+ "<BR>");
		out.println(
			"Le no de version retourn� par getServerInfo() est : "
				+ carRouges
				+ getVersionServeur(getServletContext().getServerInfo())
				+ finCarRouges
				+ "<BR>");
		// on tente de lister les attributs d�finis
		java.util.Enumeration<String> e = getServletContext().getAttributeNames();
		boolean boolADesAttributs = false;
		while (e.hasMoreElements()) {
			boolADesAttributs = true;
			String nomAttribut = (String) e.nextElement();
			out.println(
				"Il existe un attribut de nom '"
					+ carRouges
					+ nomAttribut
					+ finCarRouges
					+ "' et sa valeur est '"
					+ carRouges
					+ getServletContext().getAttribute(nomAttribut)
					+ finCarRouges
					+ "'<BR>");
		}
		if (!boolADesAttributs)
			out.println("Je n'ai trouv� aucun attribut d�fini.");

		out.println("<hr><H2 align=\"left\">Quelques infos sur le client</H2>");
		out.println(
			"L'appel req.getRemoteHost() retourne : "
				+ carRouges
				+ req.getRemoteHost()
				+ finCarRouges
				+ "<BR>");
		out.println(
			"L'appel req.getRemoteAddr() retourne : "
				+ carRouges
				+ req.getRemoteAddr()
				+ finCarRouges
				+ "<BR>");

		// enfin, la fin de la page HTML
		out.println("<HR></BODY> </HTML>");
	}
}

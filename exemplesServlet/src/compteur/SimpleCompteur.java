package compteur;

import java.util.*;
import java.io.*;
import javax.servlet.*;



// noter le implements
public class SimpleCompteur extends GenericServlet  {
	private Compteur UnCompteur;
	private String strDateDemarrage;

	// cette routine est fournie � titre "gracieux" si cette
	// information int�resse quelqu'un...
	public String getServletInfo(){
	 		return "Servlet Compteur minimal";
 	}
	public void init(ServletConfig cfg) throws ServletException {
		super.init();	// ne pas oublier
		// instanciation
		UnCompteur = new Compteur();
		// la date de d�marrage : c'est simple.
		strDateDemarrage	= new Date().toString();
	}
  	public void service( ServletRequest req, ServletResponse res )
	     throws ServletException, IOException {
  	  
	  // Retourne les donn�es
	  res.setContentType("text/html");
   	  PrintWriter sortie = res.getWriter();
   	  // g�n�ration dynamique de la page HTML
	  sortie.println("<HTML>");
	  sortie.println("<HEAD><TITLE>Compteur simple</TITLE></HEAD>");
	  sortie.println("<BODY>");
  	  
  	  // Incr�mente le compteur sans controle
	  // sortie.println("<BIG>Cette page a �t� acc�d�e " + UnCompteur.Incremente() + " fois.</BIG>");
  	  
  	  // Incr�mente le compteur avec controle
	  sortie.println("<BIG>On a d�j� acc�d� � cette page " + UnCompteur.Incremente(req.getRemoteAddr()) + " fois.</BIG>");
	  
	  sortie.println("</BODY></HTML>");
	  // vide le flux
	  sortie.flush();
	}
}

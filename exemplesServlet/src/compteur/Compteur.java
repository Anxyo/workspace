package compteur;
import java.util.*;

class Compteur {
	private int NbFois = 0;
	private String strAdresse = null;
	private String strDate = null;

int getNb() {
	return NbFois;
}
	// incr�mente sans �tat d'ame
	public int Incremente() {
		strDate = new Date().toString();
		return ++NbFois;
	}
	// incr�mente si les adresses IP ont chang�es
	public synchronized int Incremente(String strIP) {
			// ne pas oublier la premi�re condition !
			//if (strAdresse != null && strAdresse.compareTo(strIP) != 0)
				++NbFois;
			strDate = new Date().toString();
			strAdresse = strIP;
			return NbFois;
	}
}

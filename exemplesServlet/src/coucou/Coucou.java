package coucou;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class Coucou extends HttpServlet {

  // on souhaite r�pondre uniquement � la commande HTTP GET
  public void doGet(HttpServletRequest req, HttpServletResponse res)
                               throws ServletException, IOException {

    // notre r�ponse sera uniquement du texte
    // doit �tre pr�cis� avant l'�mission des autres datas
    res.setContentType("text/html");
	// le flux de sortie : noter que c'est un Writer (text)
    PrintWriter out = res.getWriter();
	// g�n�ration dynamique de la page HTML
    out.println("<HTML>");
    out.println("<HEAD><TITLE>Premi�re servlet</TITLE></HEAD>");
    out.println("<BODY>");
    out.println("<BIG>Coucou, � studieux stagiaire !</BIG>");
    out.println("</BODY></HTML>");
  }
  
  	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		doGet(req,resp);
	}
}
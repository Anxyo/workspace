package jndi;

import java.io.*;
import java.sql.*;
import javax.sql.*;
import javax.naming.*;
import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class ServletJndi extends javax.servlet.http.HttpServlet implements
		javax.servlet.Servlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
		 PrintWriter out = response.getWriter();
		 out.println("<HTML>");
		 out.println(" <HEAD><TITLE>Une servlet utilisant Jndi</TITLE></HEAD>");
		 out.println(" <BODY>");
		 try {
			 InitialContext initCtx = new InitialContext();
			 DataSource ds = (DataSource)initCtx.lookup("java:comp/env/jdbc/test");
			 Connection cnx = ds.getConnection();
			 
			 out.println("<br><br>");
			 out.println("Connexion ouverte � travers la datasource jdbc/test ...<br>");
			 Statement stmt = cnx.createStatement();
			 ResultSet rs = stmt.executeQuery("select * from clients");
			 while (rs.next() ) {
				 String ligne = "Raison sociale : " + rs.getString("r_sociale")
				 				+ ", ville : " + rs.getString("ville")
				 				+ ", pays : " + rs.getString("pays") + "<br>";
				 out.println(ligne);
			 }
			 
			 rs.close();
			 stmt.close();
			 cnx.close();
			 initCtx.close();
			 out.println("<br><br>");
			 out.println("D�connexion effectu�e<br>");
			 }
		  catch(Exception e) {
			 out.println("<br><br>");
			 out.println("Pb de connexion.<br>");
			 out.println(e.getMessage() + "<br>");
			 }
			 out.println(" </BODY>");
			 out.println("</HTML>");
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
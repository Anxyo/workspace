package datessi;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

@SuppressWarnings("serial")
public class dateSSI extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		res.setContentType("text/html");
		PrintWriter sortie = res.getWriter();
		Date DateDuJour = new Date();
		sortie.println(DateDuJour.toString());
	}
}

package cookies;

import java.io.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class Cookies extends HttpServlet {
	private int cptPage = 0;

	public void service(HttpServletRequest req, HttpServletResponse res)
		throws IOException {
		boolean cookieTrouve = false;
		Cookie LeCookie = null;

		// Avant d'appeler getWriter()
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();

		// des cookies dans la requ�te ?
		Cookie[] LesCookies = req.getCookies();

		// oui
		if (LesCookies != null) {
			for (int i = 0; i < LesCookies.length; i++) {
				LeCookie = LesCookies[i];
				if (LeCookie.getName().equals("Compteur")) {
					cookieTrouve = true;
					break;
				}
			}
		}

		if (cookieTrouve == false) {
			// cr�e un nouveau cookie et d�finit sa date de p�remption(!)
			LeCookie = new Cookie("Compteur", "1");
			LeCookie.setMaxAge(10);
			// ajout � la r�ponse 
			res.addCookie(LeCookie);
		}

		out.println(
			"<html><head>\n"
				+ "<title>Compteur utilisant des cookies</title>\n"
				+ "</head>\n"
				+ "<body bgcolor=#ffffff> <font face=\"Comic Sans MS\" size+=4 color=blue>"
				+ "<center><h1>Compteur utilisant des cookies</h1></center>");
		cptPage++;
		out.println("<font color=red>");
		out.println(
			"<p><br><br><br>Des utilisateurs ont atteints cette page "
				+ cptPage
				+ " fois.");
		out.println("</font>");

		// le diagnostic
		out.println("<font color=green>");
		if (cookieTrouve) {
			int nbCookies = Integer.parseInt(LeCookie.getValue());
			nbCookies++;
			// le nouvelle valeur du cookie
			LeCookie.setValue(String.valueOf(nbCookies));
			LeCookie.setMaxAge(30);
			// ajout � la r�ponse
			res.addCookie(LeCookie);
			out.println(
				"<p>Vous �tes venus "
					+ LeCookie.getValue()
					+ " fois dans les 30 derni�res secondes.");
		} else {
			out.println(
				"<p>Je ne vous connais pas encore ! Ou votre navigateur "
					+ "a horreur des cookies.");
		}

		out.println("</font><p></p>");
		out.println("<font color=black>");
		out.println(
			"<A HREF='"
				+ req.getRequestURL()
				+ "'>"
				+ "Pour recharger la page</A>");
		out.println("</font><p></p><hr>");
		out.println("<font color=darkblue>");
		out.println("Quelques informations suppl�mentaires : <br><ul>");
		out.println(
			"<li>req.getContextPath() retourne : "
				+ req.getContextPath()
				+ "<br>");
		out.println(
			"<li>req.getServletPath() retourne : "
				+ req.getServletPath()
				+ "<br>");
		out.println(
			"<li>req.getRequestURI() retourne : "
				+ req.getRequestURI()
				+ "<br>");
		out.println(
			"<li>req.getRequestURL() retourne : "
				+ req.getRequestURL()
				+ "<br>");
		out.println(
			"<li>req.getRealPath(&lt;Appli web courante&gt;) retourne : "
				+ getServletContext().getRealPath(req.getContextPath())
				+ "<br>");

		out.println("<lu></font>");
		out.println("</body></html>");
	}
}

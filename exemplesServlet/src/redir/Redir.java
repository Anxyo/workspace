package redir;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


@SuppressWarnings("serial")
public class Redir extends HttpServlet {

public void doPost(HttpServletRequest req, HttpServletResponse res)
			 throws ServletException, IOException{

	// on retrouve l'URL choisie
	String strChoix = req.getParameter("slcURLS");
	if (strChoix.equals("IBM"))
		strChoix = "http://www.software.ibm.com/";
	else if (strChoix.equals("Sun Microsystems"))
		strChoix = "http://java.sun.com/";
	else if (strChoix.equals("Microsoft"))
		strChoix = "http://msdn.microsoft.com/";
	else	// on reste en local
		strChoix = "http://localhost:8080/";

	// et on redirige le client...
	res.sendRedirect(strChoix);

}
}

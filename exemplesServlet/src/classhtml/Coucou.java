package classhtml;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/*
La classe de servlet minimale se servant
de la classe HTML.
 */
@SuppressWarnings("serial")
public class Coucou extends HttpServlet {

	// ce code s'est notablement allégé...
	public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		res.setContentType("text/html");
		PrintWriter sortie = res.getWriter();
		HTML pageRéponse = new HTML("Coucou");
		pageRéponse.ajoute("Ceci est une servlet minimale !");
		sortie.println(pageRéponse.page());
	}
}

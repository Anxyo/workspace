package classhtml;

class HTML {
	// le buffer de la page : laiss� accessible pour
	// permettre d'y effectuer des op�rations non
	// support�es (et peu fr�quentes, sinon on devra
	// �crire une m�thode).
	StringBuffer buffer;
	// constantes repr�sentant des balises HTML
	static final int NORMAL = 0;
	static final int TITRE = 1; // <H1>
	static final int LIGNE = 2; // <HR>

	// constructeur � un argument : la chaine plac�e entre <TITLE> </TITLE>
	HTML(String titre) {
		buffer = new StringBuffer(2048); // buffer de 2k
		buffer.append("<html><head><title>");
		buffer.append(titre);
		buffer.append("</title></head><body>");
	}

	/*
	 Ajoute du texte � la page, on peut pr�ciser
	 le type de balise HTML ("type" prendra une des valeurs
	 des constantes) et si on souhaite g�n�rer un
	 saut de ligne.
	 */
	void ajoute(int type, String texte, boolean SautLigne) {
		switch (type) {
			case NORMAL :
				buffer.append(texte);
				break;

			case TITRE :
				buffer.append("<h1>");
				buffer.append(texte);
				buffer.append("</h1>");
				break;

			case LIGNE :
				buffer.append("<hr/>");
				break;

			default :
				break;
		}
		if (SautLigne)
			buffer.append("<br/>");
	}
	/*
	  Ajoute du texte en mode normal
	  sans saut de ligne.
	*/
	void ajoute(String texte) {
		this.ajoute(NORMAL, texte, false);
	}
	/*
	Ajoute du texte normal avec un saut de ligne
	�ventuel.
	*/
	void ajoute(String texte, boolean SautLigne) {
		this.ajoute(NORMAL, texte, SautLigne);
	}
	/*
	Retourne le buffer complet avec les balises
	de fin ajout�es.
	*/
	String page() {
		buffer.append("</body> </html>");
		return buffer.toString();
	}
}

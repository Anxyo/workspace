package chnesvlt1;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

@SuppressWarnings("serial")
public class Suite extends javax.servlet.http.HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter sortie = res.getWriter();
		sortie.println("<html><head><title>Chainage de servlets</title></head><body>");
		sortie.println("<center><h1>Servlet Suite</h1></center>");
		sortie.println("<hr/>");
		// Un message sans int�r�t...
		sortie.println("<br><strong> Emis par la seconde servlet (Suite) du chainage</strong>");

		// tente de retrouver les attributs param1 et param2
		String p1, p2;
		if ((p1 = (String) req.getAttribute("Param1")) != null)
			sortie.println("<p />Valeur de l'attribut Param1 = " + p1);
		else
			sortie.println("<br/>Je n'ai pas retrouv� d'attribut Param1 !");

		if ((p2 = (String) req.getAttribute("Param2")) != null)
			sortie.println("<br/>Valeur de l'attribut Param2 = " + p2);
		else
			sortie.println("<br/>Je n'ai pas retrouv� d'attribut Param2 !");

		// fin de page
		sortie.println("</body></html>");
	}
}


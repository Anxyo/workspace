package chnesvlt1;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

@SuppressWarnings("serial")
public class ChneSvlt extends javax.servlet.http.HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {
		String rqt = req.getQueryString();
		if (rqt != null) {
			// ajout de 2 attributs � la requete
			req.setAttribute("Param1", "Il �tait un ");
			req.setAttribute("Param2", "petit navireuh.. ");
			// obtention d'un dispatcher nomm�

			RequestDispatcher rqd =
				getServletContext().getNamedDispatcher("Suite");
			// passe le controle � la servlet "Suite"
			rqd.forward(req, res);
		}
		res.setContentType("text/html");
		PrintWriter sortie = res.getWriter();
		sortie.println(
			"<html><head><title>Chainage de servlets</title></head><body>");
		sortie.println("<center><h1>Chainage de servlets</h1></center>");
		sortie.println("<hr/>");
		// Envoi de quelques donn�es
		sortie.println(
			"<br/>Ligne g�n�r�e par ChneSvlt, avant le passage du controle � la servlet Suite<p/>");
		// un lien pour proposer le chainage de servlets
		sortie.println(
			"<a href='"	+ req.getRequestURL()
				+ "?chainage=onyva!'>"
				+ "Pour recharger la page et appeler la servlet Suite</A>");
		sortie.println("</body></html>");
	}
}

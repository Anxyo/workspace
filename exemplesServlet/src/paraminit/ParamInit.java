package paraminit;

import java.io.*;
import java.util.*;
import javax.servlet.*;

@SuppressWarnings("serial")
public class ParamInit extends GenericServlet {
	
	public void service(ServletRequest req, ServletResponse res)
		throws ServletException, IOException {
		
		// drapeau pour d�tecter si au moins un param�tre d'init existe
		boolean UnParametreAuMoins = false;
		ServletContext ctx = this.getServletContext(); // contexte applicatif
		// ceci n'a pas chang�
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();

		// le code de d�but de la page HTML (on n'est pas
		// oblig� mais c'est plus "propre")
		out.println(
			"<HTML><HEAD><TITLE>Param�tres d'initialisation</TITLE></HEAD><BODY>");
		out.println(
			"<H1 align=\"center\">Param�tres d'initialisation</H1><HR>");
		out.println(
			"<H3 align=\"left\">Param�tres de contexte (port�e application web) : </H3>");
		// obtient la liste des param�tres
		Enumeration<String> enumParamCtx = ctx.getInitParameterNames();
		while (enumParamCtx.hasMoreElements()) {
			UnParametreAuMoins = true;
			// le nom du param�tre, d'abord
			String nomParam = (String) enumParamCtx.nextElement();
			// puis sa valeur
			out.println(
				"Param�tre '"
					+ nomParam
					+ "' = '"
					+ this.getServletContext().getInitParameter(nomParam)
					+ "'<BR>");
		}
		// si pas de param. de contexte on pr�vient l'utilisateur
		if (!UnParametreAuMoins)
			out.println("Aucun param�tre de contexte n'a �t� d�fini !");

		// pour les param�tres sp�cifiques
		UnParametreAuMoins = false;

		// on r�cup�re, l� encore, toute la liste dans une �num�ration
		Enumeration<String> enumSpecif = getInitParameterNames();
		// le titre du paragraphe
		out.println(
			"<H3 align=\"left\">Param�tres sp�cifiques (port�e servlet) : </H3>");
		// on parcourt l'�num�ration
		while (enumSpecif.hasMoreElements()) {
			UnParametreAuMoins = true;
			// le nom du param�tre, d'abord
			String nomParam = (String) enumSpecif.nextElement();
			// puis sa valeur
			out.println(
				"Param�tre '"
					+ nomParam
					+ "' = '"
					+ getInitParameter(nomParam)
					+ "'<BR>");
		}
		// si pas de param. on pr�vient l'utilisateur
		if (!UnParametreAuMoins)
			out.println("Aucun param�tre sp�cifique n'a �t� d�fini !");

		// enfin, la fin de la page HTML
		out.println("<HR></BODY> </HTML>");

		// facultatif
		out.close();
	}
}

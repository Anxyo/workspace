package apisession;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class ApiSession extends HttpServlet {
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {
		long delta = 0;
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();

		// Obtient un objet session, (cr�ation si n�cessaire)
		HttpSession session = req.getSession(true);
		
		// tente de retrouver de quand date le dernier passage
		Long precedent = (Long) session.getAttribute("trace_passage");
		if (precedent == null) {
			precedent = new Long(System.currentTimeMillis() + "");
		} else {
			// quelle diff�rence ?
			delta = System.currentTimeMillis() - precedent.longValue();
			// nouvelle valeur
			precedent = new Long(System.currentTimeMillis() + "");
		}
		session.setAttribute("trace_passage", precedent);
		session.setAttribute("toto", new Integer(23));
		out.println("<html><head><title>ApiSession</title></head>");
		out.println("<body><h1>Intervalles de passage</h1>");

		// Affichage des donn�es
		if (delta != 0)
			out.println(
				"<br/>Votre dernier passage date de "
					+ millisToChaine(delta)
					+ " .");
		else
			out.println("<br/>Bienvenue ! C'est votre premier passage !");

		out.println(
			"<p/><hr/><bold><font color='green'>Ent�tes re�us : </bold><br/>");
		java.util.Enumeration<String> enumH = req.getHeaderNames();
		java.util.Enumeration<String> enumL;
		String nomH, sLigne;
		while (enumH.hasMoreElements()) {
			nomH = (String) enumH.nextElement();
			enumL = req.getHeaders(nomH);
			out.println(
				"<p/>Un ent�te de nom '"
					+ nomH
					+ "' a �t� re�u. Il contient : <br/>");
			while (enumL.hasMoreElements()) {
				sLigne = (String) enumL.nextElement();
				out.println("<em>" + sLigne + "</em><br/>");
			}
		} // fin while
		out.println("</font></body></html>");
	}

	// routine de service formatant un nombre de ms en une chaine d'heure
	String millisToChaine(long ms) {

		int jours = (int) (ms / (1000 * 60 * 60 * 24));
		ms = ms - (1000 * 60 * 60 * 24 * jours);
		int heures = (int) (ms / (1000 * 60 * 60));
		ms = ms - (1000 * 60 * 60 * heures);
		int minutes = (int) (ms / (1000 * 60));
		ms = ms - (1000 * 60 * minutes);
		int secondes = (int) (ms / (1000));

		return jours
			+ " jours "
			+ heures
			+ " heures "
			+ minutes
			+ " minutes "
			+ secondes
			+ " secondes";

	}
}

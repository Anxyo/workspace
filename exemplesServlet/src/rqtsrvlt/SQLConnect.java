package rqtsrvlt;

/**
 * Premier bean : utilis� pour se connecter aux donn�es
 * et ex�cuter une requ�te ou une instruction SQL.
 */
import java.sql.*;

public class SQLConnect {
	private Connection cnx = null;
	private Statement st = null;
	private String strDriver = "";
	private String strUrl = "";
	private String strUid = "";
	private String strPwd = "";
	private String strSQL = "";
	private java.sql.ResultSet rsResultat = null;

	/**
	 * routine de service tentant de charger le driver
	 */
	private boolean chargeDriver() {
		try {
			// tentative de chargement du driver
			Class.forName(strDriver).newInstance();
		} catch (Exception exc) {
			return false;
		}
		return true;
	}

	/**
	 * routine de service tentant d'effectuer une connexion
	 */
	private boolean connexion() {
		// tentative de connexion
		try {
			cnx = DriverManager.getConnection(strUrl, strUid, strPwd);
		} catch (SQLException sqle) {
			return false;
		}
		return true;
	}

	/**
	 * Charge le driver, se connecte et ex�cute la requ�te. Aucun ResultSet
	 * nouveau n'est g�n�r�.
	 */
	public boolean exec() throws RequeteException {
		if (!chargeDriver())
			throw new RequeteException("Impossible de charger le driver : "
					+ strDriver);
		if (!connexion())
			throw new RequeteException("Impossible de se connecter � l'URL : "
					+ strUrl);
		try {
			st = cnx.createStatement(); // ne pas oublier !
			st.executeUpdate(strSQL);
		} catch (SQLException sqle) { // erreur de syntaxe SQL...
			throw new RequeteException("Erreur de syntaxe SQL : " + strSQL);
		}
		return true;
	}

	/**
	 * Charge le driver, se connecte et ex�cute la requ�te. Apr�s l'ex�cution de
	 * cette m�thode, si tout se passe bien rs contient la r�f�rence vers un
	 * ResultSet valide.
	 */
	public boolean execSQL() throws RequeteException {
		if (!chargeDriver())
			throw new RequeteException("Impossible de charger le driver : "
					+ strDriver);
		if (!connexion())
			throw new RequeteException("Impossible de se connecter � l'URL : "
					+ strUrl);
		try {
			st = cnx.createStatement();
			setResultat(st.executeQuery(strSQL));
		} catch (SQLException sqle) { // erreur de syntaxe SQL...
			throw new RequeteException("Erreur de syntaxe SQL : " + strSQL);
		}
		return true;
	}

	public void ferme() throws RequeteException {
		try {
			if (cnx != null)
				cnx.close();
		} catch (SQLException e) {
			throw new RequeteException(
					"Erreur lors de la fermeture de la connexion : "
							+ e.getMessage());
		}
	}

	public String getDriver() {
		return strDriver;
	}

	public String getPwd() {
		return strPwd;
	}

	public java.sql.ResultSet getResultat() {
		return rsResultat;
	}

	public String getTextSQL() {
		return strSQL;
	}

	public String getUid() {
		return strUid;
	}

	public String getUrl() {
		return strUrl;
	}

	public void setDriver(String Driver) {
		strDriver = Driver;
	}

	public void setPwd(String Pwd) {
		strPwd = Pwd;
	}

	public void setResultat(java.sql.ResultSet Resultat) {
		rsResultat = Resultat;
	}

	public void setSQL(String TextSQL) {
		strSQL = TextSQL;
	}

	public void setUid(String Uid) {
		strUid = Uid;
	}

	public void setUrl(String Url) {
		strUrl = Url;
	}
}

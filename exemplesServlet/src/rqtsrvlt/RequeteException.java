package rqtsrvlt;

/**
 * Classe exception associ�e au 2 beans
 */
@SuppressWarnings("serial")
public class RequeteException extends Exception {
	public RequeteException() {
		super();
	}

	public RequeteException(String s) {
		super(s);
	}
}

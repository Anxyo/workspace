package rqtsrvlt;

/**
  classe de la servlet
*/

import javax.servlet.http.*;
import java.io.*;


@SuppressWarnings("serial")
public class RqtSrvlt extends HttpServlet {
	SQLConnect u = null;
	SQLSelect s = null;
	String DebutPage = null;
	String DebutTableau = null;
	String FinPage = null;
/**
 * Commentaire relatif au constructeur RqtSrvlt.
 */
public RqtSrvlt() {
	super();
}
/**
 * Tente de se d�connecter de la base si ce n'est d�j� fait
 * 
 */
public void destroy() {
// tente de se d�connecter de la base
if (u != null) {
	try {
		u.ferme();
		}
	catch (Exception e) {}
	}
}
/*
 * Le code de traitement des requetes
 */
public void doGet(HttpServletRequest req, HttpServletResponse res) throws javax.servlet.ServletException, IOException {
	// init
	///////
	PrintWriter sortie = res.getWriter();
	res.setContentType("text/html");

	// r�cup�ration des arguments
	/////////////////////////////
	String strDriver = req.getParameter("txtDriver");
	String strUrl = req.getParameter("txtUrl");
	String strUid = req.getParameter("txtUid");
	String strPwd = req.getParameter("txtPwd");
	String strRqt = req.getParameter("txtRqt");
	// construction des beans utilis�s
	//////////////////////////////////
	u = new SQLConnect();
	s = new SQLSelect();
	// on renseigne les propri�t�s
	//////////////////////////////
	u.setDriver(strDriver);
	u.setUrl(strUrl);
	u.setUid(strUid);
	u.setPwd(strPwd);
	u.setSQL(strRqt);
	// on veut voir les noms des colonnes
	s.setInsereNomCol(true);
	// pour faire un tableau HTML
	s.setSepDeb("<tr><td>");
	s.setSepCol("</td><td>");
	s.setSepFin("</td></tr>");
	// tente d'effectuer la requ�te
	try {
		u.execSQL();
		// connexion du premier bean au second
		s.setResultat(u.getResultat());
	}
	catch (Exception e) {
		System.out.println("Exception lors de l'ex�cution de la requ�te : " + e.getMessage());
	}
	// On renvoie le r�sultat au client
	///////////////////////////////////
	sortie.println(DebutPage + DebutTableau);
	// on lit le bloc du tableau et
	// on retourne les datas au client
	sortie.println(s.getBloc() + FinPage);
}

public void doPost(HttpServletRequest req, HttpServletResponse res) throws javax.servlet.ServletException, java.io.IOException {
	// d�legation � doGet()
	doGet(req,res);
	}
/**
 * Initialise les champs pour la construction du tableau HTML
 */
public void init() {
DebutPage = "<html><head><title>R�sultat de la requete</title></head><body bgcolor=yellow>" +
					"<h1 align=center> R�sultats de votre requete </h1>";
DebutTableau = "<table border='1' cellspacing='1' cellpadding='1' align='center'>";
FinPage = "</table><br></body></html>";
}
}

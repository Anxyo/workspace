package rqtsrvlt;

import java.sql.*;

public class SQLSelect {
	@SuppressWarnings("unused")
	private ResultSet rs = null;
	private ResultSet rsResultat = null;
	private int intIndice = 0;
	private String strValeur;
	private String strSepDeb;
	private String strSepFin;
	private String strSepCol;
	private int strNbCols = 0;
	private ResultSetMetaData rsmd;
	private String strNomCol;
	private boolean bInsereNomCol = false;
	/**
	 * valeur pass�e � la propri�t� Indice pour indiquer que l'on souhaite que
	 * le r�sultat soit une ligne enti�re, toute valeur positive indiquant un
	 * indice de colonne.
	 */
	public final static int RET_LIGNE = 0;

	public SQLSelect() {
		super();
	}

	/**
	 * Ex�cution de la m�thode avance.
	 * 
	 * @return boolean
	 */
	public boolean avance() {
		boolean bAvance;
		try {
			bAvance = rsResultat.next();
		} catch (SQLException e) {
			bAvance = false;
		}
		return bAvance;
	}

	/**
	 * Ins�rez la description de la m�thode � cet endroit. Date de cr�ation :
	 * (23/01/00 22:35:30)
	 * 
	 * @return java.lang.String
	 */
	public String getBloc() {
		String tmp = "";
		int intTmp = this.intIndice; // sauvegarde
		this.intIndice = RET_LIGNE;
		if (bInsereNomCol) {
			int i = 1, NbCols = this.getNbCols();
			try {
				rsmd = this.getResultat().getMetaData();
				tmp = new String(strSepDeb);
				for (; i <= NbCols; i++) {
					tmp += rsmd.getColumnLabel(i);
					if (i < NbCols)
						tmp += strSepCol;
				} // fin du for
				tmp += strSepFin; // s�parateur de fin
			} catch (SQLException e) {
				System.out
						.println("Erreur lors de l'acc�s aux m�tadatas, indice : "
								+ i + ", infos : " + e.getMessage());
			}
		}
		do {
			tmp += this.getValeur();
		} while (avance());
		this.intIndice = intTmp;
		return tmp;
	}

	public int getIndice() {
		return intIndice;
	}

	public boolean getInsereNomCol() {
		return bInsereNomCol;
	}

	public int getNbCols() {
		return strNbCols;
	}

	public String getNomCol() {
		return strNomCol;
	}

	public ResultSet getResultat() {
		return rsResultat;
	}

	public String getSepCol() {
		return strSepCol;
	}

	public String getSepDeb() {
		return strSepDeb;
	}

	public String getSepFin() {
		return strSepFin;
	}

	public String getValeur() {
		String strTempo = null;

		// si on veut juste une colonne
		// /////////////////////////////
		if (intIndice > 0) {
			try {
				strValeur = strSepDeb + rsResultat.getString(intIndice)
						+ strSepFin;
			} catch (SQLException e) {
				strValeur = strSepDeb + "" + strSepFin;
			} // fin catch
		} // fin if

		// si on veut une ligne compl�te
		// //////////////////////////////
		else if (intIndice == RET_LIGNE) {
			try {
				strTempo = strSepDeb;
				for (int i = 1; i <= strNbCols; i++) {
					// si pas premi�re ni derni�re colonne
					// on ajoute le s�parateur de colonne
					if ((i > 1) && (i <= strNbCols))
						strTempo += strSepCol;
					// ajoute la valeur du champ
					strTempo += rsResultat.getString(i).trim();
					if (rsResultat.wasNull())
						strTempo += "null";
				} // fin du for
					// ajout du s�parateur final
				strTempo += strSepFin;
				strValeur = strTempo;
			} // fin du try
			catch (SQLException e) {
				strValeur = "";
			} // fin du catch
		} // fin else
		return strValeur;
	}

	public void setIndice(int Indice) throws RequeteException {
		String nNomCol = null;
		try {
			if ((Indice < RET_LIGNE) || (Indice > strNbCols))
				throw new RequeteException(
						"Valeur de la prop. Indice invalide : " + Indice);
			// la valeur est correcte
			intIndice = Indice;
			// on met � jour la prop. Nom
			if (Indice > 0) { // pas si toute la ligne
				nNomCol = rsmd.getColumnName(Indice);
				// pas d'exception ? On met � jour le nom de colonne !
				strNomCol = nNomCol;
			} else
				strNomCol = "";
		} catch (SQLException e) {

		}
		intIndice = Indice;
	}

	public void setInsereNomCol(boolean InsereNomCol) {
		bInsereNomCol = InsereNomCol;
	}

	public void setNomCol(java.lang.String NomCol) {
		int nNbCols = 0;
		String strNom;
		try {
			nNbCols = rsmd.getColumnCount();
			for (int i = 1; i <= nNbCols; i++) {
				strNom = rsmd.getColumnName(i);
				if (strNom.equals(NomCol)) {
					intIndice = i; // maj de la prop indice
					strNomCol = NomCol;
					break; // on a trouv�, on sort de la boucle
				} // fin if
			} // fin for
			// si arriv� ici sans changement, pas de pb...
		} // fin try

		catch (SQLException e) {

		} // fin catch

		strNomCol = NomCol;
	}

	public void setResultat(java.sql.ResultSet Resultat)
			throws RequeteException {
		rsResultat = Resultat;
		// mise � jour du nb de colonnes
		try {
			rsmd = rsResultat.getMetaData();
			strNbCols = rsmd.getColumnCount();
		} catch (SQLException e) {
			strNbCols = 0;
			throw new RequeteException(
					"Impossible d'obtenir les m�tadatas sur l'ensemble r�sultat");
		}
	}

	public void setSepCol(java.lang.String SepCol) {
		strSepCol = SepCol;
	}

	public void setSepDeb(java.lang.String SepDeb) {
		strSepDeb = SepDeb;
	}

	public void setSepFin(java.lang.String SepFin) {
		strSepFin = SepFin;
	}
}

package chnesvlt2;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

@SuppressWarnings("serial")
public class ChneSvlt extends javax.servlet.http.HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter sortie = res.getWriter();
		sortie.println(
			"<html><head><title>Inclusion de servlets</title></head><body>");
		sortie.println("<center><h1>Inclusion de servlets</h1></center>");
		sortie.println("<hr/>");
		// Envoi de quelques donn�es avant l'appel
		sortie.println(
			"<br/>Ligne g�n�r�e par ChneSvlt, AVANT l'appel de la servlet inclue<p/>");
		// obtention d'un dispatcher nomm�
		RequestDispatcher rqd =
			getServletContext().getNamedDispatcher("Inclue");
		if (rqd != null) {
			// appel de la servlet 
			rqd.include(req, res);
		} else {
			sortie.println(
				"<br/>Impossible d'obtenir un dispatcher pour la servel 'Inclue' !<p/>");
		}
		// pour bien montrer que l'on est revenu...
		sortie.println(
			"<p/>Ligne g�n�r�e par ChneSvlt, APRES le retour de l'appel de la servlet inclue<p/>");
		sortie.println("</body></html>");
	}
}

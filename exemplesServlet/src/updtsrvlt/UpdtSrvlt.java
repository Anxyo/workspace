package updtsrvlt;
/**
* classe de la servlet
*/
import rqtsrvlt.*;
import javax.servlet.http.*;
import java.io.*;


@SuppressWarnings("serial")
public class UpdtSrvlt extends javax.servlet.http.HttpServlet {
	SQLConnect u = null;
	String DebutPage = null;
	String FinPage = null;

public void destroy() {
// tente de se d�connecter de la base
if (u != null) {
	try {
		u.ferme();
		}
	catch (Exception e) {}
	}
}

public void doGet(HttpServletRequest req, HttpServletResponse res) throws javax.servlet.ServletException, IOException {
	// init
	///////
	PrintWriter sortie = res.getWriter();
	res.setContentType("text/html");
	// r�cup�ration des arguments
	/////////////////////////////
	String strDriver = req.getParameter("txtDriver");
	String strUrl = req.getParameter("txtUrl");
	String strCode = req.getParameter("txtCode");
	String strRSoc = req.getParameter("txtRSoc");
	String strAdresse = req.getParameter("txtAdresse");
	String strZip = req.getParameter("txtZip");
	String strVille = req.getParameter("txtVille");
	String strPays = req.getParameter("txtPays");
	// l'instruction INSERT
	///////////////////////
	String strRqt = "UPDATE clients SET r_sociale = '" + strRSoc.trim() +
						"', adresse = '" + strAdresse.trim()+ "', cde_postal = '" +
						strZip.trim()+ "', ville = '" + strVille.trim()+ "', pays = '" +
						strPays.trim() + "' WHERE code=" + strCode + ";";
	System.out.println(strRqt);
	// construction des beans utilis�s
	//////////////////////////////////
	u = new SQLConnect();
	// on renseigne les propri�t�s
	//////////////////////////////
	u.setDriver(strDriver);
	u.setUrl(strUrl);
	
	u.setUid("root");	// A MODIFIER !
	u.setPwd("mysql");	// A MODIFIER !
	
	u.setSQL(strRqt);
	// tente d'effectuer la requ�te
	boolean bResultat = false;
        String msg = "";
	try {
		bResultat = u.exec();
	}
	catch(Exception e) {
		try {u.ferme();} catch(Exception f){}
                msg = e.getMessage();
		System.out.println("Exception lors de l'ex�cution de l'update : " + msg);
	}

	// On renvoie un diagnostic au client
	///////////////////////////////////////
	sortie.println(DebutPage);
        if (bResultat == true)
          sortie.println("Mise � jour effectu�e.");
        else
          sortie.println(msg);
	sortie.println(FinPage);
}


public void doPost(HttpServletRequest req, HttpServletResponse res) throws javax.servlet.ServletException, java.io.IOException {
	// d�legation � doGet()
	doGet(req,res);
	}

public void init() {
DebutPage = "<HTML><HEAD><TITLE>R�sultat de la requete</TITLE></HEAD><BODY bgcolor=yellow>" +
					"<H1 align=center> R�sultats de votre requete </H1>";
FinPage = "</TABLE><BR></BODY></HTML>";
}
}

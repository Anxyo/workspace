package filtres;

import javax.servlet.*;

public class Filtre1 implements Filter {

	// un constructeur sans param�tre est requis
	public Filtre1() {

	}

	// les trois m�thodes de l'interface Filter
	public void init(FilterConfig fcg) throws ServletException {
		System.err.println("M�thode init() de Filtre1");
	}

	public void doFilter(ServletRequest rq, ServletResponse rs, FilterChain chn)
			throws ServletException, java.io.IOException {
		System.err.println("doFilter() de Filtre1, AVANT le chainage");
		rq.setAttribute("attrCpy", "Copyright St� ARATI");
		
		chn.doFilter(rq, rs);
		
		System.err.println("doFilter() de Filtre1, APRES le chainage");
	}

	public void destroy() {
		System.err.println("M�thode destroy() de Filtre1");
	}

}

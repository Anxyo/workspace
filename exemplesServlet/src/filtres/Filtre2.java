package filtres;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class Filtre2 implements Filter {

	public Filtre2() {
	}
	// les trois m�thodes de l'interface Filter
	public void init(FilterConfig fcg) throws ServletException {
		System.err.println("M�thode init() de Filtre2");
	}

	public void doFilter(ServletRequest rq, ServletResponse rs, FilterChain chn)
			throws ServletException, java.io.IOException {
		String rqt = ((HttpServletRequest)rq).getServletPath();
		java.io.PrintWriter sortie = null;
		if (!"/DessinDynamique".equals(rqt)){  // pas pour le dessin dynamique !
			sortie = rs.getWriter();
			System.err.println("doFilter() de Filtre2, AVANT le chainage");			
		}
		chn.doFilter(rq, rs);
		System.err.println("doFilter() de Filtre2, APRES le chainage");
		if (sortie != null) 
			// Pour g�n�rer un commentaire � la fin du document HTML
			// cr�� par la servlet.
			sortie.println("\n<!-- Cette page fait partie de l'application de Mise � Jour -->");

	}

	public void destroy() {
		System.err.println("M�thode destroy() de Filtre2");
	}

}



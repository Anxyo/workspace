package filtres;

import javax.servlet.*;
import java.io.*;

@SuppressWarnings("serial")
public class ServletCible extends GenericServlet {

	public void service(ServletRequest req, ServletResponse res)
			throws ServletException, java.io.IOException {
		res.setContentType("text/html");
		// le flux de sortie : noter que c'est un Writer (text)
		PrintWriter out = res.getWriter();
		// g�n�ration dynamique de la page HTML
		out.println("<html>");
		out.println("<head><title>Servlet cible</title></head>");
		out.println("<body>");
		out.println("Attribut ins�r� par le filtre : "
				+ (String) req.getAttribute("attrCpy"));
		out.println("</body></html>");
	}
}


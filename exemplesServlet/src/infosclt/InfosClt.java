package infosclt;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class InfosClt extends HttpServlet {

	// une table de hashing est bien pratique : on retrouve une r�f�rence
	// � partir d'une chaine de caract�res (dictionnaire).
	Hashtable<String,Date> lesAcces = new Hashtable<String,Date>();

  public void doGet(HttpServletRequest req, HttpServletResponse res)
							   throws ServletException, IOException {

	res.setContentType("text/html");
	PrintWriter sortie = res.getWriter();

	// le d�but de la page HTML
	sortie.println("<HTML><HEAD><TITLE>Infos sur le client</TITLE></HEAD><BODY>");
	sortie.println("<H1 align=\"center\">Infos sur le client</H1><HR>");

	// les 3 informations disponibles
	String strNomHost = req.getRemoteHost();
	String strAdresse = req.getRemoteAddr();
	String strUtilisateur = req.getRemoteUser();
	// pour les deux premi�res, pas de pb :
	sortie.println("Nom de machine : " + strNomHost + " <BR>");
	sortie.println("Adresse        : " + strAdresse + " <BR>");
	// pour la troisi�me, c'est plus compliqu�...
	if (strUtilisateur == null) {     // pas d'infos disponibles
	  sortie.println("Nom            : je ne connais pas votre nom.");
	}
	else {
	  // on connait le nom et on l'affiche
	  sortie.println("Nom            : " + strUtilisateur);
	  // on tente de retrouver la date d'acc�s
	  // le nom de l'utilisateur sert de cl�
	  Date dernierAcces = lesAcces.get(strUtilisateur);
	  if (dernierAcces == null) {
		sortie.println("C'est votre premi�re visite ! Bienvenu !");
	  }
	  else {
		sortie.println("Votre visite pr�c�dente date du " + lesAcces.get(strUtilisateur));
	  }
	  // si l'utilisateur est "Administrateur", petit traitement sp�cial !
	  if (strUtilisateur.equals("Administrateur")) {
		sortie.println("Comment allez-vous, cher ami ? <BR>");
	  }
	  // m�morisation pour le prochain passage
	  lesAcces.put(strUtilisateur, new Date());
	  // fin du document HTML
	  sortie.println("<HR></BODY> </HTML>");
} // fin de doGet()


}
}

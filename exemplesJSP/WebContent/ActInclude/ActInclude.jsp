<html>
<head>
<title></title>
</head>
<body bgcolor="#00CCFF">
	<h2 align="center">Directive include et action include</h2>
	<p>&nbsp;</p>
	<hr>
	Insertion de la page AInclureDir.html avec la
	<strong>directive</strong> include. Son contenu suit :
	<br>
	<%@ include file="AInclureDir.html"%>
	<hr>
	Insertion de la page AInclureAct.html avec
	<strong>l'action</strong> include. Son contenu suit :
	<br>
	<jsp:include page="AInclureAct.html" flush="true" />
	<hr>
	<br>
	<center>
		Si vous modifiez ces deux ressources sans recompiler, seule
		l'inclusion effectu�e par<strong> l'action</strong> include sera prise
		en compte.
	</center>
</body>
</html>

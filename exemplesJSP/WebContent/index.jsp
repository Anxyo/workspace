<html>
<head>
<title>Menu d'acc�s aux exemples de servlets</title>
</head>
<body>
	<center>
		<h2>Choisissez un des liens suivants :</h2>
	</center>
	<p></p>
	<table border="1">
		<tr>
			<td>1 - <a href="JSPMini.jsp">Page JSP minimale</a>
			</td>
		</tr>
		<tr>
			<td>2 - <a href="./AInclure/Lettre.jsp">Exemple
					d'utilisation de la directive &lt;%@include&gt;.</a>
			</td>
		</tr>
		<tr>
			<td>3 - <a href="./Objets/Objets.jsp">Utilisation simple de
					quelques objets de base.</a>
			</td>
		</tr>
		<tr>
			<td>4 - <a href="./Erreurs/Erreurs.jsp">Exemple
					d'utilisation d'une page de traitement d'erreurs.</a>
			</td>
		</tr>
		<tr>
			<td>5 - <a href="./Redir/MotPasse.html">Exemple de
					redirection utilisant &lt;jsp:forward&gt;.</a>
			</td>
		</tr>
		<tr>
			<td>6 - <a href="./ActInclude/ActInclude.jsp">Utilisation de
					&lt;jsp:include&gt; et de &lt;jsp:param&gt;.</a>
			</td>
		</tr>
		<tr>
			<td>7 - <a href="./SyntaxeXML/JspMiniXML.jsp">Utilisation de
					lasyntaxe XML (JSP 1.2 seulement).</a>
			</td>
		</tr>
		<tr>
			<td>8 - <a href="./Epargne/Epargne1.html">Utilisation de
					beans dans des pages JSP.</a>
			</td>
		</tr>
		<tr>
			<td>9 - <a href="./TagMini/mini.jsp">Exemple minimal d'une
					extension de balise</a>
			</td>
		</tr>
		<tr>
			<td>10- <a href="./TagBody/Corps.jsp">Traitement du texte
					inter-balise</a>
			</td>
		</tr>
		<tr>
			<td>11- <a href="./TagAttributs/Attributs.jsp">Traitement
					des attributs de balises</a>
			</td>
		</tr>
	</table>
</body>
</html>

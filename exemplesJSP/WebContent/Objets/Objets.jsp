<%@ page session="true"%>
<html>
<head>
<title></title>
</head>
<body bgcolor="yellow">
	<p align="center">
		<font face="Arial"><font face="" size="5"><strong>Objets
					implicites</strong>
		</font>
		</font>
	</p>
	<hr>
	<p>
		<font face="Arial">On va retrouver tous les grands objets
			d&eacute;j&agrave; rencontr&eacute;s dans l'API Servlet disponibles,
			dans le contexte d'ex&eacute;cution des pagees JSP. Ainsi,
			automatiquement, on va disposer des objets :</font>
	</p>
	<ul>
		<li><font face="Arial">request (correspondance dans l'API
				Servlet : ServletRequest).</font>
		</li>
		<li><font face="Arial">response (correspondance dans l'API
				Servlet : ServletResponse, normalement peu utile).</font>
		</li>
		<li><font face="Arial">session (correspondance dans l'API
				Servlet : HttpSession).</font>
		</li>
		<li><font face="Arial">config (correspondance dans l'API
				Servlet : ServletConfig).</font>
		</li>
		<li><font face="Arial">application (correspondance dans
				l'API Servlet : ServletContext).</font>
		</li>
	</ul>
	<p>
		<font face="Arial">Ainsi que quelques objets utiles, non issus
			de l'API Servlet. Citons :</font>
	</p>
	<ul>
		<li><font face="Arial">exception : accessible dans les
				pages de traitement d'erreurs.</font>
		</li>
		<li><font face="Arial">out : objet permettant
				d'&eacute;crire dans le flux de sortie, normalement peu utile dans
				les pages JSP.</font>
		</li>
		<li><font face="Arial">pageContext : objet regroupant des
				r&eacute;f&eacute;rences vers les autres objets.</font>
		</li>
	</ul>
	<p>
		<font face="Arial">A titre d'exemple d'usage de ces objets,
			voici quelle est la requ�te HTTP utilis�e pour acc�der � cette page
			JSP : <%= request.getMethod() %>. </font>
	</p>
	<p>
		<font face="Arial"> <% if (session.isNew())
  		out.println("La session est toute neuve");
  	 else
  	 	out.println("La session existait d�j�");
  	 	%> </font>
	</p>
	<br>

	<p>
		<font face="Arial"></font>
	</p>
	<hr>
</body>
</html>

<?xml version="1.0" encoding="UTF-8"?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" version="1.2">
	<jsp:directive.page contentType="text/html" isThreadSafe="true" />
	<html>
<head>
<title>Page JSP minimale</title>
</head>
<body>
	<strong>Dans une page JSP on peut trouver :<br />
	</strong>
	<ul>
		<li>des <strong>directives</strong>, globales à la page.</li>

		<li>des <strong>déclarations</strong> (ou "éléments de script")
			de code java, également globales.</li>

		<li>des <strong>scriptlets</strong>, insérés dans la méthode
			principaleservice() de la servlet générée.</li>

		<li>des <strong>expressions</strong>, affichées telles quelles</li>

		<li>des <strong>balises spéciales</strong>("actions"), modifiant
			le comportement à l'exécution.</li>
	</ul>
	<br />
	<strong>Résultat de l'exécution : </strong>
	<jsp:declaration>
		// déclarations (éléments de script): portée page
		public void toto() {
		String s = "Coucou";
		}
		int h = 25;
		String s = "";
	</jsp:declaration>

	<jsp:scriptlet>
		// scriptlets : bien distinguer des déclarations, 
		// portée méthode ppale
		// Attention l'erreur "vue" par Eclipse a la ligne suivante n'en est pas une !
		for (int i = 1; i &lt;= 10; i++) {
		     s = s + i; h++;
	</jsp:scriptlet>


	<jsp:expression>
		i
	</jsp:expression>
	<!-- scriptlet : la fin de la boucle ci-dessus -->
	<jsp:scriptlet> 
		} 
	</jsp:scriptlet>
	<!-- Affiche la valeur de la variable s -->
	<jsp:expression> 
		s 
	</jsp:expression>
	<br />
</body>
	</html>
</jsp:root>
<jsp:useBean id="epargne" scope="page" class="epargne.beanEpargne">

	<jsp:setProperty name="epargne" property="montant" param="txtMontantB" />

	<%-- le commentaire suivant est envoy� au client mais pas moi ! --%>
	<!-- Remplace :
   String strMontant = request.getParameter("txtMontantB");
   epargne.setMontant(Double.valueOf(strMontant).doubleValue());
  -->

	<jsp:setProperty name="epargne" property="taux" param="txtTauxB" />
	<!-- Remplace :
   String strTaux = request.getParameter("txtTauxB");
   epargne.setTaux(Double.valueOf(strTaux).doubleValue());
  -->

	<jsp:setProperty name="epargne" property="duree" param="txtDureeB" />
	<!-- Remplace :
  String strDuree = request.getParameter("txtDureeB");
  epargne.setDuree(Integer.valueOf(strDuree).intValue());
  -->

</jsp:useBean>

<html>
<head>
<title>Rendement</title>
</head>
<body bgcolor="yellow">



	<p align="center">
		<strong><font face="Arial">RENDEMENT D'UNE EPARGNE</font>
		</strong> <br>
		<strong><font face="Arial" size="4">(utilisation d'un
				bean)</font>
		</strong>
	</p>
	<hr/>
	<p>
		<font face="Arial">Voici quel est le r&eacute;sultat de votre
			calcul :</font>
	</p>

	<table width="75%" border="1" cellspacing="1" cellpadding="1"
		align="center">
		<tr>
			<td><font face="Arial">Montant &eacute;pargn&eacute; : </font></td>
			<td><font face="Arial"><jsp:getProperty name="epargne"
						property="montant" /></font>
			</td>
		</tr>
		<tr>
			<td><font face="Arial">Taux, en % :</font>
			</td>
			<td><font face="Arial"><jsp:getProperty name="epargne"
						property="taux" /></font>
			</td>
		</tr>
		<tr>
			<td><font face="Arial">Dur&eacute;e, en ann&eacute;es : </font>
			</td>
			<td><font face="Arial"><jsp:getProperty name="epargne"
						property="duree" /></font>
			</td>
		</tr>
		<tr>
			<td><font face="Arial">R&eacute;sultat : </font>
			</td>
			<td><font face="Arial"><jsp:getProperty name="epargne"
						property="valeur" /></font>
			</td>
		</tr>
	</table>
	<p/>

</body>
</html>
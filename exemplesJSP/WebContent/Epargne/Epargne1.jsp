<html>
<head>
<title>Rendement</title>
</head>
<body bgcolor="yellow">

	<p align="center">
		<strong><font face="Arial">RENDEMENT D'UNE EPARGNE</font>
		</strong>
	</p>
	<hr>
	<p>
		<font face="Arial">Voici quel est le r&eacute;sultat de votre
			calcul :</font>
	</p>

	<% String strMontant = request.getParameter("txtMontant"); %>
	<% String strTaux = request.getParameter("txtTaux"); %>
	<% String strDuree = request.getParameter("txtDuree"); %>
	<% String strResultat = calcul(strMontant,strTaux,strDuree); %>

	<%!
// routine de calcul, se charge des conversions
private String calcul(String mt, String tx, String du) {
double dblResultat = 0, dblMt = 0, dblTx = 0;
int intDuree = 0;
try {
	dblMt = Double.valueOf(mt).doubleValue();
	dblTx = Double.valueOf(tx).doubleValue();
	intDuree = Integer.valueOf(du).intValue();
}
catch (Exception ne) {
	return "Paramètres incorrects";
}
dblResultat = dblMt;
for (int i  = 1; i <= intDuree; i++)
	 dblResultat *= (1 + (dblTx / 100));
// conversion en chaine
return ""+dblResultat;
}
%>

	<table width="75%" border="1" cellspacing="1" cellpadding="1"
		align="center">
		<tr>
			<td><font face="Arial">Montant &eacute;pargn&eacute; : </font></td>
			<td><font face="Arial"><%= strMontant %> </font>
			</td>
		</tr>
		<tr>
			<td><font face="Arial">Taux, en % :</font>
			</td>
			<td><font face="Arial"><%= strTaux %></font>
			</td>
		</tr>
		<tr>
			<td><font face="Arial">Dur&eacute;e, en ann&eacute;es : </font>
			</td>
			<td><font face="Arial"><%= strDuree %></font>
			</td>
		</tr>
		<tr>
			<td><font face="Arial">R&eacute;sultat : </font>
			</td>
			<td><font face="Arial"> <%= strResultat %> </font>
			</td>

		</tr>
	</table>
	<p>&nbsp;</p>

</body>
</html>
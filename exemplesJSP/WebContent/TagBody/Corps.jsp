<%@  taglib uri="/arati1" prefix="ar3"%>
<html>
<head>
<title>tagLib Exemple2</title>
</head>
<body bgcolor="#ffff00">
	<h2 align="center">
		<em>Balises spécifiques : manipulation du texte inter-balises</em>
	</h2>
	<p />
	Insertion de texte effectuée avec une balise spécifique :
	<br/>
	<br/>

	<hr/>
	<ar3:entete3 adresse="12 rue du Pot de Fer-75005-Paris"
		majuscules="oui">Texte du premier bloc</ar3:entete3>

	<hr/>
	<ar3:entete3 adresse="12 rue du Pot de Fer-75005-Paris"
		majuscules="non">Second bloc</ar3:entete3>

	<hr/>
</body>
</html>

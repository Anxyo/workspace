<%@  taglib uri="/arati1" prefix="ar2"%>
<html>
<head>
<title>tagLib Exemple3</title>
</head>
<body bgcolor="#ffff00">
	<h2 align="center">
		<em>Balises sp�cifiques : exemple minimal</em>
	</h2>
	<p/>
	Insertion de texte effectu�e avec une balise sp�cifique :
	<br/><br/><hr/>
	<ar2:entete2 adresse="12 rue du Pot de Fer-75005-Paris"
		majuscules="oui">
		<i>Ceci est le "corps" (body) situ� entre les deux balises du
			premier bloc </i>
		<br/>
	</ar2:entete2>

	<hr/>
	<ar2:entete2 adresse="12 rue du Pot de Fer-75005-Paris"
		majuscules="non">
		<i>Ceci est le "corps" (body) situ� entre les deux balises du
			second bloc </i>
		<br/>
	</ar2:entete2>
	<hr/>
</body>
</html>

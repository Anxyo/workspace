<%-- page de traitementement d'erreurs --%>
<%@ page errorPage="TraitErreurs.jsp"%>
<html>
<head>
<title>Traitement d'erreurs</title>
</head>
<body bgcolor="#ffff00">
	<h2 align="center">Traitement d'erreurs</h2>
	<p>Cette&nbsp;page effectue une op�ration ill�gale : elle tente de
		faire une division par 0 ... une fois sur deux !</p>
	<p>Elle intercepte l'exception correspondante et en l�ve une
		nouvelle, ce qui produit l'invocation de la page de traitement
		d'erreurs ("TraitErreurs.jsp").</p>
	<br>
	<%
  try {
    int valeur;
    // une fois sur 2 seulement : on utilise la session
    Integer drapeau = null;
    // lecture du drapeau dans la session
    if ((drapeau = (Integer)session.getAttribute("drapeau")) == null)
      drapeau = new Integer(0);

    //incr�mentation
    valeur = drapeau.intValue() + 1;
    // m�morisation dans la session
    session.setAttribute("drapeau",new Integer(valeur));
    // fait on la division par 0 ?
    if (valeur % 2 == 0) {
      // division par 0
      int i = 0;
      i = 15 / i;
    }
    // si pas de division, passe ici
%>
	Valeur stock�e dans la session :
	<%= valeur%>
	<br>
	<br> Pas de division par 0 cette fois ci !
	<br>
	<center>
		<strong>Rafraichissez la page !</strong>
	</center>
	<%
  }
  catch (Exception e) {
      throw new Exception("Division par 0 produisant un branchement vers la page de traitement d'erreurs");
  }
%>
</body>
</html>

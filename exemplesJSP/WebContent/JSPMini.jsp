<%@ page import="java.util.*" %>
<html>
<head>
<title>Page JSP minimale</title>
</head>
<body>
	<strong>Dans une page JSP on peut trouver :<br> </strong>
	<ul>
		<li>des <strong>directives</strong>, globales � la page.</li>
		<li>des <strong>d�clarations</strong> (ou "�l�ments de script")
			de code java, �galement globales.</li>
		<li>des <strong>scriptlets</strong>, ins�r�s dans la m�thode
			principale service() de la servlet g�n�r�e.</li>
		<li>des <strong>expressions</strong>, affich�es telles quelles</li>
		<li>des <strong>balises sp�ciales</strong>("actions"), modifiant
			le comportement � l'ex�cution.</li>
	</ul>
	<br>
	<strong>R�sultat de l'ex�cution : </strong>
	<%!
// d�clarations (�l�ments de script): port�e page
public void toto() {
String s = "Coucou";
}
int h = 25;
String s = "";
%>
	<%
// scriptlets : bien distinguer des d�clarations, 
// port�e m�thode ppale
for (int i = 1; i <= 10; i++) {
     s = s + i; h++;
%>
	<%-- Je suis un commentaire cach� --%>
	<!-- expression : affiche la valeur de i -->
	<%= i%>
	<!-- scriptlet : la fin de la boucle ci-dessus -->
	<% } %>
	<!-- Affiche la valeur de la variable s -->
	<%= s %>
	<br>
</body>
</html>

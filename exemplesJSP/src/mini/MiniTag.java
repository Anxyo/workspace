package mini;

import java.io.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;

@SuppressWarnings("serial")
public class MiniTag extends TagSupport {
	
  // appel�e lors de la rencontre du d�but de la balise custom

  public int doStartTag() throws JspTagException {
    try {
      pageContext.getOut().write("<i><strong>Emis par doStartTag().</strong></i><br>");
    }
    catch (IOException e) {
      throw new JspTagException("Exception lev�e dans doStartTag()");
    }
    return TagSupport.EVAL_BODY_INCLUDE;
  }

  // appel�e lors de la rencontre de la fin de la balise custom
  public int doEndTag() throws JspTagException {
    try {
        pageContext.getOut().write("<i><strong>Emis par doEndTag().</strong></i><br>");
    }
    catch (IOException e) {
      throw new JspTagException("Exception lev�e dans doEndTag()");
    }
    return TagSupport.EVAL_PAGE; // il faut �valuer le reste de la page
    // sinon, devrait retourner SKIP_PAGE
  }
}


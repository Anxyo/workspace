package attributs;

import java.io.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;


@SuppressWarnings("serial")
public class AttrTag extends TagSupport {
  // les datas des propri�t�s
  String strAdresse = null;
  String strMajuscules = null;

  // --------------------------------------------------------
  // Propri�t�s
  public void setAdresse(String strAdresse) {
    this.strAdresse = strAdresse;
  }
  public String getAdresse() {
    return strAdresse;
  }
  public void setMajuscules(String strMajuscules) {
    this.strMajuscules = strMajuscules;
  }
  public String getMajuscules() {
    return strMajuscules;
  }
  // --------------------------------------------------------
  // appel�e lors de la rencontre du d�but de la balise custom
  public int doStartTag() throws JspTagException {
    try {
      pageContext.getOut().write("<i><strong>ARATI</strong></i><br/>");
      if (strMajuscules.equals("oui"))
              pageContext.getOut().write("<i><strong>"+strAdresse.toUpperCase()+"</strong></i><br/>");
      else
            pageContext.getOut().write("<i><strong>"+strAdresse+"</strong></i><br/>");
    }
    catch (IOException e) {
      throw new JspTagException("Exception lev�e dans doStartTag()");
    }
    return EVAL_BODY_INCLUDE;
  }
  // plus de gestionnaire doEndTag() : inutile ici...
}


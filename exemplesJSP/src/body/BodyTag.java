package body;

import java.io.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;


// noter que la superclasse n'est plus la m�me
@SuppressWarnings("serial")
public class BodyTag extends BodyTagSupport {
  // les datas des propri�t�s
  String strAdresse = null;
  String strMajuscules = null;

  // --------------------------------------------------------
  // Propri�t�s
  public void setAdresse(String strAdresse) {
    this.strAdresse = strAdresse;
  }
  public String getAdresse() {
    return strAdresse;
  }
  // l'impl�mentation de la property 'majuscules'
  public void setMajuscules(String strMajuscules) {
    this.strMajuscules = strMajuscules;
  }
  public String getMajuscules() {
    return strMajuscules;
  }
  // --------------------------------------------------------
  // appel�e lors de la rencontre du d�but de la balise custom
  public int doStartTag() throws JspTagException {
    try {
      pageContext.getOut().write("<i><strong>ARATI</strong></i><br/>");
      if (strMajuscules.equals("oui"))
              pageContext.getOut().write("<i><strong>"+strAdresse.toUpperCase()+"</strong></i><br/>");
      else
            pageContext.getOut().write("<i><strong>"+strAdresse+"</strong></i><br/>");
    }
    catch (IOException e) {
      throw new JspTagException("Exception lev�e dans doStartTag()");
    }
    return BodyTagSupport.EVAL_BODY_BUFFERED; // noter la modification de la valeur retourn�e
  }

  // doEndTag() retrouve le corps du texte
    public int doEndTag() throws JspTagException {
    // on r�cup�re le texte ins�r� entre les balises de d�but et de fin
    // et on mesure sa longueur
    String c = getBodyContent().getString();
    int longueur = c.length();
    try {
            pageContext.getOut().write("<br><i>Le texte inter-balises a une longueur de : " + longueur+" caract�res.</i><br/>");
            pageContext.getOut().write("<i>Son contenu �tait : \""+c+"\".</i><br/>");
        }
    catch (IOException e) {
      throw new JspTagException("Exception lev�e dans doEndTag()");
    }
    return EVAL_PAGE;
  }
}


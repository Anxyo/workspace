package operation;

public class Operation {
	String mOperande1, mOperande2, mOperateur;

	public String getOperande1() {
		return mOperande1;
	}

	public void setOperande1(String operande1) {
		this.mOperande1 = operande1;
	}

	public String getOperande2() {
		return mOperande2;
	}

	public void setOperande2(String operande2) {
		this.mOperande2 = operande2;
	}

	public String getOperateur() {
		return mOperateur;
	}

	public void setOperateur(String operateur) {
		if (operateur != null)
			this.mOperateur = operateur.trim();
	}
	public String getResultat(){
		String resultat = null;
		if (	(!this.mOperateur.equals("+")) &&
				(!this.mOperateur.equals("-")) &&
				(!this.mOperateur.equals("*")) &&
				(!this.mOperateur.equals("/")))
			resultat = "Operateur non support�";
		else
		{
		try {
			double operande1 = Double.parseDouble(this.mOperande1);
			double operande2 = Double.parseDouble(this.mOperande2);
			if (mOperateur.equals("+")) {
				resultat = (operande1 + operande2) + "";
			}
			else if (mOperateur.equals("-")){
				resultat = (operande1 - operande2) + "";
			}
			else if (mOperateur.equals("*")){
				resultat = (operande1 * operande2) + "";
			}
			else if (mOperateur.equals("/")){
				if (operande2 != 0)
					resultat = (operande1 / operande2) + "";
				else
					resultat = "Pas de division par 0 !";
			}
		}
		catch (Exception exc){
			resultat = "Operandes incorrects";
		}
		}	// fin du else
		return resultat;
	}
	
	}

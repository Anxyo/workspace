package epargne;

public class beanEpargne {
	private double dblMontant = 0;
	private double dblTaux = 0;
	private int intDuree = 0;
	private double dblValeur = 0;
	private boolean monAttribut;


	public beanEpargne() {
		super();
	}
	/**
	 * Extrait la valeur de la propri�t� duree (int).
	 */
	public int getDuree() {
		return intDuree;
	}
	/**
	 * D�finit la valeur de propri�t� Duree (int).
	 */
	public void setDuree(int Duree) {
		intDuree = Duree;
	}
	/**
	 * Extrait la valeur de la propri�t� montant (double).
	 */
	public double getMontant() {
		return dblMontant;
	}
	/**
	 * Extrait la valeur de la propri�t� taux (double).
	 */
	public double getTaux() {
		return dblTaux;
	}
	public double getValeur() {
		// effectue le calcul
		double v = dblMontant;
		for (int nb = 1; nb <= intDuree; nb++)
			v *= 1 + (dblTaux / 100);
		dblValeur = v;
		return dblValeur;
	}

	
	/**
	 * D�finit la valeur de propri�t� montant (double).
	 */
	public void setMontant(double Montant) {
		dblMontant = Montant;
	}
	/**
	 * D�finit la valeur de propri�t� taux (double).
	 */
	public void setTaux(double Taux) {
		dblTaux = Taux;
	}
}


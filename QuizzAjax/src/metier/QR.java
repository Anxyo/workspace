package metier;

// Un bean est un 'POJO' ('bon vieil objet Java')
public class QR {
	String question, reponse;
	
	public QR() {} // obligatoire
	public QR(String question, String reponse) {
		super();
		this.question = question;
		this.reponse = reponse;
	}
	// propriété 'question'
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}
	// propriété 'reponse'
	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}
	
	
	// éventuellement des méthodes en plus...
	
}

package metier;

import java.util.List;
import java.util.Vector;

public class Questionnaire {
	private List<QR> qrs = null; // listes, maps et les sets
	
	public Questionnaire() {
		qrs = new Vector<QR>();
		initQuestionnaire();
	}
	private void initQuestionnaire() {
		// ici cr�ation de question
		qrs.add(new QR("Qui a cr�� le langage Java","James Gosling"));
		qrs.add(new QR("Qui a cr�� le langage C++","Bjarne Stroustrup"));
		qrs.add(new QR("Qui a cr�� le langage C","Dennis Ritchie"));
		
	}
	public List<QR> getQrs() {
		return qrs;
	}
	
	
}

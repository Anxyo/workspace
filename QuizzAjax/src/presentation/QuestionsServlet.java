package presentation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import metier.QR;
import metier.Questionnaire;

/**
 * Servlet implementation class QuestionsServlet
 */
@WebServlet("/Questions")
public class QuestionsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   private Questionnaire questionnaire;
    
    @Override
	public void init() throws ServletException {
    	questionnaire = new Questionnaire();
		super.init();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// si la requete ne contient aucun parametre (si aucun parametre index n'y
	// pr�sent) elle retourne la liste compl�te.
	String strIndex = request.getParameter("index");
	int index = 0;
	QuestionDTO dto;
	response.setContentType("application/json");
	Gson gs = new Gson();
	
	if (strIndex == null) {
		String js = gs.toJson(questionnaire.getQrs());
		PrintWriter out = response.getWriter();
		out.append(js);
		out.flush();
	} else {
		index = Integer.parseInt(strIndex);
		dto = new QuestionDTO();
		
		if ((index < 0)||(index >= questionnaire.getQrs().size())){
			dto.setDataOK(false);
			dto.setMessage("Index incorrect !");
		} else {
			dto.setDataOK(true);
			List<QR> liste = questionnaire.getQrs();
			QR laQuestionDemandee = liste.get(index);
			dto.setQuestion(laQuestionDemandee);
			//dto.setQuestion(questionnaire.getQuestions().get(index));
		}
		response.getWriter().append(gs.toJson(dto)).flush();
	}
	// Si elle contient le parametre, retourne juste la question � cette position.
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			doGet(request, response);
	}

}

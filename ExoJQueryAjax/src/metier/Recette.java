package metier;

public class Recette {
	String titre;
	int tempsPreparation; // en minutes
	int nbPersonnes;
	String difficulte;
	String [] ingredients;
	String [] etapes;
	String urlImage;

	public Recette(String titre, int tempsPreparation, int nbPersonnes, String difficulte, String urlImage) {
		this.titre = titre;
		this.tempsPreparation = tempsPreparation;
		this.nbPersonnes = nbPersonnes;
		this.difficulte = difficulte;
		this.urlImage = urlImage;
	}
	
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public int getTempsPreparation() {
		return tempsPreparation;
	}
	public void setTempsPreparation(int tempsPreparation) {
		this.tempsPreparation = tempsPreparation;
	}
	public int getNbPersonnes() {
		return nbPersonnes;
	}
	public void setNbPersonnes(int nbPersonnes) {
		this.nbPersonnes = nbPersonnes;
	}
	public String getDifficulte() {
		return difficulte;
	}
	public void setDifficulte(String difficulte) {
		this.difficulte = difficulte;
	}
	public String getUrlImage() {
		return urlImage;
	}
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	// les deux tableaux
	public String[] getIngredients() {
		return ingredients;
	}
	public void setIngredients(String[] ingredients) {
		this.ingredients = ingredients;
	}
	public String[] getEtapes() {
		return etapes;
	}
	public void setEtapes(String[] etapes) {
		this.etapes = etapes;
	}
	
}

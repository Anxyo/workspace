package presentation;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import metier.Recette;

/**
 * Servlet implementation class RecettesAjax
 */
@WebServlet("/RecettesAjax")
public class RecettesAjax extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Map<String, Recette> recettes = null;
	
	
	@Override
	public void init() throws ServletException {
		recettes = new ConcurrentHashMap<>();
		Recette recette = new Recette("clafoutis", 20, 6, "facile", "clafoutis.jpg");
		String [] ingredients = {
			    "500 g de cerises noires (type burlat)",
			    "4 oeufs",
			    "75 g de farine",
			    "100 g de sucre en poudre + 20 g pour le moule",
			    "2 cuil. � soupe de sucre glace",
			    "15 cl de cr�me liquide",
			    "35 cl de lait",
			    "1 cuil. � soupe de rhum ou de kirsch",
			    "20 g de beurre"
		};
		recette.setIngredients(ingredients);
		String [] etapes = {
				"1. Pr�chauffez le four � th 7 (210�). Cassez les �ufs dans une jatte contenant le sucre en poudre. Fouettez (au fouet � main) pour obtenir un m�lange mousseux.",
				"2. Ajoutez petit � petit la farine tamis�e toujours en fouettant. Versez ensuite la cr�me liquide et le lait en filet sans cesser de fouetter, et enfin le kirsch.",
				"3. Beurrez copieusement un plat en porcelaine � feu, et saupoudrez le fond et les bords de sucre en poudre. Rincez et essuyez les cerises, �queutez-les et �talez-les dans le moule.",
				"4. Versez la p�te dans le plat en proc�dant doucement pour ne pas �chahuter� les cerises. Glissez le plat au four et laissez cuire 30 � 35 min, jusqu�� ce que le clafoutis soit dor�. Poudrez-le de sucre glace et servez, chaud, ti�de ou froid."
		};
		recette.setEtapes(etapes);
		recettes.put(recette.getTitre(),recette);
		// autant de fois que n�cessaire... Normalement viendrait d'un SGBD...
		super.init();
	}
	
    public RecettesAjax() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RecetteDTO dto = null;
		String nomRecette = request.getParameter("recette");
		if (nomRecette != null) {
			Recette laRecette = recettes.get(nomRecette);
			if (laRecette != null) {
				dto = new RecetteDTO(laRecette,true); // des donn�es !
			} else
				dto = new RecetteDTO(null,false); // pas de donn�es
		} else
			dto = new RecetteDTO(null,false); // pas de donn�es
		
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		Gson gs = new Gson();
		String json = gs.toJson(dto);
		response.getWriter().append(json).flush();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

package presentation;

import metier.Recette;

public class RecetteDTO {
	private Recette recette;
	private boolean dataOK;
	
	public RecetteDTO(Recette recette, boolean dataOK) {
		super();
		this.recette = recette;
		this.dataOK = dataOK;
	}
	public Recette getRecette() {
		return recette;
	}
	public void setRecette(Recette recette) {
		this.recette = recette;
	}
	public boolean isDataOK() {
		return dataOK;
	}
	public void setDataOK(boolean dataOK) {
		this.dataOK = dataOK;
	}
}

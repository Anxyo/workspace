// Interne.java

// Exemple :
// - d'une classe imbriqu�e static
// - d'une classe interne (classe imbriqu�e non static)

package interne;

// la classe publique
public class Interne {

   public static void main(String args[]) {
   Contenant c1 = new Contenant();
   
   // Cr�ation depuis l'ext�rieur de la classe container
   // --------------------------------------------------
   // noter la syntaxe de cr�ation d'une instance 
   // de l'inner classe : on doit utiliser une 
   // instance d�j� cr��e de la classe container :
  Contenant.Contenu c2 =  c1.new Contenu();  
  c2.imbr1Contenu = 15;
  // Cr�ation d'une instance de la classe imbriqu�e static
  // Aucun nom d'instance n'est n�c�ssaire.
  Contenant.ContenuStatic c3 = new Contenant.ContenuStatic();
   }
}


// la classe contenante :
// ----------------------
class Contenant {
    private int imbr1Contenant;
    int imbr2Contenant;
    static int imbr3Contenant;
    
    Contenu ci = null;
    
    //  classe interne
    // ---------------
    // ("innerclass")
    
    // Comme pour tout champ, un des
    // sp�cificateurs de port�e suivant peut etre
    // utilis� pour sp�cifier son accessibilit� :
    
    // public
    // private
    // protected
    class Contenu {
	// champs d'instance
	int imbr1Contenu;
	
	// champs static : ill�gal car une classe interne 
	// est � l'int�rieur d'une instance de sa classe container.
	// static int mbrStatic;
	
	// constructeur classe interne
	Contenu(){
	    // acc�s libre au champ priv� du container
	    imbr1Contenu = imbr1Contenant;
	}
	
	// une m�thode
	
    }	// fin classe Contenu
	// ------------------
   
    // Classe imbriqu�e statique
    // -------------------------
    // N'est associ�e
    // � aucune instance de la classe Contenant.
     static 
    // public
    // private
    // protected
    class ContenuStatic {
    
	// constructeur
	ContenuStatic(){
	   // ill�gal car imbr1Contenant n'est pas static
	   // imbr1Contenant = 20;
	   // N�cessite la cr�ation d'une instance
	   Contenant instance = new Contenant();
	   // ill�gal car imbr1 est private
	    //instance.imbr1Contenant = 20;
	   // l�gal
	   instance.imbr2Contenant = 20;
	   // OK �galement : pas d'instance n�cessaire
	   // car imbr3 est �galement static
	   imbr3Contenant = 20;
	} // fin du constructeur
    
    }   // fin classe ContenuStatic
	// ------------------------

    // constructeur
    public Contenant(){
	// construction depuis l'int�rieur de la classe
	// On peut utiliser la syntaxe simplifi�e suivante :
	ci = new Contenu();
	// qu'il faut comprendre comme :
	// (l�gal mais lourd)
	ci = this.new Contenu();
    }  // fin du constructeur
}	// fin classe Contenant
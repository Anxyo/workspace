// FichierCopie.java
// Petit programme de copie de fichier depuis la ligne de commande

package fichiercopie;
import java.io.*;

public class FichierCopie {

  // Routine main(). Appelle copie().
  public static void main(String[] args) {
    if (args.length != 2)    // combien d'arguments ?
      System.out.println("Utilisation : java FichierCopie <source> <destination>");
    else {
      // appel de Copie() 
      try { 
	Copie(args[0], args[1]); 
	}
      catch (IOException e) { 
	System.out.println(e.getMessage()); 
	}
    }
  }

  // routine de copie
  // ----------------
  public static void Copie(String nom_source, String nom_cible) throws IOException{
    File source = new File(nom_source);  // d�finit des objets File � partir des chaines
    File dest = new File(nom_cible);
    
    // Le fichier source existe ?
    if (!source.exists())
      Lanceioexception("Je ne trouve pas le fichier : " + nom_source);
    // Copie du fichier bloc par bloc
    FileInputStream srcStream = null;		    // Stream de lecture du fichier source
    FileOutputStream dstStream = null;		    // Stream d'�criture vers fichier de destination
    try {
      srcStream = new FileInputStream(source);      // Cr�ation flux d'entr�e
      dstStream = new FileOutputStream(dest);       // Cr�ation flux de sortie
      byte[] buffer = new byte[4096];		    // tableau d'octets (e/s par blocs de 4 ko)
      int nb_octets;				    // combien d'octets dans le buffer
      
      // Lit un bloc de donn�es dans le buffer puis les �crit vers la copie
      // boucle tant que non en fin de fichier (quand read() retourne -1).
     while((nb_octets = srcStream.read(buffer)) != -1) // tant que non EOF
        dstStream.write(buffer, 0, nb_octets);         //   �criture du bloc 
    }
    // ferme les flux dans le finally
    finally {
      if (srcStream != null) 
	try { srcStream.close(); } catch (IOException e) { ; }
      if (dstStream != null) 
	try { dstStream.close(); } catch (IOException e) { ; }
    }
  }

  
  private static void Lanceioexception(String msg) throws IOException { 
    throw new IOException(msg); 
  }
}

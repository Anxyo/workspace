package utilisationdethis;

public class UtilisationDeThis {
    static Date premier = null;
    public static void main(String args[]) {
	Date d1 = new Date(1,1,1);
	Date d2 = new Date(2,2,2);
	Date d3 = new Date(3,3,3);
	Date d4 = new Date(4,4,4);
	premier = d1;
	d2.ajouter(d1);
	d3.ajouter(d2);
	d4.ajouter(d3);
	parcourt();
    }
    // parcourt la liste
    public static void parcourt(){
    Date d = premier;
    while (d != null) {
	System.out.println(d.toString());
	d = d.suivante;
    }
    }
    
}



class Date {
    // variables d'instances
    private int jour, mois, ann�e;
    Date pr�c�dante = null, 
	suivante = null;
    
    // variable de classe
    static int format = 1;
    
    // constructeur sans argument
    public Date() {
	// appelle le constructeur � 3 arguments
	// le 8 d�cembre 1997 est la date par d�faut...
	this(8,12,97);
	// Attention : ceci serait incorrect dans le cas 
	// d'un constructeur, mais correct pour l'appel
	// d'une m�thode. 
	// this.Date(8,12,97);
    }
    
    // constructeur � 3 arguments
    public Date(int j, int m, int a) { 
	jour = j; mois = m; ann�e = a;
    }
    
    public void affecter(int j, int m, int a) { 
	jour = j; mois = m; ann�e = a;
    }

    // on controle le format d'affichage par format    
    public String toString(){
	String s;
	if (format == 1)
	    s = new String("Jour = "+jour+" mois = "+mois+" ann�e = "+ann�e);
	else
	    s = new String("Month = "+mois+" day = "+jour+" year = "+ann�e);    
	return s;
    }    
    
    public void ajouter(Date avant) {
	// se place apr�s, donc...
	pr�c�dante = avant;
	avant.suivante = this;
    }
}


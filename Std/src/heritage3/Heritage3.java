// Heritage3.java
// Red�finition de m�thodes

package heritage3;

public class Heritage3 {
 public static void main(String args[]) {
    Date d1 = new Date(18,8,95);
    Date d2 = new DateAnglaise(5,21,72);
    DateAnglaise Lendemain = new DateAnglaise(5,22,72);
    
    d1.imprimer();	    // affiche 18/8/95
    
    d2.imprimer();	    // affiche 5/21/72
    ((Date)d2).imprimer();  // affiche encore 5/21/72
//  d2.imprimerfr();	    // Ill�gal : typage statique 
			    // pas de m�thode imprimerfr() dans Date !
    Lendemain.imprimer();   // affiche 5/22/72
    Lendemain.imprimerfr(); // affiche 22/5/72
 }
 // Donc : 
 // - depuis l'ext�rieur de la classe le typage dynamique est TOUJOURS utilis�
 // - depuis l'int�rieur d'une classe on peut distinguer entre la red�finition et 
 //   l'impl�mentation de la super classe en utilisant le mot cl� super.
    
}


class DateAnglaise extends Date {
    // constructeur � 3 arg.
    public DateAnglaise(int m, int j, int a) {
        super(j, m, a) ;        // appel constructeur de la classe de base
    }
    
    // impression "� l'anglaise"   
    public void imprimer() {
        System.out.println(mois + "/" + 
		jour + "/" + ann�e);
    }
    // impression "� la francaise"   
    public void imprimerfr() {
	super.imprimer();   // retrouve l'impl�mentationde la superclasse
    }
}


class Date {
    protected int jour, mois, ann�e;

    public Date(){  // date par d�faut 
	jour = 8;
	mois = 11;
	ann�e = 97;
    }
    
    public Date(int j, int m, int a){ 
	jour = j;
	mois = m;
	ann�e = a;
    }
    // impression "� la francaise"   
    public void imprimer() {
        System.out.println(jour + "/" + mois + "/" + ann�e);
    }
}

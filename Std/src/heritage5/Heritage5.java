// Heritage5.java
// classes abstraites

package heritage5;

public class Heritage5 {
 public static void main(String args[]) {
//  La ligne qui suit est ill�gale : on ne
//  peut pas cr�er d'objet d'une classe abstraite
//  Date d1 = new Date(18,8,95);

    Date d2 = new DateAnglaise(5,21,72);
    Date d3 = new DateFran�aise(21,5,72);

//  Impossible, pour la meme raison...    
//  Date d4 = new DatePapou�te(5,21,72);
    }   
}

// -----------------------------------------------------------------------

class DateAnglaise extends Date {
    // constructeur � 3 arg.
    public DateAnglaise(int m, int j, int a) {
        super(j, m, a) ;        // appel constructeur de la classe de base
    }
    
    // impression "� l'anglaise"   
    public void imprimer() {
        System.out.println(mois + "/" + 
		jour + "/" + ann�e);
    }
}

// -----------------------------------------------------------------------

class DateFran�aise extends Date {
    // constructeur � 3 arg.
    public DateFran�aise(int j, int m, int a) {
        super(j, m, a) ;        // appel constructeur de la classe de base
    }
    
    // impression "� la francaise"   
    public void imprimer() {
        System.out.println(jour + "/" + mois + "/" + ann�e);
    }
}

// -----------------------------------------------------------------------

abstract class DatePapou�te extends Date {
    // constructeur � 3 arg.
    public DatePapou�te(int j, int m, int a) {
        super(j, m, a) ;        // appel constructeur de la classe de base
    }
    
    // On n'a pas impl�ment�e de m�thode imprimer()
    // ---> cette classe est �galement abstract !
    // On DOIT donc faire figurer le mot abstract
    // dans sa d�claration !
}

// -----------------------------------------------------------------------
abstract class Date {
    protected int jour, mois, ann�e;

    public Date(){  // date par d�faut 
	jour = 8;
	mois = 11;
	ann�e = 97;
    }
    
    public Date(int j, int m, int a){ 
	jour = j;
	mois = m;
	ann�e = a;
    }
    
    // m�thode abstraite
    public abstract void imprimer();

}

// Interface2.java
// H�ritage multiple d'interfaces.

package interface2;

public class Interface2 {

    public static void main(String args[]) {
    Date d1 = new DateFrancaise();
    d1.imprimer();
    }
}

// --------------------------------------------------------------------------
interface Date {
    static final int defJour = 8; // on peut d�fir des constantes.
    int defMois = 10;		  // ici static final est IMPLICITE
    int defAnn�e = 72;		  // ici static final est IMPLICITE �galement
    
    void imprimer();		  // ici abstract et public sont IMPLICITES
}

// --------------------------------------------------------------------------
interface Description {
    String defEvenement = "Pas d'initialisation effectu�e";
    // une paire d'accesseurs pour lire / �crire un �v�nement
    void    setEvnt(String UnEvnt);
    String  getEvnt();
}
// --------------------------------------------------------------------------
interface DateStd extends Date, Description {	// H�ritage multiple...
						// Uniquement pour les interfaces !
    
}

// --------------------------------------------------------------------------
class DateFrancaise implements DateStd {
    private int jour, mois, ann�e;
    
    public DateFrancaise() {
	jour = defJour;
	mois = defMois;
	ann�e = defAnn�e;
    }

    public DateFrancaise(int m, int j, int a) {
	jour = j;
	mois = m;
	ann�e = a;
    }
    
    public void imprimer(){	// doit etre public
	System.out.println(jour + "/" + mois + "/" + ann�e);    
    }
	// mettre en commentaire une des lignes suivantes. Que se passe t'il ?
    public String  getEvnt() {return null;}
    public void  setEvnt(String s) {}
    
}


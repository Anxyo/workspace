// fichier : Nan.java
package nan;

public class Nan {

    // deux donn�es membres priv�es
    private float i,j;	
    
    // -----------------------
    // constructeur par d�faut
    // -----------------------
    public Nan ()  {i = 0; j = 0;} 

    // --------------------------------------------------
    // Cette m�thode met en �vidence NaN ("Not a Number")
    // --------------------------------------------------
    public void divise(){
    float k = i / j;	 // op�ration ind�finie si i == 0 ET si j == 0
	 // De ce fait le r�sultat vaut NaN...
    if (k != java.lang.Float.NaN)
			System.out.println("k semble etre diff�rent de NaN !");	
    if (java.lang.Float.isNaN(k))
			System.out.println("Pourtant, k vaut NaN !");
    }

    // ------------------------------------------------------------------
    // routine de comparaison avec NEGATIVE_INFINITY et POSITIVE_INFINITY
    // ------------------------------------------------------------------
    public void compare()	{
    // tentative de division par +0
    i = 1.0f / +0f;
    j = 1.0f / -0f;   // ici, ne pas oublier le f du -0

    // d'abord pour i...
    if (i == java.lang.Float.POSITIVE_INFINITY)
      	System.out.println("i == infini positif");
    else if (i == java.lang.Float.NEGATIVE_INFINITY)
      	System.out.println("i == infini n�gatif");	
    // ...puis pour j
    if (j == java.lang.Float.POSITIVE_INFINITY)
      	System.out.println("j == infini positif");
    else if (j == java.lang.Float.NEGATIVE_INFINITY)
      	System.out.println("j == infini n�gatif");	
    // mais les 2 z�ros sont bien une seule et meme valeur...
    if (+0f == -0f)
			System.out.println("Pourtant +0f == -0f !");
    }

    // -----------------------
    // main() : point d'entr�e
    // -----------------------

    public static void main(String args[]) {
    // cr�ation d'une nouvelle instance
    Nan refnan = new Nan(); // appel constructeur par d�faut
    // on commence par tenter de faire une op�ration ind�finie
    refnan.divise();
    // appel de la routine de comparaison
    refnan.compare();  
   }

}


// Heritage1.java
// Syntaxe de base de l'h�ritage
// S�quence d'appel des constructeurs
package heritage1;

public class Heritage1 {


    public static void main(String args[]) {
    DateEvenement UneDate = new DateEvenement(2,12,32,"Naissance de Papy");
    DateEvenement AutreDate = new DateEvenement("Naissance de Poupette");
    System.out.println(	"A la date : " + UneDate.toString() + 
			" a eu lieu : " + UneDate.quelEvenement());
    }
}


class DateEvenement extends Date {
    private String evenement = null ;

    // constructeur � un seul arg.
    public DateEvenement(String e) {
    // implicitement appel du constructeur sans arg. de Date
    // Il faut qu'il y en ait un, sous peine d'erreur de compilation.
    // super();
    evenement = e ; 
    System.out.println("Constructeur classe DateEvenement 1 arg.");
    }
    // constructeur � 4 arg.
    public DateEvenement(int j, int m, int a, String e) {
        super(j, m, a) ;        // appel constructeur de la classe de base
        evenement = e ;         // Init. d'un champs de la classe d�riv�e
        System.out.println("Constructeur classe DateEvenement 4 arg.");
    }
    // retourne 
    public String quelEvenement() { 
	return evenement ; 
    }
    
    /* Tous les champs / m�thodes de Date sont �galement pr�sents
       grace � l'h�ritage.
       On a donc, en fait aussi tout ce qui suit :
       
        private int jour, mois, ann�e;

	public Date(){  // date par d�faut 
	    jour = 8;
	    mois = 11;
	    ann�e = 97;
	}
    
	public Date(int j, int m, int a){ 
	    jour = j;
	    mois = m;
	    ann�e = a;
	}
    
	public String toString() {
	    return "Date : jour = " + jour + " mois = " + mois + " ann�e = " + ann�e;
	}
        */
}


class Date {
    private int jour, mois, ann�e;

    public Date(){  // date par d�faut 
	jour = 8;
	mois = 11;
	ann�e = 97;
	System.out.println("Constructeur classe Date sans arg.");
    }
    
    public Date(int j, int m, int a){ 
	jour = j;
	mois = m;
	ann�e = a;
	System.out.println("Constructeur classe Date 3 arg.");
    }
    
    public String toString() {
	return "jour = " + jour + " mois = " + mois + " ann�e = " + ann�e;
    }

}

package reflexion;

import java.lang.reflect.*;

import static java.lang.System.out;

public class AfficheMethodes {

	@SuppressWarnings("unchecked")
	public static void main(String args[]) {
		// si la ligne de commande est vide
		if (args.length < 1) {
			args = new String[]{"java.lang.String"};	// par d�faut
		}
		
		try {
			StringBuffer sb = new StringBuffer();
			// retrouve l'information de classe...
			Class c = Class.forName(args[0]);
			// les modificateurs
			// (y compris celles de la ou des classe(s) de base)
			Method[] methodes = c.getMethods();
			// ... et des constructeurs
			Constructor[] ctr = c.getConstructors();
			// Affichage
			out.println("Methodes : ");
			for (Method m : methodes) {
				out.println("M�thode : " + m.toString());
				Class [] parametres = m.getParameterTypes();
				if (parametres.length > 0) {
					if (sb.length() > 0)
						sb.delete(0, sb.length() - 1);
					for (Class p : parametres)
						sb.append(p.getSimpleName() + ", ");
					out.println("   Param�tres : " + sb.toString());
				}
			}
			out.println("Constructeurs : ");
			for (int i = 0; i < ctr.length; i++)
				out.println(ctr[i].toString());

		} catch (ClassNotFoundException e) {
			out.println("Je n'ai pas trouvee de classe de ce nom...");
		}
	}
}
package reflexion;

public abstract class A {
	int b;
	static int a = 12;
	
	void toto(){}		// toto_void
	void toto(int a){}	// toto_int

	
	public A(int t){
		int i;
		toto(12);
		
	}
	
	public A(int u, int v){
		this(u*v);
	}
	
	

}

package reflexion;

import static java.lang.System.out;
import java.lang.reflect.*;

@SuppressWarnings("unchecked")
public class AfficheChamps {
	public static void main(String args[]) {
		String s = "La chaine de test";
		// retrouve l'information de classe � partir d'une instance
		Class c = s.getClass();
		// affiche le type
		out.println("Nom de classe obtenue par r�flexion : " + c.toString());
		// liste des champs
		Field[] champs =c.getFields();
		// Affichage
		out.println("Champs : ");
		for (Field f : champs)
			out.println(f.toString());
	}
}

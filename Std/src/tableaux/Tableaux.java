// Tableaux.java
// Cr�ation et usage de base des tableaux

package tableaux;

public class Tableaux {
    
    public static void main(String args[]) {
	// Phase 1 : DECLARATION
	int [] MonTab = {12,15,22};		// tableau d'entiers
	int [] tonTab = {12,15,22};
	MonTab[0] = 15;
	System.out.println(tonTab[0]);
	String [] TabObj;	// tableau de r�ferences d'objets de type String
        AutreClasse Tab2D[][];	// tableau � 2 dim. de r�f. d'objets AutreClasse
	
	// Phase 2 : CONSTRUCTION
	int taille = 10;
	MonTab = new int[20];				// alloc. 20 entiers
	TabObj = new String[2 * taille + 1];		// util. d'une expression
	Tab2D = new  AutreClasse[taille][taille + 1];	// alloc. des 2 dimensions
							
	// initialisation explicite			
	int  TabInts [ ]  = {	10,-15,27,1012,19};	// tableau de 5 entiers
	Date [ ] tabDate  = { 	new Date (15,9,57),	// tableau de 3 dates
				new Date (28,5,57), 
				new Date (19,3,91)};
	
	// affichage d'un �l�ment du tableau
	System.out.println(tabDate[1].toString());
							
	// initialisation avec une boucle
	double TabDbl[] = new double[100];	// d�claration + construction
	for (int i = 0; i < TabDbl.length; i++)	// boucle de parcourt
	    TabDbl[i] = (double)i * i;		// valeur calcul�e
	    
	System.out.println("Somme de tous les �l�ments : " + somme(TabDbl));    
    }
    
    
    // transmission d'un tableau comme argument
    static double somme(double [] t) {
	double s = 0;
	int i;
	for (i = 0; i < t.length; i++)
	    s += t[i];
	return s;    
    }
}

// une petite classe pour le fun
class AutreClasse {
    int UnMembre;
    int UnAutreMembre;
}

// classe Date
class Date {
    int jour, mois, an;
    // constructeur
    Date(int j, int m, int a) {jour = j; mois = m; an = a;}

    // m�thode de conversion toString()
    public String toString(){return "Date : jour = "+
				    jour +
				    " mois = " +
				    mois +
				    " an = " +
				    an;}

}

// Constructeurs.java
// exemple de d�claration et d'usage de constructeurs
package constructeurs;

public class Constructeurs {

    public static void main(String args[]) {
	// d'accord, le constructeur existe
	Date UneDate = new Date();
	// d'accord, le constructeur existe aussi...
	Date UneAutreDate = new Date(1,1,50);
	// pas d'accord : pas de constructeur � deux arguments !
//	Date EncoreUneAutreDate = new Date(1,12);	
	
	System.out.println(UneDate.toString());
	System.out.println(UneAutreDate.toString());	
    }
}



class Date {
    private int jour, mois, ann�e;

    // constructeur sans argument
    public Date() {
	// appelle le constructeur � 3 arguments
	// le 8 d�cembre 1997 est la date par d�faut...
	this(8,12,97);
	// Attention : ceci serait incorrect dans le cas 
	// d'un constructeur, mais correct pour l'appel
	// d'une m�thode. 
	// this.Date(8,12,97);
    }
    
    // constructeur � 3 arguments
    public Date(int j, int m, int a) { 
	jour = j; mois = m; ann�e = a;
    }
    
    public void affecter(int j, int m, int a) { 
	jour = j; mois = m; ann�e = a;
    }
    
    public String toString(){
	return "Jour = "+jour+" mois = "+mois+" ann�e = "+ann�e;
    }    

    protected void finalize() throws Throwable {
	super.finalize() ;  // appel du finalize de la classe de base
	// en g�n�ral n'apparait pas sur la console...
	System.out.println("Appel de finalize");	
    }

}



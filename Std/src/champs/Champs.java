// fichier : Champs.java

package champs;

public class Champs {

 
public static void main(String args[]) {
	// deux instances
	Date UneDate = new Date();
	Date UneAutreDate = new Date(1,1,50);
	// affichage en fran�ais
	System.out.println(UneDate.toString());
	System.out.println(UneAutreDate.toString());	
	// plusieurs fa�ons de changer le format
	// d'abord en utilisant le nom de la classe
	Date.format = 2;
	// on peut aussi untiliser une instance de la classe
	// la modification est "vue" par toutes les instances
	UneDate.format = 2;
	System.out.println(UneDate.toString());
	System.out.println(UneAutreDate.toString());		
    }
}

class Date {
    // variables d'instances
    private int jour, mois, ann�e;
    // variable de classe
    static int format = 1;
    
    // constructeur sans argument
    public Date() {
	// appelle le constructeur � 3 arguments
	// le 8 d�cembre 1997 est la date par d�faut...
	this(8,12,97);
	// Attention : ceci serait incorrect dans le cas 
	// d'un constructeur, mais correct pour l'appel
	// d'une m�thode. 
	// this.Date(8,12,97);
    }
    
    // constructeur � 3 arguments
    public Date(int j, int m, int a) { 
	jour = j; mois = m; ann�e = a;
    }
    
    public void affecter(int j, int m, int a) { 
	jour = j; mois = m; ann�e = a;
    }

    // on controle le format d'affichage par format    
    public String toString(){
	String s;
	if (format == 1)
	    s = new String("Jour = "+jour+" mois = "+mois+" ann�e = "+ann�e);
	else
	    s = new String("Month = "+mois+" day = "+jour+" year = "+ann�e);    
	return s;
    }    

}


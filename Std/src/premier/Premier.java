// fichier : Premier.java
// NOTER :
// 1 - pas de routines globales, main() est une méthode de classe
// 2 - cette classe DOIT avoir le meme nom que le fichier source
// 3 - enfin, elle doit etre public.
package premier;

public class Premier {
 int i = 10;
// méthode public (visible en dehors de la classe)
// ET static (indépendante de toute instance de la classe premier)

public static void main(String args[]) {
    // System est une classe de java.lang, qui contient une 
    // variable globale out (flux de sortie standard) avec 
    // une méthode println().
    System.out.println("Bonjour !");
    Premier p = new Premier();
    p.i = 15;
   }
}

// Variables.java
package variables;

public class Variables {
   final static int varclass = 10;  // constante de classe
   final int varinst = 10;	    // constante d'instance
   
 public int test()
   {
	final int varloc = 10;	    // constante locale
	
//	varloc++;   // ceci ne compile pas !
//	varinst++;  // ceci ne compile pas !
//	varclass++; // ni cela !
//arloc = varloc + 1;	// pas plus que cela...
	System.out.println(varloc);
	return varloc + 10;	
  }

 public static void main(String args[]) 
   {
   Variables obj = new Variables();
   obj.test();
   }
}


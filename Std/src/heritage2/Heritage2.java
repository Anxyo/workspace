// Heritage2.java
// Red�finition de champs : en g�n�ral, pas une bonne id�e...
package heritage2;

public class Heritage2 {

   public static void main(String args[]) {
    // le champ ann�e h�rit� de la classe Date 
    // est initialis� par le constructeur.
    DateEvenement UneDate = new DateEvenement(2,12,32,"Naissance de Papy");
    // initialisation du champ red�fini
    UneDate.ann�e = 55;
    // Acces au champ red�fini
    System.out.println(	"Champ ann�e red�fini : " + UneDate.ann�e);  
    // Acces au champ ann�e h�rit� : fonctionne car
    // un objet DateEvenement EST AUSSI un objet Date (h�ritage)
    System.out.println(	"Champ ann�e h�rit� : " + ((Date)UneDate).ann�e);      
    }
}


class DateEvenement extends Date {
    private String evenement = null ;
    public int ann�e;	    // champ public red�fini

    // constructeur � un seul arg.
    public DateEvenement(String e) {
    // implicitement appel du constructeur sans arg. de Date
    // Il faut qu'il y en ait un, sous peine d'erreur de compilation.
    // super();
    evenement = e ; 
    }
    // constructeur � 4 arg.
    public DateEvenement(int j, int m, int a, String e) {
        super(j, m, a) ;        // appel constructeur de la classe de base
        evenement = e ;         // Init. d'un champs de la classe d�riv�e
    }
    // retourne 
    public String quelEvenement() { 
	return evenement ; 
    }   
}


class Date {
    private int jour, mois;
    public int ann�e;	// public pour les besoins de la cause

    public Date(){  // date par d�faut 
	jour = 8;
	mois = 11;
	ann�e = 97;
    }
    
    public Date(int j, int m, int a){ 
	jour = j;
	mois = m;
	ann�e = a;
    }
    
    public String toString() {
	return "jour = " + jour + " mois = " + mois + " ann�e = " + ann�e;
    }
}


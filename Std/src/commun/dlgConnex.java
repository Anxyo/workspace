package commun;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;

public class dlgConnex extends JDialog {

	// champs
	// Button btnOk, btnConnecte;
	JButton btnOK = new JButton("OK");
	JButton btnConnexion = new JButton("Connexion");
	JButton btnDeconnexion = new JButton("D�connexion");
	// les �tiquettes
	JLabel lblDriver = new JLabel("Driver          : ");
	JLabel lblUrl = new JLabel("URL Jdbc        : ");
	JLabel lblNom = new JLabel("Nom utilisateur : ");
	JLabel lblMotPasse = new JLabel("Mot de passe    : ");
	JLabel lblEtat = new JLabel("Etat            : ");
	// les zones de texte

	// configuration pour utiliser le "pont" JdbcOdbc
	JTextField 	txtDriver = new JTextField("sun.jdbc.odbc.JdbcOdbcDriver");
	JTextField 	txtUrl = 	new JTextField("jdbc:odbc:AccessJdbc");

	JTextField txtNom = new JTextField("sa");
	JPasswordField txtMotPasse = new JPasswordField();
	JTextField txtEtat = new JTextField();

	JPanel p1a = new JPanel(), p1b = new JPanel(), p2 = new JPanel();
	// r�f�rence vers la classe principale
	Connectable appli = null;

	// ---------------------------------------------------------------
	public dlgConnex(Frame frame, String title, boolean modal) {
		super(frame, title, modal);
		try {
			InitInterface();
			pack();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// accesseurs pour renseigner les zones de texte
	



	// ---------------------------------------------------------------
	public void setAppli(Connectable app) {
		this.appli = app;
		if (appli.getConnection() == null) {
			txtEtat.setText("Vous n'�tes pas connect�.");
			btnConnexion.setEnabled(true);
			btnDeconnexion.setEnabled(false);
		} else {
			txtEtat.setText("Vous �tes connect�.");
			btnConnexion.setEnabled(false);
			btnDeconnexion.setEnabled(true);
		}
	}

	// ---------------------------------------------------------------
	private void InitInterface() throws Exception {
		this.setSize(new Dimension(400, 300));
		// utilisation d'un BorderLayout
		BorderLayout bl = new BorderLayout();
		this.getContentPane().setLayout(bl);
		// configuration des panels
		p1a.setLayout(new GridLayout(5, 1));
		p1b.setLayout(new GridLayout(5, 1));
		FlowLayout flo2 = (FlowLayout) p2.getLayout();
		flo2.setAlignment(FlowLayout.CENTER);
		// ajout des boutons
		p2.add(btnConnexion);
		p2.add(btnDeconnexion);
		p2.add(btnOK);
		// des �tiquettes
		p1a.add(lblDriver);
		p1a.add(lblUrl);
		p1a.add(lblNom);
		p1a.add(lblMotPasse);
		p1a.add(lblEtat);
		// et des zones de texte
		p1b.add(txtDriver);
		p1b.add(txtUrl);
		p1b.add(txtNom);
		p1b.add(txtMotPasse);
		p1b.add(txtEtat);
		// ajout des trois panels au dialogue
		this.getContentPane().add("West", p1a);
		this.getContentPane().add("Center", p1b);
		this.getContentPane().add("South", p2);

		// quelques d�tails sur les boutons
		btnConnexion.setMnemonic('C');
		btnConnexion.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnConnexion_actionPerformed(e);
			}
		});

		btnOK.setMnemonic('O');
		btnOK.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnOK_actionPerformed(e);
			}
		});

		btnDeconnexion.setMnemonic('C');
		btnDeconnexion.setSelected(true);
		btnDeconnexion.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeconnexion_actionPerformed(e);
			}
		});

		this.setResizable(false);
		this.setModal(true);
		this.setTitle("Connexion � une base de donn�es");
		txtEtat.setFont(new java.awt.Font("SansSerif", 2, 12));
		txtEtat.setForeground(Color.red);
		txtEtat.setEditable(false);
		txtEtat.setSelectedTextColor(Color.black);
		txtEtat.setHorizontalAlignment(SwingConstants.CENTER);
		txtUrl.setFocusAccelerator('U');
		//txtNom.setFocusAccelerator('t');
		txtMotPasse.setFocusAccelerator('M');
	}

	// ---------------------------------------------------------------
	// fermeture de la boite de dialogue
	void btnOK_actionPerformed(ActionEvent e) {

		this.dispose();
	}

	// ---------------------------------------------------------------
	// tente de se connecter
	void btnConnexion_actionPerformed(ActionEvent e) {
		String s = txtDriver.getText();
		try { // tentative de chargement du driver
			Class.forName(s);
		} catch (Exception exc) {
			txtEtat.setText("Driver non trouv� !");
			System.out.println("Driver non trouv� : " + exc.getMessage());
			return;
		}

		// tentative de connexion
		try {
			String strUrl = txtUrl.getText();
			String strUtilisateur = txtNom.getText();
			String strPwd = new String(txtMotPasse.getPassword());
			Connection cnx = DriverManager.getConnection(strUrl, strUtilisateur, strPwd);
			appli.setConnection(cnx);
		} catch (SQLException sqle) {
			txtEtat.setText("Echec de la connexion : " + sqle.getMessage());
			System.out.println("Echec de la connexion : " + sqle.getMessage());
			return;
		}
		txtEtat.setText("Connexion effectu�e");
		btnConnexion.setEnabled(false);
		btnDeconnexion.setEnabled(true);
	}

	// ---------------------------------------------------------------
	// D�connexion
	void btnDeconnexion_actionPerformed(ActionEvent e) {
		try {
			if (appli.getConnection() != null)
				appli.getConnection().close();
		} catch (SQLException ex) {
		}
		appli.setConnection(null);
		txtEtat.setText("D�connexion effectu�e");
		btnConnexion.setEnabled(true);
		btnDeconnexion.setEnabled(false);
	}
	/**
	 * Retourne le nom du driver.
	 */
	public String getDriver() {
		return txtDriver.getText();
	}

	/**
	 * Retourne le mot de passe
	 */
	public String getMotPasse() {
		return new String(txtMotPasse.getPassword());
	}

	/**
	 * Retourne le nom d'utilisateur
	 */
	public JTextField getTxtNom() {
		return txtNom;
	}

	/**
	 * Retourne l'Url JDBC
	 */
	public String getUrl() {
		return txtUrl.getText();
	}

	/**
	 * D�finit le nom du driver.
	 */
	public void setDriver(String nomDriver) {
		this.txtDriver.setText(nomDriver);
	}

	/**
	 * D�finit le mot de passe.
	 */
	public void setMotPasse(String motPasse) {
		this.txtMotPasse.setText(motPasse);
	}

	/**
	 * D�finit le nom de l'utilisateur
	 */
	public void setNom(String nom) {
		this.txtNom.setText(nom);
	}

	/**
	 * D�finit l'Url JDBC
	 */
	public void setUrl(String url) {
		this.txtUrl.setText(url);
	}

}
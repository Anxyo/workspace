package commun;
import java.sql.Connection;
/**
 *
 * Cette interface permet de d�coupler chacune des classes application
 * de la classe encapsulant la connexion � la base (dlgConnex)
 *
 *
 */
public interface Connectable {
	Connection getConnection();
	void setConnection(Connection cnx);
}

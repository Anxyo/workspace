package serialisation;

import java.awt.*;
import javax.swing.*;
import java.io.Externalizable;
import java.io.ObjectOutput;
import java.io.ObjectInput;
import java.io.IOException;

public class Losange	extends JComponent 
			implements Externalizable {
    
    private String sDessin;		// "L"  : losange, "C" : cercle

    public Losange() {
	sDessin = new String("L");		    // losange
    }

    // un peu de bureaucratie g�om�trique
    public Dimension getMinimumSize(){
	return new Dimension(100,100);
    }
	
    public Dimension getPreferredSize() {
        return new Dimension(100, 100);
    }
    
    public String getDessin() {
        return sDessin;
    }

    public void setDessin(String sNouvVal) {
	    sDessin = sNouvVal;
	    // on en profite pour demander le redessin du bean
            repaint();
    }
   
    public synchronized void paint(Graphics g) {
   	g.setColor(new Color(255,0,0));	    // pourquoi pas en rouge ?
	Dimension d = getSize();
        Rectangle r = new Rectangle(0,0,d.width,d.height);
	if (sDessin.equals("C")) {
	    g.drawOval(r.x,r.y,r.width ,r.height );
	}
	else {
		int [] xPoints = {r.x + r.width / 2,
				  r.x ,
				  r.x + r.width / 2,
				  r.x  + r.width};
			  
		int [] yPoints = {  r.y ,
				    r.y + r.height / 2,
				    r.y  + r.height,
				    r.y + r.height / 2};
			    
		g.drawPolygon(xPoints, yPoints, 4);
	}	
    }
 

    // les deux routines de l'interface Externalizable 
    // -----------------------------------------------
    public void writeExternal(ObjectOutput out) 
		throws IOException {
    	// s�rialisation de la chaine sDessin
        out.writeUTF(sDessin);
	System.err.println("writeExternal");
        }	// fin de writeExternal()

    public void readExternal(ObjectInput in) 
		throws IOException, ClassNotFoundException {
	    // retrouve le type de trac�
	    sDessin = in.readUTF();
     	    System.err.println("readExternal");	    
    }	// fin de readExternal()
    
    
    public static void main(String [] args) {
    	JFrame frm = new JFrame("Utilisation de ce fichu losange...");
    	frm.add(new Losange());
    	frm.setSize(400, 300);
    	frm.setVisible(true);
    	
    }
}

package serialisation;

import java.io.*;
import java.awt.Point;

import static java.lang.System.out;
import static java.lang.System.err;

public class ObjIO {
	// le point en question
	Point UnPoint;

	public ObjIO() {
		UnPoint = new Point(15, 16);
	}

	public static void main(String args[]) {
		ObjIO UneInstance = new ObjIO();
		if (UneInstance.EcritPoint("test.ser")) { // par exemple...
			// si l'�criture s'est bien pass�e
			UneInstance.UnPoint = new Point();
			out.println("Valeur courante de UnPoint = " + UneInstance.UnPoint);
			UneInstance.UnPoint = UneInstance.LitPoint("test.ser");
			if (UneInstance.UnPoint != null)
				out.println("Apr�s appel de LitPoint(), UnPoint = "
						+ UneInstance.UnPoint);
			out.println("Si les valeurs sont 15,16 c'est que la s�rialisation s'est bien pass�e...");
		}
	}

	protected Point LitPoint(String NomFichier) {
		Point ALire = null;
		FileInputStream fichier;
		ObjectInputStream obj;
		try {
			fichier = new FileInputStream(NomFichier);
			obj = new ObjectInputStream(fichier);
			ALire = (Point) obj.readObject(); // lecture effective
			obj.close();
		} catch (IOException pbIO) {
			err.println("LitPoint(), erreur d'E/S : " + pbIO);
		} catch (ClassNotFoundException pbRTTI) {
			err.println("Type de donn�es inconnu : " + pbRTTI);
		}
		return ALire; // peut etre null
	}

	protected boolean EcritPoint(String NomFichier) {
		FileOutputStream fichier;
		ObjectOutputStream obj;
		boolean R�sultat = false;
		try {
			fichier = new FileOutputStream(NomFichier);
			obj = new ObjectOutputStream(fichier);
			obj.writeObject(UnPoint); // �criture effective
			obj.close();
			R�sultat = true;
		} catch (IOException pbIO) {
			err.println("EcritPoint(), erreur d'E/S : " + pbIO);
		}

		return R�sultat;
	}
}

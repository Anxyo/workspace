package serialisation;

import java.io.*;


public class UneClasse implements Serializable {
    // quelques champs de statuts diff�rents du point de vue de la s�rialisation
    protected int UneDonnee1;				// s�rialis�e
    protected long UneDonnee2;				// s�rialis�e
    static final long serialVersionUID = -1083332832613236806L;    

    transient protected long UneDonnee3;			// non s�rialis�e : d�clar�e transient
    public static final int UneDonneeDeClasse = 10;	// non s�rialis�e : donn�e de classe

    // constructeur 
    public UneClasse(){
    }	// fin du constructeur

    // pour changer la signature de serialver
    protected void Inutile() {
	System.out.println("Affichage inutile depuis Inutile()");
    }	// fin de Inutile()     
    
    
    // la tr�s classique toString()
    public String toString(){
    return  "UneDonnee1 = "	+ UneDonnee1 + 
	    ", UneDonnee2 = "	+ UneDonnee2 +
	    ", UneDonnee3 = "	+ UneDonnee3;
    }	// fin de toString()
    
    
}
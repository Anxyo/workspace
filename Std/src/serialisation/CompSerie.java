package serialisation;

import java.io.*;

//Différence entre compatibilité binaire et compatibilité de sérialisation

//1 - On lance  CompSerie Ecrire, puis CompSerie Lire. On note que les deux 
//  opérations se passent bien.

//2 - On appelle serialver en gardant la méthode Inutile() en faisant :
//	serialver CompSerie.UneClasse

//Il affiche :
//CompSerie.UneClasse:    static final long serialVersionUID = -1083332832613236806L;

//3 -  On supprime la méthode Inutile() de UneClasse et on lance CompSerie Lire.
//	On a une erreur : il n'y a plus de compatibilité de sérialisation ("Serialization
//	compatibility").
//	D'autre part la classe n'est plus, non plus, compatible "binairement" 
//	(il manque une méthode).

//4 - On insère la déclaration :
//		static final long serialVersionUID = -1083332832613236806L;
//  Enfin on lance à nouveau CompSerie Lire. On note que tout se passe bien :  la 
//  compatibilité de sérialisation est revenue. Par contre il n'y a toujours 
//  pas de compatibilité binaire...

//En résumé : - il ne faut pas confondre "compatibilité du point de vue de la sérialisation"
//		("Serialization compatibility") et "compatibilité du point de vue binaire"
//		("Binary compatibility").
//	       - on peut forcer la compatibilité de sérialisation par un SVUID : le fait
//		 de le définir explicitement fait que le compilateur ne recalcule pas de
//		 signature d'authentification lors de la reconstruction du programme et
//            garantie ainsi la compatibilité de sérialisation.
//          - la sérialisation ne concerne que l'état des objets et non leurs méthodes.
//		 C'est bien sur différent du point de vue binaire...

public class CompSerie implements Serializable {
	private static final long serialVersionUID = 145903864580131669L;

	public static void main(String args[]) {

		UneClasse obj1 = null;

		// lecture ou écriture ?
		if (args.length == 0) {
			System.out.println("Syntaxe : ");
			System.out.println("CompSerie Lire | Ecrire");
		} else {
			// lecture
			if (args[0].equals("Lire")) {
				obj1 = (UneClasse) LitObjet("test.ser");
				System.out.println("Lecture effectuée");
			}
			// écriture
			else if (args[0].equals("Ecrire")) {
				obj1 = new UneClasse();
				EcritObjet(obj1, "test.ser");
				System.out.println("Ecriture effectuée");
			}

		}

	}

	public static Object LitObjet(String NomFichier) {
		FileInputStream fichier;
		ObjectInputStream obj;
		Object ALire = null; // pour le signifier explicitement

		try {
			fichier = new FileInputStream(NomFichier);
			obj = new ObjectInputStream(fichier);
			ALire = obj.readObject();
			obj.close();
		} catch (IOException pbIO) {
			System.err.println("LitObjet(): pb d'E/S");
		} catch (ClassNotFoundException pbFormat) {
			System.err.println("LitObjet(): pb de type");
		}
		return ALire;
	} // fin de LitObjet()

	public static void EcritObjet(Object AEcrire, String NomFichier) {
		FileOutputStream fichier;
		ObjectOutputStream obj;
		try {
			fichier = new FileOutputStream(NomFichier);
			obj = new ObjectOutputStream(fichier);
			obj.writeObject(AEcrire);
			obj.close();
		} catch (IOException pbIO) {
			System.err.println("EcritObjet(): pb d'E/S");
		}
	}

}

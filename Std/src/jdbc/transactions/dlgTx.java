package jdbc.transactions;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.sql.*;
import java.awt.event.*;
import commun.*;

/**
 * Cette classe montre un usage minimal des m�thodes setAutocommit(), commit() 
 * et rollback(). 
 * 
 * La gestion des transactions depuis du code Java est certes possible, mais
 * on peut, peut-�tre lui pr�f�rer l'insertion des ordres SQL �quivalents dans 
 * des proc�dures stock�es.
 */
public class dlgTx extends JDialog {
	// champs d'interface utilisateur
	JPanel pHaut = new JPanel();
	JPanel pCentre = new JPanel();
	JPanel pBas = new JPanel();
	JButton btnSelect = new JButton("Affiche");
	JButton btnUpdate = new JButton("Update");
	JButton btnCommit = new JButton("Commit");
	JButton btnRollback = new JButton("Rollback");

	JTextField txtEtat = new JTextField(22);
	JButton btnOK = new JButton("Fermer");

	JScrollPane jScrollPane1 = new JScrollPane();
	JTextArea txtResult = new JTextArea(10, 29);

	// champs jdbc
	Statement st;
	ResultSet rs;

	// r�ference vers la classe principale
	private Connectable appli = null;

	// le drapeau pour la mise a jour
	boolean bMajuscules = false; // commence en minuscules

	// constructeur �
	public dlgTx(Frame frame, String titre, boolean modal) {
		super(frame, titre, modal);
		try {
			InitInterface(titre, modal);
			pack();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	public void setAppli(Connectable appli) {
		if (appli != null) {
			this.appli = appli;
			try {
				st = appli.getConnection().createStatement();
			} catch (SQLException sqle) {
				sqle.printStackTrace();
				}
		} // fin du if
		else {
				txtEtat.setText("Vous n'�tes pas connect� !");
		}
	}
	
	void InitInterface(String titre, boolean modal) throws Exception {
		this.setSize(new Dimension(600, 400));
		// on utilise un BorderLayout manager
		this.getContentPane().setLayout(new BorderLayout());
		// configuration des composants
		// ----------------------------
		btnOK.setMnemonic('O');
		btnOK.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnOK_actionPerformed(e);
			}
		});

		btnSelect.setMnemonic('S');
		btnSelect.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSelect_actionPerformed(e);
			}
		});
		btnUpdate.setMnemonic('U');
		btnUpdate.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdate_actionPerformed(e);
			}
		});
		btnCommit.setMnemonic('C');
		btnCommit.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnCommit_actionPerformed(e);
			}
		});
		btnRollback.setMnemonic('R');
		btnRollback.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRollback_actionPerformed(e);
			}
		});

		// zone d'affichage de l'�tat courant
		txtEtat.setFont(new java.awt.Font("Dialog", 2, 12));
		txtEtat.setForeground(Color.red);
		txtEtat.setEditable(false);
		txtEtat.setHorizontalAlignment(SwingConstants.CENTER);
		// ajout de la zone de texte au scrollpane
		jScrollPane1.setAutoscrolls(true);
		jScrollPane1.getViewport().add(txtResult, null);
		// insertion dans le dialogue en utilisant les panels
		FlowLayout flo1;
		flo1 = (FlowLayout) pHaut.getLayout(); // retrouve le layoutmanager
		flo1.setAlignment(FlowLayout.RIGHT); // centrage des composants
		// ajout des boutons

		flo1 = (FlowLayout) pCentre.getLayout();
		flo1.setAlignment(FlowLayout.RIGHT);

		flo1 = (FlowLayout) pBas.getLayout();
		flo1.setAlignment(FlowLayout.RIGHT);
		// ajout des composants
		pHaut.add(btnSelect);
		pHaut.add(btnUpdate);
		pHaut.add(btnCommit);
		pHaut.add(btnRollback);

		pCentre.add(jScrollPane1);
		pBas.add(txtEtat);
		pBas.add(btnOK);

		// ajout au dialogue
		this.getContentPane().add("North", pHaut);
		this.getContentPane().add("Center", pCentre);
		this.getContentPane().add("South", pBas);

		// configuration du dialogue
		this.setResizable(false);
		this.setModal(modal);
		this.setTitle(titre);
	}

	void btnOK_actionPerformed(ActionEvent e) {
		this.dispose();
	}

	void btnSelect_actionPerformed(ActionEvent e) {
		String r = "SELECT * from authors";
		// si on est connect�
		if (appli.getConnection() != null) {
			try {
				txtEtat.setText("");
				rs = st.executeQuery(r);
			} catch (SQLException sqle) { // erreur de syntaxe
				txtEtat.setText("Erreur de syntaxe SQL...");
				return;
			}
			// ici, on a un ResultSet normalement constitu�
			AfficheResultSet(rs, txtResult);
		} // fin du if

		else {
			// code diagnostic
			txtEtat.setText("Vous n'�tes pas connect� !");
		} // fin else
	}

	void btnUpdate_actionPerformed(ActionEvent e) {
		String r =
			(bMajuscules)
				? "UPDATE authors SET au_lname = UPPER(au_lname)"
				: "UPDATE authors SET au_lname = LOWER(au_lname)";

		// si on est connect�
		if (appli.getConnection() != null) {
			try {
				txtEtat.setText("");
				appli.getConnection().setAutoCommit(false);
				st.executeUpdate(r);
				// pour voir
				btnSelect_actionPerformed(e);
				// pour la prochaine fois
				bMajuscules = !bMajuscules;
			} catch (SQLException sqle) { // erreur de syntaxe
				txtEtat.setText("Erreur de syntaxe SQL...");
				return;
			}
		} else {
			// code diagnostic
			txtEtat.setText("Vous n'�tes pas connect� !");
		} // fin else			
	}

	void btnCommit_actionPerformed(ActionEvent e) {
		// si on est connect�
		if (appli.getConnection()!= null) {
			try {
				txtEtat.setText("");
				appli.getConnection().commit();
				// pour voir
				btnSelect_actionPerformed(e);

			} catch (SQLException sqle) {
				sqle.printStackTrace();
				return;
			}
		} else {
			// code diagnostic
			txtEtat.setText("Vous n'�tes pas connect� !");
		} // fin else			
	}

	void btnRollback_actionPerformed(ActionEvent e) {
		// si on est connect�
		if (appli.getConnection()!= null) {
			try {
				txtEtat.setText("");
				appli.getConnection().rollback();
				// pour voir
				btnSelect_actionPerformed(e);
			} catch (SQLException sqle) {
				sqle.printStackTrace();
				return;
			}
		} else {
			// code diagnostic
			txtEtat.setText("Vous n'�tes pas connect� !");
		} // fin else			
	}

	// routine d'affichage du resultset
	void AfficheResultSet(ResultSet r, JTextArea t) {
		String UneLigne = "";
		try {
			// combien de colonnes dans le resultset ?
			ResultSetMetaData meta = r.getMetaData();
			int nbcol = meta.getColumnCount();
			t.setText("");
			// boucle des lignes
			while (r.next()) {
				// boucle des colonnes
				for (int i = 1; i <= nbcol; i++) {
					UneLigne = UneLigne + r.getString(i) + "\t";
				} // fin du for

				// ajout d'un saut de ligne
				UneLigne = UneLigne + "\n";

				// si t est null, affiche sur la console
				if (t == null) {
					System.out.println(UneLigne);
				} else { // affichage dans la zone de texte
					t.append(UneLigne);
				}
				UneLigne = ""; // on recommence...
			} // fin du while
		} // fin du try
		catch (SQLException sqle) {
			// code de trace
		}
	} // fin de la routine

} // fin de la classe

package jdbc.exceptions.sql;
import java.sql.*;



public class Exceptions {

	public static void main(String[] args) {
		Connection cnx;
		Statement st;
		String rqt = "INSERT into test (ATronquer) VALUES (ABCDEFGHIJKLMNOPRSTUV)";
		
		// chargement du driver
		try { // tentative de chargement du driver
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception exc) {
			System.out.println("Driver non trouv� : " + exc.getMessage());
			return;
		}

		// tentative de connexion
		try {
			//String strUrl = "jdbc:mysql://localhost/labase";
			String strUrl = "jdbc:mysql://localhost/labase";
			String strUtilisateur = "sa";
			String strPwd = "";
			cnx = DriverManager.getConnection(strUrl, strUtilisateur, strPwd);
		} catch (SQLException sqle) {
			System.out.println("Echec de la connexion : " + sqle.getMessage());

			while (sqle != null) {
				System.out.println(
					"getSQLState()  retourne : " + sqle.getSQLState());
				System.out.println(
					"getErrorCode() retourne : " + sqle.getErrorCode());
				sqle = sqle.getNextException(); // une autre exception ?
			}
			return;
		}
		// �mission d'une requete
		try {
			st = cnx.createStatement();
			st.executeUpdate(rqt);
			SQLWarning sqlw = st.getWarnings();
			while (sqlw != null) {
				System.out.println("Warning : "+ sqlw.getMessage());
				// d'autres warning ?
				sqlw = sqlw.getNextWarning();
			}
			st.clearWarnings();
		} catch (SQLException sqle) {
			System.out.println("Pb lors de l'INSERT : " + sqle.getMessage());
			try {System.out.println("SQL Natif : " + cnx.nativeSQL(rqt));}catch (Exception exc){}
			while (sqle != null) {
				System.out.println(
					"getSQLState()  retourne : " + sqle.getSQLState());
				System.out.println(
					"getErrorCode() retourne : " + sqle.getErrorCode());
				sqle = sqle.getNextException(); // une autre exception ?
			}
		}
		finally {
			try {cnx.close();} catch (Exception exc){}
		}

	} // fin du main
}

package jdbc.resultset.scrollable;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.sql.*;
import commun.*;

/**
*/

public class Cadre1 extends JFrame {
	JPanel contentPane;
	BorderLayout borderLayout1 = new BorderLayout();
	JMenuBar jMenuBar1 = new JMenuBar();
	JMenu jMenu1 = new JMenu();
	JMenu jMenu2 = new JMenu();
	JMenuItem jmiConnexion = new JMenuItem();
	JMenuItem jmiRequete = new JMenuItem();
	JMenuItem jmiParcourt = new JMenuItem();
	JMenuItem jmiQuitter = new JMenuItem();
	private Connectable appli = null;

	// Construit le cadre
	public Cadre1() {
		enableEvents(AWTEvent.WINDOW_EVENT_MASK);
		try {
			InitInterface();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void setAppli(Connectable app) {
		if (app != null)
			this.appli = app;
	}

	private void InitInterface() throws Exception {
		//setIconImage(Toolkit.getDefaultToolkit().createImage(Cadre1.class.getResource("[Votre ic�ne]")));
		contentPane = (JPanel) this.getContentPane();
		contentPane.setLayout(borderLayout1);
		this.setJMenuBar(jMenuBar1);
		this.setSize(new Dimension(400, 300));
		this.setTitle("Utilisation d'un ResultSet scrollable");
		jMenu1.setFont(new java.awt.Font("Dialog", 1, 12));
		jMenu1.setActionCommand("Fichier");
		jMenu1.setMnemonic('F');
		jMenu1.setText("Fichier");
		jMenu2.setFont(new java.awt.Font("Dialog", 1, 12));
		jMenu2.setToolTipText("");
		jMenu2.setMnemonic('S');
		jMenu2.setText("SGBD");
		jmiConnexion.setToolTipText("");
		jmiConnexion.setActionCommand("Connexion");
		jmiConnexion.setMnemonic('X');
		jmiConnexion.setText("Connexion / D�connexion");
		jmiConnexion.setAccelerator(
			javax.swing.KeyStroke.getKeyStroke(
				88,
				java.awt.event.KeyEvent.CTRL_MASK,
				false));
		jmiConnexion.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jmiConnexion_actionPerformed(e);
			}
		});
		jmiRequete.setMnemonic('M');
		jmiRequete.setText("Emettre une requ�te");
		jmiRequete.setAccelerator(
			javax.swing.KeyStroke.getKeyStroke(
				82,
				java.awt.event.KeyEvent.CTRL_MASK,
				false));
		jmiRequete.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jmiRequete_actionPerformed(e);
			}
		});
		jmiParcourt.setMnemonic('R');
		jmiParcourt.setText("Parcourir un ResultSet scrollable");
		jmiParcourt.setAccelerator(
			javax.swing.KeyStroke.getKeyStroke(
				80,
				java.awt.event.KeyEvent.CTRL_MASK,
				false));
		jmiParcourt.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jmiParcourt_actionPerformed(e);
			}
		});
		jmiQuitter.setMnemonic('Q');
		jmiQuitter.setText("Quitter");
		jmiQuitter.setAccelerator(
			javax.swing.KeyStroke.getKeyStroke(
				81,
				java.awt.event.KeyEvent.CTRL_MASK,
				false));
		jmiQuitter.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jmiQuitter_actionPerformed(e);
			}
		});
		jMenuBar1.add(jMenu1);
		jMenuBar1.add(jMenu2);
		jMenu2.add(jmiConnexion);
		jMenu2.addSeparator();
		jMenu2.add(jmiRequete);
		jMenu2.addSeparator();
		jMenu2.add(jmiParcourt);
		jMenu1.add(jmiQuitter);
	}

	// Red�fini, ainsi nous pouvons sortir quand la fen�tre est ferm�e
	protected void processWindowEvent(WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID() == WindowEvent.WINDOW_CLOSING) {
			System.exit(0);
		}
	}

	// fin du programme
	void jmiQuitter_actionPerformed(ActionEvent e) {
		// est on encore connect� ?
		if (appli.getConnection() != null) {
			// si oui, on se d�connecte
			try {
				appli.getConnection().close();
			} catch (SQLException ex) {
			}
			appli.setConnection(null);
		}
		// et on part
		this.dispose();
		System.exit(0);
	}

	// fait apparaitre le dialogue de connexion
	void jmiConnexion_actionPerformed(ActionEvent e) {
		dlgConnex dlg = new dlgConnex(this, "Connexion", true);
		dlg.setAppli(appli);
		dlg.setModal(true);
		dlg.setDriver("com.mysql.jdbc.Driver");
		dlg.setUrl("jdbc:mysql://localhost/labase");
		dlg.setLocation(300, 300); // par exemple
		dlg.show();
	}

	// dialogue de requete
	void jmiRequete_actionPerformed(ActionEvent e) {
		dlgRequete dlg = new dlgRequete(this, "Emission d'une requ�te", true);
		dlg.setAppli(appli);
		dlg.setModal(true);
		dlg.setLocation(300, 300); // par exemple
		dlg.show();
	}

	// dialogue de parcourt
	void jmiParcourt_actionPerformed(ActionEvent e) {
		dlgParcourt dlg =
			new dlgParcourt(this, "Parcourt d\'un ResultSet scrollable", true);
		dlg.setAppli(appli);
		//dlg.setModal(true);
		dlg.setLocation(300, 300); // par exemple
		dlg.show();
	}
}
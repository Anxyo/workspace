package jdbc.resultset.scrollable;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.sql.*;
import java.awt.event.*;
import commun.*;

/**
 */

public class dlgRequete extends JDialog {
	// champs d'interface utilisateur
	JPanel pHaut = new JPanel();
	JPanel pCentre = new JPanel();
	JPanel pBas = new JPanel();
	JLabel lblResult = new JLabel("R�sultats : ");
	JLabel lblRqt = new JLabel("Requ�te   : ");

	JButton btnOK = new JButton("Fermer");
	JButton btnEnvoi = new JButton("Ex�cuter");
	JTextField txtEtat = new JTextField(32);

	JScrollPane jScrollPane1 = new JScrollPane();
	JTextArea txtResult = new JTextArea(10, 40);
	JScrollPane jScrollPane2 = new JScrollPane();
	JTextArea txtSQL = new JTextArea(3, 40);
	// champs jdbc
	Statement st;
	ResultSet rs;
	// r�ference vers la classe principale
	private Connectable appli = null;


	// constructeur
	public dlgRequete(Frame frame, String titre, boolean modal) {
		super(frame, titre, modal);
		try {
			InitInterface(titre, modal);
			pack();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	void InitInterface(String titre, boolean modal) throws Exception {
		this.setSize(new Dimension(600, 400));
		// on utilise un BorderLayout manager
		this.getContentPane().setLayout(new BorderLayout());
		// configuration des composants
		// ----------------------------
		lblRqt.setDisplayedMnemonic('R');
		//this.getContentPane().setSize(616, 621);
		btnOK.setMnemonic('O');
		btnOK.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnOK_actionPerformed(e);
			}
		});
		btnEnvoi.setMnemonic('X');
		btnEnvoi.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEnvoi_actionPerformed(e);
			}
		});
		// zone d'affichage de l'�tat courant
		txtEtat.setFont(new java.awt.Font("Dialog", 2, 12));
		txtEtat.setForeground(Color.red);
		txtEtat.setEditable(false);
		txtEtat.setHorizontalAlignment(SwingConstants.CENTER);
		// zone de requete
		txtSQL.setFocusAccelerator('R');
		txtSQL.setText(
			"SELECT * From <nom(s) de(s) Table(s)> WHERE <crit�re(s)>;");
		txtSQL.setToolTipText("Saisissez votre requete ici");
		// ajout des zones de texte aux scrollpanes
		jScrollPane1.setAutoscrolls(true);
		jScrollPane1.getViewport().add(txtResult, null);

		jScrollPane2.setAutoscrolls(true);
		jScrollPane2.getViewport().add(txtSQL, null);

		// insertion dans le dialogue en utilisant les panels
		FlowLayout flo1;
		flo1 = (FlowLayout) pHaut.getLayout(); // retrouve le layoutmanager
		flo1.setAlignment(FlowLayout.CENTER); // centrage des composants
		flo1 = (FlowLayout) pCentre.getLayout();
		flo1.setAlignment(FlowLayout.CENTER);
		flo1 = (FlowLayout) pBas.getLayout();
		flo1.setAlignment(FlowLayout.CENTER);
		// ajout des composants
		pHaut.add(lblRqt);
		pHaut.add(jScrollPane2);
		pCentre.add(lblResult);
		pCentre.add(jScrollPane1);
		pBas.add(txtEtat);
		pBas.add(btnEnvoi);
		pBas.add(btnOK);

		// ajout au dialogue
		this.getContentPane().add("North", pHaut);
		this.getContentPane().add("Center", pCentre);
		this.getContentPane().add("South", pBas);

		// configuration du dialogue
		this.setResizable(false);
		this.setModal(modal);
		this.setTitle(titre);
	}

	void btnOK_actionPerformed(ActionEvent e) {
		this.dispose();
	}

	public void setAppli(Connectable app) {
		// ex�cute la requete
		if (app != null) {
			this.appli = app;
			// on est connect� ?
			if (appli.getConnection() == null)
				txtEtat.setText("Vous n'�tes pas connect� !");
		}
	}

	void btnEnvoi_actionPerformed(ActionEvent e) {
		// le texte de la requete
		String r = txtSQL.getText().trim();
		// si on est connect�
		if (appli.getConnection()!= null) {
			try {
				txtEtat.setText("");
				st = appli.getConnection().createStatement();
				rs = st.executeQuery(r);
			} catch (SQLException sqle) { // erreur de syntaxe
				txtEtat.setText("Erreur de syntaxe SQL...");
				return;
			}
			// ici, on a un ResultSet normalement constitu�
			AfficheResultSet(rs, txtResult);
		} // fin du if

		else {
			// code diagnostique
			txtEtat.setText("Vous n'�tes pas connect� !");
		} // fin else
	}

	// routine d'affichage du resultset
	void AfficheResultSet(ResultSet r, JTextArea t) {
		String UneLigne = "";
		try {
			// combien de colonnes dans le resultset ?
			ResultSetMetaData meta = r.getMetaData();
			int nbcol = meta.getColumnCount();
			t.setText("");
			// boucle des lignes
			while (r.next()) {
				// boucle des colonnes
				for (int i = 1; i <= nbcol; i++) {
					UneLigne = UneLigne + r.getString(i) + "\t";
				} // fin du for

				// ajout d'un saut de ligne
				UneLigne = UneLigne + "\n";

				// si t est null, affiche sur la console
				if (t == null) {
					System.out.println(UneLigne);
				} else { // affichage dans la zone de texte
					t.append(UneLigne);
				}
				UneLigne = ""; // on recommence...
			} // fin du while
		} // fin du try
		catch (SQLException sqle) {
			// code de trace
		}
	} // fin de la routine

} // fin de la classe

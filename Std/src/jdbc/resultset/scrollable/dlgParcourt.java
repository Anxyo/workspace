package jdbc.resultset.scrollable;

import java.awt.*;
import javax.swing.*;
import java.sql.*;
import java.awt.event.*;
import commun.*;

/**
 * Utilise les fonctionnalit�s JDBC 2.0,
 * ici, la possibilit� de scroller dans les resultsets.
 * On n'a donc plus besoin de maintenir explicitement 
 * un buffer de r�sultats, jdbc lui-m�me s'en charge.
 */

public class dlgParcourt extends JDialog {

	// constantes d�finissant l'action effectu�e lors d'une �criture
	// des donn�es de l'�cran dans le Rs (voir la routine EcritDonnees()).
	private static final int NON_MAJ = 0;
	// copie uniquement les donn�es dans le RS
	private static final int MAJ_BASE = 1;
	// en plus, maj des donn�es dans la bases
	private static final int INS_BASE = 2;
	// ins�re un nouvel enregistrement dans la base

	// champs d�finissant l'interface utilisateur
	JPanel pHaut = new JPanel(), pGauche = new JPanel(), pCentre = new JPanel();

	JButton btnSuivant = new JButton(">"),
		btnDernier = new JButton(">>"),
		btnPrecedent = new JButton("<");
	JButton btnPremier = new JButton("<<"),
		btnOK = new JButton("OK"),
		btnEnr = new JButton("Enregistre");
	JButton btnNouveau = new JButton("Nouveau"),
		btnSupprime = new JButton("Supprime");

	JTextField txtCode = new JTextField(),
		txtRSoc = new JTextField(),
		txtAdresse = new JTextField();
	JTextField txtCodePostal = new JTextField(),
		txtVille = new JTextField(),
		txtPays = new JTextField();
	JTextArea txtEtat = new JTextArea(3, 30);
	JScrollPane spEtat = new JScrollPane();

	JLabel lblCode = new JLabel("Code employ� : "),
		lblRSociale = new JLabel("Raison sociale : ");
	JLabel lblAdresse = new JLabel("Adresse : "),
		lblCodeP = new JLabel("Code postal : ");
	JLabel lblVille = new JLabel("Ville : "), lblPays = new JLabel("Pays : ");

	// autres champs
	Connectable appli = null;
	boolean bNouveau = false;

	// pour JDBC
	ResultSet rs = null;
	Statement st = null;

	// pointeur d'enregistrements
	int ptr = 0;
	int taille = 0;

	// Constructeur
	public dlgParcourt(Frame frame, String title, boolean modal) {
		super(frame, title, modal);
		try {
			InitInterface();
			pack();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// Cr�ation de l'interface utilisateur
	void InitInterface() throws Exception {

		// boutons
		// -------
		btnSuivant.setFont(new java.awt.Font("Dialog", 1, 12));
		btnSuivant.setToolTipText("Ligne suivante");
		btnSuivant.setHorizontalTextPosition(SwingConstants.CENTER);
		btnSuivant.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSuivant_actionPerformed(e);
			}
		});
		btnDernier.setFont(new java.awt.Font("Dialog", 1, 12));
		btnDernier.setToolTipText("Derni�re ligne");
		btnDernier.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDernier_actionPerformed(e);
			}
		});
		btnPrecedent.setFont(new java.awt.Font("Dialog", 1, 12));
		btnPrecedent.setToolTipText("Ligne pr�c�dente");
		btnPrecedent.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnPrecedent_actionPerformed(e);
			}
		});
		btnPremier.setFont(new java.awt.Font("Dialog", 1, 12));
		btnPremier.setToolTipText("Premi�re ligne");
		btnPremier.setHorizontalAlignment(SwingConstants.LEFT);
		btnPremier.setHorizontalTextPosition(SwingConstants.CENTER);
		btnPremier.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnPremier_actionPerformed(e);
			}
		});
		btnEnr.setMnemonic('E');
		btnEnr.setFont(new java.awt.Font("Dialog", 1, 12));
		btnEnr.setToolTipText("Enregistre les donn�es courantes");
		btnEnr.setHorizontalTextPosition(SwingConstants.CENTER);
		btnEnr.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEnr_actionPerformed(e);
			}
		});
		btnSupprime.setMnemonic('S');
		btnSupprime.setFont(new java.awt.Font("Dialog", 1, 12));
		btnSupprime.setToolTipText("Tente de supprimer la ligne courante");
		btnSupprime.setHorizontalTextPosition(SwingConstants.CENTER);
		btnSupprime.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSupprime_actionPerformed(e);
			}
		});
		btnNouveau.setMnemonic('N');
		btnNouveau.setFont(new java.awt.Font("Dialog", 1, 12));
		btnNouveau.setToolTipText("Ajoute une ligne");
		btnNouveau.setHorizontalTextPosition(SwingConstants.CENTER);
		btnNouveau.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNouveau_actionPerformed(e);
			}
		});

		btnOK.setMnemonic('O');
		btnOK.setFont(new java.awt.Font("Dialog", 1, 12));
		btnOK.setToolTipText("Ferme cette fen�tre");
		btnOK.setHorizontalTextPosition(SwingConstants.CENTER);
		btnOK.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnOK_actionPerformed(e);
			}
		});
		// zones de texte
		txtEtat.setFont(new java.awt.Font("Dialog", 2, 12));
		txtEtat.setForeground(Color.red);
		txtEtat.setEditable(false);
		txtEtat.setText("Appuyez sur un des boutons de navigation");
		spEtat.setAutoscrolls(true);
		spEtat.getViewport().add(txtEtat, null);

		// pas de changement du code, par d�faut (cl� primaire)
		txtCode.setEditable(false);

		// dialogue
		this.setResizable(false);

		// placement des composants dans les panels
		// d'abord les boutons
		pHaut.setLayout(new GridLayout(2, 4)); // 2 lignes de 4 colonnes
		pHaut.add(btnPremier);
		pHaut.add(btnPrecedent);
		pHaut.add(btnSuivant);
		pHaut.add(btnDernier);
		pHaut.add(btnNouveau);
		pHaut.add(btnEnr);
		pHaut.add(btnSupprime);
		pHaut.add(btnOK);
		// puis les �tiquettes
		pGauche.setLayout(new GridLayout(6, 1));
		pGauche.add(lblCode);
		pGauche.add(lblRSociale);
		pGauche.add(lblAdresse);
		pGauche.add(lblVille);
		pGauche.add(lblCodeP);
		pGauche.add(lblPays);
		// et les JTextField(s)
		pCentre.setLayout(new GridLayout(6, 1));
		pCentre.add(txtCode);
		pCentre.add(txtRSoc);
		pCentre.add(txtAdresse);
		pCentre.add(txtVille);
		pCentre.add(txtCodePostal);
		pCentre.add(txtPays);
		// ajout des panel au dialogue
		this.getContentPane().add("North", pHaut);
		this.getContentPane().add("West", pGauche);
		this.getContentPane().add("Center", pCentre);
		this.getContentPane().add("South", spEtat);
		txtEtat.setEditable(false);

	}

	public void setAppli(Connectable app) {
		// ex�cute la requete
		if (app != null) {
			this.appli = app;
			// on est connect� ?
			if (appli.getConnection() == null) {
				txtEtat.setText("Vous n'�tes pas connect� !");
			} else
				// construit la liste des enregistrements
				ConstruitRs();
		}
	}

	// routines �venementielles
	// ------------------------
	void btnPremier_actionPerformed(ActionEvent e) {
		if (bNouveau) {
			bNouveau = false;
		}
		try {
			if (rs.first()) {
				// affichage des donn�es
				LitDonnees();
			}
		} catch (SQLException exc) {
			exc.printStackTrace();
		}
		txtEtat.setText("Appuyez sur un des boutons de navigation");
	}

	void btnPrecedent_actionPerformed(ActionEvent e) {
		if (bNouveau) {
			bNouveau = false;
		}
		try {
			if (rs.previous())
				// affichage des donn�es
				LitDonnees();
			else
				rs.first();

		} catch (SQLException exc) {
			exc.printStackTrace();
		}
		txtEtat.setText("Appuyez sur un des boutons de navigation");
	}

	void btnSuivant_actionPerformed(ActionEvent e) {
		if (bNouveau) {
			bNouveau = false;
		}
		try {
			if (rs.next())
				// affichage des donn�es
				LitDonnees();

			else // apr�s la derni�re ligne valide
				rs.last();
		} catch (SQLException exc) {
			exc.printStackTrace();
		}
		txtEtat.setText("Appuyez sur un des boutons de navigation");
	}

	void btnDernier_actionPerformed(ActionEvent e) {
		if (bNouveau) {
			bNouveau = false;
		}
		try {
			if (rs.last()) {
				// affichage des donn�es
				LitDonnees();
			}
		} catch (SQLException exc) {
			exc.printStackTrace();
		}
		txtEtat.setText("Appuyez sur un des boutons de navigation");
	}

	void btnEnr_actionPerformed(ActionEvent e) {
		try {

			if (bNouveau) {
				// on se place sur le buffer d'insertion
				rs.moveToInsertRow();
				EcritDonnees(false, INS_BASE);
				bNouveau = false;
				rs.last();
				LitDonnees();
			} else {
				EcritDonnees(false, MAJ_BASE);
			}
			txtEtat.setText("Appuyez sur un des boutons de navigation");
		} catch (Exception exc) {
			// code de trace
			txtEtat.setText("Erreur de mise � jour : " + exc.getMessage());
			System.out.println("Erreur de mise � jour : " + exc.getMessage());
		}
	}

	void btnNouveau_actionPerformed(ActionEvent e) {
		// remplit les zones de blancs
		txtCode.setText("<g�n�r� automatiquement>");
		txtCode.setEditable(false);
		txtRSoc.setText("");
		txtAdresse.setText("");
		txtCodePostal.setText("");
		txtVille.setText("");
		txtPays.setText("");
		// drapeau : bNouveau vrai
		bNouveau = true;
		// on DOIT sauvegarder par "Sauve" : on pr�vient...
		txtEtat.setText("Sauvegardez les modifications par 'Enregistrer' !");
	}

	void btnSupprime_actionPerformed(ActionEvent e) {
		int reponse;

		try {
			// on a demand� la suppression d'un enregistrement qui
			// n'existait pas...
			if (bNouveau) {
				bNouveau = false;
				reponse =
					JOptionPane.showConfirmDialog(
						this,
						"Etes-vous s�r d'abandonner l'ajout d'enregistrement en cours ?",
						"Confirmation de l'abandon d'ajout",
						JOptionPane.YES_NO_OPTION);
				if (reponse != JOptionPane.YES_OPTION)
					return;
				// affichage des donn�es
				LitDonnees();
			} else {
				reponse =
					JOptionPane.showConfirmDialog(
						this,
						"Etes-vous s�r de vouloir supprimer cette ligne ?",
						"Confirmation de suppression",
						JOptionPane.YES_NO_OPTION);
				if (reponse != JOptionPane.YES_OPTION)
					return;
				// quel est le num�ro de ligne � effacer ?
				int no_ligne = rs.getRow();
				// tente de supprimer la ligne courante
				rs.deleteRow();
				// se repositionne sur l'enregistrement pr�c�dent, s'il y en a un...
				if (no_ligne > 1)
					rs.absolute(--no_ligne);
				else
					rs.absolute(1);
				// rafraichit l'affichage
				LitDonnees();
			}

			txtEtat.setText("Appuyez sur un des boutons de navigation");
		} catch (Exception exc) {
			// code de trace
			txtEtat.setText(exc.getMessage());
			exc.printStackTrace();
		}

	}

	void btnOK_actionPerformed(ActionEvent e) {
		try {
			if (rs != null)
				rs.close();
			if (st != null)
				st.close();
		} catch (SQLException sqle) {
		}
		this.dispose();
	}

	// Ajout d'une ligne

	// Mise � jour d'une ligne

	// ---------------------------------------
	// Routines de gestion du buffer de lignes
	// ---------------------------------------

	// construit le Resultset
	private void ConstruitRs() {
		// ex�cute la requete
		if (appli.getConnection() != null) {
			try {
				st =
					appli.getConnection().createStatement(
						ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_UPDATABLE);
				rs =
					st.executeQuery("SELECT * FROM clients ORDER BY code ASC;");
				// affichage des donn�es, s'il y en a...
				if (rs.last()) { // il y a des donn�es {
					rs.first(); // on retourne au d�but
					LitDonnees();
				}

			} catch (SQLException sqle) {
				// code de trace
				txtEtat.setText("Erreur SELECT : " + sqle.getMessage());
			}

		} // fin du if
	}

	// transfert des donn�es depuis le vector vers l'�cran
	private void LitDonnees() {
		try {
			txtCode.setText(Integer.toString(rs.getInt("code")));
			txtRSoc.setText(nonNull(rs.getString("r_sociale")));
			txtAdresse.setText(rs.getString("adresse"));
			txtCodePostal.setText(nonNull(rs.getString("cde_postal")));
			txtVille.setText(nonNull(rs.getString("ville")));
			txtPays.setText(nonNull(rs.getString("pays")));
		} catch (SQLException exc) {
			exc.printStackTrace();
		}
	}

	// Tx des donn�es depuis l'�cran vers le RS, et vers la base
	// suivant la valeur de actionBase.
	private void EcritDonnees(boolean ecritCle, int actionBase) {
		try {
			if (ecritCle)
				rs.updateInt("code", Integer.parseInt(txtCode.getText()));

			rs.updateString("r_sociale", txtRSoc.getText());
			rs.updateString("adresse", txtAdresse.getText());
			rs.updateString("cde_postal", txtCodePostal.getText());
			rs.updateString("ville", txtVille.getText());
			rs.updateString("pays", txtPays.getText());

			if (actionBase == MAJ_BASE)
				rs.updateRow();

			else if (actionBase == INS_BASE) {
				rs.insertRow();
				// Attention : en cas d'ouble de la ligne suivante,
				// le rs n'est plus modifiable apr�s une insertion !
				// Un bug du driver jdbc ?
				rs.moveToCurrentRow();
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}

	// pour �viter de tenter d'afficher un Null
	private String nonNull(String AAfficher) {
		return (AAfficher == null) ? "" : AAfficher;
	}

} // fin de la classe

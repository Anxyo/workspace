package jdbc.resultset.scrollable;

import javax.swing.UIManager;
import java.awt.*;
import java.sql.*;
import commun.*;

public class AppliScroll implements Connectable {
	boolean packFrame = false;

	// r�f�rences vers les objets JDBC allou�s
	Connection connect = null;

	public AppliScroll() {
		Cadre1 frame = new Cadre1();
		frame.setAppli(this);

		if (packFrame) {
			frame.pack();
		} else {
			frame.validate();
		}
		// Centrer la fen�tre
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = frame.getSize();
		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}
		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}
		frame.setLocation(
			(screenSize.width - frameSize.width) / 2,
			(screenSize.height - frameSize.height) / 2);
		frame.setVisible(true);
	}
	
	// impl�mentation de Connectable
	public Connection getConnection() {
		return connect;	
	}
	public void setConnection(Connection cnx) {
		this.connect = cnx;		
	}
	
	// M�thode principale
	public static void main(String[] args) {
		try {
			//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");

		} catch (Exception e) {
			e.printStackTrace();
		}
		new AppliScroll();
	}
}
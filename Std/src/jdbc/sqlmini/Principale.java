package jdbc.sqlmini;
import java.sql.*;
public class Principale {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// uniquement pour pour SQLEXPRESS
		SQLMini.setInstance("SQLEXPRESS");	
		SQLMini.getConnection();
		ResultSet rs = SQLMini.ExecuteSelect("SELECT * FROM AUTHORS");
		SQLMini.AfficheResultSet(rs);
		SQLMini.closeConnexion();
	}
}

package jdbc.sqlmini;
import java.sql.*;

public class SQLMini {
	private static Connection mCnx = null;
	public static String mDriver = "net.sourceforge.jtds.jdbc.Driver";
	
	public static String mUrlJdbc = "jdbc:jtds:sqlserver://localhost:1433/";
	private static String mNomBase = "pubs";
	private static String mUid = "sa";
	private static String mPwd = "arati";
	private static String mInstance = "";
	
	// routine de dialogue avec la base
	public static Connection getConnection() {
		String url = mUrlJdbc + mNomBase;
		if (!mInstance.equals(""))
			url = url + ";"+ mInstance +";";
		// tentative de chargement du driver
		try { 
			Class.forName(mDriver).newInstance();
		} catch (Exception exc) {
			System.out.println("Driver non trouv�");
			return null;
		}
		// tentative de connexion
		try {
			
			mCnx = DriverManager.getConnection(url, mUid, mPwd);
			
		} catch (SQLException sqle) {
			System.out.println("Pb de connexion");
		}
		return mCnx;
	}
	public static void closeConnexion() {
		if (mCnx != null) {
			try {
				
				mCnx.close();
				mCnx = null;
				
			} catch (SQLException sqle) {}
		}
	}

	public static ResultSet ExecuteSelect(String sql) {
		ResultSet rs = null;
		Statement st = null;
		if (mCnx != null) {
			
			try 
			{
				st = mCnx.createStatement();
				rs = st.executeQuery(sql);
			}
			catch (SQLException sqle){
				rs = null;
			}
		}
		return rs;
	}
	public static int Execute(String sql){
		Statement st = null;
		int nbLignes = 0;
		if (mCnx != null) {
			
			try 
			{
				st = mCnx.createStatement();
				nbLignes = st.executeUpdate(sql);
			}
			catch (SQLException sqle){
				nbLignes = -1;
			}
		}
		return nbLignes;
	}
	//	 routine d'affichage d'un resultset
	 public static void AfficheResultSet(ResultSet r) {
		String UneLigne = "";
		try {
		// combien de colonnes dans le resultset ?
		ResultSetMetaData meta = r.getMetaData();
		int nbcol = meta.getColumnCount();
		// boucle des lignes
		while (r.next()) {
		    // boucle des colonnes
		    for (int i = 1; i <= nbcol; i++) {
			UneLigne = UneLigne + r.getString(i)+"\t";
			}   // fin du for
			System.out.println(UneLigne);
		    UneLigne = "";  // on recommence...
		 }  // fin du while
		}	// fin du try
		catch (SQLException sqle) {}
	    }	
	// accesseurs g�n�riques
	public static String getNomBase() {
		return mNomBase;
	}
	public static void setNomBase(String nomBase) {
		mNomBase = nomBase;
	}
	public static String getPwd() {
		return mPwd;
	}
	public static void setPwd(String pwd) {
		mPwd = pwd;
	}
	public static String getUid() {
		return mUid;
	}
	public static void setUid(String uid) {
		mUid = uid;
	}
	public static String getInstance() {
		return mInstance;
	}
	public static void setInstance(String instance) {
		mInstance = instance;
	}
}

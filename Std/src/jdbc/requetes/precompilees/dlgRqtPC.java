package jdbc.requetes.precompilees;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.sql.*;
import java.util.*;
import java.awt.event.*;
import commun.*;

public class dlgRqtPC extends JDialog {
	// champs d'interface utilisateur
	// pas mal de panels...
	JPanel pHaut = new JPanel();
	JPanel pCentre = new JPanel();
	JPanel pCentreh = new JPanel(); // pour le choix des params
	JPanel pCentrehg = new JPanel(); // pour mettre la valeur + type du param
	JPanel pCentrehgBtn = new JPanel();
	// pour les deux boutons d'ajout / suppr de param.
	JPanel pCentreb = new JPanel(); // les r�sultats sont l�
	JPanel pBas = new JPanel();
	// les �tiquettes
	JLabel lblParams = new JLabel("Param�tres ");
	JLabel lblResult = new JLabel("R�sultats : ");
	JLabel lblRqt = new JLabel("Requ�te   : ");
	// et les boutons
	JButton btnOK = new JButton("Fermer");
	JButton btnEnvoi = new JButton("Ex�cuter");
	JButton btnAjoutLst = new JButton("Ajouter");
	JButton btnSupprLst = new JButton("Supprimer");
	// zones de saisie / affichage
	JTextField txtEtat = new JTextField(32);
	JTextField txtValParam = new JTextField();
	// combo des types de param�tre
	JComboBox cmbTypes = new JComboBox();
	// liste des param�tres choisis
	Vector v = new Vector();
	JList lstParam;
	JScrollPane scp;
	// la zone de saisie de la requete pr�compil�e
	JTextArea txtSQL = new JTextArea(2, 40);
	JScrollPane jScrollPane1 = new JScrollPane();
	// le r�sultat      
	JTextArea txtResult = new JTextArea(4, 40);
	JScrollPane jScrollPane2 = new JScrollPane();
	// champs jdbc
	Statement st;
	String rqt = "";		 // le texte de la requete pr�compil�e
	PreparedStatement pst;   // requete pr�compil�e
	ResultSet rs;
	// r�ference vers la classe principale
	private Connectable appli = null;

	// constructeur
	public dlgRqtPC(Frame frame, String titre, boolean modal) {
		super(frame, titre, modal);
		try {
			InitInterface(titre, modal);
			pack();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	void InitInterface(String titre, boolean modal) throws Exception {
		this.setSize(new Dimension(500, 400));
		// on utilise un BorderLayout manager
		this.getContentPane().setLayout(new BorderLayout());
		// configuration des composants
		// ----------------------------
		lblRqt.setDisplayedMnemonic('R');
		btnOK.setMnemonic('O');
		btnOK.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnOK_actionPerformed(e);
			}
		});
		btnEnvoi.setMnemonic('X');
		btnEnvoi.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEnvoi_actionPerformed(e);
			}
		});
		btnAjoutLst.setMnemonic('A');
		btnAjoutLst.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAjoutLst_actionPerformed(e);
			}
		});
		btnSupprLst.setMnemonic('S');
		btnSupprLst.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSupprLst_actionPerformed(e);
			}
		});
		
		// zone d'affichage de l'�tat courant
		txtEtat.setFont(new java.awt.Font("Dialog", 2, 12));
		txtEtat.setForeground(Color.red);
		txtEtat.setEditable(false);
		txtEtat.setHorizontalAlignment(SwingConstants.CENTER);
		
		// le combo des types de donn�es
		cmbTypes.addItem("int");
		cmbTypes.addItem("float");
		cmbTypes.addItem("double");
		cmbTypes.addItem("String");
		cmbTypes.setEditable(false); // on ne peut pas saisir de nouveaux types
		lstParam = new JList(v);
		
		//	lstParam.setVisibleRowCount(3);	
		scp = new JScrollPane(lstParam);
		scp.setPreferredSize(new Dimension(200, 40));
		
		// zone de requete
		txtSQL.setFocusAccelerator('R');
		txtSQL.setText(
			"SELECT * From clients WHERE code < ?;");
		txtSQL.setToolTipText("Saisissez votre requete ici");
		// ajout des zones de texte aux scrollpanes
		jScrollPane1.setAutoscrolls(true);
		jScrollPane1.getViewport().add(txtResult, null);
		jScrollPane2.setAutoscrolls(true);
		jScrollPane2.getViewport().add(txtSQL, null);
		// insertion dans le dialogue en utilisant les panels
		// on commence par pHaut
		pHaut.setLayout(new BorderLayout());
		// ajout des composants � pHaut
		pHaut.add("West", lblRqt);
		pHaut.add("East", jScrollPane2);
		// pCentrehgBtn
		FlowLayout flo1 = (FlowLayout) pCentrehgBtn.getLayout();
		flo1.setAlignment(FlowLayout.RIGHT);
		pCentrehgBtn.add(btnAjoutLst);
		pCentrehgBtn.add(btnSupprLst);
		// puis pCentrehg
		JPanel pPP = new JPanel();
		GridLayout glm = new GridLayout(2, 2); // 5 lignes de 1 colonne
		pPP.setLayout(glm);
		pPP.add(new JLabel("Choisissez / saisissez un type : "));
		pPP.add(cmbTypes);
		pPP.add(new JLabel("D�finissez sa valeur : "));
		pPP.add(txtValParam);
		pCentrehg.setLayout(new BorderLayout());
		pCentrehg.add("North", pPP);
		pCentrehg.add("South", pCentrehgBtn);
		// puis pCentreh
		pCentreh.setLayout(new BorderLayout());
		pCentreh.add("Center", pCentrehg);
		// le panel contenant le choix du param + les boutons
		pCentreh.add("East", scp); // la zone de liste dans son JScrollPane
		// puis pCentreb
		pCentreb.setLayout(new BorderLayout());
		// ajout des composants � pCentreb
		pCentreb.add("West", lblResult);
		pCentreb.add("East", jScrollPane1);
		// et pBas
		pBas.add("West", txtEtat);
		pBas.add("Center", btnEnvoi);
		pBas.add("East", btnOK);
		// ajout de pCentreb et pCentreh � pCentre
		// qui utilise un BorderLayout
		pCentre.setLayout(new BorderLayout());
		pCentre.add("North", pCentreh);
		pCentre.add("South", pCentreb);
		// ajout au dialogue
		this.getContentPane().add("North", pHaut);
		this.getContentPane().add("Center", pCentre);
		this.getContentPane().add("South", pBas);
		// configuration du dialogue
		this.setResizable(false);
		this.setModal(modal);
		this.setTitle(titre);
	}

	void btnOK_actionPerformed(ActionEvent e) {
		this.dispose();
	}

	void btnAjoutLst_actionPerformed(ActionEvent e) {
		String val = txtValParam.getText().trim();
		String vType = (String) cmbTypes.getSelectedItem();
		// si c'est bon, ajoute au Vector
		String s = val + " (" + vType + ")";
		v.add(s);
		// rafraichit la liste
		lstParam.setListData(v);
	}

	void btnSupprLst_actionPerformed(ActionEvent e) {
		int indice = lstParam.getSelectedIndex();
		if (indice != -1) {
			v.remove(indice);
			// rafraichit la liste
			lstParam.setListData(v);
		}

	}

	public void setAppli(Connectable app) {
		// ex�cute la requete
		if (app != null) {
			this.appli = app;
			// on est connect� ?
			if (appli.getConnection() == null)
				txtEtat.setText("Vous n'�tes pas connect� !");
		}
	}

	void btnEnvoi_actionPerformed(ActionEvent e) {
		// le texte de la requete
		String r = txtSQL.getText().trim();
		// si on est connect�
		if (appli.getConnection() != null) {
			try {
				txtEtat.setText("");
				// si le contenu de la requete pr�compil�e a chang�, on la reconstruit
				if (!txtSQL.getText().equals(rqt)) {
					rqt = txtSQL.getText();
					pst = appli.getConnection().prepareStatement(rqt);
				}
				// passage des param�tres				
				// on va parcourir la liste des param�tres, 
				int nb_params = v.size();
				for (int i = 0; i < nb_params; i++)
				{
					String s = (String)v.elementAt(i);
					String sVal = s.substring(0,s.indexOf(' '));
					String sType = s.substring(s.indexOf('(')+1, s.indexOf(')'));
					// En fonction de leur type... 
					if (sType.equals("int")) {
						pst.setInt(i + 1, Integer.parseInt(sVal));
					}
					else if (sType.equals("float")) {
						pst.setFloat(i + 1, Float.parseFloat(sVal));	
					}
					else if (sType.equals("double")) {
						pst.setDouble(i + 1, Double.parseDouble(sVal));							
					}
					else if (sType.equals("String")) {
							pst.setString(i + 1, sVal);								
					}
				}
				// Ex�cution de la requete
				rs = pst.executeQuery();
				// ici, on a un ResultSet normalement constitu�
				AfficheResultSet(rs, txtResult);

			} catch (SQLException sqle) { // erreur de syntaxe
				txtEtat.setText("Erreur de syntaxe SQL...");
				sqle.printStackTrace();
				return;
			}
			catch (Exception exc) {
				txtEtat.setText("Probl�me de conversion de donn�es...");	
				exc.printStackTrace();	
			}
		}

		else {
			// code diagnostique
			txtEtat.setText("Vous n'�tes pas connect� !");
		} // fin else 
	}

	// routine d'affichage du resultset
	void AfficheResultSet(ResultSet r, JTextArea t) {
		String UneLigne = "";
		try {
			// combien de colonnes dans le resultset ?
			ResultSetMetaData meta = r.getMetaData();
			int nbcol = meta.getColumnCount();
			t.setText("");
			// boucle des lignes
			while (r.next()) {
				// boucle des colonnes
				for (int i = 1; i <= nbcol; i++) {
					UneLigne = UneLigne + r.getString(i) + "\t";
				} // fin du for

				// ajout d'un saut de ligne
				UneLigne = UneLigne + "\n";

				// si t est null, affiche sur la console
				if (t == null) {
					System.out.println(UneLigne);
				} else { // affichage dans la zone de texte
					t.append(UneLigne);
				}
				UneLigne = ""; // on recommence...
			} // fin du while
		} // fin du try
		catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	} // fin de la routine

} // fin de la classe

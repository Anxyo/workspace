package jdbc.requetes.parcourtrs;

import java.awt.*;
import javax.swing.*;
import java.sql.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.util.*; // pour la classe Vector
import commun.*;

/**
 * Titre :        JDBC1
 * Description :  Utilisation de base de l'API JDBC :
 * - Emission d'une requ�te SELECT
 * - parcours et affichage d'un ensemble r�sultats
 * - on supposera que l'on doit g�rer nous m�me le buffer d'enregistrements
 *   (driver jdbc 1.0 seulement).
 * - on a 6 colonnes : un entier (Code, qui est la cl� primaire) et 5 String
 *   (RSoc, Adresse, CodePostal, Ville, Pays)
 *    
 */

public class dlgParcourt extends JDialog {

	// champs d�finissant l'interface utilisateur
	JPanel pHaut = new JPanel(), pGauche = new JPanel(), pCentre = new JPanel();

	JButton btnSuivant = new JButton(">"),
		btnDernier = new JButton(">>"),
		btnPrecedent = new JButton("<");
	JButton btnPremier = new JButton("<<"),
		btnOK = new JButton("OK"),
		btnEnr = new JButton("Enregistre");
	JButton btnNouveau = new JButton("Nouveau"),
		btnSupprime = new JButton("Supprime");

	JTextField txtCode = new JTextField(),
		txtRSoc = new JTextField(),
		txtAdresse = new JTextField();
	JTextField txtCodePostal = new JTextField(),
		txtVille = new JTextField(),
		txtPays = new JTextField();
	JTextArea txtEtat = new JTextArea(3, 30);
	JScrollPane spEtat = new JScrollPane();

	JLabel lblCode = new JLabel("Code employ� : "),
		lblRSociale = new JLabel("Raison sociale : ");
	JLabel lblAdresse = new JLabel("Adresse : "),
		lblCodeP = new JLabel("Code postal : ");
	JLabel lblVille = new JLabel("Ville : "), lblPays = new JLabel("Pays : ");

	// autres champs
	Connectable appli = null;
	boolean bNouveau;

	// pour JDBC
	ResultSet rs = null;
	Statement st = null;

	// liste des enregistements
	Vector Liste;
	// pointeur d'enregistrements
	int ptr;

	// Constructeur
	public dlgParcourt(Frame frame, String title, boolean modal) {
		super(frame, title, modal);
		try {
			InitInterface();
			pack();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// Cr�ation de l'interface utilisateur
	void InitInterface() throws Exception {

		// boutons
		// -------
		btnSuivant.setFont(new java.awt.Font("Dialog", 1, 12));
		btnSuivant.setToolTipText("Ligne suivante");
		btnSuivant.setHorizontalTextPosition(SwingConstants.CENTER);
		btnSuivant.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSuivant_actionPerformed(e);
			}
		});
		btnDernier.setFont(new java.awt.Font("Dialog", 1, 12));
		btnDernier.setToolTipText("Derni�re ligne");
		btnDernier.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDernier_actionPerformed(e);
			}
		});
		btnPrecedent.setFont(new java.awt.Font("Dialog", 1, 12));
		btnPrecedent.setToolTipText("Ligne pr�c�dente");
		btnPrecedent.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnPrecedent_actionPerformed(e);
			}
		});
		btnPremier.setFont(new java.awt.Font("Dialog", 1, 12));
		btnPremier.setToolTipText("Premi�re ligne");
		btnPremier.setHorizontalAlignment(SwingConstants.LEFT);
		btnPremier.setHorizontalTextPosition(SwingConstants.CENTER);
		btnPremier.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnPremier_actionPerformed(e);
			}
		});
		btnEnr.setMnemonic('E');
		btnEnr.setFont(new java.awt.Font("Dialog", 1, 12));
		btnEnr.setToolTipText("Enregistre les donn�es courantes");
		btnEnr.setHorizontalTextPosition(SwingConstants.CENTER);
		btnEnr.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEnr_actionPerformed(e);
			}
		});
		btnSupprime.setMnemonic('S');
		btnSupprime.setFont(new java.awt.Font("Dialog", 1, 12));
		btnSupprime.setToolTipText("Tente de supprimer la ligne courante");
		btnSupprime.setHorizontalTextPosition(SwingConstants.CENTER);
		btnSupprime.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSupprime_actionPerformed(e);
			}
		});
		btnNouveau.setMnemonic('N');
		btnNouveau.setFont(new java.awt.Font("Dialog", 1, 12));
		btnNouveau.setToolTipText("Ajoute une ligne");
		btnNouveau.setHorizontalTextPosition(SwingConstants.CENTER);
		btnNouveau.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNouveau_actionPerformed(e);
			}
		});

		btnOK.setMnemonic('O');
		btnOK.setFont(new java.awt.Font("Dialog", 1, 12));
		btnOK.setToolTipText("Ferme cette fen�tre");
		btnOK.setHorizontalTextPosition(SwingConstants.CENTER);
		btnOK.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnOK_actionPerformed(e);
			}
		});
		// zones de texte    
		txtEtat.setFont(new java.awt.Font("Dialog", 2, 12));
		txtEtat.setForeground(Color.red);
		txtEtat.setEditable(false);
		txtEtat.setText("Appuyez sur un des boutons de navigation");
		spEtat.setAutoscrolls(true);
		spEtat.getViewport().add(txtEtat, null);

		// pas de changement du code, par d�faut (cl� primaire)
		txtCode.setEditable(false);

		// dialogue   
		this.setResizable(false);

		// placement des composants dans les panels
		// d'abord les boutons
		pHaut.setLayout(new GridLayout(2, 4)); // 2 lignes de 4 colonnes
		pHaut.add(btnPremier);
		pHaut.add(btnPrecedent);
		pHaut.add(btnSuivant);
		pHaut.add(btnDernier);
		pHaut.add(btnNouveau);
		pHaut.add(btnEnr);
		pHaut.add(btnSupprime);
		pHaut.add(btnOK);
		// puis les �tiquettes
		pGauche.setLayout(new GridLayout(6, 1));
		pGauche.add(lblCode);
		pGauche.add(lblRSociale);
		pGauche.add(lblAdresse);
		pGauche.add(lblVille);
		pGauche.add(lblCodeP);
		pGauche.add(lblPays);
		// et les JTextField(s)
		pCentre.setLayout(new GridLayout(6, 1));
		pCentre.add(txtCode);
		pCentre.add(txtRSoc);
		pCentre.add(txtAdresse);
		pCentre.add(txtVille);
		pCentre.add(txtCodePostal);
		pCentre.add(txtPays);
		// ajout des panel au dialogue
		this.getContentPane().add("North", pHaut);
		this.getContentPane().add("West", pGauche);
		this.getContentPane().add("Center", pCentre);
		this.getContentPane().add("South", spEtat);
		txtEtat.setEditable(false);
		// init du vector
		Liste = new Vector();
	}

	public void setAppli(Connectable app) {
		// ex�cute la requete
		if (app != null) {
			this.appli = app;
			// construit la liste des enregistrements
			ConstruitListe();
		}
	}

	// routines �venementielles
	// ------------------------
	void btnPremier_actionPerformed(ActionEvent e) {

		if (bNouveau) {
			bNouveau = false;
			txtCode.setEditable(false);
		}
		ptr = 0;
		// affichage des donn�es
		LitDonnees((Enrgt) Liste.elementAt(ptr));
		txtEtat.setText("Appuyez sur un des boutons de navigation");
	}

	void btnPrecedent_actionPerformed(ActionEvent e) {
		if (bNouveau) {
			bNouveau = false;
			txtCode.setEditable(false);
		}
		if (ptr > 0)
			ptr--;
		// affichage des donn�es
		LitDonnees((Enrgt) Liste.elementAt(ptr));
		txtEtat.setText("Appuyez sur un des boutons de navigation");
	}

	void btnSuivant_actionPerformed(ActionEvent e) {
		if (bNouveau) {
			bNouveau = false;
			txtCode.setEditable(false);
		}
		if (ptr < Liste.size() - 1)
			ptr++;
		// affichage des donn�es
		LitDonnees((Enrgt) Liste.elementAt(ptr));
		txtEtat.setText("Appuyez sur un des boutons de navigation");
	}

	void btnDernier_actionPerformed(ActionEvent e) {
		if (bNouveau) {
			bNouveau = false;
			txtCode.setEditable(false);
		}
		ptr = Liste.size() - 1;
		// affichage des donn�es
		LitDonnees((Enrgt) Liste.elementAt(ptr));
		txtEtat.setText("Appuyez sur un des boutons de navigation");
	}

	void btnEnr_actionPerformed(ActionEvent e) {
		if (bNouveau) { // INSERT
			try {
				// tente d'effectuer l'INSERT
				String s =
					"INSERT INTO clients "
						+ "(code, r_sociale, adresse, cde_postal, ville, pays)"
						+ "\n"
						+ "VALUES ("
						+ txtCode.getText().trim()
						+ ", "
						+ "'"
						+ txtRSoc.getText().trim()
						+ "' , '"
						+ txtAdresse.getText().trim()
						+ "' , '"
						+ txtCodePostal.getText().trim()
						+ "' , '"
						+ txtVille.getText().trim()
						+ "' , '"
						+ txtPays.getText().trim()
						+ "');";
				// trace
				System.out.println(s);
				st.executeUpdate(s); //tente l'insert 
				// sauvegarde dans le vector
				int posInsert =
					LocalisePosition(
						Integer.parseInt(txtCode.getText().trim()));
				Enrgt enr1 =
					new Enrgt(
						Integer.parseInt(txtCode.getText().trim()),
						txtRSoc.getText().trim(),
						txtAdresse.getText().trim(),
						txtCodePostal.getText().trim(),
						txtVille.getText().trim(),
						txtPays.getText().trim());
				// trace
				System.out.println("Taille de liste : " + Liste.size());
				System.out.println("Indice : " + posInsert);
				// ajoute � la liste m�moire
				Liste.insertElementAt(enr1, posInsert);
				// positionnement courant sur cet enregistement
				ptr = posInsert;
				bNouveau = false; // insertion termin�e
				txtEtat.setText("Appuyez sur un des boutons de navigation");
			} catch (Exception exc) {
				// code de trace	
				txtEtat.setText("Erreur d'insertion : " + exc.getMessage());
				System.out.println("Erreur d'insertion : " + exc.getMessage());
			} finally {
				// drapeau INSERT � false maintenant
				bNouveau = false;
				txtCode.setEditable(false);
			}
		} // fin du if INSERT

		else { // UPDATE
			try {
				// tente d'effectuer la mise � jour
				String s =
					"UPDATE clients SET "
						+ "r_sociale = '"
						+ txtRSoc.getText().trim()
						+ "' , "
						+ "\n"
						+ "adresse = '"
						+ txtAdresse.getText().trim()
						+ "' , "
						+ "\n"
						+ "cde_postal = '"
						+ txtCodePostal.getText().trim()
						+ "' , "
						+ "\n"
						+ "ville = '"
						+ txtVille.getText().trim()
						+ "' , "
						+ "\n"
						+ "pays = '"
						+ txtPays.getText().trim()
						+ "'\n"
						+ "WHERE code = "
						+ txtCode.getText().trim()
						+ ";";
				// trace
				System.out.println(s);
				st.executeUpdate(s);
				// sauvegarde dans le vector
				EcritDonnees((Enrgt) Liste.elementAt(ptr));
				txtEtat.setText("Appuyez sur un des boutons de navigation");
			} catch (Exception exc) {
				// code de trace	
				txtEtat.setText("Erreur de mise � jour : " + exc.getMessage());
				System.out.println(
					"Erreur de mise � jour : " + exc.getMessage());
			}
		} // fin du else INSERT

	}

	void btnNouveau_actionPerformed(ActionEvent e) {
		// remplit les zones de blancs
		txtCode.setText("");
		txtCode.setEditable(true);
		txtRSoc.setText("");
		txtAdresse.setText("");
		txtCodePostal.setText("");
		txtVille.setText("");
		txtPays.setText("");
		// drapeau : Nouveau vrai
		bNouveau = true;
		// on DOIT sauvegarder par "Sauve" : on pr�vient
		txtEtat.setText("Sauvegardez les modifications par 'Enregistrer' !");
	}

	void btnSupprime_actionPerformed(ActionEvent e) {
		int reponse =
			JOptionPane.showConfirmDialog(
				this,
				"Etes-vous sur de vouloir supprimer cette ligne ?",
				"Confirmation de suppression",
				JOptionPane.YES_NO_OPTION);
		if (reponse != JOptionPane.OK_OPTION)
			return;
		if (bNouveau) {
			bNouveau = false;
			txtCode.setEditable(false);
		} else {
			try {
				// tente de supprimer la ligne courante
				String s =
					"DELETE FROM clients WHERE code = "
						+ txtCode.getText().trim()
						+ ";";
				// trace
				System.out.println(s);
				st.executeUpdate(s);
				txtEtat.setText("Appuyez sur un des boutons de navigation");
			} catch (Exception exc) {
				// code de trace	
				txtEtat.setText(exc.getMessage());
				System.out.println(
					"Erreur lors du DELETE : " + exc.getMessage());
			}

		}
		RafraichitListe();
		// affichage des donn�es
		LitDonnees((Enrgt) Liste.elementAt(ptr));
	}

	void btnOK_actionPerformed(ActionEvent e) {
		try {
			if (rs != null)
				rs.close();
			if (st != null)
				st.close();
		} catch (SQLException sqle) {
		}
		this.dispose();
	}

	// Ajout d'une ligne

	// Mise � jour d'une ligne

	// ---------------------------------------
	// Routines de gestion du buffer de lignes
	// ---------------------------------------

	// construit la liste des enregistrements
	private void ConstruitListe() {
		// ex�cute la requete
		if (appli.getConnection() != null) {
			try {
				st = appli.getConnection().createStatement();
				rs =
					st.executeQuery("SELECT * FROM clients ORDER BY code ASC;");

				// boucle de parcourt du resultset
				while (rs.next()) {
					Enrgt er =
						new Enrgt(
							rs.getInt("code"),
							nonNull(rs.getString("r_sociale")),
							nonNull(rs.getString("adresse")),
							nonNull(rs.getString("cde_postal")),
							nonNull(rs.getString("ville")),
							nonNull(rs.getString("pays")));
					Liste.addElement(er);
				} // fin du while
				// initialisation du ptr
				ptr = 0;
				// affichage des donn�es, s'il y en a...
				if (Liste.size() != 0)
					LitDonnees((Enrgt) Liste.elementAt(ptr));
			} catch (SQLException sqle) {
				// code de trace	
				txtEtat.setText("Erreur SELECT : " + sqle.getMessage());
			}

		} // fin du if
	}

	// transfert des donn�es depuis le vector vers l'�cran
	private void LitDonnees(Object o) {
		Enrgt er = (Enrgt) o;
		txtCode.setText(Integer.toString(er.nCode));
		txtRSoc.setText(er.sRSoc);
		txtAdresse.setText(er.sAdresse);
		txtCodePostal.setText(er.sCodePostal);
		txtVille.setText(er.sVille);
		txtPays.setText(er.sPays);
	}

	// transfert des donn�es depuis l'�cran vers le vector
	private void EcritDonnees(Object o) {
		Enrgt er = (Enrgt) o;
		er.nCode = Integer.parseInt(txtCode.getText());
		er.sRSoc = txtRSoc.getText();
		er.sAdresse = txtAdresse.getText();
		er.sCodePostal = txtCodePostal.getText();
		er.sVille = txtVille.getText();
		er.sPays = txtPays.getText();
	}

	private void RafraichitListe() {
		Liste.removeAllElements();
		ConstruitListe();
		ptr = 0;
	}

	// localise la position dans la liste ou ins�rer un enrgt dont le code est pass�
	int LocalisePosition(int ValCode) {
		int pos = 0;
		if (Liste.size() == 0)
			return 0; // liste vide
		// liste non vide
		Enrgt enr = (Enrgt) Liste.elementAt(pos);
		// on parcourt les �l�ments du vector qui est tri�
		// par code, et on s'arrete d�s que l'on en recontre un dont
		// le code soit sup�rieur � la valeur pass�e.
		while (enr.nCode < ValCode) {
			pos++;
			if (pos < Liste.size())
				enr = (Enrgt) Liste.elementAt(pos);
			else
				break; // fini
		}
		return pos;
	}

	// pour �viter de tenter d'afficher un Null
	private String nonNull(String AAfficher) {
		return (AAfficher == null) ? "" : AAfficher;
	}

} // fin de la classe

// Mod�lise un enregistrement
class Enrgt {
	int nCode;
	String sRSoc;
	String sAdresse;
	String sCodePostal;
	String sVille;
	String sPays;

	// constructeur
	Enrgt(
		int mnCode,
		String msRSoc,
		String msAdresse,
		String msCodePostal,
		String msVille,
		String msPays) {
		nCode = mnCode;
		sRSoc = msRSoc;
		sAdresse = msAdresse;
		sCodePostal = msCodePostal;
		sVille = msVille;
		sPays = msPays;
	}
}
package jdbc.metadatas;

import java.sql.*;

public class LitInfos {

	public static void main(String args[]) {
		Connection c = null; // r�g�rence vers l'objet Connection JDBC
		String driver = null, serveur = null, uid = null, motpasse = null;
		// n�cessaires � la connexion
		String base = null; // le nom de la base � ajouter � l'URL du serveur
		String table = null; // nom optionnel de la table
		try {
			// analyse de la ligne de commande
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-d"))
					driver = args[++i]; // -d <driver>
				else if (args[i].equals("-s"))
					serveur = args[++i]; // -s <serveur>
				else if (args[i].equals("-u"))
					uid = args[++i]; // -u <uid>
				else if (args[i].equals("-p"))
					motpasse = args[++i]; // -p <motpasse>
				else if (args[i].equals("-t"))
					table = args[++i]; // -t <nom table>
				else if (base == null)
					base = args[i]; // <nom base>
				else
					throw new IllegalArgumentException(
						"Argument inconnu : " + args[i]);
			} // fin du for

			// NOTER : dans le cas du bridge JDBCODBC on devra utiliser :
			// -d sun.jdbc.odbc.JdbcOdbcDriver
			// -s jdbc:odbc:
			// <nom base> est le nom de la DSN

			// Dans les autres cas :
			// -d jdbc.MonDriver	(si MonDriver est le nom de classe du driver)
			// -s est l'URL

			// si on n'a rien dit pour l'uid et le mot de passe
			if (uid == null)
				uid = "sa";
			if (motpasse == null)
				motpasse = "";

			// au moins un serveur et unom de base doivent avoir �t� indiqu�s
			if ((serveur.length() == 0) && (base.length() == 0))
				throw new IllegalArgumentException("Pas de nom de base ou de serveur sp�cifi�.");

			// Charge le driver si sp�cifi� (peut etre d�j� charg�)
			if (driver != null)
				Class.forName(driver);

			// Tente d'ouvrie la connexion
			c = DriverManager.getConnection(serveur + base, uid, motpasse);

			// Obtient l'objet DataBaseMetaData pour la connexion.
			DatabaseMetaData md = c.getMetaData();

			// Affichage des donn�es de base
			System.out.println(
				"SGBD : "
					+ md.getDatabaseProductName()
					+ " Version : "
					+ md.getDatabaseProductVersion());
			System.out.println(
				"Driver JDBC : "
					+ md.getDriverName()
					+ " Version : "
					+ md.getDriverVersion());
			System.out.println("Base : " + md.getURL());
			System.out.println("Utilisateur : " + md.getUserName());
			// infos g�n�rales sur la base
			System.out.println("\nInformations g�n�rales sur la Base : ");
			System.out.println("------------------------------------");
			System.out.print("La base utilise un fichier par table : ");
			String s = (md.usesLocalFilePerTable())?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base utilise des fichiers du syst�me de fichier local : ");
			s = (md.usesLocalFiles())?"Oui":"Non";
			System.out.println(s);
			System.out.println("La taille max du nom d'un catalogue est : " + md.getMaxCatalogNameLength() + " cars.");
			System.out.println("La taille max du nom d'une table est : " + md.getMaxTableNameLength() + " cars.");
			System.out.println("La taille max du nom d'une colonne est : " + md.getMaxColumnNameLength() + " cars.");
			System.out.println("La taille max d'une ligne de table est : " + md.getMaxRowSize() + " cars.");
			System.out.println("La taille max d'un nom d'utilisateur est : " + md.getMaxUserNameLength() + " cars.");
			System.out.println("La taille max d'une requ�te SQL est : " + md.getMaxStatementLength() + " cars.");
			System.out.println("Le nombre max de colonnes dans une table est : " + md.getMaxColumnsInTable()); 
			System.out.println("Le nombre max de colonnes dans un SELECT est : " + md.getMaxColumnsInSelect()); 
			System.out.println("Le nombre max de tables dans un SELECT est : " + md.getMaxTablesInSelect()); 
			System.out.println("Le nombre max de connexions simultan�es est : " + md.getMaxConnections()); 			
			System.out.print("La base est ouverte en lecture seule : ");
			s = (md.isReadOnly())?"Oui":"Non";
			System.out.println(s);
			System.out.println("\nSupport de la syntaxe SQL : ");
			System.out.println("--------------------------");
			System.out.print("La base supporte la grammaire SQL minimale : ");
			s = (md.supportsMinimumSQLGrammar())?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte la syntaxe SQL 92 minimale : ");
			s = (md.supportsANSI92EntryLevelSQL())?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte un niveau interm�diaire de syntaxe SQL 92 : ");
			s = (md.supportsANSI92IntermediateSQL())?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte pleinement la syntaxe SQL 92 : ");
			s = (md.supportsANSI92FullSQL())?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte les sous-req�tes corr�l�es : ");
			s = (md.supportsCorrelatedSubqueries())?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte des expressions dans la clause ORDER BY : ");
			s = (md.supportsExpressionsInOrderBy())?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte les jointures externes : ");
			s = (md.supportsOuterJoins())?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte la clause GROUP BY : ");
			s = (md.supportsGroupBy())?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte les proc�dures stock�es : ");
			s = (md.supportsStoredProcedures())?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base permet de faire des UNION de r�sultats de requ�tes : ");
			s = (md.supportsUnion())?"Oui":"Non";
			System.out.println(s);
			System.out.print("Une seule proc�dure ou requ�te peut elle g�n�rer des jeux multiples de r�sultats : ");
			s = (md.supportsMultipleResultSets())?"Oui":"Non";
			System.out.println(s);
			System.out.println("\nSupport des transactions : ");
			System.out.println("------------------------");
			System.out.print("La base supporte les transactions : ");
			s = (md.supportsTransactions())?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte les transactions multiples (� travers des cnx diff�rentes) : ");
			s = (md.supportsMultipleTransactions())?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte le niveau de transaction TRANSACTION_NONE : ");
			s = (md.supportsTransactionIsolationLevel(Connection.TRANSACTION_NONE))?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte le niveau de transaction TRANSACTION_READ_COMMITTED  : ");
			s = (md.supportsTransactionIsolationLevel(Connection.TRANSACTION_READ_COMMITTED))?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte le niveau de transaction TRANSACTION_READ_UNCOMMITTED : ");
			s = (md.supportsTransactionIsolationLevel(Connection.TRANSACTION_READ_UNCOMMITTED))?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte le niveau de transaction TRANSACTION_REPEATABLE_READ : ");
			s = (md.supportsTransactionIsolationLevel(Connection.TRANSACTION_REPEATABLE_READ))?"Oui":"Non";
			System.out.println(s);
			System.out.print("La base supporte le niveau de transaction TRANSACTION_SERIALIZABLE : ");
			s = (md.supportsTransactionIsolationLevel(Connection.TRANSACTION_SERIALIZABLE ))?"Oui":"Non";
			System.out.println(s);
			System.out.print("Le niveau de transaction par d�faut est : ");			
			switch(md.getDefaultTransactionIsolation()) {
				case Connection.TRANSACTION_NONE :
					s = "TRANSACTION_NONE";
					break;
					
				case Connection.TRANSACTION_READ_COMMITTED :
					s = "TRANSACTION_READ_COMMITTED";
					break;
					
				case Connection.TRANSACTION_READ_UNCOMMITTED :
					s = "TRANSACTION_READ_UNCOMMITTED";
					break;
					
				case Connection.TRANSACTION_REPEATABLE_READ :
					s = "TRANSACTION_REPEATABLE_READ";					
					break;
					
				case Connection.TRANSACTION_SERIALIZABLE :
					s = "TRANSACTION_SERIALIZABLE";				
					break;				
			}
			System.out.println(s);
			System.out.println("\nSupport des batches : ");
			System.out.println("--------------------");
			System.out.print("La base supporte les mises � jour par lots : ");
			s = (md.supportsBatchUpdates())?"Oui":"Non";
			System.out.println(s);



			// Si on n'a pas pr�cis� de nom de table, on liste toutes les
			// tables de la base. Le r�sultat est rang� dans un ResultSet
			// comme pour les donn�es ordinaires.
			if (table == null) {
				System.out.println("\nTables : ");
				ResultSet r = md.getTables(null, null, "%", null);
				while (r.next())
					System.out.println("\t" + r.getString(3));
				// nom des tables : 3�me colonne
			} // fin du if

			// Si on a donn� un nom de table, on en liste toutes les colonnes
			else {
				System.out.println("Colonnes de la table " + table + ": ");
				ResultSet r = md.getColumns(null, null, table, "%");
				while (r.next()) // parcourt du r�sultat
					// affiche le nom de la colonne (indice 4), le type (colonne 6) et la taille (colonne 7)
					System.out.println(
						"\t"
							+ r.getString(4)
							+ " : "
							+ r.getString(6)
							+ " : "
							+ r.getString(7));
			} // fin du else
		} // fin du try

		// imprime un message d'erreur en cas de pb
		catch (Exception e) {
			System.out.println(e);
			if (e instanceof SQLException)
				System.out.println(((SQLException) e).getSQLState());
			System.out.println(
				"Utilisation : java LitInfos [-d <driver] [-s <serveur>]\n"
					+ "\t[-u <uid>] [-p <motpasse>] <nom base>");
		} // fin du catch

		// ne pas oublier !
		finally {
			try {
				c.close();
			} catch (Exception e) {
			}
		} // fin du finally
	} // fin du main()
} // fin de la classe

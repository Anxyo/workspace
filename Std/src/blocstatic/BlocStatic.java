// fichier : BlocStatic.java

package blocstatic;

public class BlocStatic {

    /**
     * Constructor.
     */
    public BlocStatic () {
    }

    public static void main(String args[]) {
    Date UneDate = new Date(11,11,18);
    if ((UneDate.format == 1) && (UneDate.langue == 1))
        System.out.println("Fin de la guerre : " + UneDate.toString());	
    else if ((UneDate.format == 2) && (UneDate.langue == 2))
        System.out.println("End of war : " + UneDate.toString());	    
    
    }
}

class Date {
    // variables d'instances
    private int jour, mois, ann�e;
    Date pr�c�dante = null, 
	suivante = null;
    
    // variable de classe
    static int format;	// jj-mm-aa (1) ou mm-jj-aa (2)
    static int langue;  // fran�ais (1) ou anglais (2)  
    
    // et un joli bloc d'initialisation static
    static {
	//format = 1;
	format = 2;
	if (format == 1)
	    langue = 1;
	else
	    langue = 2;    
    }
    
	
    // constructeur sans argument
    public Date() {
	// appelle le constructeur � 3 arguments
	// le 8 d�cembre 1997 est la date par d�faut...
	this(8,12,97);
	// Attention : ceci serait incorrect dans le cas 
	// d'un constructeur, mais correct pour l'appel
	// d'une m�thode. 
	// this.Date(8,12,97);
    }
    
    // constructeur � 3 arguments
    public Date(int j, int m, int a) { 
	jour = j; mois = m; ann�e = a;
    }
    
    public void affecter(int j, int m, int a) { 
	jour = j; mois = m; ann�e = a;
    }

    // on controle le format d'affichage par format    
    public String toString(){
	String s;
	if (format == 1)
	    s = new String("Jour = "+jour+" mois = "+mois+" ann�e = "+ann�e);
	else
	    s = new String("Month = "+mois+" day = "+jour+" year = "+ann�e);    
	return s;
    }    
    
    public void ajouter(Date avant) {
	// se place apr�s, donc...
	pr�c�dante = avant;
	avant.suivante = this;
    }
}


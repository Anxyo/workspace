// fichier anonyme.java

// Utilisation d'une classe interne et / ou
// d'une classe anonyme pour impl�mmenter
// une routine �v�nementielle (usage
// habituel d'une classe anonyme)

package anonyme;
import java.applet.Applet;
import java.awt.event.*;
import java.awt.*;

public class Anonyme extends Applet {
    Point pr�c�dant = null;
   
    public void init() {
    // Version utilisant une classe interne (d�l�gation)
     addMouseListener(new GestSouris());

    /*
    // Version utilisant une classe anonyme
    addMouseListener(new MouseAdapter()		    // cr�ation d'une nouvelle instance qui �tend MouseAdapter
      {	 // d�but classe anonyme
	 int D�finiPourRien = 12;		    // autoris� ici...
	 // Aucun constructeur autoris�,
	 // en effet, quel serait son nom ?
	 
	 // par constre on peut cr�er (depuis JDK 1.1)
	 // un bloc d'initialisation d'instance :
	 {
	    D�finiPourRien = 25;    // sorte de constructeur anonyme
	 }
	 
	public void mousePressed(MouseEvent e){	    // seule m�thode red�finie de MouseAdapter
	    if (pr�c�dant == null)
		pr�c�dant = e.getPoint();
	    else {
		Point p = e.getPoint();
		Graphics g = getGraphics();
		g.drawLine(pr�c�dant.x,pr�c�dant.y, p.x,p.y);
		pr�c�dant = p ;
	    }	// fin else
	  }	// fin m�thode
	}	// fin classe anonyme
	);	// parenth�se fermante de l'appel � addMouseListener()
    */
    }		// fin de init()
    
    

    // Impl�mentation utilisant une classe interne
    class GestSouris extends MouseAdapter {
	public void mousePressed(MouseEvent e){
	    if (pr�c�dant == null)
		pr�c�dant = e.getPoint();
	    else {
		Point p = e.getPoint();
		Graphics g = getGraphics();
		g.drawLine(pr�c�dant.x,pr�c�dant.y, p.x,p.y);
		pr�c�dant = p ;
	    } // fin else
			
	}	// fin m�thode
    }		// fin classe interne
  /**/
}   // fin de la classe principale


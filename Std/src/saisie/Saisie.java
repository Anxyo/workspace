// Saisie.java
// exemple ultra-simple de saisie console
package saisie;

import java.io.*;

public class Saisie {
    public static void main(String args[]) {
    String ligne;
    InputStreamReader isr = new InputStreamReader(System.in);
    BufferedReader in = new BufferedReader(isr);
    do
    {
	try
	{
	// lecture sur le flux d'entr�e
	ligne = in.readLine();
	}
	catch (IOException e) {
	    System.err.println("Erreur de lecture");
	    break;
	}
	
    // envoi en sortie les donn�es lues
    System.out.println(ligne);
    }
    while (!ligne.equals("fin"));
  }
}


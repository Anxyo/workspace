// Exception1.java
// Exemple simple de mise en oeuvre d'exceptions
package exceptions1;

public class Exceptions1 {

   public static void main(String args[]) {
	int [] tab = new int[10];
	int i;
	try {
	    for (i = 0; i < 10; i++)
		tab[i] = i;
	    // ici, pas de pb	
	    System.out.println("Valeur de rang " + 5 + " = " + tab[5]);
	    // ici lev�e d'exception
	    System.out.println("Valeur de rang " + 15 + " = " + tab[15]);
	    System.out.println("Valeur de rang " + 3 + " = " + tab[3]);
	}
	// c'est une exception "non control�e" : pas besoin de clause throws

// Que se passe t'il sion enl�ve les commentaires des 3 lignes ci-dessous ? Pourquoi ?
//	catch (Exception e) {
//	    System.out.println("Index ill�gal : " + e.getMessage());	        
//	}

	catch (ArrayIndexOutOfBoundsException e) {
	    System.out.println("Index ill�gal : " + e.getMessage());	        
	    }

	finally {
	    System.out.println("Bloc finally");
	}    
    }
}


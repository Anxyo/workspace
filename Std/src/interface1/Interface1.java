// Interface1.java
// impl�mentation d'une interface
// tenter de changer DAteFrancaise en DateFran�aise. 
// Que se passe t'il ? A qui la faute ?
package interface1;

public class Interface1 {

    public static void main(String args[]) {
    DateAnglaise da = new DateAnglaise();
    Date d1 = new DateFrancaise();
    Date d2 = da;
    d1.imprimer();
    d2.imprimer();
    //d2.Hello();
    da.Hello();
    }
}

// --------------------------------------------------------------------------

interface Date {
    static final int defJour = 8; // on peut d�fir des constantes.
    int defMois = 10;		  // ici static final est IMPLICITE
    int defAnn�e = 72;		  // ici static final est IMPLICITE �galement
    
    void imprimer();		  // ici abstract et public sont IMPLICITES
}

// --------------------------------------------------------------------------

class DateFrancaise implements Date {	// noter la syntaxe
    private int jour, mois, ann�e;
    
    public DateFrancaise() {
	jour = defJour;
	mois = defMois;
	ann�e = defAnn�e;
    }

    public DateFrancaise(int m, int j, int a) {
	jour = j;
	mois = m;
	ann�e = a;
    }
    
    public void imprimer(){	// doit etre public
	System.out.println(jour + "/" + mois + "/" + ann�e);    
    }
}

// --------------------------------------------------------------------------

class DateAnglaise implements Date {	// noter la syntaxe
    private int jour, mois, ann�e;

    public DateAnglaise() {
	jour = defJour;
	mois = defMois;
	ann�e = defAnn�e;
    }
    
    public DateAnglaise(int m, int j, int a) {
	jour = j;
	mois = m;
	ann�e = a;
    }
    
    public void imprimer(){    // doit etre public
	System.out.println(mois + "/" + jour + "/" + ann�e);        
    }
    public void Hello() {
    
    }
}

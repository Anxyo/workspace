// Exceptions2.java
// d�finition d'une classe d'exception sp�cifique
// et utilisation de  throws / throw
package exceptions2;

public class Exceptions2 {


//  Premi�re version possible de main() : on se sert d'une classe
//  ----------------------------------
//  qui l�ve des exceptions control�es que l'on n'intercepte pas.
//  On DOIT utiliser la clause throws dans l'en-tete de la m�thode
//  puisque l'exception qui sera �ventuellement lev�e va la traverser.
    public static void main(String args[]) throws DateNonConforme {
	Date UneDate = new Date(1,1,1);
	Date DateAPb = new Date(2,15,97);
    }

/*
//  Seconde version possible de main() : on se sert d'une classe
//  ----------------------------------
//  qui l�ve des exceptions control�es que l'on intercepte dans un 
//  bloc try / catch. On n'est plus requis de mettre la clause throws
//  puisque l'exception ne va plus "traverser" cette m�thode.
    public static void main(String args[]) {
	Date UneDate, DateAPb;
	try {
	UneDate = new Date(1,1,1);
	DateAPb = new Date(2,15,97);
	}
	catch (DateNonConforme e) {
	    System.out.println("Exception intercept�e : " + e.getMessage());
	}
	System.out.println("Apr�s le catch");
    }
}
*/

/*
//  Troisi�me version possible de main() : on se sert d'une classe
//  ----------------------------------
//  qui l�ve des exceptions control�es que l'on intercepte EN PARTIE  
//  (pas forc�ment toutes). Il faut mettre la clause throws
//  puisque une exception peut � nouveau potentiellement "traverser" main().
    public static void main(String args[]) throws DateNonConforme {
	Date UneDate = new Date(1,2,3), 
	     DateAPb = new Date(31,32,33);
	try {
	UneDate = new Date(1,1,1);
	DateAPb = new Date(2,15,97);
	}
	catch (DateNonConforme e) {
	    System.out.println("Exception intercept�e : " + e.getMessage());
	}
	System.out.println("Apr�s le catch");
    }
*/    
}





class Date {
    private int jour, mois, ann�e;
    
    // dans le constructeur on fait un controle rustique de validit�
    // de la date. En cas de pb, on l�ve une exception DateNonConforme.
    public Date(int j, int m, int a) throws DateNonConforme {
	// si le jour est invalide, on sort tout de suite
	// meme si le mois est �galement invalide...
	if ((j < 1) || (j > 31)) {
	    throw new DateNonConforme("jour invalide : " + j);
	}
	if ((m < 1) || (m > 12)) {
	    throw new DateNonConforme("mois invalide : " + m);
	}
	jour = j;
	mois = m;
	ann�e = a;
    }
    
    public String toString() {
	return "Date : jour = " + jour + " mois = " + mois + " ann�e = " + ann�e;
    }
}


// construction d'une nouvelle classe d'exception
// h�rit�e d'Exception
class DateNonConforme extends Exception {

    // le constructeur appelle un des constructeurs de la classe de base
    public DateNonConforme(String e) {
        super("Date non Conforme:" + e);
    }
}



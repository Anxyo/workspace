// FichierAffiche.java
// affiche le contenu d'un fichier texte sur la console
// le premier argument de la ligne de commande est le r�pertoire contenant le fichier
// le second est le nom du fichier lui-meme
package fichieraffiche;

import java.io.*;

public class FichierAffiche {

    public static void main(String args[]) {
	
	File f;
	FileReader in = null;
	try {
	    f = new File(args[0], args[1]);	    // Cr�e un objet File
	    in = new FileReader(f);		    // Cr�e un flux de caract�res pour lire
	    int taille = (int) f.length();	    // taille du fichier
	    char[] data = new char[taille];	    // allocation de l'espace n�cessaire
	    int cars_lus = 0;                       // nb de caract�res effectivement lus
	    while(cars_lus < taille)		    // boucle de lecture
		cars_lus += in.read(data, cars_lus, taille - cars_lus);
	    System.out.println(new String(data));   // Affiche le texte
	    }
	    // si un probl�me se pose ...
	    catch (IOException e) { 
		System.out.println("Ah ! Une cagade !");       // pb
	    }

	    finally {		    // il faut fermer le flux !
		try { 
		    if (in != null) 
			in.close(); 
		    } catch (IOException e) {	
			} 
		    }	// fin du finally
    
	  } // fin du main(=
}	   // fin de la classe

  
   
    
 

// Heritage4.java
// classes et m�thodes finales
package heritage4;

public class Heritage4 {
 public static void main(String args[]) {
    Date d1 = new Date(12,12,3256); // un peu plus tard...
    }   
}

// -----------------------------------------------------------------------

class DateAnglaise extends Date {
    // constructeur � 3 arg.
    public DateAnglaise(int m, int j, int a) {
        super(j, m, a) ;        // appel constructeur de la classe de base
    }
    
    // impression "� l'anglaise"
    // impossible de red�finir cette m�thode
    // ===> erreur de compilation ligne suivante
    /*
    public void imprimer() {
        System.out.println(mois + "/" + 
		jour + "/" + ann�e);
    } */
}

// -----------------------------------------------------------------------

class Date {
    protected int jour, mois, ann�e;

    public Date(){  // date par d�faut 
	jour = 8;
	mois = 11;
	ann�e = 97;
    }
    
    public Date(int j, int m, int a){ 
	jour = j;
	mois = m;
	ann�e = a;
    }
    
    // m�thode finale : ne pourra etre red�finie
    final public void imprimer() {
        System.out.println(jour + "/" + 
		mois + "/" + ann�e);    
    }

}

// thread5.java
// Utilisation de synchronized
package threading;

public class Thread5 {

	public static void main(String args[]) {
		Comptage c = new Comptage();
		MonThread t1 = new MonThread("t1", c); // partage de c entre threads
		MonThread t2 = new MonThread("t2", c);
		t1.start();
		t2.start();
		// on attend la mort des threads
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
		}
		// ici les compteurs internes devraient etre affich�s
		// affichage du compteur partag�
		System.out.println("valeur compteur partag� : " + c.valeur);
	}
}

class MonThread extends Thread {
	String s;
	Comptage interne;
	Comptage externe;

	MonThread(String s, Comptage c) {
		// affectation
		this.s = s;
		// instanciation / init des compteurs
		interne = new Comptage();
		externe = c;
	}
	public void run() {
		int i = 0;
		while (i < 1000) {
			interne.incr�mente();
			externe.incr�mente();
			i++;
			yield();
		}
		// affichage compteur interne
		System.out.println("compteur interne : " + interne.valeur);

	}

}

// classe pour la cr�ation d'un objet partag�
class Comptage {
	int valeur;
	public String toString() {
		return "valeur = " + valeur;
	}

	synchronized public void incr�mente(){	// r�sultat correct !
	//public void incr�mente() { // r�sultat incorrect !
		int i = valeur;
		i++;
		// on attend un peu...
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
		}
		valeur = i;
	}
}
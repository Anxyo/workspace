// thread4.java
// autre fa�on de cr�er des threads
// Thread implemente Runnable donc on peut
// directement cr�er une classe d�riv�e de Thread.
package threading;

class ThreadTest4 extends Thread { 
    String s; 
    ThreadTest4(String s)    { 
	this.s = s; }
	 
    public void run() {  
        while (true)  { 
            System.out.println(s); 
            try { 
		Thread.sleep(1000); 
		}  
            
	    catch (InterruptedException e) {  } 
        } 
    } 
} 
 
 
public class Thread4 {
    public static void main(String argv[]) { 
        ThreadTest4 JA = new ThreadTest4("JA"); 
        ThreadTest4 VA = new ThreadTest4("VA"); 
        JA.start(); 
        VA.start(); 
    }        
}

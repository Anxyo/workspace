// Thread2.java
// Pourquoi ne pas mettre la cr�ation de l'objet Thread
// et l'appel � start() dans le constructeur de ThreadTest ?
// Un petit pb... suivant les plates-formes fonctionne
// ou pas...
// WiN32 OK, UNIX pb

// Suivant les plateformes on aura un comportement
// diff�rent pour l'ordonancement des threads de 
// meme priorit�. Dans le cas de threads de priorit�s
// diff�rentes le probl�me est plus simple. Il serait
// n�cessaire, pour que le programme marche DANS TOUS
// LES CAS d'utiliser yield() (voir projet thread6). 
package threading;

class ThreadTest2 implements Runnable { 
    java.lang.String s; 
    Thread t ; 
    ThreadTest2(String s)    {  
        this.s = s;  
        t = new Thread(this) ; 
	t.start() ;  
    } 
    public void run() {  
        while (true)  System.out.println(s);  
    }  
} 
 
public class Thread2 { 
    public static void main(String argv[]) { 
        @SuppressWarnings("unused")
		ThreadTest2 JA = new ThreadTest2("JA");
        @SuppressWarnings("unused")
        ThreadTest2 VA = new ThreadTest2("VA"); 
    }        
}

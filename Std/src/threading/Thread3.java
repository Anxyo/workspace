// Thread3.java
// pour donner un peu d'oxyg�ne � chaque thread
// on repasse explicitement le controle en endormant le thread courant
// -> ex�cution beaucoup plus r�guli�re... 
// Attention : ce sommeil est lourd... Pour le stopper retirer les
// commentaires : appel de interrupt().
package threading;

class ThreadTest3 implements Runnable { 
    java.lang.String s; 
    Thread t ; 
    ThreadTest3(String s)    {  
        this.s = s;  
        t = new Thread(this) ; 
	t.start() ;   
    } 
    
    public void run() {  
	
	// temporisation raisonnable
        while (true)  
	{

	    System.out.println(s);  
/*	    try {t.sleep(100);	// dodo
	    }
	    catch (InterruptedException e) {}
	}
*/
	// temporisation beaucoup moins raisonable !
	try {   Thread.sleep(1000 * 60 * 60); }
	catch (InterruptedException e) {
	System.out.println(s + " ! Pourquoi m'avoir r�veill�, je dormais si bien !");
	}
}}  
} 
 
public class Thread3 { 
    public static void main(String argv[]) { 
	System.out.println("D�marrage des threads secondaires");
        ThreadTest3 JA = new ThreadTest3("JA"); 
        ThreadTest3 VA = new ThreadTest3("VA"); 
	
	
	// le thread principal s'endort 10 secondes 
	try { Thread.sleep(10000); }
	    catch (InterruptedException e) {}
	System.out.println("Je suis le thread principal et j'ai dormis 10 s");
	JA.t.interrupt();	// debout !
	VA.t.interrupt();	// debout ! 
    }        
}



package Metier;

public class Produit {

	int codeProd;
	String nomProduit;
	float prixUnitaire;
	String image;
	String descriptif;
	int quantite;
	
	
	public Produit(int codeProd, String nomProduit, int prixUnitaire, String image, String descriptif) {
		super();
		this.codeProd = codeProd;
		this.nomProduit = nomProduit;
		this.prixUnitaire = prixUnitaire;
		this.image = image;
		this.descriptif = descriptif;
	}


	public int getCodeProd() {
		return codeProd;
	}


	public void setCodeProd(int codeProd) {
		this.codeProd = codeProd;
	}


	public String getNomProduit() {
		return nomProduit;
	}


	public void setNomProduit(String nomProduit) {
		this.nomProduit = nomProduit;
	}


	public float getPrixUnitaire() {
		return prixUnitaire;
	}


	public void setPrixUnitaire(float prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public String getDescriptif() {
		return descriptif;
	}


	public void setDescriptif(String descriptif) {
		this.descriptif = descriptif;
	}


	@Override
	public String toString() {
		return "Produit:\n codeProd= " + codeProd + ",\n nomProduit= " + nomProduit + ",\n prixUnitaire= " + prixUnitaire
				+ ",\n image= " + image + ",\n descriptif= " + descriptif;
	}
	
	
	
	
}

package Metier;

import java.util.ArrayList;

public class Client {

	int idClient;
	String nomClient;
	String prenomClient;
	String adresse;
	
	ArrayList<Commande> commande;
	
	
	public Client(int idClient, String nomClient, String prenomClient, String adresse) {
		super();
		this.idClient = idClient;
		this.nomClient = nomClient;
		this.prenomClient = prenomClient;
		this.adresse = adresse;
		commande = new ArrayList<Commande>();
	}
	

	
	public int getIdClient() {
		return idClient;
	}


	public String getNomClient() {
		return nomClient;
	}


	public String getPrenomClient() {
		return prenomClient;
	}


	public String getAdresse() {
		return adresse;
	}


	public ArrayList<Commande> getCommande() {
		return commande;
	}



	@Override
	public String toString() {
		return "Client [idClient=" + idClient + ", nomClient=" + nomClient + ", prenomClient=" + prenomClient
				+ ", adresse=" + adresse + "]";
	}
	
	
	
}

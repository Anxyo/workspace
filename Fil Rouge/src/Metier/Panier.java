package Metier;

import java.util.ArrayList;
import java.util.List;

public class Panier {

	public List<OptionAchat> detailPanier;

	public Panier(List<OptionAchat> optionAchats) {
		optionAchats = new ArrayList<OptionAchat>();
	}

	
	public void ajoutPanier(OptionAchat optionAchats) {
		detailPanier.add(optionAchats);
	}


	public List<OptionAchat> getDetailPanier() {
		return detailPanier;
	}


	@Override
	public String toString() {
		return "Panier [detailPanier= " + detailPanier + "]";
	}
	
	
	
	
}

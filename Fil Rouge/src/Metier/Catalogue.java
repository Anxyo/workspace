package Metier;

import java.util.List;
import java.util.Vector;

public class Catalogue {

	private static Catalogue leCatalogue = null;  //bloque la possibilit� de cr�er plusieurs catalogues
	private static List<Produit> produits;
	
	public OptionAchat optAchat;

	
	//Constructeur: initialiser notre catalogue
	private Catalogue() {
		produits = new Vector<Produit>();
		initCatalogue();
	}
	

	//Simulation d'une base de donn�e
	public void initCatalogue() {
		Produit p1 = new Produit(0, "chat noir", 10, "jolie photo", "joli petit chat � vendre" );
		Produit p2 = new Produit(1, "chat blanc", 15, "jolie photo", "joli petit chat � vendre" );
		Produit p3 = new Produit(2, "chat vert", 50, "jolie photo", "joli petit chat � vendre" );
		ajoutProdCata(p1);
		ajoutProdCata(p2);
		ajoutProdCata(p3);
		OptionAchat optAchat = new OptionAchat(p3,2);
	}
	
	
	public static Catalogue getInstance() {
		if (leCatalogue == null) {
			leCatalogue = new Catalogue();
		}
		return leCatalogue;
	}
	
	
	//ajoute les produits dans notre catalogue
	public void ajoutProdCata(Produit p) {
		produits.add(p);	
	}
	
	
	
	public List<Produit> getList() {
		return produits; 
	}


	public int getNbProduits() {
		int nbProds = produits.size();
		return nbProds;
	}

	
	
	
	


	
	
	
	
}

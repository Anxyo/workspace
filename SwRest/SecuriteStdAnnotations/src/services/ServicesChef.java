package services;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

@Path("/chefs")
@RolesAllowed("chef")
@Produces(MediaType.TEXT_PLAIN)
public class ServicesChef {
	@Context
    UriInfo uri;	
	
	@GET
	public String getChefs() {
		return "getChefs(), uri = " + uri.getPath();
	}
	
	@GET
	@Path("{nom}")
	public String getChef(@PathParam("nom") String nom) {
		return "getChef(), nom = " + nom + ", uri = " + uri.getPath();
	}
	
}

package metier;

import java.util.List;

public interface ICatalogue {

	///////////////////////////////// M�thodes d'instance ////////////////////////////
	/**
	 * Retourne la liste des produits du catalogue
	 * @return
	 */
	List<Produit> getProduits();

	/**
	 * Ajoute un produit au catalogue
	 * @param p le produit � ajouter
	 */
	void ajoutProduit(Produit p);

	/**
	 * Retourne le nombre de produits au catalogue
	 * @return
	 */
	int getNbProduits();
}
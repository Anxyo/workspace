package metier;

import com.google.gson.annotations.Expose;

/**
 * Repr�sente une ligne de d�tail dans une commande.
 * Reprend la structure d'une ligne de d�tail du panier,
 * avec, en plus, une r�f�rence vers la commande associ�e.
 * @author Administrateur
 *
 */

public class DetailCommande {
	@Expose(serialize = true)
	private Integer id;
	
	@Expose(serialize = false)
	private Commande commande;
	
	@Expose(serialize = true)
	private Produit produit;
	
	@Expose(serialize = true)
	private int quantite;
	
	@Expose(serialize = true)
	private double sousTotal;
	
	public DetailCommande() {}	// n�cessaire pour JPA

	public DetailCommande(Commande commande, DetailPanier dp) {
		this.commande = commande;
		produit = dp.getProduit();
		quantite = dp.getQuantite();
		sousTotal = dp.getSousTotal();
	}
	
	// accesseurs	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Commande getCommande() {
		return commande;
	}
	public Produit getProduit() {
		return produit;
	}
	public int getQuantite() {
		return quantite;
	}
	public double getSousTotal() {
		return sousTotal;
	}

	public void setCommande(Commande commande) {
		this.commande = commande;
	}

	public void setProduit(Produit produit) {
		this.produit = produit;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public void setSousTotal(double sousTotal) {
		this.sousTotal = sousTotal;
	}

	@Override
	public String toString() {
		return "Produit : " + produit.getNom() + " " + produit.getDescription() +
				", qut� : " + quantite + ", sous total : " + sousTotal;
	}
}

package metier;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.annotations.Expose;
/**
 * Repr�sente une commande pass�e par un client.
 * Elle reprend le contenu du panier, et les objets
 * DetailPanier serviront � cr�er les DetailCommande.
 * @author Administrateur
 *
 */

public class Commande {
	@Expose(serialize = true)
	private Integer id;
	
	@Expose(serialize = true)
	private Date dateCommande;
	
	@Expose(serialize = true)
	private double total;
	
	@Expose(serialize = false)
	private Client client;
	
	@Expose(serialize = true)
	private List<DetailCommande> details;
	
	// constructeurs
	public Commande() {} // n�cessaire pour JPA
	public Commande(Client client, Integer id, Date dateCommande, double total) {
		this.client = client;
		this.id = id;
		this.dateCommande = dateCommande;
		this.total = total;
		details = new ArrayList<DetailCommande>();
	}
	
	// accesseurs
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getDateCommande() {
		return dateCommande;
	}
	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public List<DetailCommande> getDetails() {
		return details;
	}	
	
	// methodes
	/**
	 * Ajoute une ligne de d�tail � cette commande, � partir
	 * d'une option d'achat du panier.
	 * @param dp Le DetailPanier � ajouter
	 */
	public void ajouteDetailCommande(DetailPanier dp) {
		details.add(new DetailCommande(this,dp));
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Commande no ");
		sb.append(String.valueOf(id));
		sb.append(", ");
		sb.append(client.toString());
		sb.append(", total : ");
		sb.append(String.valueOf(total));
		sb.append(" :\n");
		for (DetailCommande dc : details) {
			sb.append(" - " + dc + "\n");
		}
		return sb.toString();
	}
}

package metier;

public class DetailPanier {
	private Produit produit;
	private int quantite;
	private double sousTotal;
	
	// constructeur
	public DetailPanier(Produit produit, int quantite, double sousTotal) {
		this.produit = produit;
		this.quantite = quantite;
		this.sousTotal = sousTotal;
	}
	// accesseurs
	public Produit getProduit() {
		return produit;
	}
	public void setProduit(Produit produit) {
		this.produit = produit;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	public double getSousTotal() {
		return sousTotal;
	}
	public void setSousTotal(double sousTotal) {
		this.sousTotal = sousTotal;
	}
}

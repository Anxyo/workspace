package presentation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import metier.Client;
import metier.Commande;
import metier.DetailCommande;
import metier.Produit;
import presentation.dto.CommandeDTO;
/**
 * Bilan de modification des classes m�tier :
 *     Ajouter des setters aux DetailCommande ? Normalement non,
 *     car cr��s � partir d'un panier.
 * 
 * @author Administrateur
 *
 */
public class Principale {

	public static void main(String[] args) {
	// d�finition de quelques produits
	List<Produit> produits = new ArrayList<Produit>();
	List<Commande> commandes = new ArrayList<Commande>();
	List<CommandeDTO> cmdDTO = new ArrayList<CommandeDTO>();
	
	produits.add(new Produit(1,"Johnny Halliday", "Les 100 plus belles chansons",119.90,null));
	produits.add(new Produit(2,"Johnny Halliday", "Premier concert enregistr�",17.53,null));
	produits.add(new Produit(3,"Johnny Halliday", "Marie",1.89,null));
	produits.add(new Produit(4,"Johnny Halliday", "Ca ne finira jamais",10.99,null));
	produits.add(new Produit(5,"Johnny Halliday", "Olympia 2000",16.91,null));
	
	// cr�ation d'un client
	Client clt = new Client("Brugnard", "Raoul", "15 Bd Pasteur - 75015 - Paris");	
	// ajout d'une ou deux commandes
	Commande cmd1 = new Commande(clt,1,new Date(), 1000);
	Commande cmd2 = new Commande(clt,2,new Date(), 500);
	commandes.add(cmd1);
	commandes.add(cmd2);
	DetailCommande dc = new DetailCommande();
	dc.setId(1);
	dc.setCommande(cmd1);
	dc.setProduit(produits.get(0));
	dc.setQuantite(1);
	dc.setSousTotal(119.90);
	cmd1.getDetails().add(dc);
	dc = new DetailCommande();
	dc.setId(2);
	dc.setCommande(cmd1);
	dc.setProduit(produits.get(1));
	dc.setQuantite(2);
	dc.setSousTotal(35.06);
	cmd1.getDetails().add(dc);
	dc = new DetailCommande();
	dc.setId(3);
	dc.setCommande(cmd2);
	dc.setProduit(produits.get(2));
	dc.setQuantite(2);
	dc.setSousTotal(3.78);
	cmd2.getDetails().add(dc);
	// construction d'une liste des DTO associ�s
	 for (Commande cmd : commandes) {
		CommandeDTO dto = new CommandeDTO(cmd);
		cmdDTO.add(dto);
	}
	 
	 GsonBuilder builder1 = new GsonBuilder();
	 builder1.setPrettyPrinting();
	 builder1.setDateFormat("yyyy-MM-dd");
	 Gson gs1 = builder1.create();
	 
	 GsonBuilder builder2 = new GsonBuilder();
	 builder2.excludeFieldsWithoutExposeAnnotation();
	 builder2.setPrettyPrinting();
	 builder2.setDateFormat("yyyy-MM-dd");
	 Gson gs2 = builder2.create();
	 
	 String jsonDTO = gs1.toJson(cmdDTO);
	 System.out.println("JSON DTO : " + jsonDTO);
	 String jsonDirect = gs2.toJson(commandes);
	 System.out.println("JSON Direct : " + jsonDirect);
	 
  }
}

package presentation.dto;

import metier.DetailCommande;
import metier.Produit;

public class DetailCommandeDTO {
	private Produit produit;
	private int quantite;
	private double sousTotal;
	
	public DetailCommandeDTO(DetailCommande dc) {
		this.produit = dc.getProduit();
		this.quantite = dc.getQuantite();
		this.sousTotal = dc.getSousTotal();
	}

	public Produit getProduit() {
		return produit;
	}

	public void setProduit(Produit produit) {
		this.produit = produit;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public double getSousTotal() {
		return sousTotal;
	}

	public void setSousTotal(double sousTotal) {
		this.sousTotal = sousTotal;
	}
}

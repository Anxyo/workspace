var json = `[
  {
    "id": 1,
    "dateCommande": "2018-01-16",
    "total": 1000.0,
    "details": [
      {
        "produit": {
          "id": 1,
          "nom": "Johnny Halliday",
          "description": "Les 100 plus belles chansons",
          "prix": 119.9
        },
        "quantite": 1,
        "sousTotal": 119.9
      },
      {
        "produit": {
          "id": 2,
          "nom": "Johnny Halliday",
          "description": "Premier concert enregistré",
          "prix": 17.53
        },
        "quantite": 2,
        "sousTotal": 35.06
      }
    ]
  },
  {
    "id": 2,
    "dateCommande": "2018-01-16",
    "total": 500.0,
    "details": [
      {
        "produit": {
          "id": 3,
          "nom": "Johnny Halliday",
          "description": "Marie",
          "prix": 1.89
        },
        "quantite": 2,
        "sousTotal": 3.78
      }
    ]
  }
]`;
var obj1 = JSON.parse(json);
console.log(obj1[0].dateCommande);
var uneDate = new Date(obj1[1].dateCommande);
console.log(uneDate.toString());

package services;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Classe application. D�rive de la classe abstraite Application, dont voici la structure :
 * 
 * public abstract class Application {
 * 		private static final Set<Object> emptySet = Collections.emptySet();
 * 		public abstract Set<Class<?>> getClasses();
 * 		public Set<Object> getSingletons() {
 * 			return emptySet;
 * 		}
 * 	}
 * La classe Application sert � simplifier le d�ploiement en enregistrant les classes de 
 * ressources aupr�s du serveur.
 *  
 * @author JM
 *
 */
@ApplicationPath("/services/*")
public class Rest4Application extends Application {

	// attributs de support des mod�les d'activation de JAX-RS : 
	//     par requete ou singletons 
	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> instParRequete = new HashSet<Class<?>>();
	
	// instanciation des objets ressource
	public Rest4Application() {
		singletons.add(new RessourceClient());
	}
	
	// retourne un Set d'instances de classes de ressources instanci�s pour chaque requete
	@Override
	public Set<Class<?>> getClasses() {
		return instParRequete;
	}
	
	// retourne un Set d'instances de classes de ressources singletons
	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}

/*
  <client>
   <prenom>Max</prenom>
   <nom>LaMenace</nom>
   <adresse>12 rue du Bois</adresse>
   <ville>Nantes</ville>
   <zip>44000</zip>
 </client>
*/

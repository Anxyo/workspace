package client;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

public class InvocationClient {

	public static void main(String[] args) {
		Client client = ClientBuilder.newClient();
		
		//HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("sylvain", "niavlys");
		HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("jm", "mj");

		client.register(feature);
		
		String urlCible = "http://localhost:8080/SecuriteStd/rest/chefs";
		Builder builder = client.target(urlCible).request();
		String reponse = null;
		try {
			reponse = builder.get(String.class);
			System.out.println(reponse);
		} catch (NotAuthorizedException nae) {
			System.out.println("Acc�s non autoris� : " + nae.getMessage());
		}catch (ForbiddenException fe) {
			System.out.println("Acc�s interdit : " + fe.getMessage());
		}
		
		client.close();

	}

}

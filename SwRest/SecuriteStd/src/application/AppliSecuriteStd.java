package application;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * Suppose la définition de ce tomcat-users.xml
 
 <tomcat-users version="1.0" xmlns="http://tomcat.apache.org/xml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://tomcat.apache.org/xml tomcat-users.xsd">
	  <role rolename="chef"/>
	  <role rolename="clampin"/>
	  <user password="neitsabes" roles="chef" username="sebastien"/>
	  <user password="niavlys" roles="chef" username="sylvain"/>
	  <user password="mj" roles="clampin" username="jm"/>
</tomcat-users>

 */


@ApplicationPath("rest")
public class AppliSecuriteStd extends ResourceConfig {
	public AppliSecuriteStd() {
		packages("services");
	}
}

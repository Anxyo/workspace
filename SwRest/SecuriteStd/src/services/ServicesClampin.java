package services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

@Path("/clampins")
@Produces(MediaType.TEXT_PLAIN)
public class ServicesClampin {
	@Context
    UriInfo uri;	
	
	@GET
	public String getClampins() {
		return "getClampins(), uri = " + uri.getPath();
	}
	
	@GET
	@Path("{nom}")
	public String getClampin(@PathParam("nom") String nom) {
		return "getClampin(), nom = " + nom + ", uri = " + uri.getPath();
	}
}

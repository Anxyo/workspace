package test;

import static org.junit.Assert.assertTrue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class LivreTest {
	
	private static Client client;
	
	@BeforeClass
	public static void init() {
		// une seule instance ('heavyweight' !) est r�utilis�e par chacun des tests
		client = ClientBuilder.newClient();
	}

	@AfterClass
	public static void close() {
		// fermeture connexion r�seau (remise dans le pool de la cnx)
		client.close();
	}
	
	@Test
	public void testGetLivre1() {
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest3/services/livres/matrix/Decoin/1967/Laurence;genre=roman";
		Builder builder = client.target(urlCible).request();
		String reponse = builder.get(String.class);
		System.out.println(reponse);
		assertTrue(reponse.contains("Decoin"));
		
	}

	@Test
	public void testGetLivre2() {
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest3/services/livres/segment/Decoin/Laurence;genre=roman/1967";
		Builder builder = client.target(urlCible).request();
		String reponse = builder.get(String.class);
		System.out.println(reponse);
		assertTrue(reponse.contains("Decoin"));		
	}

	@Test
	public void testGetLivre3() {
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest3/services/livres/segments/Decoin/La mise au monde/Laurence/annee/1967-1969";
		Builder builder = client.target(urlCible).request();
		String reponse = builder.get(String.class);
		System.out.println(reponse);
		assertTrue(reponse.contains("Decoin"));		
	}
	

	@Test
	public void testGetLivre4() {
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest3/services/livres/uriinfo/Decoin/Laurence;genre=roman/1967";
		Builder builder = client.target(urlCible).request();
		String reponse = builder.get(String.class);
		System.out.println(reponse);
		assertTrue(reponse.contains("Decoin"));		
	}

}

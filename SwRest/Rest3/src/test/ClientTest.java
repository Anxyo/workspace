package test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.Response;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe de tests JUnit utilisant l'API cliente de JAX-RS, vue plus tard. 
 *
 */
public class ClientTest {
	private static Client client;
	
	@BeforeClass
	public static void init() {
		// une seule instance ('heavyweight' !) est r�utilis�e par chacun des tests
		client = ClientBuilder.newClient();
	}

	@AfterClass
	public static void close() {
		// fermeture connexion r�seau (remise dans le pool de la cnx)
		client.close();
	}

	@Test
	public void testGetClient() {
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest3/services/clients/1";
		Builder builder = client.target(urlCible).request();
		assertTrue(builder.get(String.class).contains("LaMenace"));
		
	}

	@Test
	public void testParametresClient() {
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest3/services/clients/1";
		Builder builder = client.target(urlCible).request();
		assertTrue(builder.get(String.class).contains("LaMenace"));
	}
	
	@Test
	public void testParametresRequete2() {
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest3/services/clients?debut=0&nbre=2";
		Builder builder = client.target(urlCible).request();
		String reponse = builder.get(String.class); 
		assertTrue(reponse.contains("LaMenace")&&reponse.contains("Howe"));
	}
	
	@Test
	public void testParametresRequete3() {
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest3/services/clients/uriinfo?debut=0&nbre=2";
		Builder builder = client.target(urlCible).request();
		String reponse = builder.get(String.class); 
		assertTrue(reponse.contains("LaMenace")&&reponse.contains("Howe"));	
	}
	
}

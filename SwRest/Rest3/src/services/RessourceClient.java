package services;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;

import metier.Client;

@Path("/clients")
public class RessourceClient {
	private Map<Integer, Client> clients = new ConcurrentHashMap<Integer, Client>();
	private AtomicInteger idCourant = new AtomicInteger();

	public RessourceClient() {
		// ajoute quelques clients permettant de tester tout de suite le GET

		Client client = new Client();
		client.setId(idCourant.incrementAndGet());
		client.setPrenom("Max");
		client.setNom("LaMenace");
		client.setAdresse("12 rue du Bois");
		client.setVille("Nantes");
		client.setZip("44000");
		clients.put(client.getId(), client);

		client = new Client();
		client.setId(idCourant.incrementAndGet());
		client.setPrenom("Steve");
		client.setNom("Howe");
		client.setAdresse("1 rue du Oui");
		client.setVille("Etampes");
		client.setZip("91300");
		clients.put(client.getId(), client);

		client = new Client();
		client.setId(idCourant.incrementAndGet());
		client.setPrenom("Vincent");
		client.setNom("Peirani");
		client.setAdresse("7 rue de l'Accordeon");
		client.setVille("Paris");
		client.setZip("75005");
		clients.put(client.getId(), client);

		client = new Client();
		client.setId(idCourant.incrementAndGet());
		client.setPrenom("Stochelo");
		client.setNom("Rosenberg");
		client.setAdresse("12 rue des Manouches");
		client.setVille("Samois sur seine");
		client.setZip("77920");
		clients.put(client.getId(), client);

		client = new Client();
		client.setId(idCourant.incrementAndGet());
		client.setPrenom("Ulf");
		client.setNom("Wakenius");
		client.setAdresse("3 allee Youn Sun Nah");
		client.setVille("Glandieu");
		client.setZip("01300");
		clients.put(client.getId(), client);

		
	}

	// M�thodes publi�es
	// //////////////////

	// Obtention d'un client, � partir de son id
	@GET
	@Path("{id}")
	@Produces("application/xml")
	public StreamingOutput getClient(@PathParam("id") int id) {
		final Client client = clients.get(id);
		if (client == null) {
			// d�clenche une exception packageant une 'ressource non trouv�e'
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		// retourne une repr�sentation XML du client trouv�, g�n�r�e la m�thode
		// utilitaire ecritXMLClient()
		return new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException,
					WebApplicationException {
				ecritClient(outputStream, client);
			}
		};
	}

	@GET
	@Produces("application/xml")
	public StreamingOutput getClients(	final @QueryParam("debut") @DefaultValue("1") int debut,
										final @QueryParam("nbre")  @DefaultValue("2") int nbre) {
		// retourne une instance d'une classe anonyme impl�mentant l'interface
		// StreamingOutput
		// (une seule m�thode � impl�menter : write())
		return new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException,
					WebApplicationException {
				int i = 0;
				PrintStream writer = new PrintStream(outputStream);
				writer.println("<clients>");
				for (Client clt : clients.values()) {
					if (i >= debut && i < debut + nbre)
						ecritClient(writer, clt);
					i++;
				}
				writer.println("</clients>");
			}
		};
	}

	// Obtention de plusieurs clients, 2eme technique
	@GET
	@Path("uriinfo")	// sert � diff�rentier de l'URI de la requete precedente
	@Produces("application/xml")
	public StreamingOutput getClients(@Context UriInfo info) {
		 int debut = 0;
	     int nbre = 2;
	     // tous les param�tres de la requ�te sont pr�sent dans l'instance de l'UriInfo re�ue.
	     if (info.getQueryParameters().containsKey("debut")){
	         debut = Integer.valueOf(info.getQueryParameters().getFirst("debut"));
	     }
	     if (info.getQueryParameters().containsKey("nbre")){
	         nbre = Integer.valueOf(info.getQueryParameters().getFirst("nbre"));
	     }
	     return getClients(debut, nbre);
	}

	/**
	 * Ecriture d'un bloc XML repr�sentant un client, dans un flux de sortie.
	 * 
	 * @param os
	 *            Le flux utilis�
	 * @param client
	 *            le client � �crire
	 */
	private void ecritClient(OutputStream os, Client client) {
		PrintStream writer = new PrintStream(os);
		writer.println("<client id=\"" + client.getId() + "\">");
		writer.println(" <prenom>" + client.getPrenom() + "</prenom>");
		writer.println(" <nom>" + client.getNom() + "</nom>");
		writer.println(" <adresse>" + client.getAdresse() + "</adresse>");
		writer.println(" <ville>" + client.getVille() + "</ville>");
		writer.println(" <zip>" + client.getZip() + "</zip>");
		writer.println("</client>");
	}
}

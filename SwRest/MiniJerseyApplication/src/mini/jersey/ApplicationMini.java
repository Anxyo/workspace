package mini.jersey;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("rest")
public class ApplicationMini extends Application {
	private Set<Class<?>> instParRequete = new HashSet<Class<?>>();
	
	public ApplicationMini() {
		instParRequete.add(ServiceMini.class);
	}
	
	@Override
	public Set<Class<?>> getClasses() {
		return instParRequete;
	}
	
	
}

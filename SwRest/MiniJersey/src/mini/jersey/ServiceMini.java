package mini.jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/mini")
public class ServiceMini {

	  // produit du text/xml
	  @GET
	  @Produces(MediaType.TEXT_XML)
	  public String xmlMini() {
	    return "<?xml version=\"1.0\"?>\n<mini>ServiceMini, en xml...</mini>";
	  }

	  // produit du text/html
	  @GET
	  @Produces(MediaType.TEXT_HTML)
	  public String htmlMini() {
	    return "<html><head><title>ServiceMini</title></head>"
	        + "<body><h1 style='text-align: center'>ServiceMini, en HTML...</h1></body></html>";
	  }	
}

package test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.Response;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe de tests JUnit utilisant l'API cliente de JAX-RS, vue plus tard. 
 *
 */
public class ClientTest {
	private static Client client;
	
	@BeforeClass
	public static void init() {
		// une seule instance ('heavyweight' !) est r�utilis�e par chacun des tests
		client = ClientBuilder.newClient();
	}

	@AfterClass
	public static void close() {
		// fermeture connexion r�seau (remise dans le pool de la cnx)
		client.close();
	}

	@Test
	public void testGetClient() {
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest1Interfaces/services/clients/19";
		Builder builder = client.target(urlCible).request();
		assertTrue(builder.get(String.class).contains("LaMenace"));
		
	}

	@Test
	public void testCreeClient() {
		StringBuilder sb = new StringBuilder();
		sb.append("<client>");
		sb.append("<prenom>Pierre</prenom>");
		sb.append("<nom>Dupont-Lajoie</nom>");
		sb.append("<adresse>23 rue Robert Lindet</adresse>");
		sb.append("<ville>Paris</ville>");
		sb.append("<zip>75015</zip>");
		sb.append("</client>");
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest1Interfaces/services/clients";
		Builder builder = client.target(urlCible).request();
		Response reponse = builder.post(Entity.xml(sb.toString()));
		assertEquals(201,reponse.getStatus());	// cr�� ?
	}
}

package services;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;




/**
 * On a regroup� dans une interface le contrat publi� d'une ressource.
 * C'est une approche souvent souhaitable, d�couplant le contrat du service
 * de son impl�mentation.
 *
 */

public interface IRessourceClient {

	/**
	 * 	Cr�ation d'un client
	 * @param is le flux d'entr�e
	 * @return une r�ponse indiquant que l'objet a bien �t� cr�� (code HTTP 201)
	 */
	@POST
	@Consumes("application/xml")
	Response creeClient(InputStream is);
	
	/**
	 * Obtention d'un client, � partir de son id
	 * @param id l'id du client
	 * @return un flux de sortie support de la g�n�ration du client demand�
	 */
	@GET
	@Path("{id}")
	@Produces("application/xml")
	public StreamingOutput getClient(@PathParam("id") int id);

	
}

package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Classe de tests JUnit utilisant l'API cliente de JAX-RS, vue plus tard. 
 *
 */
public class ClientTest {
	private static Client client;
	
	@BeforeClass
	public static void init() {
		// une seule instance ('heavyweight' !) est r�utilis�e par chacun des tests
		client = ClientBuilder.newClient();
	}

	@AfterClass
	public static void close() {
		// fermeture connexion r�seau (remise dans le pool de la cnx)
		client.close();
	}

	@Test
	public void testGetClient() {
		String urlCible = "http://localhost:8080/Rest6/services/clients/19";
		String leClient = client.target(urlCible).request().get(String.class);
		assertTrue(leClient.contains("LaMenace"));
	}

	@Test
	public void testCreeClient() {
		String clt = "{\"id\":19,\"nom\":\"Howe\",\"prenom\":\"Steve\",\"adresse\":\"1 rue du Oui\",\"ville\":\"Etampes\",\"zip\":\"91300\"}";
			Response reponse = client.target("http://localhost:8080/Rest6/services/clients")
	              .request().post(Entity.json(clt));
		assertEquals(201,reponse.getStatus());	// cr�� ?
	}
}

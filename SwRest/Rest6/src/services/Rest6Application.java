package services;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@ApplicationPath("/services/*")
public class Rest6Application extends Application {

	// attributs de support des mod�les d'activation de JAX-RS : 
	//     par requete ou singletons 
	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> instParRequete = new HashSet<Class<?>>();
	
	// instanciation des objets ressource
	public Rest6Application() {
		singletons.add(new RessourceClient());
		singletons.add(new JacksonJsonProvider());
	}
	
	// retourne un Set d'instances de classes de ressources instanci�s pour chaque requete
	@Override
	public Set<Class<?>> getClasses() {
		return instParRequete;
	}
	
	// retourne un Set d'instances de classes de ressources singletons
	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}


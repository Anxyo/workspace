package services;

import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import metier.Client;


@Path("/clients")
public class RessourceClient {
	private Map<Integer, Client> clients = new ConcurrentHashMap<Integer, Client>();
	private AtomicInteger idCourant = new AtomicInteger();

	public RessourceClient() {
		// ajoute un client permettant de tester tout de suite le GET
		Client client = new Client();
		client.setId(19);
		client.setPrenom("Max");
		client.setNom("LaMenace");
		client.setAdresse("12 rue du Bois");
		client.setVille("Nantes");
		client.setZip("44000");
		clients.put(client.getId(), client);
	}

	// M�thodes publi�es
	// //////////////////
	/**
	 * Cr�ation d'un client
	 * 
	 * @param is le flux d'entr�e
	 * @return une r�ponse indiquant que l'objet a bien �t� cr�� (code HTTP 201)
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response creeClient(Client client) {
		client.setId(idCourant.incrementAndGet());
		clients.put(client.getId(), client);
		System.out.println("Client cr��, id = " + client.getId());
		return Response.created(URI.create("/clients/" + client.getId()))
				.build();
	}

	// Obtention d'un client, � partir de son id
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Client getClient(@PathParam("id") int id) {
		final Client client = clients.get(id);
		if (client == null) {
			// d�clenche une exception packageant une 'ressource non trouv�e'
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		// retourne une repr�sentation JSON du client trouv�
		return client;
	}
}

package presentation.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import metier.Commande;
import metier.DetailCommande;

public class CommandeDTO {
	private Integer id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date dateCommande;
	private double total;
	private List<DetailCommandeDTO> details =  new ArrayList<DetailCommandeDTO>();
	public CommandeDTO() {}
	public CommandeDTO(Commande cmd) {
		this.id = cmd.getId();
		this.dateCommande = cmd.getDateCommande();
		this.total = cmd.getTotal();
		for (DetailCommande dc : cmd.getDetails()) {
			DetailCommandeDTO dcDTO = new DetailCommandeDTO(dc);
			details.add(dcDTO);
		}
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getDateCommande() {
		return dateCommande;
	}
	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public List<DetailCommandeDTO> getDetails() {
		return details;
	}
	public void setDetails(List<DetailCommandeDTO> details) {
		this.details = details;
	}
}

var json = `[ {
  "id" : 1,
  "dateCommande" : "2018-01-16",
  "total" : 154.96,
  "details" : [ {
    "id" : 1,
    "produit" : {
      "id" : 1,
      "nom" : "Johnny Halliday",
      "description" : "Les 100 plus belles chansons",
      "prix" : 119.9,
      "urlImage" : null
    },
    "quantite" : 1,
    "sousTotal" : 119.9
  }, {
    "id" : 2,
    "produit" : {
      "id" : 2,
      "nom" : "Johnny Halliday",
      "description" : "Premier concert enregistré",
      "prix" : 17.53,
      "urlImage" : null
    },
    "quantite" : 2,
    "sousTotal" : 35.06
  } ]
}, {
  "id" : 2,
  "dateCommande" : "2018-01-16",
  "total" : 3.78,
  "details" : [ {
    "id" : 3,
    "produit" : {
      "id" : 3,
      "nom" : "Johnny Halliday",
      "description" : "Marie",
      "prix" : 1.89,
      "urlImage" : null
    },
    "quantite" : 2,
    "sousTotal" : 3.78
  } ]
} ]`;
var jsonDTO = `[ {
  "id" : 1,
  "dateCommande" : "2018-01-16",
  "total" : 154.96,
  "details" : [ {
    "produit" : {
      "id" : 1,
      "nom" : "Johnny Halliday",
      "description" : "Les 100 plus belles chansons",
      "prix" : 119.9,
      "urlImage" : null
    },
    "quantite" : 1,
    "sousTotal" : 119.9
  }, {
    "produit" : {
      "id" : 2,
      "nom" : "Johnny Halliday",
      "description" : "Premier concert enregistré",
      "prix" : 17.53,
      "urlImage" : null
    },
    "quantite" : 2,
    "sousTotal" : 35.06
  } ]
}, {
  "id" : 2,
  "dateCommande" : "2018-01-16",
  "total" : 3.78,
  "details" : [ {
    "produit" : {
      "id" : 3,
      "nom" : "Johnny Halliday",
      "description" : "Marie",
      "prix" : 1.89,
      "urlImage" : null
    },
    "quantite" : 2,
    "sousTotal" : 3.78
  } ]
} ]`;

var obj1 = JSON.parse(json);
console.log(obj1[0].dateCommande);
var uneDate = new Date(obj1[1].dateCommande);
console.log(uneDate.toString());


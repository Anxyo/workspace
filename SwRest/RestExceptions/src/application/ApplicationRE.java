package application;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import services.ClientNonTrouveMapper;
import services.RessourceCodesDefaut;
import services.RessourceException1;
import services.RessourceException2;
import services.TestSingleton1;
import services.TestSingleton2;

/**
 * Si cette classe d�rivait de ResourceConfig, tout cela ne serait pas n�cessaire car :
 * - on annoterait les classes destin�es � cr�er des singletons par @Singleton.
 * - on ne ferait aucune configuration sur les classes instanci�es par requete.
 * - on devrait cependant appeler la m�thode packages() dans le constructeur pour indiquer 
 *   quels sont les noms des packages � scanner pour localiser les ressources � publier. 
 *
 */
@ApplicationPath("rest")
public class ApplicationRE extends Application {
	    private Set<Object> singletons = new HashSet<Object>();
		private Set<Class<?>> instParRequete = new HashSet<Class<?>>();
		
		public ApplicationRE() {
			// ressources publi�es en mode 'instances par requete'
			instParRequete.add(TestSingleton1.class);
			instParRequete.add(RessourceCodesDefaut.class);
			instParRequete.add(RessourceException1.class);
			instParRequete.add(RessourceException2.class);
			
			// enregistrement manuel du mapper
			instParRequete.add(ClientNonTrouveMapper.class);
			
			// ressources publi�es en mode 'instance unique' (singleton)
			singletons.add(new TestSingleton2());
			
		}
		
		
		
		// retourne un Set d'instances de classes de ressources instanci�s pour chaque requete
		@Override
		public Set<Class<?>> getClasses() {
			return instParRequete;
		}
		
		// retourne un Set d'instances de classes de ressources singletons
		@Override
		public Set<Object> getSingletons() {
			return singletons;
		}
}
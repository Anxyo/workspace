package services;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Le r�le d'un ExceptionMapper est d'associer une exception Java � une r�ponse http.
 * C'est donc lui qui va g�n�rer tout les d�tails de l'objet Response r�sultant du
 * d�clenchement de l'exception. 
 *
 */

@Provider	// ne pas oublier, sinon pas pris en compte
public class ClientNonTrouveMapper implements ExceptionMapper<ClientNonTrouveException>{
	@Override
	public Response toResponse(ClientNonTrouveException arg0) {
		return Response.status(Response.Status.NOT_FOUND).build();	// code http 404
	}
}

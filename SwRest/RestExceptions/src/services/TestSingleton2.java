package services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/single2")
public class TestSingleton2 {
	private int i = 0; 			
	
	@GET
	@Path("{inc}")
	@Produces("application/json")
	public int getCompteur(@PathParam("inc") int increment) {
		i += increment;
		return i;
	}
}

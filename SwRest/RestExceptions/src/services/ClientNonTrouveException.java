package services;

/**
 * Exception applicative minimale. 
 *
 */
public class ClientNonTrouveException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	public ClientNonTrouveException() {}
	public ClientNonTrouveException(String msg) {super(msg);}
	public ClientNonTrouveException(String msg, Throwable inner) {super(msg, inner);}
}

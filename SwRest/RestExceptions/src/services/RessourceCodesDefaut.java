package services;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import metier.Client;


@Path("/clients")
public class RessourceCodesDefaut {
	private Map<Integer, Client> clients = new HashMap<Integer, Client>();
	
	public RessourceCodesDefaut() {
		Client client = new Client();
		client.setId(19);
		client.setPrenom("Max");
		client.setNom("LaMenace");
		client.setAdresse("12 rue du Bois");
		client.setVille("Nantes");
		client.setZip("44000");
		clients.put(client.getId(), client);	
	}
	
	@GET
	@Path("{id}")
	@Produces("application/json")
	public Response getClient(@PathParam("id") int id) {
		Client clt = clients.get(id);
		Response reponse;
		if (clt != null)
			 reponse = Response.ok(clt).build();
		else
			reponse = Response.noContent().build();
		return reponse;		// retourne le client ou null si non trouv�
	}
	
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Client creeClient(Client nouveauClient) {
		clients.put(nouveauClient.getId(), nouveauClient);
		return nouveauClient;
	}
	
	@PUT
	@Path("{id}")
	@Consumes("application/json")
	public void majClient(@PathParam("id") int id, Client clt) {
		if (clients.containsKey(id))
			clients.replace(id, clt);
	}
	
	@Path("{id}")
	@DELETE
	public void supprimeClient(@PathParam("id") int id) {
		clients.remove(id);		
	}
}

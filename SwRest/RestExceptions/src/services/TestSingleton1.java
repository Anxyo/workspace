package services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * Dans ce cas (le cas par d�faut o� rien n'est g�r�, on a une instanciation par requ�te.
 * De ce fait, la totalisation du compteur ne peut fonctionner.
 * 
 * @author JM
 *
 */


@Path("/single1")
public class TestSingleton1 {
	private int i = 0;  // difficile de faire plus simple, potentiellement incorrect...
	
	@GET
	@Path("{inc}")
	@Produces("application/json")
	public int getCompteur(@PathParam("inc") int increment) {
		i += increment;
		return i;
	}
}

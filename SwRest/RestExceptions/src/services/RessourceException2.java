package services;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import metier.Client;

@Path("/except2")
public class RessourceException2 {
	private Map<Integer, Client> clients = new HashMap<Integer, Client>();
	
	public RessourceException2() {
		Client client = new Client();
		client.setId(19);
		client.setPrenom("Max");
		client.setNom("LaMenace");
		client.setAdresse("12 rue du Bois");
		client.setVille("Nantes");
		client.setZip("44000");
		clients.put(client.getId(), client);	
	}
	
	@GET
	@Path("{id}")
	@Produces("application/json")
	public Response getClient(@PathParam("id") int id) {
		Client clt = clients.get(id);
		Response reponse;
		if (clt != null)
			 reponse = Response.ok(clt).build();
		else
			throw new ClientNonTrouveException("Client non trouv�");
		return reponse;		// retourne le client ou lance une exception si non trouv�
	}
}

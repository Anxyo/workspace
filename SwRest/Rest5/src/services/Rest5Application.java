package services;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/services/*")
public class Rest5Application extends Application {

	// attributs de support des mod�les d'activation de JAX-RS : 
	//     par requete ou singletons 
	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> instParRequete = new HashSet<Class<?>>();
	
	// instanciation des objets ressource
	public Rest5Application() {
		singletons.add(new RessourceClient());
	}
	
	// retourne un Set d'instances de classes de ressources instanci�s pour chaque requete
	@Override
	public Set<Class<?>> getClasses() {
		return instParRequete;
	}
	
	// retourne un Set d'instances de classes de ressources singletons
	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}


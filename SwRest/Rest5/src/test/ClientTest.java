package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Classe de tests JUnit utilisant l'API cliente de JAX-RS, vue plus tard. 
 *
 */
public class ClientTest {
	private static Client client;
	
	@BeforeClass
	public static void init() {
		// une seule instance ('heavyweight' !) est r�utilis�e par chacun des tests
		client = ClientBuilder.newClient();
	}

	@AfterClass
	public static void close() {
		// fermeture connexion r�seau (remise dans le pool de la cnx)
		client.close();
	}

	@Test
	public void testGetClient() {
		String urlCible = "http://localhost:8080/Rest5/services/clients/19";
		metier.Client leClient = client.target(urlCible).request().get(metier.Client.class);
		assertTrue(leClient.getNom().equals("LaMenace"));
	}

	@Test
	public void testCreeClient() {
		metier.Client clt = new metier.Client();
		clt.setNom("Howe");
		clt.setPrenom("Steve");
		clt.setAdresse("1 rue du Oui");
		clt.setVille("Etampes");
		clt.setZip("91300");		
		Response reponse = client.target("http://localhost:8080/Rest5/services/clients")
	              .request().post(Entity.xml(clt));
		assertEquals(201,reponse.getStatus());	// cr�� ?
	}
}

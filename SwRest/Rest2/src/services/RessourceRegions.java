package services;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;


@Path("/clients")
public class RessourceRegions {
	 private RessourceClient regionIleDeFrance = new RessourceClient();
	 private RessourceClientNomPrenom regionPACA = new RessourceClientNomPrenom();

	   @Path("{region}")
	   public Object getDatabase(@PathParam("region") String region)
	   {
	      if (region.equals("idf"))
	      {
	         return regionIleDeFrance;
	      }
	      else if (region.equals("paca"))
	      {
	         return regionPACA;
	      }
	      else return null;	// r�gion non (encore) support�e (!)
	   }
}

package presentation;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import metier.Client;
import metier.Commande;
import metier.Panier;
import service.Facade;

/**
 * Ce servlet est invoqu� pour le traitement des commandes :
 * 1 - Depuis panier.jsp, avec POST , param�tre (bouton submit) 'commander', pour commander
 * 	   	Produit l'affichage de connexion.jsp si la variable uid n'est pas dans la session.
 * 	   	Produit l'affichage direct de commande.jsp, si la variable uid est d�j� dans la session.
 * 
 * 2 - Depuis connexion.jsp : 
 * 		- avec POST, param�tre (bouton submit) 'connexion', si l'utilisateur est d�j� enregistr�.
 * 		- avec POST, param�tre (bouton submit) 'creation', si l'utilisateur doit �tre cr��. 
 * 3 - Depuis commande.jsp :
 * 		- avec POST, param�tre (bouton submit) 'confirmation', confirme la commande, et produit :
 * 			. la cr�ation et l'enregistrement de l'objet commande 
 * 			. la suppression du panier
 * 			. l'affichage de la page merci.html.
 */
@WebServlet("/Commande/*")
public class ServletCommande extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private final static String COMMANDER = "commander";		// demande � commander, depuis panier.jsp (POST)
    private final static String CONNEXION = "connexion";		// demande de connexion, depuis connexion.jsp (POST)
    private final static String CREATION = "creation";			// demande de cr�ation d'un utilisateur, depuis connexion.jsp (POST)
    private final static String CONFIRMATION = "confirmation";	// confirmation de commande, depuis commande.jsp (POST)
    private final static String LIEN_ACCUEIL = "accueil";		// arriv�e -> clic d'un lien sur la page d'accueil (GET)
    private final static String CNX_LISTECOMMANDES = 
    									"cnx_listeCommandes";	// demande authentification  depuis la page liste_commandes.jsp (POST)   

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	if (request.getParameter(LIEN_ACCUEIL) != null){	// on a cliqu� sur le lien de consultation de la liste des commandes
    		doPost(request,response);
    	}
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// retrouve la session de l'utilisateur
		HttpSession session = request.getSession(true);
		// et son panier, s'il existe
		Panier panier = (Panier)session.getAttribute("panier");
		// instantiation de la fa�ade
		Facade facade = new Facade();
		// connexion au panier
		facade.setPanier(panier);
		
		String nom = null, motPasse = null, prenom = null, adresse = null;
		
		// depuis panier.jsp
		if (request.getParameter(COMMANDER) != null){ 					
			// Produit l'affichage direct de commande.jsp, si la variable uid est d�j� dans la session.
			if (session.getAttribute("uid") != null) {
				request.getRequestDispatcher("/commandes/commande.jsp").forward(request, response);
			} else {  // Produit l'affichage de connexion.jsp si la variable uid n'est pas dans la session.
				request.getRequestDispatcher("/commandes/connexion.jsp").forward(request, response);
			}
			// depuis connexion.jsp
		} else if (request.getParameter(CONNEXION) != null){ 			
			nom = request.getParameter("nom");
			motPasse = request.getParameter("pwd");
			// recherche si une combinaison nom + mot de passe existe
			Client clt = facade.rechercheClient(nom, motPasse);
			if (clt != null) {
				// m�morise les infos de connexion
				session.setAttribute("uid", nom);
				session.setAttribute("pwd", motPasse);
				// passe � la page de confirmation de commande
				request.getRequestDispatcher("/commandes/commande.jsp").forward(request, response);
			} else {	// retourne vers la page de connexion
				request.setAttribute("rechercheClient","Identifiants inconnus !");
				request.getRequestDispatcher("/commandes/connexion.jsp").forward(request, response);
			}
		} else if (request.getParameter(CREATION) != null){				// depuis connexion.jsp
			nom = request.getParameter("nom");
			motPasse = request.getParameter("pwd");
			prenom = request.getParameter("prenom");
			adresse = request.getParameter("adresse");
			facade.creeClient(motPasse, nom, prenom, adresse);
			// m�morise les infos de connexion
			session.setAttribute("uid", nom);
			session.setAttribute("pwd", motPasse);
			// passe � la page de confirmation de commande
			request.getRequestDispatcher("/commandes/commande.jsp").forward(request, response);			
		} else if (request.getParameter(CONFIRMATION) != null){			// depuis commande.jsp
			nom = (String)session.getAttribute("uid");
			motPasse = (String)session.getAttribute("pwd");
			// cr�ation de la commande
			Commande cmd = facade.passeCommande(nom, motPasse);
			if (cmd == null) {
				System.out.println("ServletCommande.doPost() : la commande n'a pas �t� cr��e !");
			}
			// suppression du panier de la session
			session.removeAttribute("panier");
			// page de remerciement
			request.getRequestDispatcher("/commandes/merci.html").forward(request, response);
		}
		else if (request.getParameter(LIEN_ACCUEIL) != null){			// depuis accueil.html
			// si d�j� logg�
			if (session.getAttribute("uid") != null) {
				// retrouve la liste des commandes pour ce client
				nom = (String)session.getAttribute("uid");
				motPasse = (String)session.getAttribute("pwd");
				System.out.println("Nom, depuis la session : " + nom);
				System.out.println("Pwd, depuis la session : " + motPasse);
				Client clt = facade.rechercheClient(nom, motPasse);
				System.out.println("Client retrouv� : " + clt);
				List<Commande> commandes = clt.getCommandes();
				// Place listeCommandes dans la requ�te
				request.setAttribute("listeCommandes", commandes);
			}
			// passe la main � liste_commandes.Jsp, logg� ou pas...
			request.getRequestDispatcher("/commandes/liste_commandes.jsp").forward(request, response);
		}
		else if (request.getParameter(CNX_LISTECOMMANDES) != null){			// depuis liste_commandes.jsp
			nom = request.getParameter("nom");
			motPasse = request.getParameter("pwd");
			// recherche si une combinaison nom + mot de passe existe
			Client clt = facade.rechercheClient(nom, motPasse);
			if (clt != null) {
				List<Commande> commandes = clt.getCommandes();
				// Place listeCommandes dans la requ�te
				request.setAttribute("listeCommandes", commandes);				
			} else {
				request.setAttribute("rechercheClient","Identifiants inconnus !");
			}
			// re-passe la main � liste_commandes.Jsp, logg� ou pas...
			request.getRequestDispatcher("/commandes/liste_commandes.jsp").forward(request, response);
		}
	}


}

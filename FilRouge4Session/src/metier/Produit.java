package metier;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 * Repr�sente un produit :
 * <ul>
 * <li>Tel que pr�sent� dans le catalogue.</li>
 * <li>Associ� au DetailPanier.</li>
 * <li>Associ� au DetailCommande.</li>
 * </ul>
 * @author Administrateur
 *
 */

@NamedQuery(name="Produit.FindAll", query="select p from Produit p")

@Entity
public class Produit {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String nom;
	private String description;
	private double prix;
	private String urlImage;
	
	// constructeur(s)
	public Produit() {}
	
	public Produit(String nom, String description, double prix, String urlImage) {
		this.nom = nom;
		this.description = description;
		this.prix = prix;
		this.urlImage = urlImage;
	}
	// accesseurs

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	@Override
	public String toString() {
		return "Produit [id=" + id + ", nom=" + nom + ", description=" + description + ", prix=" + prix
				+ ", urlImage=" + urlImage + "]";
	}
	
}

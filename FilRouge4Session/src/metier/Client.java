package metier;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


@NamedQuery(name="Client.Find", query="SELECT c FROM Client c WHERE c.nom= ?1 AND c.motPasse= ?2")
@Entity
public class Client {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String nom;
	private String prenom;
	private String adresse;
	private String motPasse;
	
	@OneToMany(mappedBy="client", 
			cascade=CascadeType.ALL, 
			fetch=FetchType.EAGER)
	private List<Commande> commandes;  // peut �tre une map ?
	
	// constructeur
	public Client() {}
	
	public Client(String nom, String prenom, String adresse) {
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		commandes =  new ArrayList<Commande>();
	}
	// accesseurs

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public List<Commande> getCommandes() {
		return commandes;
	}
	public String getAdresse() {
		return adresse;
	}

	public String getMotPasse() {
		return motPasse;
	}

	public void setMotPasse(String motPasse) {
		this.motPasse = motPasse;
	}

	@Override
	public String toString() {
		return "Client [code=" + id + ", nom=" + nom + ", prenom=" + prenom + ", adresse=" + adresse + 
				", nb commandes : " + commandes.size() + 
				"]";
	}
	
	
}

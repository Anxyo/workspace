package metier;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * Repr�sente une ligne de d�tail dans une commande.
 * Reprend la structure d'une ligne de d�tail du panier,
 * avec, en plus, une r�f�rence vers la commande associ�e.
 * @author Administrateur
 *
 */

@Entity
public class DetailCommande {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	private Commande commandes; 
	
	@OneToOne
	private Produit produit;
	private int quantite;
	private double sousTotal;
	
	// cr�e un DetailCommande � partir d'un DetailPanier
	public DetailCommande() {}
	
	public DetailCommande(Commande commande, DetailPanier dp) {
		this.commandes = commande;
		produit = dp.getProduit();
		quantite = dp.getQuantite();
		sousTotal = dp.getSousTotal();
	}
	
	// accesseurs	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Commande getCommande() {
		return commandes;
	}
	public Produit getProduit() {
		return produit;
	}
	public int getQuantite() {
		return quantite;
	}
	public double getSousTotal() {
		return sousTotal;
	}

	@Override
	public String toString() {
		return "Produit : " + produit.getNom() + " " + produit.getDescription() +
				", qut� : " + quantite + ", sous total : " + sousTotal;
	}
	
}

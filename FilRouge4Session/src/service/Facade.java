package service;

import java.util.Date;

import metier.Catalogue;
import metier.Client;
import metier.Commande;
import metier.DetailPanier;
import metier.Panier;
import metier.Produit;
import persistance.ClientDAO;
import persistance.CommandeDAO;
/**
 * Repr�sente la fa�ade applicative (couche de services).
 * Regroupe les traitements n�cessaire � une session 
 * de consultation / achat d'un visiteur.
 * @author Administrateur
 *
 */
public class Facade {
	private Catalogue leCatalogue;
	//	int positionCourante = -1; // maintenue d�sormais sur le client
	
	// le panier et la commande �ventuelle
	private Panier panier;
	// private Commande cmd;	// TODO : garder ?
	
	// le client associ� (�ventuel)
	// private Client client;	// TODO : garder ?
	
	// constructeur
	public Facade() {
		leCatalogue = Catalogue.getInstance();
	}
	
	/////////////////////////// Catalogue ////////////////////
	/**
	 * Retourne le produit situ� � une position pass�e en param�tre
	 * dans le catalogue 
	 * @param positionCourante (manipul�e par la logique de navigation)
	 * @return le produit concern�
	 */
	public Produit getProduit(int positionCourante) {
		// plus � la charge de la fa�ade mais du servlet de navigation
		// positionCourante++;
		if (positionCourante == leCatalogue.getNbProduits()) 
			positionCourante = 0;
		Produit produitCourant = leCatalogue.getProduits().get(positionCourante);
		return produitCourant;
	}
	/**
	 * Retourne le nombre de produits au catalogue
	 * @return le nombre de produits au catalogue
	 */
	public int getNbProduits() {
		return leCatalogue.getNbProduits();
	}
	
	/**
	 * Ajout au panier du produit couremment examin�.
	 * @param positionCourante
	 * @param quantite
	 */
	///////////////////////// Panier //////////////////////////////
	
	public void ajoutePanier(int positionCourante, int quantite) {
		Produit produitCourant = leCatalogue.getProduits().get(positionCourante);
		panier.ajoutOption(produitCourant,quantite);
	}

	// accesseurs
	public Panier getPanier() {
		return panier;
	}

	public void setPanier(Panier panier) {
		this.panier = panier;
	}

	/** 
	 * Passe commande en transformant le panier en une commande.
	 * Suppose d'avoir initialis� le panier, et transmis les identifiants
	 * d'un client existant.
	 * @return
	 */
	public Commande passeCommande(String uid, String pwd) {
		Client client = rechercheClient(uid, pwd);
		Commande cmd = null;
		if (client != null) {
			// cr�ation de la commande
			cmd = new Commande(client, null, new Date(), panier.getTotal());
			// connexion de la commande au client
			client.getCommandes().add(cmd);
			// ajout des d�tails de la commande
			for (DetailPanier dp : panier.getOptions()) {
				cmd.ajouteDetailCommande(dp);
			}
			// enregistre la commande
			CommandeDAO dao= new CommandeDAO();
			dao.enregistre(cmd);	// attribution d'un id lors de l'enregistrement
			
		}  else {
			System.out.println("Facade.passeCommande(), le client n'a pas �t� retrouv�, donc commande non cr��e !");
		}
		return cmd;
	}
	
	/**
	 * Recherche un client par ses nom/mot de passe
	 * (d�l�gue la recherche � ClientDAO)
	 * @param nom Le nom du client � rechercher
	 * @param motPasse Le mot de passe saisi
	 * @return Le client s'il existe, ou null, dans le cas contraire
	 */
	public Client rechercheClient(String nom, String motPasse){
		ClientDAO dao = new ClientDAO();
		Client clt = dao.getClient(nom, motPasse);
		if (clt == null) {
			System.out.println("Facade.rechercheClient(), le client n'a pas �t� retrouv� !");
		}
		return clt; 
	}
	
	/**
	 * Collecte les informations pour d�finir un nouveau client, 
	 * le cr�e puis l'enregistre. 
	 * @param prenom Le prenom du client
	 * @param nom Le nom du client
	 * @param adresse L'adresse du client
	 * @return Le nouveau client
	 */
	public Client creeClient(String pwd, String nom, String prenom, String adresse) {
			Client client = new Client(nom, prenom, adresse);
			client.setMotPasse(pwd);
			ClientDAO dao = new ClientDAO();
			dao.enregistreClient(client);
			return client;
	}
}

package persistance;

import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import metier.Client;
import metier.Commande;
import metier.Produit;

public class Repository {
	private static Repository instance;
	
	// une collection pour les produits
	// une collection pour les clients
	// une collection pour les commandes

	private Vector<Produit> produits;
	private ConcurrentHashMap<String, Client> clients;
	private ConcurrentHashMap<Integer, Commande> commandes;
	// des objets 'thread safe' g�n�rant les ids uniques de nos produits, 
	// clients et commandes
	private AtomicInteger compteurProduits, compteurClients, compteurCommandes, compteurDetailCommande;


	
	private Repository() {
		// cr�e les compteurs
		compteurProduits = new AtomicInteger(0);
		compteurClients  = new AtomicInteger(0);
		compteurCommandes = new AtomicInteger(0);
		compteurDetailCommande = new AtomicInteger(0);
		// instancier les collections
		produits = new Vector<Produit>();
		clients = new ConcurrentHashMap<String, Client>();
		commandes = new ConcurrentHashMap<Integer, Commande>();
		// d�finir les produits
		//initProduits();
	}
	
	public static Repository getInstance() {
		if (instance == null) {
			instance = new Repository();
		}
		return instance;
	}
	

	public List<Produit> getProduits() {
		return produits;
	}

	public Map<String, Client> getClients() {
		return clients;
	}

	public Map<Integer, Commande> getCommandes() {
		return commandes;
	}

	public AtomicInteger getCompteurClients() {
		return compteurClients;
	}

	public AtomicInteger getCompteurCommandes() {
		return compteurCommandes;
	}
	
	

		public AtomicInteger getCompteurDetailCommande() {
		return compteurDetailCommande;
	}

		// initialise le catalogue avec des produits
		/*private void initProduits() {
			produits.add(new Produit(compteurProduits.getAndIncrement(), "Pat Metheny Group", "American Garage",6.99, "am_garage.jpg"));
			produits.add(new Produit(compteurProduits.getAndIncrement(), "Adele", "21",20.45, "21.jpg"));
			produits.add(new Produit(compteurProduits.getAndIncrement(), "Sarah Vaughan", "With Clifford Brown",8.61, "sarah.jpg"));
			produits.add(new Produit(compteurProduits.getAndIncrement(), "John Coltrane", "With Kenny Burrell",10, "coltrane.jpg"));
			produits.add(new Produit(compteurProduits.getAndIncrement(), "Larry Coryell", "Spaces",20.91, "spaces.jpg"));
			produits.add(new Produit(compteurProduits.getAndIncrement(), "Philip Catherine", "Summer Night",9.99, "philip.jpg"));			
		}*/

}

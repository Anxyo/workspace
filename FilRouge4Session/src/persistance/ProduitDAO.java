package persistance;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import metier.Client;
import metier.Produit;

public class ProduitDAO {
	// charger les produits depuis la table Produit de la base de donn�es
	public List<Produit> chargeProduits() {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("FilRouge4");
		EntityManager em = emf.createEntityManager();
		List<Produit> result = null;

		em.getTransaction().begin();
		TypedQuery<Produit> laRqt = em.createNamedQuery("Produit.FindAll", Produit.class);
		result = laRqt.getResultList();

		if (result.size()==0) {
			CreerProduits();
			result = laRqt.getResultList();
		}

		em.close();
		emf.close();

		return result;
	}

	public void CreerProduits() {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("FilRouge4");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();
		Produit P1 = new Produit("Pat Metheny Group", "American Garage", 6.99, "am_garage.jpg");
		em.persist(P1);
		Produit P2 = new Produit("Adele", "21", 20.45, "21.jpg");
		em.persist(P2);
		Produit P3 = new Produit("Sarah Vaughan", "With Clifford Brown", 8.61, "sarah.jpg");
		em.persist(P3);
		Produit P4 = new Produit("John Coltrane", "With Kenny Burrell", 10, "coltrane.jpg");
		em.persist(P4);
		Produit P5 = new Produit("Larry Coryell", "Spaces", 20.91, "spaces.jpg");
		em.persist(P5);
		Produit P6 = new Produit("Philip Catherine", "Summer Night", 9.99, "philip.jpg");
		em.persist(P6);
		em.getTransaction().commit();

		em.close();
		emf.close();

	}

}

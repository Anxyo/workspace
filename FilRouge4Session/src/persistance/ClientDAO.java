package persistance;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import metier.Client;

public class ClientDAO {
	public Client getClient(String nom, String motPasse) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("FilRouge4");
	    EntityManager em = emf.createEntityManager();
	 
	    em.getTransaction().begin();
	
	    TypedQuery<Client> laRqt = em.createNamedQuery("Client.Find", Client.class);
	    laRqt.setParameter(1, nom);
	    laRqt.setParameter(2, motPasse);
	  
		Client result;
		
		try {
			result = laRqt.getSingleResult();
		} catch (Exception e) {
			result = null;
		}
		em.close();
		emf.close();
		
		return result;
		
		
		/*return Repository 	.getInstance()
						.getClients()
							.get(nom + motPasse);*/
	}
	
	public void enregistreClient(Client client) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("FilRouge4");
	    EntityManager em = emf.createEntityManager();
	    
	    em.getTransaction().begin();
		em.persist(client);
		em.getTransaction().commit();
		
		em.close();
		emf.close();
		
	}
}

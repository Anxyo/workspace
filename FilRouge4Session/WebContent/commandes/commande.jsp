<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Passage de commande</title>
<link rel="stylesheet" href="css/styles.css">
<style>
hr{
    height: 12px;
    border: 0;
    box-shadow: inset 0 12px 12px -12px rgba(255, 0, 0, 0.5);
    
   }
   h1 {
   		color: red;
   		font-style: italic;
   		float: left;
   }

   #infosPanier{
   		border: solid 3px darkgrey; 
   		border-radius: 5px; 
   		padding: 5px 5px 5px 5px;
   		margin-top: 10px;
   		margin-right: 10px;
   		margin-left: 10px;
   		height: 100px;
   		text-align: center;
   }
   
   article {
   		height :220px; 
  		border: solid 5px darkgrey; 
  		border-radius: 10px;
   }
   nav {
   		clear: both;
   }
   fieldset {
   		border: solid 3px darkgrey; 
   		border-radius: 5px; 
   		padding: 5px 5px 5px 5px;
   		text-align: center;
   		margin-right: 10px;
   		margin-left: 10px;
   		margin-top: 25px;
   }
   
</style>
</head>
<body bgcolor="white">
<header>
	<img src="images/logo5.png"	style="float: left; border-radius: 30px;margin-left: 30px;display: inline-block;" width="75%" height="100px">
	<h1>&nbsp;&nbsp;Shop</h1>
	<br style="clear: both;">
	<br>
	<hr >
</header>
<br>
	
<h2 style="text-align: center;">Passer commande</h2>
<br>
<br>
<article>
	<div id='infosPanier'>
	
		<p>Votre panier contient actuellement ${panier.options.size()} article(s), pour un total de ${panier.total} euros.</p>
		
	</div>
	<div id='fldsetConfirmation' style='text-align: center;'>
		<p>Confirmez-vous cette commande ?</p>
		<form action='Commande' method="post" style='display: inline-block;'>
						<input type="submit" value="Oui, je confirme ma commande" name="confirmation" style="width: 250px; ">
						<span style='display: inline-block;width: 50px;'></span>
		</form>
		<form action='Catalogue?position=0' method="post" style='display: inline-block;'>
					<input type="submit" value="Non ! Je veux modifier mon panier" name="edit_panier" style="width: 250px; ">		 
		</form>
	</div>
</article>
</body>
</html>
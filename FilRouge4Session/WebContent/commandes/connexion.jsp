<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Connexion</title>
<link rel="stylesheet" href="css/styles.css">
<style>

   h1 {
   		float: left;
   }
 
   
   article {
   		height :420px; 
  		border: solid 5px darkgrey; 
  		border-radius: 10px;
  		padding: 10px 10px 10px 10px;
   }   
</style>
<script src='script/jquery.js'></script>
<script>
$(function(){
	function affZones(){
		if ($("#cnx").prop("checked")){
			$("#identifiants").show();
			$("#infos").hide();
			$("#btnCnx").show();
			$("#intitule").show();

		} else {
			$("#identifiants").show();
			$("#infos").show();
			$("#btnCnx").hide();
			$("#intitule").hide();
		}
	}
	affZones();
	
	$("#cnx").change(affZones);
	$("#new").change(affZones)
})
</script>
</head>
<body bgcolor="white">
<header>
	<img src="images/logo5.png"	style="float: left; border-radius: 30px;margin-left: 30px;display: inline-block;" width="75%" height="100px">
	<h1>&nbsp;&nbsp;Shop</h1>
	<br style="clear: both;">
	<br>
	<hr >
</header>
<br>
	
<h2 style="text-align: center;">Informations de connexion / personnelles</h2>
<br>

<article >			
		<form action='Commande' method="post">
			<fieldset id='choix'>
				<input type='radio' name='choix' id='cnx' checked><label for='cnx' >J'ai d�j� un compte</label><br>
				<input type='radio' name='choix' id='new'><label for='new' >Je n'ai pas encore de compte</label><br><br>
			</fieldset>
			<fieldset id='identifiants'>
				<legend><span style="font-weight: bold;" id='intitule'>J'ai d�j� un compte, </span>mes identifiants sont :</legend>
				<label for='nom'>Nom : </label><br><input type="text" size="40" name="nom" id="nom" value="arati" required><br>
				<label for='pwd'>Mot de passe (6 car. minimum) : </label><br><input type="password" size="40" name="pwd" id="pwd" value="arati" required><br>
				<br>
				<input type="submit" value="Connexion" name="connexion" style="width: 250px;" id='btnCnx'>
				<span style='display: inline-block;width: 50px;'></span>
				<c:choose> 
					<c:when test = '${! empty rechercheClient}'>  
						<span style='color: red'>${rechercheClient}</span>
					</c:when>
				</c:choose>
			</fieldset>

			<fieldset style="margin-top: 40px;" id='infos'>
				<legend><span style="font-weight: bold;">Je cr�e mon compte</span>, je dois saisir <span style="font-weight: bold;">en plus</span> : </legend>
				<label for='prenom'>Prenom : </label><br><input type="text" size="40" name="prenom" id="prenom" value="jm"><br>
				<label for='adresse'>Adresse : </label><br><input type="text" size="40" name="adresse" id="adresse" value="12 rue arati"><br>
				<br>
				<input type="submit" value="Cr�ation du compte et connexion" name="creation" style="width: 250px; " id='btnCreation'>
			</fieldset>
		</form>
		
</article>
<p style="font-style: italic;">Note : dans le monde r�el, cette page DEVRAIT utiliser une connexion s�curis�e (https) !</p>
</body>
</html>
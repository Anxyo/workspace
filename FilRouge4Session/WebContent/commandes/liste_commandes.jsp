<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="f"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Liste de commandes</title>
<link rel="stylesheet" href="css/styles.css">
<style>
hr{
    height: 12px;
    border: 0;
    box-shadow: inset 0 12px 12px -12px rgba(255, 0, 0, 0.5);
    
   }
   h1 {
   		color: red;
   		font-style: italic;
   		float: left;
   }
    
   article {
  		border: solid 3px darkgrey; 
  		border-radius: 10px;
  		margin: 10px 10px 10px 10px;
  		
   }
	fieldset {
		border: solid 3px darkgrey; 
  		border-radius: 10px;
  		margin: 10px 10px 10px 10px;
	}
    th {
   	border-bottom: 2px solid darkgrey;
   }
   td {
   	padding-left: 10px;
   	padding-right: 10px;
   }
   table {
   	width: 60%;
   	margin: 0 auto 20px auto;	/* ruse pour le centrer */
   }
</style>
</head>
<body bgcolor="white">
<header>
	<img src="images/logo5.png"	style="float: left; border-radius: 30px;margin-left: 30px;display: inline-block;" width="900px" height="100px">
	<h1>&nbsp;&nbsp;&nbsp;SHOP</h1>
	<br style="clear: both;">
	<br>
	<hr >
</header>
<br>
	
<h2 style="text-align: center;">Liste de vos commandes</h2>

<br>
<!--  Visible si l'uid n'est pas pr�sent dans la session (utilisateur non authentifi�) -->
	<c:if test='${empty uid}'>
			<form action='Commande' method="post">
				<fieldset >
					<legend><span style="font-weight: bold;">Mes identifiants</span></legend>
					<label for='nom'>Nom : </label><br><input type="text" size="40" name="nom" id="nom" required><br>
					<label for='pwd'>Mot de passe (6 car. minimum) : </label><br><input type="password" size="40" name="pwd" id="pwd" required><br>
					<br>
					<input type="submit" value="Connexion" name="cnx_listeCommandes" style="width: 250px;">
					<span style='display: inline-block;width: 50px;'></span>
					<c:if test = '${not empty rechercheClient}'>  
							<span style='color: red'>${rechercheClient}</span>
					</c:if>
				</fieldset>			 
			</form>
	</c:if>


<!--  Visible si l'uid est pr�sent dans la session (utilisateur authentifi�) ET 
		si listeCommandes a bien �t� transmise dans la requete -->
 
	<c:if test='${not empty listeCommandes}'>  
		<article >			
			<table>
				<tr>
					<th>No</th>
					<th>Client</th>
					<th>Total</th>
					<th>Date</th>
				</tr>
				<c:forEach items="${listeCommandes}" var="commande">
					<tr>
						<td>${commande.id}</td>
						<td>${commande.client.prenom}&nbsp;${commande.client.nom}</td>
						<td>${commande.total}</td>
						 <td><f:formatDate pattern ="dd/MM/yyyy" value="${commande.dateCommande}" /></td>
					</tr>
				</c:forEach>
			</table>
		</article>
	</c:if>

	<p>Retourner � la <a href="index.html">page d'accueil</a></p>
</body>
</html>
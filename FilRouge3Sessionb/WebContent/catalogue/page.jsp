<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Catalogue des produits</title>
<link rel="stylesheet" href="css/styles.css">
<style>

   h1 {
   		float: left;
   }
   #imageProduit {
   		float: left;
   		margin-top: 10px;
   		margin-left: 10px;
   		margin-bottom: 10px;
   		padding-right: 10px;
   		border-right: 2px solid darkgrey;
   }
   #detailsProduit {
   		float: left;
   }
   
   #infosPanier{
   		float: right;
   		border: solid 3px darkgrey; 
   		border-radius: 5px; 
   		padding: 5px 5px 5px 5px;
   		margin-top: 10px;
   		margin-right: 10px;
   		height: 250px;
   		text-align: center;
   }
  
   article {
   		height :500px; 
  		border: solid 5px darkgrey; 
  		border-radius: 10px;
   }
   nav {
   		clear: both;
   }
   
   
</style>

<script src="./script/jquery.js" ></script>

<script>
$(function(){
	
	let urlServeur = "Catalogue";

	
	function majProduit(produit){
		$('#nomProduit').html(produit.nom),
		$('#descriptionProduit').html(produit.description),
		$('#prixProduit').html(produit.prix),
		$('#imageP').attr('src', 'images/' + produit.urlImage);
	}
	
	$('#suiv').click(function(){
		$.getJSON(urlServeur, {rqt_ajax:'rqt_ajax', suivant:'suivant'}, majProduit);
	});
	
	$('#prec').click(function(){
		$.getJSON(urlServeur, {rqt_ajax:'rqt_ajax', precedent:'precedent'}, majProduit);
		//$.ajax({url: urlServeur, data:{rqt_ajax:'rqt_ajax', precedent:'precedent'}, succes: majProduit});
	});
	
	
});


</script>

</head>
<body bgcolor="white">
<header>
	<img src="images/logo5.png"	style="float: left; border-radius: 30px;margin-left: 30px;display: inline-block;" width="75%" height="100px">
	<h1>&nbsp;&nbsp;Shop</h1>
	<br style="clear: both;">
	<br>
	<hr >
</header>
	
<h2 style="text-align: center;">Parcourt du catalogue</h2>
<br>
<br>
<article id='affichageProduit'>
			
		<!-- ------- -->
		<!-- L'image -->
		<!-- ------- -->			
		<div id='imageProduit' style=''>
			 <img id = 'imageP' src='images/${produit.urlImage}' style="border-radius: 10px;">
		</div>
		
		<!-- ------------------ -->
		<!-- D�tails du produit -->
		<!-- ------------------ -->
		<div id='detailsProduit'>
			<h3 id='nomProduit'style="margin-left: 20px;">${produit.nom}</h3>
			<h3 id='descriptionProduit' style="margin-left: 20px;font-style: italic;">${produit.description}</h3><br>
			<h3 id='prixProduit' style="margin-left: 20px;font-size: 1em;">Prix : ${produit.prix} &euro; !</h3>
		</div>
		<form action='Catalogue' method="post">
			<div id='infosPanier'>
				<img src='images/panier.png'>
				<c:choose> 
					<c:when test = '${empty panier or panier.options.size() == 0}'>  
						<br>Panier vide<br><br>
					</c:when>
					<c:otherwise>
						<br>${panier.options.size()} article(s)<br><br>
					</c:otherwise>
				</c:choose>
				<label for="qute">Qut� : </label><input type="number" value="1" min="1" max="99" step="1" name="qute" size="2" id="qute"><br>
				<br>
				<input type="button" value="Ajouter au panier" 	id="ajout_panier" 
					style="width: 150px"><br><br>
				<input type="button" value="Voir panier" 		id="voir_panier"	
					style="width: 150px">
			</div>
			<nav>
				<input type="button" value="Produit pr�c�dent" id="prec" 
					style="width: 200px; float: left;margin-left: 10px;">
				<input type="button" value="Produit suivant" id="suiv" 	
					style="width: 200px;float: right;margin-right: 10px;">
			</nav> 
		</form>

</article>
</body>
</html>
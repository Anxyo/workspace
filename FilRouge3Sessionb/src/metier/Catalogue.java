package metier;

import java.util.List;
import java.util.Vector;
/**
 * Repr�sente le catalogue des produits propos�s au visiteur.
 * Un seul objet de ce type devrait exister ("singleton").
 * Cet objet unique sera en lecture seule, une fois initialis�.
 * 
 * Notes :
 * Pour plus de d�tails sur le pattern singleton regarder :
 * 		https://fr.wikipedia.org/wiki/Singleton_(patron_de_conception)
 * @author Administrateur
 *
 */
public class Catalogue {
	private List<Produit> produits;
	
	///////////////////////// Impl�mentation du singleton //////////////////////////
	private static class Container {
		private static Catalogue leCatalogue = new Catalogue(); 
	}
	
	// le constructeur private, effectuant l'initialisation de l'unique objet
	private Catalogue() {
		produits = new Vector<Produit>();
		initCatalogue();
	}
	
	// pour acc�der � l'instance unique
	public static Catalogue getInstance() {
		return Container.leCatalogue;
	}

	///////////////////////////////// M�thodes d'instance ////////////////////////////
	/**
	 * Retourne la liste des produits du catalogue
	 * @return
	 */
	public List<Produit> getProduits() {
		return produits;
	}
	
	/**
	 * Ajoute un produit au catalogue
	 * @param p le produit � ajouter
	 */
	public void ajoutProduit(Produit p) {
		produits.add(p);
	}
	/**
	 * Retourne le nombre de produits au catalogue
	 * @return
	 */
	public int getNbProduits() {
		return produits.size();
	}
	
	// initialise le catalogue avec des produits
	private void initCatalogue() {
		try {
			
			produits.add(new Produit(1, "Pat Metheny Group", "American Garage",6.99, "am_garage.jpg"));
			produits.add(new Produit(2, "Adele", "21",20.45, "21.jpg"));
			produits.add(new Produit(3, "Sarah Vaughan", "With Clifford Brown",8.61, "sarah.jpg"));
			produits.add(new Produit(4, "John Coltrane", "With Kenny Burrell",10, "coltrane.jpg"));
			produits.add(new Produit(5, "Larry Coryell", "Spaces",20.91, "spaces.jpg"));
			produits.add(new Produit(6, "Philip Catherine", "Summer Night",9.99, "philip.jpg"));			
		} catch (Exception exc) {
			exc.printStackTrace();
		}	
	}
}

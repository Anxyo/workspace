package metier;

import java.util.ArrayList;
import java.util.List;

public class Client {
	private Integer id;
	private String nom;
	private String prenom;
	private String adresse;
	private String motPasse;
	
	private List<Commande> commandes;  // peut �tre une map ?
	
	// constructeur
	public Client(String nom, String prenom, String adresse) {
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
		commandes =  new ArrayList<Commande>();
	}
	// accesseurs

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public List<Commande> getCommandes() {
		return commandes;
	}
	public String getAdresse() {
		return adresse;
	}

	public String getMotPasse() {
		return motPasse;
	}

	public void setMotPasse(String motPasse) {
		this.motPasse = motPasse;
	}

	@Override
	public String toString() {
		return "Client [code=" + id + ", nom=" + nom + ", prenom=" + prenom + ", adresse=" + adresse + 
				", nb commandes : " + commandes.size() + 
				"]";
	}
	
	
}

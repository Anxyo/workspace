package service;

import metier.Catalogue;
import metier.Panier;
import metier.Produit;
/**
 * Repr�sente la fa�ade applicative (couche de services).
 * Regroupe les traitements n�cessaire � une session 
 * de consultation / achat d'un visiteur.
 * @author Administrateur
 *
 */
public class Facade {
	private Catalogue leCatalogue;
	//	int positionCourante = -1; // maintenue d�sormais sur le client
	
	// le panier et la commande �ventuelle
	private Panier panier;
	// private Commande cmd;	// TODO : garder ?
	
	// le client associ� (�ventuel)
	// private Client client;	// TODO : garder ?
	
	// constructeur
	public Facade() {
		leCatalogue = Catalogue.getInstance();
	}
	
	/////////////////////////// Catalogue ////////////////////
	/**
	 * Retourne le produit situ� � une position pass�e en param�tre
	 * dans le catalogue 
	 * @param positionCourante (manipul�e par la logique de navigation)
	 * @return le produit concern�
	 */
	public Produit getProduit(int positionCourante) {
		// plus � la charge de la fa�ade mais du servlet de navigation
		// positionCourante++;
		if (positionCourante == leCatalogue.getNbProduits()) 
			positionCourante = 0;
		Produit produitCourant = leCatalogue.getProduits().get(positionCourante);
		return produitCourant;
	}
	/**
	 * Retourne le nombre de produits au catalogue
	 * @return le nombre de produits au catalogue
	 */
	public int getNbProduits() {
		return leCatalogue.getNbProduits();
	}
	
	/**
	 * Ajout au panier du produit couremment examin�.
	 * @param positionCourante
	 * @param quantite
	 */
	///////////////////////// Panier //////////////////////////////
	
	public void ajoutePanier(int positionCourante, int quantite) {
		Produit produitCourant = leCatalogue.getProduits().get(positionCourante);
		panier.ajoutOption(produitCourant,quantite);
	}

	// accesseurs
	public Panier getPanier() {
		return panier;
	}

	public void setPanier(Panier panier) {
		this.panier = panier;
	}

	
}

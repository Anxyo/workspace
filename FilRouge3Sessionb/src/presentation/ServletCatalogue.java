package presentation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import metier.Panier;
import metier.Produit;
import service.Facade;

/**
 * Ce servlet est invoqu� lors du parcourt du catalogue.
 */
@WebServlet("/Catalogue/*")
public class ServletCatalogue extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private final static String PAGE_SUIVANTE = "suivant";	// navigation avant
    private final static String PAGE_PRECEDENTE = "precedent";	// navigation arri�re    
    private final static String AJOUT_PANIER = "ajout_panier";	// demande d'achat de l'article courant
    private final static String VOIR_PANIER = "voir_panier";	// demande � voir son panier
    private final static String EDIT_PANIER = "edit_panier";	// demande d'�dition du panier
    private final static String RQT_AJAX = "rqt_ajax";     
    
    
    public ServletCatalogue() {
        super();
        // TODO Auto-generated constructor stub
    }
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			
			Integer position = 0;
			// retrouve la session de l'utilisateur
			HttpSession session = request.getSession(true);
			// et son panier, s'il existe
			Panier panier = (Panier)session.getAttribute("panier"); // �ventuellement null
			// identifie quel est le code du produit qui etait visible
			position = (Integer)session.getAttribute("position"); // �ventuellement null
			if (position == null)
				position = 0;
			
			// instantiation de la fa�ade
			Facade facade = new Facade();
			facade.setPanier(panier);
			
			// Demande d'ajout au panier
			if (request.getParameter(AJOUT_PANIER) != null){
				if (panier == null) {
					panier = new Panier();
					session.setAttribute("panier", panier);
				}
				// retrouve la quantite
				int quantite = 1;
				try {
					quantite = Integer.parseInt(request.getParameter("qute"));
				} catch (Exception exc) {}
				facade.setPanier(panier);
				facade.ajoutePanier(position, quantite);
			} 
			// Demande � voir son panier (pas de retour)
			else if (request.getParameter(VOIR_PANIER) != null){ 			
				// la page peut essayer d'acceder directement au panier present dans la session
				request.getRequestDispatcher("/catalogue/panier.jsp").forward(request, response);
				return;
			}
			
			// Demande � �diter son panier (renvoie � nouveau la page du panier)
			else if (request.getParameter(EDIT_PANIER) != null){
				// combien d'�l�ments dans le panier
				int quantite = 0;
				Produit p = null;
				for (int i = panier.getOptions().size() - 1; i >= 0; i--) {
					try {
						quantite = Integer.parseInt(request.getParameter("qute_" + i));
						// si la quantite vaut 0, on supprime cet article du panier
						if (quantite == 0) {
							panier.getOptions().remove(i);
						} else {	// met � jour la quantit� et le sous total
							panier.getOptions().get(i).setQuantite(quantite);
							// retrouve le produit
							p = panier.getOptions().get(i).getProduit();
							// et recalcule le sous-total
							panier.getOptions().get(i).setSousTotal(quantite * p.getPrix());
						}
					} catch(Exception exc) {}
				}
				// la page peut acc�der directement au panier present dans la session
				request.getRequestDispatcher("/catalogue/panier.jsp").forward(request, response);
				return;
			}
			
			// Si on arrive ici, passe �ventuellement, � la page suivante / pr�c�dente
			int nouvellePosProduit = position;
			
			
			if (request.getParameter(RQT_AJAX) != null) {
				//demande page suivante ou prec
				//incr�menter la position
				//tester si position valide, sinon on repart de position 0
				//lire le produit, le s�rialiser en Json, et l'envoyer au navigateur
				
				Gson gs = new Gson();
				response.setCharacterEncoding("UTF_8");
				response.setContentType("application/json");
				
				
				if (request.getParameter(PAGE_SUIVANTE) != null) {
					nouvellePosProduit = position + 1;
					nouvellePosProduit = nouvellePosProduit % facade.getNbProduits();
				}
				if (request.getParameter(PAGE_PRECEDENTE) != null) {
					
					nouvellePosProduit = (nouvellePosProduit > 0)?(nouvellePosProduit - 1) : (facade.getNbProduits()-1);
				}
					session.setAttribute("position", nouvellePosProduit);
					
					Produit produitaAfficher = facade.getProduit(nouvellePosProduit);
					response.getWriter().append(gs.toJson(produitaAfficher)).flush();

				return;
			}
			
			else if (request.getParameter(PAGE_SUIVANTE) != null){
				nouvellePosProduit = position + 1;
				nouvellePosProduit = nouvellePosProduit % facade.getNbProduits();
			} else if(request.getParameter(PAGE_PRECEDENTE) != null) {
				nouvellePosProduit = position - 1;
				nouvellePosProduit = (nouvellePosProduit < 0)?(facade.getNbProduits() + nouvellePosProduit) : nouvellePosProduit; 
			}
			// sauvegarde dans la session
			session.setAttribute("position",nouvellePosProduit);
			// affichage de la page (peut �tre la page suivante, pr�c�dente ou la m�me)
			Produit aAfficher = facade.getProduit(nouvellePosProduit);
			
			// range le produit correspondant dans la request...
			request.setAttribute("produit", aAfficher);
			request.setAttribute("position", nouvellePosProduit);
			// ...et affiche la page.
			request.getRequestDispatcher("/catalogue/page.jsp").forward(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			doGet(request, response);
	}

}

package sw;
import javax.jws.WebService;

@WebService
public interface IServicePersonne {
	Personne getPersonne(String nomPersonne);
	String creePersonne(String nom, String prenom, int age);
}

package sw;
import javax.xml.ws.*;

public class PublicationService {
	public static void main(String[] args) {
		// instanciation de l'implémentation 
		ServiceImpl impl = new ServiceImpl();
		Endpoint ep = Endpoint.publish("http://localhost:8888/servicePersonne", impl);
		System.out.println("Publication OK ?" + ep.isPublished());
	}
}

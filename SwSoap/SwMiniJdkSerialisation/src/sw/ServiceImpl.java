package sw;
import java.util.HashMap;
import java.util.Map;

import javax.jws.WebService;

@WebService(endpointInterface="sw.IServicePersonne")
public class ServiceImpl implements IServicePersonne {
	private Map<String, Personne> personnes = new HashMap<String,Personne>();
	
	@Override
	public String creePersonne(String nom, String prenom, int age) {
		personnes.put(nom, new Personne(nom,prenom, age));
		return nom;
	}

	@Override
	public Personne getPersonne(String nomPersonne) {
		return personnes.get(nomPersonne);
	}

}


package sw.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getPersonneResponse", namespace = "http://sw/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPersonneResponse", namespace = "http://sw/")
public class GetPersonneResponse {

    @XmlElement(name = "return", namespace = "")
    private sw.Personne _return;

    /**
     * 
     * @return
     *     returns Personne
     */
    public sw.Personne getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(sw.Personne _return) {
        this._return = _return;
    }

}

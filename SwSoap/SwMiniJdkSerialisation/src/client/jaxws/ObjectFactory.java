
package client.jaxws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the client.jaxws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreePersonne_QNAME = new QName("http://sw/", "creePersonne");
    private final static QName _CreePersonneResponse_QNAME = new QName("http://sw/", "creePersonneResponse");
    private final static QName _GetPersonne_QNAME = new QName("http://sw/", "getPersonne");
    private final static QName _GetPersonneResponse_QNAME = new QName("http://sw/", "getPersonneResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: client.jaxws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreePersonne }
     * 
     */
    public CreePersonne createCreePersonne() {
        return new CreePersonne();
    }

    /**
     * Create an instance of {@link CreePersonneResponse }
     * 
     */
    public CreePersonneResponse createCreePersonneResponse() {
        return new CreePersonneResponse();
    }

    /**
     * Create an instance of {@link GetPersonne }
     * 
     */
    public GetPersonne createGetPersonne() {
        return new GetPersonne();
    }

    /**
     * Create an instance of {@link GetPersonneResponse }
     * 
     */
    public GetPersonneResponse createGetPersonneResponse() {
        return new GetPersonneResponse();
    }

    /**
     * Create an instance of {@link Personne }
     * 
     */
    public Personne createPersonne() {
        return new Personne();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreePersonne }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sw/", name = "creePersonne")
    public JAXBElement<CreePersonne> createCreePersonne(CreePersonne value) {
        return new JAXBElement<CreePersonne>(_CreePersonne_QNAME, CreePersonne.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreePersonneResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sw/", name = "creePersonneResponse")
    public JAXBElement<CreePersonneResponse> createCreePersonneResponse(CreePersonneResponse value) {
        return new JAXBElement<CreePersonneResponse>(_CreePersonneResponse_QNAME, CreePersonneResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPersonne }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sw/", name = "getPersonne")
    public JAXBElement<GetPersonne> createGetPersonne(GetPersonne value) {
        return new JAXBElement<GetPersonne>(_GetPersonne_QNAME, GetPersonne.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPersonneResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sw/", name = "getPersonneResponse")
    public JAXBElement<GetPersonneResponse> createGetPersonneResponse(GetPersonneResponse value) {
        return new JAXBElement<GetPersonneResponse>(_GetPersonneResponse_QNAME, GetPersonneResponse.class, null, value);
    }

}

package client;

import client.jaxws.IServicePersonne;
import client.jaxws.Personne;
import client.jaxws.ServiceImplService;

public class PrincipaleClt {
	public static void main(String[] args) {
		ServiceImplService sis = new ServiceImplService();
		IServicePersonne isp = sis.getServiceImplPort();
		String nom = isp.creePersonne("Test", "Pierre", 25);
		System.out.println("Tentative de cr�ation de personne, nom retourn� : " + nom);
		Personne p  = isp.getPersonne(nom);	// attention, Personne est le type g�n�r� par wsimport, PAS celui 
											// d�fini par nous dans le package sw
		System.out.println("D�tail de cette personne, nom : " + p.getNom() 
							+ ", pr�nom : " + p.getPrenom() 
							+ ", age : " + p.getAge());
	}
}

package sw.bean;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebService;

@Stateless(mappedName = "SwEjb")
@LocalBean
@WebService
public class SwEjb {
	
	public int additionEntiers(int a, int b) {
		return a + b;
	}
	
    public SwEjb() {
    }
}



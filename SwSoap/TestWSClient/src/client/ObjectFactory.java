
package client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetSaisonResponse_QNAME = new QName("http://service/", "getSaisonResponse");
    private final static QName _GetSaison_QNAME = new QName("http://service/", "getSaison");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetSaison }
     * 
     */
    public GetSaison createGetSaison() {
        return new GetSaison();
    }

    /**
     * Create an instance of {@link GetSaisonResponse }
     * 
     */
    public GetSaisonResponse createGetSaisonResponse() {
        return new GetSaisonResponse();
    }

    /**
     * Create an instance of {@link InfoFruit }
     * 
     */
    public InfoFruit createInfoFruit() {
        return new InfoFruit();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSaisonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "getSaisonResponse")
    public JAXBElement<GetSaisonResponse> createGetSaisonResponse(GetSaisonResponse value) {
        return new JAXBElement<GetSaisonResponse>(_GetSaisonResponse_QNAME, GetSaisonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSaison }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service/", name = "getSaison")
    public JAXBElement<GetSaison> createGetSaison(GetSaison value) {
        return new JAXBElement<GetSaison>(_GetSaison_QNAME, GetSaison.class, null, value);
    }

}

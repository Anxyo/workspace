
package client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for infoFruit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="infoFruit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nomFruit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prixFruit" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "infoFruit", propOrder = {
    "nomFruit",
    "prixFruit"
})
public class InfoFruit {

    protected String nomFruit;
    protected double prixFruit;

    /**
     * Gets the value of the nomFruit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomFruit() {
        return nomFruit;
    }

    /**
     * Sets the value of the nomFruit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomFruit(String value) {
        this.nomFruit = value;
    }

    /**
     * Gets the value of the prixFruit property.
     * 
     */
    public double getPrixFruit() {
        return prixFruit;
    }

    /**
     * Sets the value of the prixFruit property.
     * 
     */
    public void setPrixFruit(double value) {
        this.prixFruit = value;
    }

}

package client;

public class Principale {

	public static void main(String[] args) {
		InfoFruit info = new InfoFruit();
		info.setNomFruit("Pommes");
		info.setPrixFruit(2.5);
		String reponse = new JardinierSPImplService().getJardinierSPImplPort().getSaison(info);
		System.out.println("Reponse : " + reponse);
	}
}

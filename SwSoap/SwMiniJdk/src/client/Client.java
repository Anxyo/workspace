package client;

import java.net.URL;
import javax.xml.namespace.QName;

public class Client {
	// la classe cliente proxy a �t� g�n�r�e en utilisant la commande :
	//  wsimport -keep http://localhost:8888/service?wsdl
	
	public static void main(String[] args) throws Exception {

		// dans ce premier cas, super simple, on ne pr�cise pas l'url du service,
		// on utilise celle d�finie dans la classe proxy...
		ServiceImplService sis = new ServiceImplService();
		IService is = sis.getServiceImplPort();
		System.out.println(is.retourneInfo());
		
		// si on souhaite la pr�ciser, on utilisera plut�t :
		URL uneUrl = new URL("http://localhost:8888/service?wsdl");
		QName qn = new QName("http://sw/","ServiceImplService");
		ServiceImplService sis2 = new ServiceImplService(uneUrl,qn);
		IService is2 = sis2.getServiceImplPort();
		System.out.println(is2.retourneInfo());
	}
}

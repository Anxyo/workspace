package sw;
import javax.jws.*;
import javax.jws.soap.*;

@WebService
@SOAPBinding(style=SOAPBinding.Style.RPC)
public interface IService {
	String retourneInfo();
}

package sax.parser;

import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/*
 * Un parser SAX est compl�tement diff�rent d'un parser DOM : il ne construit pas l'arbre du document
 * qu'il analyse. Il le parcourt mais ne construit pas une image m�moire du document (un arbre DOM).
 * En fait il invoque une s�rie de callbacks que l'on aura d� cr�er (en fabriquant une classe d�riv�e
 * de DefaultHandler), au fur et � mesure de sa rencontre des divers �l�ments du document. A charge � nous
 * de traiter les donn�es qu'il nous envoie.
 * Sa mise en oeuvre est plus simple qu'un parcourt du DOM qui, typiquement, sera r�cursif.
 * 
 */
public class AnalyseDocument {

	public static void main(String[] args) {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			// ouvre le fichier � lire
			InputStream fichier = new FileInputStream("src/AValider.xml");
			SAXParser leParser = factory.newSAXParser();

			DefaultHandler leHandler = new SaxHandler();
			leParser.parse(fichier, leHandler);

		} catch (Throwable err) {
			err.printStackTrace();
		}

	}
}
/*
 * Red�finition des callbacks invoqu�s par le parser
 */
class SaxHandler extends DefaultHandler {
	StringBuilder sb = new StringBuilder();
	
	@Override	// invoqu�e au d�but de l'analyse du document
	public void startDocument() throws SAXException {
		System.out.println("D�but du document");
	}
	
	@Override	// invoqu�e � la fin de l'analyse du document
	public void endDocument() throws SAXException {
		System.out.println("Fin du document");
	}
	
	@Override	// invoqu�e � chaque balise ouvrante d'un �l�ment
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		sb.setLength(0);
		sb.append("<" + qName + " ");
		for (int i = 0; i < attributes.getLength(); i++){
			sb.append(" ");sb.append(attributes.getQName(i)); sb.append("=");sb.append(attributes.getValue(i));
		}
		sb.append(">");
		System.out.println(sb.toString());
	}
	
	@Override	// invoqu�e � la fermeture de chaque balise
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		System.out.println("</" + qName + ">");
	}
	
	@Override	// invoqu�e lors de l'analyse des caract�res ignor�s
	public void characters(char ch[], int start, int length)
			throws SAXException {
		sb.setLength(0);
		sb.append(String.valueOf(length));
		sb.append(" caract�res ignor�s");
		System.out.println("[" + sb.toString() + "]");
	}
	
	
	
}

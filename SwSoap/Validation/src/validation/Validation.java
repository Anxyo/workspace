package validation;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/*
 * Utilisation du parser XML pour valider un document relativement � un sch�ma.
 * Noter que le sch�ma lui m�me peut �tre g�n�r� � partir d'un document XML de
 * r�f�rence, et servir ensuite, � valider d'autres documents destin�s � �tre
 * "compatibles" avec le premier.
 * 
 */
public class Validation {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		// Cette technique est la fa�on "officielle" de faire
		// comment par cr�er un parser XML
	    DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	    Document document = parser.parse(new File("src/AValider.xml"));

	    // cr�ation de la factory d'analyse de sch�ma
	    SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

	    // chargement du sch�ma
	    Source schemaFile = new StreamSource(new File("src/formation.xsd"));
	    // et cr�ation de l'objet sch�ma
	    Schema schema = factory.newSchema(schemaFile);

	    // Un objet Validator est d�di� � la validation du document par le sch�ma
	    Validator validateur = schema.newValidator();

	    // valide l'arbre DOM
	    try {
	    	// valide le document � travers le DOM
	        validateur.validate(new DOMSource(document));
	        // si l'on arrive ici, c'est qu'il est valide !
	        System.out.println("Validation effectu�e sans erreur !");
	    } catch (SAXException e) {
	    	 System.out.println("Erreur de validation : " + e);
	    }
	}
}

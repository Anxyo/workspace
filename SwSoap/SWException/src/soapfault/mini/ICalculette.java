package soapfault.mini;

import javax.jws.WebService;
import javax.xml.soap.SOAPException;


@WebService
public interface ICalculette {
	double addition(double v1, double v2);
	double soustraction(double v1, double v2);
	double multiplication(double v1, double v2);
	double division(double v1, double v2) throws CalculetteException ;
}

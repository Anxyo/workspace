package soapfault.mini;


public class CalculetteException extends Exception {
	private static final long serialVersionUID = 1L;

	public CalculetteException() {
		super();
	}

	public CalculetteException(String message, Throwable cause) {
		super(message, cause);
	}

	public CalculetteException(String message) {
		super(message);
	}
}

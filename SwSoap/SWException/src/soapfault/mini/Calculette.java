package soapfault.mini;

import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.soap.SOAPFaultException;


@WebService(endpointInterface="soapfault.mini.ICalculette")
public class Calculette implements ICalculette {
	
	@Override
	public double addition(double v1, double v2) {
		return v1 + v2;
	}

	@Override
	public double soustraction(double v1, double v2) {
		return v1 - v2;
	}

	@Override
	public double multiplication(double v1, double v2) {
		return v1 * v2;
	}

	@Override
	public double division(double v1, double v2) throws CalculetteException {
		if (v2 == 0) {
			throw new CalculetteException("Second param�tre nul !");
		}
		return v1 / v2;
	}
	
}

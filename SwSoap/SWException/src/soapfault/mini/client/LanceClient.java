package soapfault.mini.client;

public class LanceClient {

	/*
	 * Cot� client, une exception serveur sera mapp�e sur une exception cliente, g�n�r�e par JAX-WS (wsimport),
	 * et de nom <nom exception serveur>_Exception, qu'il faudra g�rer dans le code client.
	 */
	
	public static void main(String[] args) {
		CalculetteService service = new CalculetteService();
		
		try {
			service.getCalculettePort().division(12, 5);
			service.getCalculettePort().division(12, 0);	// bing !
		} catch (CalculetteException_Exception e) {
			e.printStackTrace();
		}
	}

}

package soapfault.webfault.client;

public class LanceClient {

	/*
	 * Rien de sp�cifique, dans cette invocation utilisant l'annotation @WebFault cot� serveur.
	 * L'exception serveur est bien "reconstitu�e" cot� client, avec les m�me r�gles de nommage :
	 *   <exception serveur>_Exception.
	 */
	
	public static void main(String[] args) {
		
		CalculetteService service = new CalculetteService();
		try {
			service.getCalculettePort().division(12, 5);
			service.getCalculettePort().division(12, 0);	// bing !
		} catch (CalculetteException_Exception e) {
			e.printStackTrace();
		}

	}

}

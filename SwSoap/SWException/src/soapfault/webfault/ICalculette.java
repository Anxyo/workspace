package soapfault.webfault;

import javax.jws.WebService;

@WebService
public interface ICalculette {
	double addition(double v1, double v2);
	double soustraction(double v1, double v2);
	double multiplication(double v1, double v2);
	double division(double v1, double v2) throws CalculetteException;
}

package soapfault.webfault;

import javax.jws.WebService;


@WebService(endpointInterface="soapfault.webfault.ICalculette")
public class Calculette implements ICalculette {
	
	@Override
	public double addition(double v1, double v2) {
		return v1 + v2;
	}

	@Override
	public double soustraction(double v1, double v2) {
		return v1 - v2;
	}

	@Override
	public double multiplication(double v1, double v2) {
		return v1 * v2;
	}

	@Override
	public double division(double v1, double v2) throws CalculetteException {
		if (v2 == 0) {
			throw new CalculetteException("Second param�tre nul !");
		}
		return v1 / v2;
	}
	
}

package soapfault.soapfactory;

import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.soap.SOAPFaultException;


@WebService(endpointInterface="soapfault.soapfactory.ICalculette")
public class Calculette implements ICalculette {
	
	@Override
	public double addition(double v1, double v2) {
		return v1 + v2;
	}

	@Override
	public double soustraction(double v1, double v2) {
		return v1 - v2;
	}

	@Override
	public double multiplication(double v1, double v2) {
		return v1 * v2;
	}

	@Override
	public double division(double v1, double v2) throws SOAPException {
		if (v2 == 0) {
			SOAPFactory factory = SOAPFactory.newInstance();
			SOAPFault faute = factory.createFault("Second param�tre nul !", new QName("Param�tre invalide"));
			throw new SOAPFaultException(faute);
		}
		return v1 / v2;
	}
	
}

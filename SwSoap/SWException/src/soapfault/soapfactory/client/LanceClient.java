package soapfault.soapfactory.client;


/*
 * Dans le cas o� l'on d�clenche une SOAPFault explicitement cot� serveur, elle sera mapp�e sur une SOAPException
 * cot� client, que l'on devra l� encore g�rer explicitement.
 */

public class LanceClient {

	public static void main(String[] args) {
		CalculetteService service = new CalculetteService();
		try {
			service.getCalculettePort().division(12, 5);
			service.getCalculettePort().division(12, 0);	// bing !
		} catch (SOAPException_Exception e) {
			e.printStackTrace();
		}
	}

}

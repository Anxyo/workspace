package editeur.xml;

import javax.swing.JTree;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

public class ExpansionArbre {
	public static void expansionArbre(JTree tree) {
	TreeNode root = (TreeNode) tree.getModel().getRoot();
	TreePath path = new TreePath(root);
	for (int k = 0; k < root.getChildCount(); k++) {
		TreeNode child = (TreeNode) root.getChildAt(k);
		expansionArbre(tree, path, child);
	}
}

public static void expansionArbre(JTree arbre, TreePath chemin,
		TreeNode noeud) {
	if (chemin == null || noeud == null)
		return;
	arbre.expandPath(chemin);
	TreePath nouveauChemin = chemin.pathByAddingChild(noeud);
	for (int k = 0; k < noeud.getChildCount(); k++) {
		TreeNode enfant = (TreeNode) noeud.getChildAt(k);
		if (enfant != null) {
			expansionArbre(arbre, nouveauChemin, enfant);
		}
	}
}
}

package editeur.xml;

import java.io.File;

class SimpleFilter extends javax.swing.filechooser.FileFilter {

	private String description = null;
	private String extension = null;

	public SimpleFilter(String extension, String description) {
		this.description = description;
		this.extension = "." + extension.toLowerCase();
	}
	
	@Override
	public String getDescription() {
		return description;
	}
	@Override
	public boolean accept(File f) {
		if (f == null)
			return false;
		if (f.isDirectory())
			return true;
		return f.getName().toLowerCase().endsWith(extension);
	}
}
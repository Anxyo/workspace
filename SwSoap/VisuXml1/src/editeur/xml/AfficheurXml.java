package editeur.xml;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;
import javax.swing.tree.*;

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class AfficheurXml extends JFrame {

	private static final long serialVersionUID = -3875951637411678106L;
	public static final String NOM_APP = "Afficheur Xml";
	protected Document document;
	protected JTree arbre;
	protected DefaultTreeModel modele;
	protected JFileChooser fChooser;
	protected File fichierCourant;

	public static void main(String argv[]) {
		AfficheurXml fenetre = new AfficheurXml();
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setVisible(true);
	}

	public AfficheurXml() {
		super(NOM_APP);
		setSize(400, 400);
		getContentPane().setLayout(new BorderLayout());
		JToolBar bo = creeBarreOutils();
		getContentPane().add(bo, BorderLayout.NORTH);
		// initialisation du JTree
		initArbre();
		JScrollPane sp = new JScrollPane();
		sp.getViewport().add(arbre);
		getContentPane().add(sp, BorderLayout.CENTER);
		fChooser = new JFileChooser();
		fChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fChooser.setFileFilter(new SimpleFilter("xml", "Fichiers Xml"));
		try {
			File dir = (new File(".")).getCanonicalFile();
			fChooser.setCurrentDirectory(dir);
		} catch (IOException ex) {
		}
	}

	private void initArbre() {
		// en ce qui concerne l'arbre...
		// Cr�ation du noeud racine
		DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode(
				"Pas de Xml charg�");
		// cr�ation du mod�le de l'arbre xml
		modele = new DefaultTreeModel(dmtn);
		// instanciation de l'arbre �partir du mod�le
		arbre = new JTree(modele);
		// un seul �l�ment s�lectionnable � la fois
		arbre.getSelectionModel().setSelectionMode(
				TreeSelectionModel.SINGLE_TREE_SELECTION);
		arbre.setShowsRootHandles(true);
		arbre.setEditable(false);
	}

	protected JToolBar creeBarreOutils() {
		JToolBar tb = new JToolBar();
		tb.setFloatable(false);

		JButton bouton = new JButton(new ImageIcon("Ouvrir.gif"));
		bouton.setToolTipText("Ouvre un fichier xml");
		ActionListener actListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ouvreDocument();
			}
		};
		bouton.addActionListener(actListener);
		tb.add(bouton);
		return tb;
	}

	public String geNomtDocument() {
		return fichierCourant == null ? "Sans nom" : fichierCourant.getName();
	}

	protected void ouvreDocument() {
		if (fChooser.showOpenDialog(AfficheurXml.this) != JFileChooser.APPROVE_OPTION)
			return;
		File f = fChooser.getSelectedFile();
		if (f == null || !f.isFile())
			return;

		try {
			// Utilise un parser DOM
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			// parsing
			document = docBuilder.parse(f);
			Element racine = document.getDocumentElement();
			racine.normalize();
			// noaude racine 
			DefaultMutableTreeNode debut = creeNoeud(racine);
			modele.setRoot(debut);
			//expansionArbre(arbre);
			fichierCourant = f;
			setTitle(NOM_APP + " [" + geNomtDocument() + "]");
		} catch (Exception ex) {
			affErreur(ex, "Erreur de lecture ou d'analyse de fichier Xml");
		} finally {
		}

	}
	/*
	 *	 
	 */
	protected DefaultMutableTreeNode creeNoeud(Node noeudDOM) {
		if (!estAffichable(noeudDOM))
			return null;
		// cr�ation du noeud JTree � partir du noeud xml DOM
		NoeudAffXml noeudJT = new NoeudAffXml(noeudDOM);
		NodeList listeNoeudsDOM = noeudDOM.getChildNodes();
		for (int k = 0; k < listeNoeudsDOM.getLength(); k++) {
			Node ndDOM = listeNoeudsDOM.item(k);
			DefaultMutableTreeNode enfantJT = creeNoeud(ndDOM); // r�cursif
			// si le noeud correspondant n'est pas null on l'ajoute
			if (enfantJT != null)
				noeudJT.add(enfantJT);
		}
		return noeudJT;
	}

	protected boolean estAffichable(Node noeud) {
		switch (noeud.getNodeType()) {
		case Node.ELEMENT_NODE:
			return true;
		case Node.TEXT_NODE:
			String texte = noeud.getNodeValue().trim();
			return !(texte.equals("") || texte.equals("\n") || texte
					.equals("\r\n"));
		}
		return false;
	}

	public void affErreur(Exception ex, String message) {
		ex.printStackTrace();
		JOptionPane.showMessageDialog(this, message, NOM_APP,
				JOptionPane.WARNING_MESSAGE);
	}



}

package editeur.xml;

import javax.swing.tree.DefaultMutableTreeNode;
import org.w3c.dom.Node;

public class NoeudAffXml extends DefaultMutableTreeNode {

	private static final long serialVersionUID = -7702023161598415436L;

	public NoeudAffXml(Node noeud) {
		// le noeud DOM est pass� comme un "user object" stock� dans le noeud JTree
		super(noeud);
	}
	
	// retourne le noeud xml associ� � ce noeud JTree
	public Node getNoeudXml() {
		Object obj = getUserObject();
		if (obj instanceof Node)
			return (Node) obj;
		return null;
	}
	
	// repr�sentation chaine de ce noeud (pour la trace)
	public String toString() {
		Node noeud = getNoeudXml();
		if (noeud == null)
			return getUserObject().toString();
		StringBuffer sb = new StringBuffer();
		switch (noeud.getNodeType()) {
		case Node.ELEMENT_NODE:
			sb.append('<');
			sb.append(noeud.getNodeName());
			sb.append('>');
			break;
		case Node.TEXT_NODE:
			sb.append(noeud.getNodeValue());
			break;
		}
		return sb.toString();
	}
}
package swcalcul;

import javax.jws.WebService;

// Attention, ne pas oublier de copier les Jars de l'impl�mentation de Metro dans le r�pertoire lib de Tomcat
// sous peine de forte d�convenue � l'�x�cution...


@WebService(endpointInterface="swcalcul.ICalculette")
public class Calculette implements ICalculette {

	@Override
	public double addition(double a, double b) {
		return a + b;
	}

	@Override
	public double soustraction(double a, double b) {
		return  a - b;
	}

	@Override
	public double multiplication(double a, double b) {
		return a * b;
	}

	@Override
	public double division(double a, double b) {
		return a / b;
	}

}
/* Attributs de l balise endpoint, suivant la doc :
 * 
name : Name of the endpoint
wsdl : Primary wsdl file location in the WAR file. For e.g. WEB-INF/wsdl/HelloService.wsdl. If this isn't specified, JAX-WS will create and publish a new WSDL. When the service is developed from Java, it is recommended to omit this attribute.
service : QName of WSDL service. For e.g. {http://example.org/}HelloService. When the service is developed from java, it is recommended to omit this attribute.
port : QName of WSDL port. For e.g. {http://example.org/}HelloPort. When the service is developed from Java, it is recommended to omit this attribute.
implementation : Endpoint implementation class name. For e.g: hello.HelloImpl. The class should have @WebService annotation. Provider based implementation class should have @WebServiceProvider annotation.
url-pattern : Should match <url-pattern> in web.xml
binding : Binding id defined in the JAX-WS API. The possible values are:
			"http://schemas.xmlsoap.org/wsdl/soap/http",
			"http://www.w3.org/2003/05/soap/bindings/HTTP/"
			If omitted, it is considered SOAP1.1 binding.
enable-mtom : Enables MTOM optimization. true or false. Default is false. 
*/

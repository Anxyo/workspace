package swcalcul;

import javax.jws.WebService;

@WebService
public interface ICalculette {
	double addition(double a, double b);
	double soustraction(double a, double b);
	double multiplication(double a, double b);
	double division(double a, double b);
}

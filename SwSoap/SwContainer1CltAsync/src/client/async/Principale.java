package client.async;

import javax.xml.ws.AsyncHandler;
import javax.xml.ws.Response;
import calculette.serveur.AdditionResponse;
import calculette.serveur.Calculette;
import calculette.serveur.CalculetteService;

public class Principale {

	public static void main(String[] args) {
		CalculetteService cs = new CalculetteService();
		Calculette cal = cs.getCalculettePort();
		// appel synchrone
		System.out.println("somme : " + cal.addition(12, 17));
		// appel asynchrone en utilisant un callback
		cal.additionAsync(12, 17, new AsyncHandler<AdditionResponse>() {
			public void handleResponse(Response<AdditionResponse> res) {
				try {
					System.out
							.println("R�sultat, avec un callback asynchrone : "
									+ res.get().getReturn());
				} catch (Exception exc) {
					exc.printStackTrace();
				}
			}
		});
		
		// 3eme technique : appel en mode asynchrone sans callback
		Response<AdditionResponse> resultat = cal.additionAsync(12, 17);
		// un peu de temps sinon on terminera sans avoir rien vu...
		try {
			Thread.sleep(1000);
		} catch (Exception exc) {
		}
		if (resultat.isDone()) {
			try {
				System.out.println("R�sultat, mod asynchrone, sans callback : "
						+ resultat.get().getReturn());
			} catch (Exception exc) {
				exc.printStackTrace();
			}
		}
	}
}

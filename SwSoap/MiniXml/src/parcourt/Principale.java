package parcourt;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Principale extends JFrame {

	private static final long serialVersionUID = 1L;
	private Document document;
	protected JFileChooser fChooser;
	public static final String NOM_APP = "Parcourt Xml";
	private JTextArea textArea;
	private int niveau = 0;

	public Principale() {

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFichier = new JMenu("Fichier");
		menuBar.add(mnFichier);

		JMenuItem mntmQuitter = new JMenuItem("Quitter");
		mntmQuitter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}

		});
		mntmQuitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
				InputEvent.CTRL_MASK));
		mnFichier.add(mntmQuitter);

		JMenu mnXml = new JMenu("Xml");
		menuBar.add(mnXml);

		JMenuItem mntmAnalyser = new JMenuItem("Analyser");
		mnXml.add(mntmAnalyser);
		mntmAnalyser.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				analyseXml();
			}

		});

		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		fChooser = new JFileChooser();
		fChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fChooser.setFileFilter(new FiltreSimple("xml", "Fichiers Xml"));
		try {
			File dir = (new File(".")).getCanonicalFile();
			fChooser.setCurrentDirectory(dir);
		} catch (IOException ex) {
		}
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	protected void analyseXml() {

		if (fChooser.showOpenDialog(this) != JFileChooser.APPROVE_OPTION)
			return;
		File f = fChooser.getSelectedFile();
		if (f == null || !f.isFile())
			return;
		try {
			// Utilise un parser DOM
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			// parsing et construction de l'arbre
			document = docBuilder.parse(f);
			Element racine = document.getDocumentElement();
			racine.normalize();
			// on commence le parcourt
			parcourtNoeud(racine);

		} catch (Exception ex) {
			affErreur(ex, "Erreur de lecture ou d'analyse de fichier Xml");
		} finally {
			setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}

	}

	protected void parcourtNoeud(Node noeudCourant) {
		this.niveau++;
		System.out.println("niveau = " + niveau);
		if (!estAffichable(noeudCourant)) {
			return;
		}
		this.textArea.append(toString(noeudCourant));
		//DefaultMutableTreeNode tn = new DefaultMutableTreeNode(noeudCourant);
		if (noeudCourant.hasChildNodes()) {
			NodeList liste = noeudCourant.getChildNodes();
			
			for (int k = 0; k < liste.getLength(); k++) {
				Node nd = liste.item(k);
				parcourtNoeud(nd); // r�cursif
				this.niveau--;
			}
		} else {
			//this.niveau--;
			System.out.println("niveau = " + niveau);
			
		}
			
	}

	public void affErreur(Exception ex, String message) {
		ex.printStackTrace();
		JOptionPane.showMessageDialog(this, message, NOM_APP,
				JOptionPane.WARNING_MESSAGE);
	}

	// un noeud est il affichable ?
	protected boolean estAffichable(Node noeud) {
		switch (noeud.getNodeType()) {
		case Node.ELEMENT_NODE:
			return true;
		case Node.TEXT_NODE:
			String texte = noeud.getNodeValue().trim();
			return !(texte.equals("") || texte.equals("\n") || texte
					.equals("\r\n"));
		}
		return false;
	}

	// fabrique la rep. chaine d'un noeud DOM
	public String toString(Node noeud) {
		if (noeud == null)
			return "";
		StringBuffer sb = new StringBuffer();
		switch (noeud.getNodeType()) {
		case Node.ELEMENT_NODE:
			sb.append(getIndentation() + "<");
			sb.append(noeud.getNodeName());
			NamedNodeMap attributs = noeud.getAttributes();
			if (attributs.getLength() > 0) {
				for (int i = 0; i < attributs.getLength(); i++) {
					sb.append(" " + attributs.item(i).getNodeName() + " = '"
							+ attributs.item(i).getNodeValue() + "'");
				}
			}
			sb.append('>');
			break;
		case Node.TEXT_NODE:
			sb.append(getIndentation() + noeud.getNodeValue());
			break;
		}
		sb.append('\n');
		return sb.toString();
	}

	 String getIndentation() {
	 if (niveau >= 0) {
	 StringBuilder sb = new StringBuilder("");
	 for (int i = 0; i < this.niveau; i++) {
	 sb.append("   ");
	 }
	 return sb.toString();
	 } else
	 return "";
	 }

	public static void main(String[] args) {
		Principale p = new Principale();
		p.setBounds(200, 200, 500, 400);
		p.setVisible(true);

	}

}

class FiltreSimple extends javax.swing.filechooser.FileFilter {

	private String description = null;
	private String extension = null;

	public FiltreSimple(String extension, String description) {
		this.description = description;
		this.extension = "." + extension.toLowerCase();
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public boolean accept(File f) {
		if (f == null)
			return false;
		if (f.isDirectory())
			return true;
		return f.getName().toLowerCase().endsWith(extension);
	}
}

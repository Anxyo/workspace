package sw.attachement;

import java.io.FileOutputStream;
import java.io.IOException;
import javax.activation.DataHandler;
import javax.jws.WebMethod;
import javax.jws.WebService;

// Ce service web re�oit un fichier joint au message SOAP d'invocation
@WebService
public class SwAttachements {
	
	@WebMethod
	public void recoitFichier(DataHandler dh) {
		FileOutputStream fos;
		try {
			// �crit simplement le fichier joint sur le disque
			fos = new FileOutputStream("image.jpg");
			dh.writeTo(fos);
			fos.flush();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

package sw.attachement;

import javax.xml.ws.Endpoint;



public class Publication {

	public static void main(String[] args) {
		// instanciation de l'implémentation 
		SwAttachements swa = new SwAttachements();
		Endpoint ep = Endpoint.publish("http://localhost:8888/attachement", swa);
		System.out.println("Publication OK ?" + ep.isPublished());
	}

}

package annotation.soapbinding;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style=Style.RPC)
public interface ICalculette {
	double addition(double v1, double v2);
	double soustraction(double v1, double v2);
	double multiplication(double v1, double v2);
	double division(double v1, double v2);
}

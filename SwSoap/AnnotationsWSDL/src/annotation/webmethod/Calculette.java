package annotation.webmethod;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class Calculette {
	
	public double addition(double v1, double v2) {
		return v1 + v2;
	}

	@WebMethod(operationName="soustractionDoubles")
	public double soustraction(double v1, double v2) {
		return v1 - v2;
	}

	@WebMethod(operationName="multiplicationDoubles")
	public double multiplication(double v1, double v2) {
		return v1 * v2;
	}

	@WebMethod(exclude=true)	// pas de division !
	public double division(double v1, double v2) {
		return v1 / v2;
	}
	
}

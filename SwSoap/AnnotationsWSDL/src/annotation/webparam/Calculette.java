package annotation.webparam;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class Calculette {
	
	public double addition(@WebParam(name="v1") double v1, @WebParam(name="v2") double v2) {
		return v1 + v2;
	}

	public double soustraction(@WebParam(name="v1") double v1, @WebParam(name="v2") double v2) {
		return v1 - v2;
	}
	
	public double multiplication(@WebParam(name="v1") double v1, @WebParam(name="v2") double v2) {
		return v1 * v2;
	}

	public double division(@WebParam(name="v1") double v1, @WebParam(name="v2") double v2) {
		return v1 / v2;
	}
}

package annotation.webresult;
import javax.xml.ws.Endpoint;


public class Lanceur {

	public static void main(String[] args) {
		Endpoint ep = Endpoint.publish("http://localhost:8888/calculette", new Calculette());
		System.out.println(ep.isPublished()?"Service publi� !":"Erreur lors de la publication !");
	}

}

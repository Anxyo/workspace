package annotation.webresult;

import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
public class Calculette  {
	
	@WebResult(name="resultatAddition")
	public double addition(double v1, double v2) {
		return v1 + v2;
	}

	@WebResult(name="resultatSoustraction")
	public double soustraction(double v1, double v2) {
		return v1 - v2;
	}

	@WebResult(name="resultatMultiplication")
	public double multiplication(double v1, double v2) {
		return v1 * v2;
	}

	@WebResult(name="resultatDivision")
	public double division(double v1, double v2) {
		return v1 / v2;
	}
	
}

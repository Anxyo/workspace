package annotation.webservice;

import javax.jws.WebService;

@WebService(endpointInterface="annotation.webservice.ICalculette", 
			portName="portCalculette", 
			serviceName="serviceCalculette")
public class Calculette implements ICalculette {
	
	@Override
	public double addition(double v1, double v2) {
		return v1 + v2;
	}

	@Override
	public double soustraction(double v1, double v2) {
		return v1 - v2;
	}

	@Override
	public double multiplication(double v1, double v2) {
		return v1 * v2;
	}

	@Override
	public double division(double v1, double v2) {
		return v1 / v2;
	}
	
}

package sw.attachement.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;


import client.SwAttachements;
import client.SwAttachementsService;

public class Principale {
	
	public static void main(String[] args) {
		SwAttachementsService service = new SwAttachementsService();
		SwAttachements swa = service.getSwAttachementsPort();
		File aAttacher = new File("src/android.jpg");
		System.out.println(aAttacher.getAbsolutePath().toString());
		byte[] octets = convertFichier2TabOctets(aAttacher);
		swa.recoitFichier(octets);			// appel service
		System.out.println("Attachement envoy�.");
	}

	static byte[] convertFichier2TabOctets(File leFichier) {
		byte[] tab = null;
		try {
			FileInputStream fis;
			fis = new FileInputStream(leFichier);
			FileChannel fc = fis.getChannel();
			tab = new byte[(int) fc.size()];	// allocation
			ByteBuffer byteBuffer = ByteBuffer.wrap(tab);	
			fc.read(byteBuffer);				// lecture du fichier
		} catch (IOException e) {
			e.printStackTrace();
		}
		return tab;
	}
}

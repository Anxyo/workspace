
package client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RecoitFichierResponse_QNAME = new QName("http://attachement.sw/", "recoitFichierResponse");
    private final static QName _RecoitFichier_QNAME = new QName("http://attachement.sw/", "recoitFichier");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RecoitFichierResponse }
     * 
     */
    public RecoitFichierResponse createRecoitFichierResponse() {
        return new RecoitFichierResponse();
    }

    /**
     * Create an instance of {@link RecoitFichier }
     * 
     */
    public RecoitFichier createRecoitFichier() {
        return new RecoitFichier();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecoitFichierResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://attachement.sw/", name = "recoitFichierResponse")
    public JAXBElement<RecoitFichierResponse> createRecoitFichierResponse(RecoitFichierResponse value) {
        return new JAXBElement<RecoitFichierResponse>(_RecoitFichierResponse_QNAME, RecoitFichierResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecoitFichier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://attachement.sw/", name = "recoitFichier")
    public JAXBElement<RecoitFichier> createRecoitFichier(RecoitFichier value) {
        return new JAXBElement<RecoitFichier>(_RecoitFichier_QNAME, RecoitFichier.class, null, value);
    }

}

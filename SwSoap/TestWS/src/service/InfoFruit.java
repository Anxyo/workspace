package service;

import java.io.Serializable;

public class InfoFruit implements Serializable{
	private static final long serialVersionUID = 1592359474760883401L;
	String nomFruit;
	double prixFruit;
	
	public InfoFruit(){}
	
	public InfoFruit(String nomFruit, double prixFruit) {
		super();
		this.nomFruit = nomFruit;
		this.prixFruit = prixFruit;
	}
	public String getNomFruit() {
		return nomFruit;
	}
	public void setNomFruit(String nomFruit) {
		this.nomFruit = nomFruit;
	}
	public double getPrixFruit() {
		return prixFruit;
	}
	public void setPrixFruit(double prixFruit) {
		this.prixFruit = prixFruit;
	}
	
}

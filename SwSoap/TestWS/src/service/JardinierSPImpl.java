package service;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class JardinierSPImpl{
	
	@WebMethod
	public String getSaison(InfoFruit leFruit) {
		String result = null; 
		switch (leFruit.getNomFruit()){
		case "Bananes":
			result = "toutes";
			break;
		case "Pommes":
			result="automne";
			break;
		case "Peches":
			result = "�t�";
			break;
		case "Raisin":
			result="automne";
			break;
		}
		return result;
	}
	
}

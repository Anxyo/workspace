package main;

import javax.xml.ws.Endpoint;

import service.JardinierSPImpl;

public class Principale {

	public static void main(String[] args) {
		Endpoint ep = Endpoint.publish("http://localhost:8888/jardinier", new JardinierSPImpl());
		if (ep.isPublished())
			System.out.println("Publication effectu�e");
		else
			System.out.println("Probl�me !");
	}

}

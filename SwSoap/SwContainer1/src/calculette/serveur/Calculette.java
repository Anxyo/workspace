package calculette.serveur;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class Calculette {
	@WebMethod
	public double addition(double a, double b){
		return a + b;
	}
	@WebMethod
	public double soustraction (double a, double b){
		return a - b;
	}
	@WebMethod
	public double multiplication(double a, double b){
		return a * b;
	}
	@WebMethod
	public double division(double a, double b){
		return a / b;
	}
}

package editeur.xml;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;

public class AfficheurXml extends JFrame {

	private static final long serialVersionUID = 6967352411023901239L;
	public static final String NOM_APP = "Afficheur Xml";
	protected Document document;
	protected JTree arbre;
	protected DefaultTreeModel modele;
	protected JTable table; 
	protected AttributTableModel modeleTable;
	protected JFileChooser fChooser;
	protected File fichierCourant;

	public static void main(String argv[]) {
		AfficheurXml fenetre = new AfficheurXml();
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setVisible(true);
	}
	
	public AfficheurXml() {
		super(NOM_APP);
		setSize(800, 400); // NEW
		getContentPane().setLayout(new BorderLayout());

		JToolBar bo = creeBarreOutils();
		getContentPane().add(bo, BorderLayout.NORTH);	

		initArbre();
		
		// init table : le mod�le pour stocker les attributs
		modeleTable = new AttributTableModel();
		table = new JTable(modeleTable);
		
		// placement des composants  
		JScrollPane sp1 = new JScrollPane(arbre);
		JScrollPane sp2 = new JScrollPane(table);
		sp2.getViewport().setBackground(table.getBackground());
		JSplitPane sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, sp1, sp2);
		sp.setDividerLocation(400);
		sp.setDividerSize(5);
		getContentPane().add(sp, BorderLayout.CENTER);
		
		// le dialogue de choix de fichiers
		fChooser = new JFileChooser();
		fChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fChooser.setFileFilter(new SimpleFilter("xml", "Fichiers Xml"));
		try {
			File dir = (new File(".")).getCanonicalFile();
			fChooser.setCurrentDirectory(dir);
		} catch (IOException ex) {
		}
	}

	private void initArbre() {
		// init arbre
		DefaultMutableTreeNode dmtn = new DefaultMutableTreeNode("Pas de Xml charg�");
		modele = new DefaultTreeModel(dmtn);
		arbre = new JTree(modele);

		arbre.getSelectionModel().setSelectionMode(
				TreeSelectionModel.SINGLE_TREE_SELECTION);
		arbre.setShowsRootHandles(true);
		arbre.setEditable(false);
		TreeSelectionListener tsListener = new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				Node noeud = getNoeudSel();
				affecteNoeudATable(noeud); // null OK
			}
		};
		arbre.addTreeSelectionListener(tsListener);
	}

	protected JToolBar creeBarreOutils() {
		JToolBar tb = new JToolBar();
		tb.setFloatable(false);

		JButton bouton = new JButton(new ImageIcon("Ouvrir.gif"));
		bouton.setToolTipText("Ouvre un fichier xml");
		ActionListener actListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ouvreDocument();
			}
		};
		bouton.addActionListener(actListener);
		tb.add(bouton);
		return tb;
	}

	public String getNomDocument() {
		return fichierCourant == null ? "Sans nom" : fichierCourant.getName();
	}

	protected void ouvreDocument() {
		if (fChooser.showOpenDialog(AfficheurXml.this) != JFileChooser.APPROVE_OPTION)
			return;
		File f = fChooser.getSelectedFile();
		if (f == null || !f.isFile())
			return;

		setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		try {
			// Utilise un parser DOM
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory
					.newDocumentBuilder();

			document = docBuilder.parse(f);

			Element racine = document.getDocumentElement();
			racine.normalize();

			DefaultMutableTreeNode debut = creeNoeud(racine);
			modele.setRoot(debut);
			affecteNoeudATable(null); // Nouveau
			fichierCourant = f;
			setTitle(NOM_APP + " [" + getNomDocument() + "]");
		} catch (Exception ex) {
			affErreur(ex, "Erreur de lecture ou d'analyse de fichier Xml");
		} finally {
			setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}


	protected DefaultMutableTreeNode creeNoeud(Node noeudDOM) {
		if (!estAffichable(noeudDOM))
			return null;
		NoeudAffXml noeudJT = new NoeudAffXml(noeudDOM);
		NodeList listeNoeudsDOM = noeudDOM.getChildNodes();
		for (int k = 0; k < listeNoeudsDOM.getLength(); k++) {
			Node nd = listeNoeudsDOM.item(k);
			DefaultMutableTreeNode enfant = creeNoeud(nd); // r�cusrsif
			if (enfant != null)
				noeudJT.add(enfant);
		}
		return noeudJT;
	}
	
	// un noeud est il affichable ?(pas de PI, de CDATA etc.)
	// uniquement un ELEMENT (noeud)  ou du texte (feuille) non vide
	protected boolean estAffichable(Node noeudDOM) {
		switch (noeudDOM.getNodeType()) {
		case Node.ELEMENT_NODE:
			return true;
		case Node.TEXT_NODE:
			String texte = noeudDOM.getNodeValue().trim();
			// uniquement si le texte inter-balises n'est pas un 'blanc' (saut de ligne ou chaine vide)
			return !(texte.equals("") || texte.equals("\n") || texte.equals("\r\n"));
		}
		return false;
	}

	// Retrouve le noeud JTree s�lectionn�
	public NoeudAffXml getNoeudSelection() {
		TreePath chemin = arbre.getSelectionPath();
		if (chemin == null)
			return null;
		Object obj = chemin.getLastPathComponent();
		if (!(obj instanceof NoeudAffXml))
			return null;
		return (NoeudAffXml) obj;
	}

	// Retourne le noeud DOM associ� au noeud JTree s�lectionn�
	public Node getNoeudSel() {
		NoeudAffXml noeud = getNoeudSelection();
		if (noeud == null)
			return null;
		return noeud.getNoeudXml();
	}

	// Affecte le noeud au mod�le de donn�es de la table
	public void affecteNoeudATable(Node noeud) {
		modeleTable.setNoeud(noeud);
	}

	public void affErreur(Exception ex, String message) {
		ex.printStackTrace();
		JOptionPane.showMessageDialog(this, message, NOM_APP, JOptionPane.WARNING_MESSAGE);
	}


}

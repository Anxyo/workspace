package editeur.xml;

import javax.swing.tree.DefaultMutableTreeNode;

import org.w3c.dom.Node;

class NoeudAffXml extends DefaultMutableTreeNode {

	private static final long serialVersionUID = 7960702781548015772L;

	public NoeudAffXml(Node noeud) {
		super(noeud);
	}

	public Node getNoeudXml() {
		Object obj = getUserObject();
		if (obj instanceof Node)
			return (Node) obj;
		return null;
	}

	public String toString() {
		Node noeud = getNoeudXml();
		if (noeud == null)
			return getUserObject().toString();
		StringBuffer sb = new StringBuffer();
		switch (noeud.getNodeType()) {
		case Node.ELEMENT_NODE:
			sb.append('<');
			sb.append(noeud.getNodeName());
			sb.append('>');
			break;
		case Node.TEXT_NODE:
			sb.append(noeud.getNodeValue());
			break;
		}
		return sb.toString();
	}
}
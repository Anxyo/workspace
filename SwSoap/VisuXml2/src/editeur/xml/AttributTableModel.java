package editeur.xml;

import javax.swing.table.AbstractTableModel;

import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

class AttributTableModel extends AbstractTableModel {
	private static final long serialVersionUID = -555256184792516615L;
	public static final int NOM_COLONNE = 0;
	public static final int VALEUR_COLONNE = 1;

	protected Node noeud;
	protected NamedNodeMap attrib;

	public void setNoeud(Node noeud) {
		this.noeud = noeud;
		this.attrib = noeud == null ? null : noeud.getAttributes();
		fireTableDataChanged();
	}

	public Node getNoeud() {
		return noeud;
	}
	
	@Override
	public int getRowCount() {
		if (attrib == null)
			return 0;
		return attrib.getLength();
	}
	@Override
	public int getColumnCount() {
		return 2;
	}
	@Override
	public String getColumnName(int nCol) {
		return nCol == NOM_COLONNE ? "Attribut" : "Valeur";
	}
	@Override
	public Object getValueAt(int nLigne, int nCol) {
		if (attrib == null || nLigne < 0 || nLigne >= getRowCount())
			return "";
		Attr attr = (Attr) attrib.item(nLigne);
		if (attr == null)
			return "";
		switch (nCol) {
		case NOM_COLONNE:
			return attr.getName();
		case VALEUR_COLONNE:
			return attr.getValue();
		}
		return "";
	}
	
	@Override
	public boolean isCellEditable(int nRow, int nCol) {
		return false;
	}
}
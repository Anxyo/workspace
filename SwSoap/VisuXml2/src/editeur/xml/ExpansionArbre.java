package editeur.xml;

import javax.swing.JTree;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

public class ExpansionArbre {
	public static void expansionArbre(JTree arbre) {
		TreeNode racine = (TreeNode) arbre.getModel().getRoot();
		TreePath chemin = new TreePath(racine);
		for (int k = 0; k < racine.getChildCount(); k++) {
			TreeNode enfant = (TreeNode) racine.getChildAt(k);
			expansionArbre(arbre, chemin, enfant);
		}
	}

	public static void expansionArbre(JTree arbre, TreePath chemin, TreeNode noeud) {
		if (chemin == null || noeud == null)
			return;
		arbre.expandPath(chemin);
		TreePath newPath = chemin.pathByAddingChild(noeud);
		for (int k = 0; k < noeud.getChildCount(); k++) {
			TreeNode child = (TreeNode) noeud.getChildAt(k);
			if (child != null) {
				expansionArbre(arbre, newPath, child);
			}
		}
	}
}

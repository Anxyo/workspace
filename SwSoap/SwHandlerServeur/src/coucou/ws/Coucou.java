package coucou.ws;

import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
@HandlerChain(file="chaine.xml")
public class Coucou {
	@WebMethod
	public String faitCoucou() {
		return "Coucou";
	}
}

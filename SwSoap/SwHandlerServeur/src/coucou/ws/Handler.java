package coucou.ws;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class Handler implements SOAPHandler<SOAPMessageContext> {

	@Override
	public void close(MessageContext arg0) {
		System.out.println("Serveur : close()");
	}

	@Override
	public boolean handleFault(SOAPMessageContext arg0) {
		System.out.println("Serveur : handleFault()");
		return false;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		SOAPMessage msg = context.getMessage();
		// analyse le sens du transport
		boolean direction = ((Boolean) context
				.get(SOAPMessageContext.MESSAGE_OUTBOUND_PROPERTY))
				.booleanValue();
		if (direction) {
			System.out.println("r�ponse");
		} else {
			System.out.println("requ�te");
		}
		// imprime le texte du message
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			msg.writeTo(baos);
			String xml = baos.toString();
			System.out.println(xml);
		} catch (SOAPException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// suite de la chaine
		return true;
	}

	@Override
	public Set<QName> getHeaders() {
		System.out.println("Serveur : getHeaders()");
		return null;
	}

}

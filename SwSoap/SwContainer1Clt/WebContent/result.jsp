<%@page contentType="text/html" pageEncoding="UTF-8" 
	import="calculette.serveur.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">
<%!
Calculette cs = new CalculetteProxy();
%>
<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>Résultat</title>
    	
  </head>
  <body>
    <h1>Résultat</h1>
    <%
    	double resultat = 0;
    	double oper1 = Double.parseDouble(request.getParameter("oper1"));
    	double oper2 = Double.parseDouble(request.getParameter("oper2"));
    	String operateur = request.getParameter("operateur");
    	if (operateur.equals("+")){
    		resultat = cs.addition(oper1,oper2);
    	} else if (operateur.equals("-")){
    		resultat = cs.soustraction(oper1,oper2);
    	} else if (operateur.equals("*")){
    		resultat = cs.multiplication(oper1,oper2);
    	}else if (operateur.equals("/")){
    		resultat = cs.division(oper1,oper2);
    	} 
    %>
    Le résultat semble être <%= resultat %> ... 
  </body>
</html> 

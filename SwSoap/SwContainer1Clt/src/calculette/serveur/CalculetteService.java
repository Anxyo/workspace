/**
 * CalculetteService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package calculette.serveur;

public interface CalculetteService extends javax.xml.rpc.Service {
    public java.lang.String getCalculettePortAddress();

    public calculette.serveur.Calculette getCalculettePort() throws javax.xml.rpc.ServiceException;

    public calculette.serveur.Calculette getCalculettePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}

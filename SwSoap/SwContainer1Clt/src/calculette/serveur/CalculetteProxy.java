package calculette.serveur;

public class CalculetteProxy implements calculette.serveur.Calculette {
  private String _endpoint = null;
  private calculette.serveur.Calculette calculette = null;
  
  public CalculetteProxy() {
    _initCalculetteProxy();
  }
  
  public CalculetteProxy(String endpoint) {
    _endpoint = endpoint;
    _initCalculetteProxy();
  }
  
  private void _initCalculetteProxy() {
    try {
      calculette = (new calculette.serveur.CalculetteServiceLocator()).getCalculettePort();
      if (calculette != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)calculette)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)calculette)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (calculette != null)
      ((javax.xml.rpc.Stub)calculette)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public calculette.serveur.Calculette getCalculette() {
    if (calculette == null)
      _initCalculetteProxy();
    return calculette;
  }
  
  public double addition(double arg0, double arg1) throws java.rmi.RemoteException{
    if (calculette == null)
      _initCalculetteProxy();
    return calculette.addition(arg0, arg1);
  }
  
  public double soustraction(double arg0, double arg1) throws java.rmi.RemoteException{
    if (calculette == null)
      _initCalculetteProxy();
    return calculette.soustraction(arg0, arg1);
  }
  
  public double division(double arg0, double arg1) throws java.rmi.RemoteException{
    if (calculette == null)
      _initCalculetteProxy();
    return calculette.division(arg0, arg1);
  }
  
  public double multiplication(double arg0, double arg1) throws java.rmi.RemoteException{
    if (calculette == null)
      _initCalculetteProxy();
    return calculette.multiplication(arg0, arg1);
  }
  
  
}
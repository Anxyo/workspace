/**
 * Calculette.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package calculette.serveur;

public interface Calculette extends java.rmi.Remote {
    public double addition(double arg0, double arg1) throws java.rmi.RemoteException;
    public double soustraction(double arg0, double arg1) throws java.rmi.RemoteException;
    public double division(double arg0, double arg1) throws java.rmi.RemoteException;
    public double multiplication(double arg0, double arg1) throws java.rmi.RemoteException;
}

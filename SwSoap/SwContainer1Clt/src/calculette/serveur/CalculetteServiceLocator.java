/**
 * CalculetteServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package calculette.serveur;

public class CalculetteServiceLocator extends org.apache.axis.client.Service implements calculette.serveur.CalculetteService {

    public CalculetteServiceLocator() {
    }


    public CalculetteServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CalculetteServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CalculettePort
    private java.lang.String CalculettePort_address = "http://localhost:8080/SwContainer1/CalculetteService";

    public java.lang.String getCalculettePortAddress() {
        return CalculettePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CalculettePortWSDDServiceName = "CalculettePort";

    public java.lang.String getCalculettePortWSDDServiceName() {
        return CalculettePortWSDDServiceName;
    }

    public void setCalculettePortWSDDServiceName(java.lang.String name) {
        CalculettePortWSDDServiceName = name;
    }

    public calculette.serveur.Calculette getCalculettePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CalculettePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCalculettePort(endpoint);
    }

    public calculette.serveur.Calculette getCalculettePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            calculette.serveur.CalculettePortBindingStub _stub = new calculette.serveur.CalculettePortBindingStub(portAddress, this);
            _stub.setPortName(getCalculettePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCalculettePortEndpointAddress(java.lang.String address) {
        CalculettePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (calculette.serveur.Calculette.class.isAssignableFrom(serviceEndpointInterface)) {
                calculette.serveur.CalculettePortBindingStub _stub = new calculette.serveur.CalculettePortBindingStub(new java.net.URL(CalculettePort_address), this);
                _stub.setPortName(getCalculettePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CalculettePort".equals(inputPortName)) {
            return getCalculettePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://serveur.calculette/", "CalculetteService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://serveur.calculette/", "CalculettePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CalculettePort".equals(portName)) {
            setCalculettePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}

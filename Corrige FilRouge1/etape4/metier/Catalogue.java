package etape4.metier;

import java.util.List;
import java.util.Vector;
/**
 * Repr�sente le catalogue des produits propos�s au visiteur.
 * Un seul objet de ce type devrait exister ("singleton").
 * Cet objet unique sera en lecture seule, une fois initialis�.
 * @author Administrateur
 *
 */
public class Catalogue {
	private List<Produit> produits;
	private static Catalogue leCatalogue = null;
	
	private Catalogue() {
		produits = new Vector<Produit>();
	}
	/**
	 * Retourne la liste des produits du catalogue
	 * @return
	 */
	public List<Produit> getListe() {
		return produits;
	}
	
	/**
	 * Ajoute un produit au catalogue
	 * @param p le produit � ajouter
	 */
	public void ajoutProduit(Produit p) {
		produits.add(p);
	}
	/**
	 * Retourne le nombre de produits au catalogue
	 * @return
	 */
	public int getNbProduits() {
		return produits.size();
	}
	
	public static Catalogue getInstance() {
		if (leCatalogue == null) {
			leCatalogue = new Catalogue();
			leCatalogue.initCatalogue();
		}
		return leCatalogue;
	}
	// initialise le catalogue avec des produits
	private void initCatalogue() {
		//ProduitDAO dao = new ProduitDAO();
		//produits = dao.chargeProduits();
		
		produits.add(new Produit(1,"Johnny Halliday", "Les 100 plus belles chansons",119.90,null));
		produits.add(new Produit(2,"Johnny Halliday", "Premier concert enregistr�",17.53,null));
		produits.add(new Produit(3,"Johnny Halliday", "Marie",1.89,null));
		produits.add(new Produit(4,"Johnny Halliday", "Ca ne finira jamais",10.99,null));
		produits.add(new Produit(5,"Johnny Halliday", "Olympia 2000",16.91,null));
		
	}
}

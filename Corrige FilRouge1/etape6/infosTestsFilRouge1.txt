Dans cette derni�re version, on va ajouter quelques tests unitaires pour v�rifier
que notre code fonctionne bien. Comme tests possible, mentionnons : 
 1 - Le parcourt du catalogue
 ============================
 	Le produit retourn� pour une position donn�e correspond-il au produit dans le
 	catalogue � cette position ?
 	Le nombre de produits retourn�s correspond il au nombre de produits du 
 	catalogue ?
 	Si on tente d'acc�der au produit en position �gale au nombre total de produits, 
 	reviens t'on au premier produit ?
 	Y a t'il bien une seule copie du catalogue ?
 	
 2 - Panier
 =========
 	Un produit ajout� au panier s'y trouve t'il effectivement ?
    
 	L'ajout du m�me produit plusieurs fois au panier en augmente t'il la
 	quantit�, si la quantit� en est > 0 ? 
 	Et si la quantit� vaut 0, la quantit� dans le panier ne devrait pas changer.
 	
 	Noter que l'on n'a pas de moyen de retirer des articles du panier !
 	
 3 - Utilisateurs et commandes
 =============================
    V�rifier que lors de la cr�ation d'un client, il est bien retourn� par la m�thode de cr�ation.
 	V�rifier que lors de la cr�ation d'une commande, le nombre de lignes de d�tail est bien le m�me
 	que le nombre d'articles dans le panier.
 	V�rifier que lors de la cr�ation d'une commande, le total de la commande est bien le m�me que le total 
 	du panier. 
 	 
 	
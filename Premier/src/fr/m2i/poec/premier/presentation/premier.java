package fr.m2i.poec.premier.presentation;

import fr.m2i.poec.premier.metier.Personne;

/** 
 * cette classe est la <em> premi�re </em> classe de notre 
 * apprentissage: elle d�montre: 
 * <ul>
 *    <li> La pr�sence d'un point d'entr�e <code> main() </code> </li>
 *    <li> Le mot clef statique  (<code> main() </code>). </li>
 * </ul>
 * @author Administrateur
 *
 */
public class premier {
	
	/**
	 * La m�thode principale (point d'entr�e)
	 * @param args un tableau de chaine de caract�res transmit par la ligne de commande
	 */
	
	public static void main(String [] args) {
		
		int a = somme(12,15);
		System.out.println(a);
		
		/**System.out.println("Bonjour nous");
		 
		*System.out.println(2);
		
		*Personne eric = null;
		*Personne pipo = null;
		
		*eric = new Personne("Paul", "Dupont", 22);  // une allocation m�moire
		*pipo = new Personne();                      // une AUTRE allocation m�moire avec le m�me �tat de base que eric
		*System.out.println(eric.toString());

		*eric.nom = "Desbois";   
		*eric.prenom = "Eric";
		*eric.parler();
		*pipo.pere = eric;
		*pipo.nom = "machin";
		*/		
	}
    
	private static int somme(int a, int b) {
	   int r = 0;
	   r = a + b;
	   return r;
	}
}

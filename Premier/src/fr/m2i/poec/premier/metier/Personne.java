package fr.m2i.poec.premier.metier;

/**
 * Représente une personne physique ou morale
 * @author Administrateur
 *
 */

public class Personne {
	public Personne pere = null, mere = null;     // attributs d'instance
	public String nom = null, prenom = null;
	private String nosecu = null;
	private int age = 0;
	
	public Personne(String prenom, String nom, int age) {
		this.prenom = prenom;
		this.nom = nom;
		if(age >= 0)
		   this.age = age;
	}

	public Personne() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * La personne vous parle
	 */
	
	public void parler() {
		System.out.println("bonjour, je suis " + this.nom + " " + this.prenom);
	}
	
	@Override
	public String toString() {
		return "nom: " + this.nom + " Prenom: " + this.prenom;
	}
}

  
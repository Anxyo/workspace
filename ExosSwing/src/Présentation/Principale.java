package Pr�sentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

/**Trois technos graphiques standards: 
 *   - awt: abstract Window Tooltip
 *   - swing (jusqu'� 2015)
 *   - java FX: 
 * 
 * @author Administrateur
 *
 */

public class Principale extends JFrame implements ActionListener {
	
	private JButton leBouton;

	public static void main(String[] args) {
		Principale p = new Principale();
	}

	
	Principale(){
		
		leBouton = new JButton("cliquez moi");
		leBouton.setBounds(50, 50, 120, 50);
		
		setBounds(100, 100, 600, 400);
		this.setLayout(null);
		this.add(leBouton);
		//gestion �v�nementielle
		//abonnement
		//GestEvent ge = new GestEvent();
		leBouton.addActionListener(this);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

	class GestEvent implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			leBouton.setText(leBouton.getText() + "+");
		}
	}
		
}


package presentation;

import static java.lang.System.out;

import java.util.Scanner;

import metier.Banque;
import metier.CompteException;
import metier.ICompte;

public class Principale {
	
	public static void main(String [] args){
		
		
		Scanner s = new Scanner(System.in);
		out.print("Entrez votre nom: ");
		String nomTitulaire = s.nextLine();
		out.print("Montant du d�pot: ");
		double depot = s.nextDouble();
		//cr�ation de la banque
		Banque b = new Banque("M2I");
		
		// cuisine pour transformer le double saisit en long.
		long leDepot = (long)(depot * 100);                        // conversion en centimes en retirant les �ventuels restes �pr�s la virgule
		try {
			String noCpt1 = b.creeCompteCourant(nomTitulaire, leDepot);
			String noCpt2 = b.creeCompteEpargne(nomTitulaire, leDepot);
			
			out.println(b.getEtatCompte(noCpt1));
			out.println(b.getEtatCompte(noCpt2));
			
			b.depot(noCpt1, 500);
			b.retrait(noCpt1, 100);
			
			out.println("Nouveau Solde: " + b.lireSolde(noCpt1) + " �cent");
			
			ICompte monCompte = b.getAccesDirect(noCpt1);
			System.out.println("Votre solde est de: " + monCompte.getSolde());

			
		}catch (CompteException e) {
			e.printStackTrace();
		}
	
	}

}

package metier;
import metier.Compte;

class Comptecourant extends Compte {

	public Comptecourant(String nomTitulaire, long leDepot) throws CompteException {
		super(nomTitulaire, leDepot);                                    //appelle de la m�thode Compte(String, long)
	}

	@Override
	public long retrait(long montantRetrait) throws CompteException {
		if(montantRetrait > 0) {
			if(montantRetrait <= this.solde) {
				this.solde -= montantRetrait;
			}else {
				throw new CompteException("Op�ration impossible, solde insuffisant", this.solde, montantRetrait, "retrait");
			}
		}else {
			throw new CompteException("Op�ration impossible, montant n�gatif", this.solde, montantRetrait, "retrait");
		}
		return this.solde;
	}
	
	
}

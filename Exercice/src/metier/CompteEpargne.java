package metier;
import metier.Compte;

class CompteEpargne  extends Compte{
	
	public static final double TAUX_STANDARD = 1.0;                                 //final d�finit une constante
	protected double tauxInteret = TAUX_STANDARD;
	
	
	public CompteEpargne(String titulaire, long depotInitial) throws CompteException {
		super(titulaire, depotInitial);
	}

	public CompteEpargne(String titulaire, long depotInitial, double tauxInteret) throws CompteException {
		super(titulaire, depotInitial);
		this.tauxInteret = tauxInteret;
	}

	
	
	@Override
	public long retrait(long montantRetrait) throws CompteException {
		//ajoute les int�r�ts
		solde += this.calculInterets(1);
		if(montantRetrait > 0) {
			if(montantRetrait <= this.solde) {
				this.solde -= montantRetrait;
			}else {
				throw new CompteException("Op�ration impossible, solde insuffisant", this.solde, montantRetrait, "retrait");
			}
		}else {
			throw new CompteException("Op�ration impossible, montant n�gatif", this.solde, montantRetrait, "retrait");
		}
		return this.solde;
	}
	
	private long calculInterets(int nbMois) {
		return (long)(this.solde * this.tauxInteret/100) * nbMois / 12;
	}
	
}

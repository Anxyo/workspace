package metier;

import java.util.HashMap;

public class Banque {
	
	HashMap<String,Compte> comptes;
	String nom;
	
	public Banque(String nomBanque) {
		this.nom = nomBanque;
		comptes = new HashMap<String,Compte>();
	}
	
	
	public String creeCompteCourant(String titulaire, long depotInitial) throws CompteException {
		Comptecourant cpt = new Comptecourant(titulaire, depotInitial);
		comptes.put(cpt.getNoCompte(), cpt);
		return cpt.getNoCompte();
	}
	
	public String creeCompteEpargne(String titulaire, long depotInitial) throws CompteException {
		CompteEpargne cpt = new CompteEpargne(titulaire, depotInitial);
		comptes.put(cpt.getNoCompte(), cpt);
		return cpt.getNoCompte();
	}

	public long depot(String noCompte, long montant) throws CompteException {
		//retrouve le compte
		Compte cpt = comptes.get(noCompte);
		long soldeCourant = 0;
		if(cpt == null)
			throw new CompteException("Compte inconnu", 0, montant, "depot");
		else {
			soldeCourant = cpt.depot(montant);
		}
		return soldeCourant;
	}
	
	public long retrait(String noCompte, long montant) throws CompteException {
		//retrouve le compte
		Compte cpt = comptes.get(noCompte);
		long soldeCourant = 0;
		if(cpt == null)
			throw new CompteException("Compte inconnu", 0, montant, "depot");
		else {
			soldeCourant = cpt.retrait(montant);
		}
		return soldeCourant;
	}
	
	public long lireSolde(String noCompte) throws CompteException {
		//retrouve le compte
		Compte cpt = comptes.get(noCompte);
		long soldeCourant = 0;
		if(cpt == null)
			throw new CompteException("Compte inconnu", 0, 0, "lecture solde");
		else {
			soldeCourant = cpt.getSolde();
		}
		return soldeCourant;
	}


	public String getEtatCompte(String noCompte) throws CompteException {
		//retrouve le compte
		Compte cpt = comptes.get(noCompte);
		String etatCompte = null;
		if(cpt == null)
			throw new CompteException("Compte inconnu", 0, 0, "lecture etat du compte");
		else {
			etatCompte = cpt.toString();
		}
		return etatCompte;
	}
	
	public ICompte getAccesDirect(String noCompte) throws CompteException {
		Compte cpt = comptes.get(noCompte);
		if(cpt == null)
			throw new CompteException("Compte inconnu", 0, 0, "lecture etat du compte");
		return cpt;
	}
	
}

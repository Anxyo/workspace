package metier;

abstract class Compte implements ICompte{
	
	//attributs (champs): "un compte en banque a un solde, un titulaire et un num�ro unique"
	protected long solde;
	protected String titulaire;
	protected String noCompte;
	private static int compteur;
	
	
	//constructeurs
	public Compte(long depotInitial) throws CompteException {
		this("compte anonyme", depotInitial);
	}
	
	
	public Compte(String titulaire) throws CompteException {
		this(titulaire, 0);
	}
	
	
	public Compte(String titulaire, long depotInitial) throws CompteException {
		if(titulaire != null)
			this.titulaire = titulaire;
		else {
			throw new CompteException("Titulaire non d�finit", this.solde, depotInitial,
					"cr�ation");	
		}
		if(depotInitial >= 0) {
			this.solde = depotInitial;
		}else {
			throw new CompteException("Depot n�gatif interdit", this.solde, depotInitial, "depot");
		}
		this.noCompte = "M2I" + ++compteur;	
	}
	
	
	
	//accesseurs
	public String getTitulaire() {
		return titulaire;
	}
	
	
	public long getSolde() {
		return solde;
	}
	
	
	public String getNoCompte() {
		return noCompte;
	}

	
	
	
	//m�thodes
	public long depot(long montantDepot) throws CompteException {
		if(montantDepot > 0) {
			this.solde = this.solde + montantDepot;    // ou this.solde += montantDepot
		}else 
			throw new CompteException("Depot n�gatif interdit", this.solde, montantDepot, "depot");
		
		return this.solde;
	}
	
	
	public abstract long retrait(long montantRetrait) throws CompteException;
	
	
	/*
	public double retrait(double montantRetrait) {
		if(montantRetrait > 0) {
			if(montantRetrait <= this.solde) {
				this.solde -= montantRetrait;
			}else {
				System.out.println("Op�ration impossible, solde insuffisant");
			}
		}else {
			System.out.println("Op�ration impossible, montant n�gatif");
		}
		return this.solde;
	}
	*/
	
	String afficheCompte() {
		String s = "Compte de " + this.titulaire
				+ ", n�: " + this.noCompte 
				+ ", solde courant: " + this.solde / 100L                //100L: 100 au format long
				+ " � et " + this.solde % 100L                               // %: modulo: ne garde que le reste de la division, soit ici les centimes
				+ " cent";
		return s;
	}
	
	
	@Override
	public String toString() {
		return this.afficheCompte();
	}
}

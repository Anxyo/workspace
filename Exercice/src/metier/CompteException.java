package metier;

public class CompteException extends Exception {
	
	private static final long serialVersionUID = 5866980760316131473L;
	long soldeCourant, montantOperation;
	String nomOperation;
	
	
	public CompteException(String message, long soldeCourant, long montantOperation, String nomOperation) {
		super(message);
		this.soldeCourant = soldeCourant;
		this.montantOperation = montantOperation;
		this.nomOperation = nomOperation;
	}


	public CompteException(String message, Throwable cause, long soldeCourant, long montantOperation,
			String nomOperation) {
		super(message, cause);
		this.soldeCourant = soldeCourant;
		this.montantOperation = montantOperation;
		this.nomOperation = nomOperation;
	}


	public long getSoldeCourant() {
		return soldeCourant;
	}


	public void setSoldeCourant(long soldeCourant) {
		this.soldeCourant = soldeCourant;
	}


	public long getMontantOperation() {
		return montantOperation;
	}


	public void setMontantOperation(long montantOperation) {
		this.montantOperation = montantOperation;
	}


	public String getNomOperation() {
		return nomOperation;
	}


	public void setNomOperation(String nomOperation) {
		this.nomOperation = nomOperation;
	}


	@Override
	public String toString() {
		return "CompteExeption [solde courant= " + soldeCourant + ", montant de l'operation= " + montantOperation
				+ ", nom de l'operation= " + nomOperation + ", Message= " + getMessage() + "]";
	}
	
	
	
	
	
	
}

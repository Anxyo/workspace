1: Un compte en banque a un solde, un titulaire et un num�ro unique (g�n�r� auto.).
   On peut y d�poser ou en retirer de l'argent.
   On aimerait avoir une m�thode permettant d'afficher tout l'�tat du compte.
   On souhaite pouvoir retrouver les infos. du compte, mais pas les modifier une fois cr�es.
   => Faire un main cr�ant quelques comptes, y faisant des op�rations et affichant l'�tat des comptes.

2: On veut maintenant utiliser des comptes courants et des comptes �pargne.
    Un compte courant permet de faire des op�rations � tout moment, tandis qu'un compte �pargne ne permet
   des op�rations qu'� un certain moment mais a un taux d'int�r�t.
    Un compte �pargne utilise un taux d'int�r�t standard, mais on peut n�gocier �ventuellement un autre taux 
   qui doit rester raisonnable 
    => on aura donc deux constructeurs:
     - Un sans taux n�goci�, recevant juste le nom et le montant du d�p�t.
     - un second, recevant en plus un taux n�goci�.
     
   Lors d'un retrait mensuel possible, on calcule les int�r�ts collect�s par ce compte.
   
   
Exercice Exceptions: 

  On va cr�er deux classes: 
  
   - une classe voiture abstraite d�finissant quatre m�thodes obligatoires pour les classes enfants: d�marrer(), stopper(), acc�l�rer() et freiner().
   - une classe de voiture pr�cise (M�gane2) qui h�rite de voiture et qui red�finit acc�l�rer.
   
      La m�thode acc�l�rer re�oit la vitesse � atteindre, et apr�s quelques secondes, elle retourne la vitesse effectivement atteinte.
      Si on passe une vitesse sup�rieure � 110, elle d�clenche une voitureException.
  
  
  
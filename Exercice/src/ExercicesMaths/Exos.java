package ExercicesMaths;

import java.util.Scanner;

public class Exos {
	
		/** Demander � un utilisateur via la console de saisir un nombre de termes
		  * pour le calcul de Fibonacci:
		  * u(n) = u(n-1) + u(n-2) u(0) == 0 et u(1) == 1
		  * ou de la s�rie harmonique : 1/1 + 1/2 + 1/3 +1/4
		  * ou la factorielle 8*7*6*5*4*3*2*1 = 8!
		  */
		
		public static void main(String [] args) {
			

			
			
			
			Scanner s = new Scanner(System.in);
			System.out.print("Entrez le nombre de termes: ");
			int nbTermes = s.nextInt();
			
			int [] tabInt = {1,2,3,4,5};
			System.out.printf("Somme= %d\n", sommeTab(tabInt));
			
			System.out.printf("S�rie harmonique de %d termes = %f", nbTermes, harmo(nbTermes));
			System.out.printf("\n La factorielle pour %d est: %f",  nbTermes, facto(nbTermes));
				
			}	
			

		private static int sommeTab(int [] tabInt) {
			int somme = 0;
			for(int i = 0; i < tabInt.length; i++) {
				somme += tabInt[i];
			}
			return somme;
		}


		private static double facto(int nbTermes) {
			int fac = 1;
			for(int i = 1; i <= nbTermes; i++) {
				fac *= i;
			}
			
			return fac;
			}


		private static double harmo(int nbTermes) {
			double r = 0;
			for(int i = 1; i < nbTermes; i++) {
				r += 1.0/i;
			}
			return r;
		}
	

}
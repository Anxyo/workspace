package Pr�sentation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Principale {

	public final static String urlJDBC = "jdbc:mysql://localhost:3306/pubs";
	public final static String uid = "root";
	public final static String pwd = "Lilou";
	public final static String nomClasseDriver = "com.mysql.jdbc.Driver";
	
	public static void main(String[] args) {
		
		try {
			
			Connection cnx = getConnection();
			
			//La classe du driver doit-�tre charg�e en m�moire
			String rqt = "SELECT * FROM stores;";
			
			ResultSet rs = getResultSet(rqt, cnx);     //tableau import� avec les donn�es du tableau de la bdd
			
			while(rs.next()) {
				System.out.printf("id= %s, nom= %s, adresse= %s, ville= %s, etat= %s, code postal= %s\n",
						rs.getString("stor_id"), rs.getString("stor_name"),
						rs.getString("stor_address"), rs.getString("city"),
						rs.getString("state"), rs.getString("zip"));
				
			}
			
			rs.close();
			cnx.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private static Connection getConnection() {
		Connection cnx = null;
		try {
			Class.forName(nomClasseDriver);
			cnx = DriverManager.getConnection(urlJDBC, uid, pwd);

		}catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
			//La classe du driver doit-�tre charg�e en m�moire
		return cnx;
	}
	
	private static ResultSet getResultSet(String LaRequeteIci, Connection cnx) {
		ResultSet rs = null;
		Statement st;
		try {
			st = cnx.createStatement();
			rs = st.executeQuery(LaRequeteIci);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

}

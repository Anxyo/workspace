package metier;

import java.util.List;
import java.util.Vector;
/**
 * Repr�sente le catalogue des produits propos�s au visiteur.
 * Un seul objet de ce type devrait exister ("singleton").
 * Cet objet unique sera en lecture seule, une fois initialis�.
 * @author Administrateur
 *
 */
public class Catalogue {
	private List<Produit> produits;
	private static Catalogue leCatalogue = null;
	
	private Catalogue() {
		produits = new Vector<Produit>();
	}
	/**
	 * Retourne la liste des produits du catalogue
	 * @return
	 */
	public List<Produit> getListe() {
		return produits;
	}
	
	/**
	 * Ajoute un produit au catalogue
	 * @param p le produit � ajouter
	 */
	public void ajoutProduit(Produit p) {
		produits.add(p);
	}
	/**
	 * Retourne le nombre de produits au catalogue
	 * @return
	 */
	public int getNbProduits() {
		return produits.size();
	}
	
	public static Catalogue getInstance() {
		if (leCatalogue == null) {
			leCatalogue = new Catalogue();
			leCatalogue.initCatalogue();
		}
		return leCatalogue;
	}
	// initialise le catalogue avec des produits
	private void initCatalogue() {
		//ProduitDAO dao = new ProduitDAO();
		//produits = dao.chargeProduits();
		
		produits.add(new Produit(1,"Bob Marley", "Exodus",29.90, "exodus.jfif"));
		produits.add(new Produit(2,"Bob Marley", "Chances Are",45.50, "chances-are.jfif"));
		produits.add(new Produit(3,"Bob Marley", "Survival",27.50,"survival.jfif"));
		produits.add(new Produit(4,"Bob Marley", "Rastaman Vibration",29.90,"rastaman-vibration.jfif"));
		produits.add(new Produit(5,"Bob Marley", "Uprising",25.50,"uprising.jfif"));
	}
}

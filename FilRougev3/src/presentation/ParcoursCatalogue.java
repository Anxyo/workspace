package presentation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import metier.Panier;
import service.Facade;


@WebServlet("/Catalogue")
public class ParcoursCatalogue extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer position = (Integer) request.getSession().getAttribute("position");
		//instanciation de la fa�ade
		Facade f = new Facade();
		//instanciation du panier
		Panier panier = (Panier) request.getSession().getAttribute("panier");
		Integer quantite = (Integer) request.getSession().getAttribute("quantite");
		
		if (position == null) {
			position = 0;
			System.out.println(position);
		}
		if (panier == null) {
			panier = new Panier();
		}
		//f.setPanier(panier);
		
		//qui m'appelle?
		if (request.getParameter("path") != null) {
			System.out.println("acceuil");
		} else if (request.getParameter("suivant") != null) {
			System.out.println("suivant");
			position++;
			System.out.println(position);
			if (position >= f.getNbProduits()) {
				position = 0;
			}	
		} else if (request.getParameter("precedent") != null) {
			System.out.println("precedent");
			position--;
			System.out.println(position);
			if (position < 0) {
				position = f.getNbProduits() - 1;
			}
		} else if (request.getParameter("ajoutPanier") != null) {
			f.ajoutePanier(position, quantite);
		} else if (request.getParameter("panier") != null) {
			panier = f.getPanier();
		}
		 
		//sauver la position
		request.getSession().setAttribute("position", position);
		// place le produit dans la requete
		request.setAttribute("produit", f.getProduit(position));
		
		//sauve le panier dans la session
		request.getSession().setAttribute("panier", panier);
		
		// passe la main au JSP
		request.getRequestDispatcher("/catalogue/page.jsp").forward(request, response);
		
	}

}

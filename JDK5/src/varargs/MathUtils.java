package varargs;


// classe minimaliste retournant le maximum (!) d'une liste de valeurs pass�es en param�tre 
public class MathUtils {

  public static int max(int... valeurs) {
    if (valeurs.length == 0) {
      throw new IllegalArgumentException("Aucune valeur transmise !");
    }

    int max = Integer.MIN_VALUE;
    for (int i : valeurs) {		// technique de l'�cr�mage de valeur
      if (i > max)
        max = i;
    }
    return max;
  }
}
package varargs;

import java.io.IOException;
import java.io.PrintStream;

public class ExemplesVarargs {

  public ExemplesVarargs() {
  }

  private int[] getListeDeNombres() {
    int[] nombres = new int[] {0, 2, 4, 6, 8, 10, 9, 7, 5, 3, 1};
    return nombres;
  }

  private String imprime(Object... valeurs) {
    StringBuilder sb = new StringBuilder();
    for (Object o : valeurs) {
      sb.append(o.toString())
        .append(" ");
    }
    return sb.toString();
  }

  public void testMethodeMax(PrintStream out) throws IOException {
    int max = MathUtils.max(22,18,45,13,-5);
    out.println("Le maximum de la liste est : " + max);
    int[] nombres = getListeDeNombres();
    max = MathUtils.max(nombres);
    out.println("Maintenant, le maximum de la liste est : " + max);
  }

  public void appelleImprime(PrintStream out) throws IOException {
    out.println(imprime("pipo", 23, -12, 1.23, getListeDeNombres()));
  }

  public void appelleArgsTableaux(PrintStream out) throws IOException {
    Object[] obj = new String[] {"Salut", "tout", "le", "monde", "ici !"};

    out.printf("%s\n", obj);
    out.printf("%s\n", (Object)obj);
  }

  public static void main(String[] args) {
    try {
      ExemplesVarargs aTester = new ExemplesVarargs();

      aTester.testMethodeMax(System.out);
      aTester.appelleImprime(System.out);
      aTester.appelleArgsTableaux(System.out);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
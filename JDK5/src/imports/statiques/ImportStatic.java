package imports.statiques;

import static java.lang.System.err;
import static java.lang.System.out;

import java.io.IOException;
import java.io.PrintStream;

public class ImportStatic {

  public static void errEcriture(PrintStream err, String msg) 
    throws IOException {
    // Noter que le param�tre err masque l'import err
    err.println(msg); 
  }

  public static void main(String[] args) {
    if (args.length < 2) {
      err.println("Usage incorrect : ImportStatic [arg1] [arg2]");
      return;
    }
    
    out.println("Bonjour " + args[0]);
    out.println("Passez une " + args[1] + " journ�e !");

    try {
      errEcriture(System.out, "Aie, la cagade !");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
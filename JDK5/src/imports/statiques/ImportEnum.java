package imports.statiques;

import static java.lang.System.out;
// noter l'usage de l'�toile
import static types.enumeres.TypeStage.*;

import java.io.IOException;
import java.io.PrintStream;

import types.enumeres.Stagiaire;

public class ImportEnum {
  private Stagiaire[] stagiaires = new Stagiaire[4];

  public ImportEnum() {
    stagiaires[0] = new Stagiaire("Pierre", "Dupont");
    stagiaires[0].setTypeStage(AUTRE);
    stagiaires[1] = new Stagiaire("Paul", "Dubois");
    stagiaires[1].setTypeStage(BUREAUTIQUE);
    stagiaires[2] = new Stagiaire("Pierre", "Durand");
    stagiaires[2].setTypeStage(RESEAU);
    stagiaires[3] = new Stagiaire("Fran�ois", "Goesling");
    stagiaires[3].setTypeStage(DEVELOPPEMENT);
  }

  public void listeStagiaires(PrintStream out) throws IOException {
    for (Stagiaire stagiaire : stagiaires) {
      if ((stagiaire.getTypeStage() == AUTRE) || 
          (stagiaire.getTypeStage() == BUREAUTIQUE)) {
        // ici, faire qquechose de sp�cifique
      }
    }
  }

  public static void main(String[] args) {
    try {
      ImportEnum imp = new ImportEnum();
      imp.listeStagiaires(out);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
package boxing;

public class ExemplesConversions {

  public static void main(String[] args) {
    // Boxing
    int pipo = 0;
     // fait automatiquement : Integer entier = new Integer(pipo);
    Integer entier = pipo;		// cette action est appel�e "boxing" ou "autoboxing"

    // Unboxing minimal
    // Fait automatiquement :  int pipo2 = entier.intValue(); 
    @SuppressWarnings("unused")
	int pipo2 = entier;			// cette action est appel�e "unboxing"

    // attention au null !
    // Integer leNull = null;
    // int j = leNull;		// NullPointerException !!!
    
    Integer compteur = 1;        // boxing
    @SuppressWarnings("unused")
	int compteur2 = compteur;     // unboxing
    while (true) {
      System.out.printf("It�ration %d%n", compteur++);
      if (compteur > 5) break;
    }

    Integer a = 5, b = 10, c;
    c = a + b;
    System.out.println("Valeur de c = " + c);
    
    Boolean drapeau1 = true;
    Boolean drapeau2 = true;
    boolean drapeau3 = false;

    @SuppressWarnings("unused")
	Boolean resultat = (drapeau1 || drapeau2) && drapeau3;

    Integer i1 = 256;
    Integer i2 = 256;

    if (i1 == i2) System.out.println("Egal !");
    else System.out.println("Non egal!");

    Boolean arrivee = false;
    Boolean retard = true;

    System.out.println(arrivee ? (retard ? "Il est plus que temps !" : "Salut !") : 
                                  (retard ? "Je n'y croyais plus !" : "A plus !"));
  
    int test = 15;
    methode1(test); // quelle m�thode est appel�e ?
    // en fait void methode1(double d), 
    // les boxing/unboxing n'affectent pas la surcharge
    // afin de rester compl�tement compatible avec le JDK 1.4...
    
  }
  
  static void methode1(double d)  {
	  System.out.println("Je suis la m�thode recevant un double.");
  }
  static void methode1(Integer i)  {
	  System.out.println("Je suis la m�thode recevant un Integer.");
  }
  
}
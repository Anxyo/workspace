// thread7.java
// ------------
// impl�mentation d'un m�canisme d'attente et de 
// rendez vous inter-threads.
// Noter que pour que cela fonctionne les deux threads
// doivent appeler Attend() / Lib�re() du meme objet
// et non de deux instances diff�rentes...
package threading;

public class Thread7 {

    public static void main(String args[]) {
    // n�cessairement final, car r�f�renc� dans des classes anonymes
    final Partage p = new Partage();
    // deux threads impl�ment�s sous forme de classes anonymes
    // seules les m�thodes run() sont red�finies.
    Thread t1 = new Thread() {
				public void run(){
				    System.out.println("t1, j'appelle Attend()...");
				    p.Attend();
				    System.out.println("t1, apr�s Attend()");
				}
				};
     Thread t2 = new Thread() {
				public void run(){
				    System.out.println("t2, bien avant Lib�re()");
				    // attend 500 ms
				    try {sleep(500);} catch(InterruptedException e){}
				    System.out.println("t2, j'appelle Lib�re()");
				    p.Lib�re();
				    System.out.println("t2, juste apr�s Lib�re()");
				     // attend � nouveau 500 ms
				    try {sleep(500);} catch(InterruptedException e){}
				    System.out.println("t2, bien apr�s Lib�re()");
				}   // fin de run
				};  // fin de la classe anonyme
    // d�marrage des deux threads
    t1.start();
    t2.start();		
    System.out.println("Thread principal : les 2 threads sont d�marr�es, j'attend leur mort...");		
    // attente de la mort des deux threads
    try {
	t1.join();
	t2.join();
	}
    catch(InterruptedException e){System.out.println(e.getMessage());}
    System.out.println("Thread principal : tout est fini, maintenant !");	
    }	// fin de main()
    
}	// fin de la classe thread7

// classe servant � impl�menter un rendez-vous avec wait() / notify()
class Partage {
    // le thread entrant dans cette routine est mis en attente
    // et ne sera lib�r� que par un autre thread entrant dans
    // la m�thode Lib�re() du MEME OBJET. 
    synchronized void Attend() {
    try {
	wait();
    }
    catch (InterruptedException e) {
	System.out.println(e.getMessage());
	}
    }
    
    // d�bloque le thread bloqu� par le wait()
    synchronized void Lib�re() {
	notify();
    }
    
}
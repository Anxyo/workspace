
// thread6.java
// Utilisation de yield()
// Manipulation des niveaux de prioritÚs.
package threading;

class ThreadTest extends Thread {
	String s;
	ThreadTest(String s) {
		this.s = s;
	}

	public void run() {
		int i = 0;
		while (i < 500) {
			System.out.println(s);
			i++;
			//this.yield();
		}
	}
}

public class Thread6 {
	public static void main(String argv[]) {
		ThreadTest JA = new ThreadTest("JA");
		ThreadTest VA = new ThreadTest("VA");

		JA.setPriority(8);
		VA.setPriority(1);
		JA.start();
		VA.start();
		try {
			JA.join();
			VA.join();
		} catch (InterruptedException e) {
		}
		System.out.println("Niveau de prioritÚ max : " + Thread.MAX_PRIORITY);
		System.out.println("Niveau de prioritÚ min : " + Thread.MIN_PRIORITY);
	}
}
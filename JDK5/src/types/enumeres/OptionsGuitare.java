package types.enumeres;

public enum OptionsGuitare implements Option {

  BOIS_ROSE(0),     
  ACAJOU(0),       
  ZIRICOTE(300),   

  EPICEA(0),          
  CEDRE(0),           

  ORME_ROSETTE(75),    
  ORME_BORD_HAUT(400), 

  INCRUST_DIAM(150),   
  INCRUST_POINT(0);       

  /** le co�t pour cette option */
  private float surcout;

  OptionsGuitare(float surcout) {
    this.surcout = surcout;
  }

  public float getSurcout() {
    return surcout;
  }

  public String getDescription() {
    switch(this) {
      case BOIS_ROSE:      	return "Fond et cot�s en bois de rose";
      case ACAJOU:      	return "Fond et cot�s en acajou";
      case ZIRICOTE:      	return "Fond et cot�s en ziricote";
      case EPICEA:        	return "Manche en �picea";
      case CEDRE:         	return "Manche en c�dre";
      case ORME_ROSETTE:    return "Rosette en orme";
      case ORME_BORD_HAUT: 	return "Bordure en haut du manche en orme";
      case INCRUST_DIAM:   
    	  					return "Incrustations en diamands et rectangles de nacre";
      case INCRUST_POINT:
    	  					return "Incrustations sous forme de points de nacre";
      default: 				return "Option inconnue !";
    }
  }
}
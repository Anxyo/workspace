package types.enumeres;

public class AnciennesOptionsGuitare {
  public static final int BOIS_ROSE     = 0x01; 
  public static final int ACAJOU      	= 0x02; 
  public static final int ZIRICOTE      = 0x04; 
  public static final int EPICEA        = 0x08; 
  public static final int CEDRE         = 0x10; 
  public static final int ORME_ROSETTE    = 0x20; 
  public static final int ORME_BORD_HAUT = 0x40; 
  public static final int INCRUST_DIAM   = 0x80; 
  public static final int INCRUST_POINT       = 0x100;
}
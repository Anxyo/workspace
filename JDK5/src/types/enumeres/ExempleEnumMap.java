package types.enumeres;

import java.io.IOException;
import java.io.PrintStream;
import java.util.EnumMap;

public class ExempleEnumMap {

  public void testEnumMap(PrintStream out) throws IOException {
  
	String [] msg = new String[] {  "Initialisation de Ant...",
									"Compilation des classes Java...",
									"Copie des fichiers...",
									"Cr�ation des fichiers jar...",
									"Cr�ation des fichiers ZIP...",
									"Construction termin�e.",
									"Erreur !"};
	out.println("Acc�s � un message : on utilise une des valeurs des constantes.");
	out.println("Exemple : message CREATION_JAR : '" + msg[AncienEtatAnt.CREATION_JAR] + "'.\n");	  
	  
	// Cr�ation d'une map avec une cl� �num�r�e et une chaine de message
    EnumMap<EtatAnt, String> msgAnt = new EnumMap<EtatAnt, String>(EtatAnt.class);

    // Initialisation de la map
    msgAnt.put(EtatAnt.INITIALISATION,	"Initialisation de Ant...");
    msgAnt.put(EtatAnt.COMPILATION,		"Compilation des classes Java...");
    msgAnt.put(EtatAnt.COPIE,      		"Copie des fichiers...");
    msgAnt.put(EtatAnt.CREATION_JAR,	"Cr�ation des fichiers jar...");
    msgAnt.put(EtatAnt.AJOUT_ZIP,      "Cr�ation des fichiers ZIP...");
    msgAnt.put(EtatAnt.TERMINE,        "Construction termin�e.");
    msgAnt.put(EtatAnt.ERREUR,        	"Erreur !");

    // parcourt de la collection
    for (EtatAnt etat : EtatAnt.values() ) {
      out.println("Pour l'etat '" + etat + "', le message associe est : " +
                  msgAnt.get(etat));
    }
  }
  
  

  public static void main(String[] args) {
    try {
      ExempleEnumMap o = new ExempleEnumMap();
      o.testEnumMap(System.out);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
package types.enumeres;

import java.io.IOException;
import java.io.PrintStream;

public class ExempleStage {

  private Stagiaire stagiaire1, stagiaire2, stagiaire3;

  public ExempleStage() { 
    stagiaire1 = new Stagiaire("Pierre", "Michon");
    stagiaire2 = new Stagiaire("Fred", "Nicaud");
    stagiaire3 = new Stagiaire("Evelyne", "Pillet");
  }

  public void expleAffectationEnum(PrintStream out) throws IOException {
    stagiaire1.setTypeStage(TypeStage.BUREAUTIQUE);
    stagiaire2.setTypeStage(TypeStage.AUTRE);
    stagiaire3.setTypeStage(TypeStage.DEVELOPPEMENT);
  }

  public void listeEnum(PrintStream out) throws IOException {
    TypeStage[] tabValeurs = TypeStage.values();

    // avec une boucle for 
    for (int i=0; i< tabValeurs.length; i++) {
      out.println("Valeur : '" + tabValeurs[i] + "'");
    }

    // avec une boucle for/in
    for (TypeStage v : tabValeurs ) {
      out.println("Valeur : '"+ v + "'");
    }
  }

  public void explUtilSwitch(PrintStream out) throws IOException {
    
    StringBuffer outputText = new StringBuffer(stagiaire1.getNomComplet());
    switch (stagiaire1.getTypeStage()) {
      case AUTRE: 
        outputText.append(" a suivi un stage d'un AUTRE type (??)");
        break;   
      case BUREAUTIQUE:
              outputText.append(" a un profil de bureauticien et a suivi un stage de base de ")
                  .append(stagiaire1.getTypeStage().toString());
        break;
      case RESEAU: // traité comme SYSTEMES
      case SYSTEMES:
        outputText.append(" a un profil d'administrateur et a suivi un stage de ")
                  .append(stagiaire1.getTypeStage().toString());
        break;
      case WEB:
          outputText.append(" a un profil de WebMaster et a suivi un stage concernant les technologies ")
          .append(stagiaire1.getTypeStage().toString());
        break;
      default:
        outputText.append(" a probablemant un profil de développeur... Pouah ! Stage : ")
                  .append(stagiaire1.getTypeStage().toString());
       break;
    }
    out.println(outputText.toString());
  }

  public static void main(String[] args) {
    try {
      ExempleStage test = new ExempleStage();
  
      test.expleAffectationEnum(System.out);
      test.listeEnum(System.out);
      test.explUtilSwitch(System.out);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
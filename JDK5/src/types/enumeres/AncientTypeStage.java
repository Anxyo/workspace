package types.enumeres;

public class AncientTypeStage {
  public static final int BUREAUTIQUE = 1;
  public static final int RESEAU = 2;
  public static final int SYSTEMES = 3;
  public static final int DEVELOPPEMENT = 4;
  public static final int WEB = 5;
  public static final int AUTRE = 6;
}

package types.enumeres;

public interface Option {

  /** Quel est le co�t pour cette option ? */
  public float getSurcout();

  /** La description de cette option */
  public String getDescription();

}
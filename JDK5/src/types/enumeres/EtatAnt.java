package types.enumeres;

public enum EtatAnt {
  INITIALISATION,
  COMPILATION,
  COPIE,
  CREATION_JAR,
  AJOUT_ZIP,
  TERMINE,
  ERREUR
}
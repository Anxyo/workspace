package types.enumeres;

public class AncienEtatAnt {
  public static final int INITIALISATION = 0;
  public static final int COMPILATION    = 1;
  public static final int COPIE      	 = 2;
  public static final int CREATION_JAR   = 3;
  public static final int AJOUT_ZIP      = 4;
  public static final int TERMINE        = 5;
  public static final int ERREUR         = 6;
}

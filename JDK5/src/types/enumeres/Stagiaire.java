package types.enumeres;

public class Stagiaire {

  private String prenom;
  private String nom;
  private TypeStage typeStage;

  public Stagiaire(String prenom, String nom) {
    this.prenom = prenom;
    this.nom = nom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public String getPrenom() {
    return prenom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public String getNom() {
    return nom;
  }

  public String getNomComplet() {
    return new StringBuffer(prenom)
           .append(" ")
           .append(nom)
           .toString();
  }

  public void setTypeStage(TypeStage typeStage) {
    this.typeStage = typeStage;
  }

  public TypeStage getTypeStage() {
    return typeStage;
  }
}
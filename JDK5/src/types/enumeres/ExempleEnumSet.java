package types.enumeres;

import java.util.EnumSet;
import java.io.PrintStream;

public class ExempleEnumSet {

	public void utilAnciennesOptions(PrintStream out) {
		// pour combiner les options on fait un OU bitwise
		int listeOptions = 	AnciennesOptionsGuitare.BOIS_ROSE |
							AnciennesOptionsGuitare.INCRUST_DIAM;
		// pour tester quelles options sont retenues, on fait
		// un AND bitwise.
		out.println("Options choisies (Ancienne technique) : ");
		out.println("- Bois de rose          : " + 
						(((listeOptions & AnciennesOptionsGuitare.BOIS_ROSE) != 0) ?"oui":"non"));
		out.println("- Acajou                : " + 
						(((listeOptions & AnciennesOptionsGuitare.ACAJOU) != 0) ?"oui":"non"));
		out.println("- Ziricote              : " + 
						(((listeOptions & AnciennesOptionsGuitare.ZIRICOTE) != 0) ?"oui":"non"));
		out.println("- Epicea                : " + 
						(((listeOptions & AnciennesOptionsGuitare.EPICEA) != 0) ?"oui":"non"));
		out.println("- C�dre                 : " + 
						(((listeOptions & AnciennesOptionsGuitare.CEDRE) != 0) ?"oui":"non"));
		out.println("- Incrustation diamants : " + 
						(((listeOptions & AnciennesOptionsGuitare.INCRUST_DIAM) != 0) ?"oui":"non"));
	}

	public void utilOptionsEnumerees(PrintStream out) {
		// pour combiner les options on utilise la m�thode static of()
		EnumSet<OptionsGuitare> options = EnumSet.of(OptionsGuitare.BOIS_ROSE,OptionsGuitare.INCRUST_DIAM);
		// pour tester quelles options sont retenues, on utilise la m�thode contains()
		out.println("\nOptions choisies : (Nouvelle technique)");
		out.println("- Bois de rose          : " + 
						(options.contains(OptionsGuitare.BOIS_ROSE) ?"oui":"non"));
		out.println("- Acajou                : " + 
						(options.contains(OptionsGuitare.ACAJOU) ?"oui":"non"));
		out.println("- Ziricote              : " + 
						(options.contains(OptionsGuitare.ZIRICOTE) ?"oui":"non"));
		out.println("- Epicea                : " + 
						(options.contains(OptionsGuitare.EPICEA)  ?"oui":"non"));
		out.println("- C�dre                 : " + 
						(options.contains(OptionsGuitare.CEDRE) ?"oui":"non"));
		out.println("- Incrustation diamants : " + 
						(options.contains(OptionsGuitare.INCRUST_DIAM) ?"oui":"non"));
	}
	
	
	public static void main(String[] args) {
		 ExempleEnumSet o = new ExempleEnumSet();
		 o.utilAnciennesOptions(System.out);
		 o.utilOptionsEnumerees(System.out);
	}

}

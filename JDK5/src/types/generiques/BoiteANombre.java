package types.generiques;

import java.util.Iterator;
import java.util.ArrayList;

// une boite contenant des nombres uniquement : noter la contrainte pos�e sur le type g�n�rique
public class BoiteANombre<N extends Number> extends Boite<N> {

	// ill�gal car une var stati est paratg�e entre des instances diff�rentes
	// pour lesquelles N est potentiellement diff�rent. De quel N parle t'on ici ?
	//private static java.util.List<N> liste = new ArrayList<N>();

	public BoiteANombre() {
		super();
	}

	// Additionne tout ce qui est dans la boite
	public double somme() {
		double total = 0;
		for (Iterator<N> i = contenu.iterator(); i.hasNext();) {
			total = total + i.next().doubleValue();
		}
		return total;
	}

	// Encore plus fort, la somme des �l�ments de 2 boites !
	// Deux versions possibles. La premi�re est un peu cauchemardesque...
	public static double somme1(Boite<? extends Number> boite1, Boite<? extends Number> boite2) {
		double total = 0;
		for (Iterator<? extends Number> i = boite1.contenu.iterator(); i.hasNext();) {
			total = total + i.next().doubleValue();
		}
		for (Iterator<? extends Number> i = boite2.contenu.iterator(); i.hasNext();) {
			total = total + i.next().doubleValue();
		}
		return total;
	}
	// La seconde est plus claire : la contrainte sur A est plac�e avant la m�thode
	public static <A extends Number> double somme2(Boite<A> boite1,
			Boite<A> boite2) {
		double total = 0;
		for (Iterator<A> i = boite1.contenu.iterator(); i.hasNext();) {
			total = total + i.next().doubleValue();
		}
		for (Iterator<A> i = boite2.contenu.iterator(); i.hasNext();) {
			total = total + i.next().doubleValue();
		}
		return total;
	}

}
// Un type g�n�rique peut d�river d'un autre et impl�menter une interface g�n�rique
@SuppressWarnings("serial")
abstract class Test<T,U,V> extends ArrayList<T> implements java.util.Map.Entry<U,V>{

}
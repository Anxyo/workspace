package types.generiques;

import java.util.ArrayList;
import java.util.List;

public class Boite<T> {

	protected List<T> contenu;

	public Boite() {
		contenu = new ArrayList<T>();
	}

	public int getTaille() {
		return contenu.size();
	}

	public boolean estVide() {
		return (contenu.size() == 0);
	}

	public void ajoute(T o) {
		contenu.add(o);
	}

	public T retire() {
		if (!estVide()) {
			return contenu.remove(0);
		} else
			return null;
	}
}
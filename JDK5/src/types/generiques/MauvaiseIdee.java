package types.generiques;

import java.util.ArrayList;
import java.util.List;

public class MauvaiseIdee {

  private static List<Integer> entiers = new ArrayList<Integer>();

  public static void remplitListe(List<Integer> liste) {
    for (Integer i : liste) {
      entiers.add(i);
    }
  }

  public static void imprimeListe() {
    for (Integer i : entiers) {
      System.out.println(i);
    }
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
public static void main(String[] args) {
    List<Integer> lesEntiers = new ArrayList<Integer>();
    lesEntiers.add(1);
    lesEntiers.add(2);
    lesEntiers.add(3);

    System.out.println("Remplissage et impression de la liste : ");
    remplitListe(lesEntiers);
    imprimeListe();
    
    // Pour �viter le contr�le de typage en phase de compilation, on peut utiliser la reflexion,
    // et forcer l'ajout d'une valeur invalide dans la Liste<Integer>. 
    // Vraiment une mauvaise id�e ! 
    List list;
    try {
      // retrouve le champ par reflexion, pour abuser le compilateur !
      list = (List)MauvaiseIdee.class.getDeclaredField("entiers").get(null);
      // ajoute une valeur incorrecte : passe � la compilation, mais casse � l'ex�cution
      list.add("Valeur incorrecte");      
    } catch (Exception e) {
      e.printStackTrace();
    }

    System.out.println("Impression avec une valeur invalide dans la liste : ");
    imprimeListe();
  }
}
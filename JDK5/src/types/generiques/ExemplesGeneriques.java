package types.generiques;

import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ExemplesGeneriques {

	public void testMapsTypeSafe(PrintStream out) throws IOException {
		Map<Integer, Integer> carres = new HashMap<Integer, Integer>();
		for (int i = 0; i < 100; i++) {
			carres.put(i, i * i);
		}
		for (int i = 0; i < 10; i++) {
			int n = i * 3;
			out.println("Le carre de " + n + " est " + carres.get(n));
		}
	}


	@SuppressWarnings("rawtypes")
	public void iterateursNonTypeSafe(PrintStream out) throws IOException {
		List listeDeChaines = getListeDeChaines();
		for (Iterator i = listeDeChaines.iterator(); i.hasNext();) {
			@SuppressWarnings("unused")
			String elemt = (String) i.next();

			// travail avec cette chaine
		}
		List<String> uniquementDesChaines = new LinkedList<String>();
		uniquementDesChaines.add("Possible");
		/**
		 * Les deux lignes suivantes produisent une erreur de compilation
		 * uniquementDesChaines.add(new StringBuilder("Pas possible !"));
		 * uniquementDesChaines.add(25);
		 */
	}

	public void iterateursTypeSafe(PrintStream out) throws IOException {
		List<String> ListeDeChaines = new LinkedList<String>();
		ListeDeChaines.add("Encore");
		ListeDeChaines.add("une");
		ListeDeChaines.add("liste");
		ListeDeChaines.add("de chaines");
		
		//for (Iterator<String> i = getListe().iterator(); i.hasNext();) {  // ClassCastException
		for (Iterator<String> i = ListeDeChaines.iterator(); i.hasNext();) {
			String s = i.next();
			out.println(s);
		}

		imprimeListeDeChaines(getListeDeChaines(), out);
	}

	@SuppressWarnings({ "unchecked", "unused","rawtypes" })
	private List getListe() {
		List liste = new LinkedList();
		liste.add(3);
		liste.add("Erreur");
		liste.add("Prochaine...");
		return liste;
	}

	private List<String> getListeDeChaines() {
		List<String> liste = new LinkedList<String>();
		liste.add("Coucou");
		liste.add("Tout");
		liste.add("le");
		liste.add("monde !");
		return liste;
	}

	public void valeursDeRetourTypeSafes(PrintStream out) throws IOException {
		List<String> chaines = getListeDeChaines();
		for (String s : chaines) { // noter la boucle
			out.println(s);
		}
	}

	private void imprimeListeDeChaines(List<String> liste, PrintStream out)
			throws IOException {
		for (Iterator<String> i = liste.iterator(); i.hasNext();) {
			out.println(i.next());
		}
	}
	
	/*
	// surcharge de la précédente revant des Object : erreur de compilation
	private void imprimeListeDeChaines(List<Object> liste, PrintStream out)
		throws IOException {
			for (Iterator<Object> i = liste.iterator(); i.hasNext();) {
				out.println(i.next());
		}
	}
	*/
	public void imprimeListe(List<?> liste, PrintStream out) throws IOException {
		for (Iterator<?> i = liste.iterator(); i.hasNext();) {
			out.println(i.next().toString());
		}
	}

	public static void main(String[] args) {
		ExemplesGeneriques tester = new ExemplesGeneriques();

		try {
			tester.iterateursNonTypeSafe(System.out);
			tester.testMapsTypeSafe(System.out);
			tester.iterateursTypeSafe(System.out);
			tester.valeursDeRetourTypeSafes(System.out);

			List<Integer> entiers = new LinkedList<Integer>();
			entiers.add(1);
			entiers.add(2);
			entiers.add(3);
			tester.imprimeListe(entiers, System.out);

			// Erreur ci-dessous : String ne
			// NumberBox<String> illegal = new NumberBox<String>();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
package annotations.custom;


import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface DetailsTODO {

  public enum Urgence { CRITIQUE, IMPORTANT, ANODIN, DOCUMENTATION };

  Urgence importance() default Urgence.IMPORTANT;
  String info();
  String assigneA();
  String dateAssignation();
}
package annotations.custom;

public class ExempleAnnotation {

  @annotations.custom.EnCours
  @TODO("Permet de connaitre le taux d'int�r�t mensuel")
  @DetailsTODO(
    importance=DetailsTODO.Urgence.CRITIQUE,
    info="Permet de connaitre le taux d'int�r�t mensuel",
    assigneA="Un formateur Java",
    dateAssignation="26/01/2008"
  )
  public void calculInterets(float amount, float rate) {
    // plus tard...
  }
}
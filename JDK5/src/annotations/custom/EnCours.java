package annotations.custom;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation pour signaler qu'une m�thode ou une classe est en cours de fabrication.
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface EnCours { }
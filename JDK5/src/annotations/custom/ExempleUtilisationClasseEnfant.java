package annotations.custom;

import annotations.custom.reflexion.Enfant;

public class ExempleUtilisationClasseEnfant {

  public static void main(String[] args) {
    try {
      Enfant enfant = new Enfant();
      enfant.print(System.out);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
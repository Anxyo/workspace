package annotations.custom;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Type d'annotation : pour indiquer qu'une t�che doit �tre termin�e
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,
         ElementType.METHOD, 
         ElementType.CONSTRUCTOR, 
         ElementType.ANNOTATION_TYPE})
public @interface TODO { 
  String value();
}
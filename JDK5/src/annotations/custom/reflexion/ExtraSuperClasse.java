package annotations.custom.reflexion;

import java.io.IOException;
import java.io.PrintStream;

import annotations.custom.EnCours;

@EnCours
public class ExtraSuperClasse {

  public void print(PrintStream out) throws IOException {
    out.println("ExtraSuperClasse vous dit...");
  }
}
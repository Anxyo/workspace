package annotations.custom.reflexion;

import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.AnnotatedElement;
import java.lang.annotation.Annotation;

import annotations.custom.EnCours;
import annotations.custom.ExempleAnnotation;
import annotations.custom.DetailsTODO;

public class ExempleReflexionSurAnnotations {

	public void testSiAnnotationPresente(PrintStream out) throws IOException {
		Class<ExtraSuperClasse> c = ExtraSuperClasse.class;
		boolean enCours = c.isAnnotationPresent(EnCours.class);
		if (enCours) {
			out.println("ExtraSuperClasse est en cours");
		} else {
			out.println("ExtraSuperClasse n'est pas en cours");
		}
	}

	public void testAnnotationHeritee(PrintStream out) throws IOException {
		Class<Enfant> c = Enfant.class;
		boolean enCours = c.isAnnotationPresent(EnCours.class);
		if (enCours) {
			out.println("Enfant est en cours");
		} else {
			out.println("Enfant n'est pas en cours");
		}
	}

	public void getInfosAnnotation(PrintStream out) throws IOException, NoSuchMethodException {
		Class<ExempleAnnotation> c = ExempleAnnotation.class;
		AnnotatedElement element = c.getMethod("calculInterets", float.class, float.class);
		DetailsTODO detailsTodo = element.getAnnotation(DetailsTODO.class);
		String assigneA = detailsTodo.assigneA();

		out.println("Le TODO est assigne � : '" + assigneA + "'");
	}

	public void imprimeAnnotations(AnnotatedElement e, PrintStream out)
			throws IOException {
		out.printf("Annotations pour '%s'%n%n", e.toString());
		Annotation[] annotations = e.getAnnotations();
		for (Annotation a : annotations) {
			out.printf("    - Annotation '%s' trouvee%n", a.annotationType().getName());
		}
	}

	public static void main(String[] args) {
		try {
			ExempleReflexionSurAnnotations x = new ExempleReflexionSurAnnotations();
			x.testSiAnnotationPresente(System.out);
			x.testAnnotationHeritee(System.out);
			x.getInfosAnnotation(System.out);
			Class<ExempleAnnotation> c = ExempleAnnotation.class;
			AnnotatedElement element = c.getMethod("calculInterets", float.class, float.class);
			x.imprimeAnnotations(element, System.out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
package annotations.custom.reflexion;

import java.io.IOException;
import java.io.PrintStream;


public class Enfant extends ExtraSuperClasse {

  public void print(PrintStream out) throws IOException {
    out.println("classe Enfant...");
  }
}
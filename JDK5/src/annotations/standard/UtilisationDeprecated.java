package annotations.standard;

public class UtilisationDeprecated {

  /**
   * Cette m�thode est d�pr�ci�e en faveur de faitQQueChoseAutre()
   * @deprecated Utilisez faitQQueChoseAutre() � sa place.
   */
  @Deprecated public void faitQQueChose() {
  
  }
  public void faitQQueChoseAutre() {
    // cens�e �tre mieux...
  }
}
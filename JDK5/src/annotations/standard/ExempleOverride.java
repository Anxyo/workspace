package annotations.standard;

public class ExempleOverride {

  public ExempleOverride() { }

  @Override
  public String toString() {
    return super.toString() + " [Implementation ExempleOverride]";
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  /**
   * Que se passe t'il si on utilise @Override sur une m�thode avec un nom incorrect.
   * Enlevez le commentaire pour le savoir !
     
  @Override
  public int hasCode() {
    return toString().hashCode();
  }
   */
}
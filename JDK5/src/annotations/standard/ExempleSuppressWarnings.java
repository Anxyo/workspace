package annotations.standard;

import java.util.ArrayList;
import java.util.List;

public class ExempleSuppressWarnings {

   @SuppressWarnings(value={"unchecked", "fallthrough", "rawtypes"})
   //@SuppressWarnings("unchecked")
  public void methodeSansGeneriques() {
    List liste = new ArrayList();
    liste.add("jdk-1.4");
  }
}
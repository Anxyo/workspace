package bouclefor.in;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DemoForIn {

	
	@SuppressWarnings({"unchecked","rawtypes"})
	public static void main(String[] args) {

		// Les collections que l'on veut parcourir
		List listeMots = new ArrayList();
		Set ensembleDeMots = new HashSet();

		// On commence par une boucle minimale dans les �l�ments d'un tableau
		// (args, ici).
		if (args.length == 0)
			args = new String[]{"un","deux","trois","quatre", "un"};
		// Elle est effectu�e pour chaque �l�ment de args, affect� � chaque fois
		// � la variable mot.
		System.out.println("Ajoute les arguments de la ligne de commande...");
		for (String mot : args) {
			System.out.print(mot + " ");
			listeMots.add(mot);
			ensembleDeMots.add(mot);
		}

		System.out.println();

		// Parcourt des �l�ments de la liste maintenant.
		// Les listes �tant ordonn�es l'ordre devrait �tre celui ci-dessus.
		System.out.println("Affichage des mots depuis la liste "
				+ "(ordonn�s, avec des duplicata)...");
		for (Object mot : listeMots) {
			System.out.print((String) mot + " ");
		}
		System.out.println();

		// Idem pour le Set, mais non ordonn� et sans duplicata
		System.out.println("Affichage des mots depuis ensembleDeMots "
				+ "(non ordonn�, sans duplicata)...");
		for (Object mot : ensembleDeMots) {
			System.out.print((String) mot + " ");
		}
	}
}

package bouclefor.in;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
@SuppressWarnings({"unchecked","rawtypes"})
public class ExempleForIn {

	
	public List getListe() {
		List liste = new LinkedList();
		for (int i = 1; i <= 100; i++) {
			liste.add("Element " + i);
		}
		return liste;
	}

	// boucle pr�-jdk 1.5
	public void testForLoop(PrintStream out) throws IOException {
		List liste = getListe(); // init liste
		for (Iterator i = liste.iterator(); i.hasNext();) {
			Object elementListe = i.next();
			out.println(elementListe.toString());

			// ici, on est cens� faire quelquechose...
		}
	}

	// boucle jdk 1.5
	public void testForInLoop(PrintStream out) throws IOException {
		List liste = getListe();

		for (Object listeElement : liste) {
			out.println(listeElement.toString());

			// ici, on est cens� faire quelquechose...
		}
	}

	// parcourt d'un tableau
	public void testBoucleTableau(PrintStream out) throws IOException {
		int[] nbresPremiers = new int[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29 };

		// boucle
		for (int n : nbresPremiers) {
			out.println(n);
		}
	}

	// parcourt d'une liste de listes
	public void testBoucleListe(PrintStream out) throws IOException {
		List[] liste = new List[3];

		liste[0] = getListe();
		liste[1] = getListe();
		liste[2] = getListe();

		for (List l : liste) {
			out.println(l.getClass().getName());  // java.util.LinkedList
		}
	}

	// Affiche la position dans la liste, impossible avec for/in
	public void determineListPosition(PrintStream out, String[] args)
			throws IOException {

		List<String> liste = new LinkedList<String>();

		for (int i = 0; i < args.length; i++) {
			liste.add("mot " + (i + 1) + ": '" + args[i] + "'");
		}

		// On peut lire les �lements de la liste mais pas les modifier
		for (String mot : liste) {
			out.println(mot);
		}
		StringBuffer chaine = new StringBuffer();
		for (int i = 0, longueur = liste.size(); i < longueur; i++) {
			if (i < (longueur - 1)) {
				chaine.append(liste.get(i)).append(", ");
			} else {
				chaine.append(liste.get(i));
			}
		}
		out.println(chaine);
	}

	// impossible de supprimer des �l�ment � travers un it�rateur
	public void supprimeElements(PrintStream out, String[] elts)
			throws IOException {

		List<String> liste = new LinkedList<String>();
		for (int i = 0; i < elts.length; i++) {
			liste.add("mot " + (i + 1) + " : '" + elts[i] + "'");
		}

		// Supprime tous les �l�ments avec un "1". Impossible avec for/in.
		for (Iterator i = liste.iterator(); i.hasNext();) {
			String mot = (String) i.next();
			if (mot.indexOf("1") != -1) {
				i.remove();
			}
		}
		// Par contre, pas de probl�me pour la lecture
		for (String mot : liste) {
			out.println(mot);
		}
	}

	public static void main(String[] args) {
		try {
			ExempleForIn x = new ExempleForIn();
			x.testForLoop(System.out);
			x.testForInLoop(System.out);
			x.testBoucleTableau(System.out);
			x.testBoucleListe(System.out);
			if (args.length == 0)
				args = new String[]{"un","deux","trois","quatre", "un"};
			x.determineListPosition(System.out, args);
			x.supprimeElements(System.out, args);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
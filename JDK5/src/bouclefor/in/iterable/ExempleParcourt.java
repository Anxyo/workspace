package bouclefor.in.iterable;

import java.io.IOException;
import java.io.PrintStream;


public class ExempleParcourt {
	private ListeDeFabricants fabricants;

	public ExempleParcourt() {
		this.fabricants = new ListeDeFabricants();
	}
	
	// itération 
	public void testListExtension(PrintStream out) throws IOException {
		// on ajoute d'abord quelques éléments
		fabricants.add("Guitares Fender");
		fabricants.add("Guitares Ovation");
		fabricants.add("Guitares Gibson");
		fabricants.add("Guitares Epiphone");

		// Iteration avec for in
		for (String fabricant : fabricants) {
			out.println(fabricant);
		}
	}

	public static void main(String[] args) {
		try {
			ExempleParcourt exemple = new ExempleParcourt();
			exemple.testListExtension(System.out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
package bouclefor.in.iterable;

import java.util.LinkedList;

@SuppressWarnings("serial")
public class ListeDeFabricants extends LinkedList<String> {
	public boolean add(String fabriquant) {
		if (fabriquant.indexOf("Guitares") == -1) {	     // "Guitares" doit �tre une sous-chaine
			return false;
		} else {
			super.add(fabriquant);
			return true;
		}
	}
}
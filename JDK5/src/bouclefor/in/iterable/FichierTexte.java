package bouclefor.in.iterable;

import java.util.Iterator;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

// Permet l'it�ration ligne apr�s ligne dans un fichier texte
public class FichierTexte implements Iterable<String> {

	final String nomFichier;

	public FichierTexte(String nomFichier) {
		this.nomFichier = nomFichier;
	}

	// La m�thode de l'interface Iterable
	public Iterator<String> iterator() {
		return new IterateurFichierTexte();
	}

	// classe interne non static impl�mentant l'it�rateur
	class IterateurFichierTexte implements Iterator<String> {

		// le stream parcouru
		BufferedReader in;

		// valeur retourn�e lors du prochain appel de next()
		String ligneSuivante;

		public IterateurFichierTexte() {
			// Ouvre le fichier, lit et m�morise la premi�re ligne.
			// On regarde la ligne suivante pour mieux g�rer hasNext().
			try {
				in = new BufferedReader(new FileReader(nomFichier));
				ligneSuivante = in.readLine();
			} catch (IOException e) {
				throw new IllegalArgumentException(e);
			}
		}

		// le test porte sur ligneSuivante
		public boolean hasNext() {
			return ligneSuivante != null;
		}

		// Retourne la ligne suivante, mais tente, avant, de lire la ligne qui
		// la suit (la nouvelle ligneSuivante !)
		public String next() {
			try {
				String resultat = ligneSuivante;
				// si pas en fin de fichier
				if (ligneSuivante != null) {
					ligneSuivante = in.readLine(); // lit une autre ligne
					if (ligneSuivante == null)
						in.close(); // fin de fichier atteinte
				}
				return resultat;
			} catch (IOException e) {
				throw new IllegalArgumentException(e);
			}
		}

		// Le fichier est consid�r� en lecture seule, pas de suppression autoris�e !
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	public static void main(String[] args) {
		String nomFichier = ".\\src\\bouclefor\\in\\FichierTexte.java";   // ce fichier...
		if (args.length > 0)
			nomFichier = args[0];

		for (String ligne : new FichierTexte(nomFichier))
			System.out.println(ligne);
	}
}

package bouclefor.in;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DemoForInListesGeneriques {

  public static void main(String[] args) {
	// noter : aucun warning pour ce fichier.... 
    List<String> listeMots = new ArrayList<String>();
    Set<String> ensembleMots = new HashSet<String>();

	// On commence par une boucle minimale dans les �l�ments d'un tableau
	// (args, ici).
    if (args.length == 0)
		args = new String[]{"un","deux","trois","quatre", "un"};
	// Elle est effectu�e pour chaque �l�ment de args, affect� � chaque fois
	// � la variable mot.
	System.out.println("Ajoute les arguments de la ligne de commande...");
    for(String mot : args) {
      System.out.print(mot + " ");
      listeMots.add(mot);
      ensembleMots.add(mot);
    }

    System.out.println();

	// Parcourt des �l�ments de la liste maintenant.
	// LEs listes �tant ordonn�es l'ordre devrait �tre celui ci-dessus.
	System.out.println("Affichage des mots depuis la liste "
			+ "(ordonn�s, avec des duplicata)...");
    for(String mot : listeMots) {
      System.out.print(mot + " ");
    }

    System.out.println();

	// Idem pour le Set, mais non ordonn� et sans duplicata
	System.out.println("Affichage des mots depuis ensembleDeMots "
			+ "(non ordonn�, sans duplicata)...");
    for(String mot : ensembleMots) {
      System.out.print(mot + " ");
    }
  }
}

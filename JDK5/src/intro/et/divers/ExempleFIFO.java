package intro.et.divers;

import java.io.IOException;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.Queue;

public class ExempleFIFO {

	public Queue<String> q;
	public ExempleFIFO() {
		q = new LinkedList<String>();
	}


	public void testFIFO(PrintStream out) throws IOException {
		q.add("Premier");
		q.add("Second");
		q.add("Troisi�me");

		Object o;
		while ((o = q.poll()) != null) {
			out.println(o);
		}
	}

	public static void main(String[] args) {
		ExempleFIFO leFIFO = new ExempleFIFO();

		try {
			leFIFO.testFIFO(System.out);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
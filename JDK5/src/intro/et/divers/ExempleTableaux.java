package intro.et.divers;

import java.util.Arrays;


public class ExempleTableaux {

  private int[] tab;

  public ExempleTableaux(int nbreValeurs) {
    tab = new int[nbreValeurs];

    for (int i=0; i < tab.length; i++) {
      tab[i] = (1000 - (300 + i));		// valeur arbitraire...
    }
  }

  public int[] get() {
    return tab;
  }

  public static void main(String[] args) {
    ExempleTableaux tester = new ExempleTableaux(50);
    int[] monTab = tester.get();

    // Comparaison de deux tableaux
    int[] monAutreTab = tester.get().clone();
    if (Arrays.equals(monTab, monAutreTab)) {
      System.out.println("Les deux tableaux sont �gaux !");
    } else {
      System.out.println("Les deux tableaux ne sont pas �gaux !");
    }

    // quelques valeurs exemples
    Arrays.fill(monAutreTab, 2, 10, new Double(Math.PI).intValue());
    monTab[30] = 98;

    // imprime le tableau non tri�
    System.out.println("Bon, voil� le tableau non tri� : ");
    System.out.println(Arrays.toString(monTab));
    System.out.println();

    // tri le tableau
    Arrays.sort(monTab);
    
    // imprime le tableau non tri�
    System.out.println("Bon, voil� le tableau tri� : ");
    System.out.println(Arrays.toString(monTab));
    System.out.println();

    // Retrouve l'indice d'une valeur sp�cifique
    int index = Arrays.binarySearch(monTab, 98);
    System.out.println("98 est situ� � l'indice " + index);

    String[][] morpion1 = { {"X", "O", "O"},
                             {"O", "X", "X"}, 
                             {"X", "O", "X"}};
    System.out.println(Arrays.deepToString(morpion1));

    String[][] morpion2 = { {"O", "O", "X"},
                              {"O", "X", "X"}, 
                              {"X", "O", "X"}};

    String[][] morpion3 = { {"X", "O", "O"},
                              {"O", "X", "X"}, 
                              {"X", "O", "X"}};

    if (Arrays.deepEquals(morpion1, morpion2)) {
      System.out.println("les grilles 1 et 2 sont �gales");
    } else {
      System.out.println("les grilles 1 et 2 ne sont pas �gales");
    }

    if (Arrays.deepEquals(morpion1, morpion3)) {
      System.out.println("les grilles 1 et 3 sont �gales");
    } else {
      System.out.println("les grilles 1 et 3 ne sont pas �gales");
    }
  }
}
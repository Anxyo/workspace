package intro.et.divers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExempleStringBuilder {


public static String ajouteElements(List<String> liste) {
    StringBuilder b = new StringBuilder();

    for (Iterator<String> i = liste.iterator(); i.hasNext(); ) {
      b.append(i.next())
       .append(" ");
    }
    return b.toString();
  }

public static void main(String[] args) {
    List<String> liste = new ArrayList<String>();
    liste.add("Il");
    liste.add("etait");
    liste.add("un");
    liste.add("petit");
    liste.add("navireuh,");
    liste.add("qui");
    liste.add("n'avait...");
    System.out.println(ExempleStringBuilder.ajouteElements(liste));
  }
}
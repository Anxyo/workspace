package intro.et.divers;

import java.util.Comparator;
import java.util.PriorityQueue;

public class ExemplePriorityQueue {

	public static void main(String[] args) {

		PriorityQueue<Integer> pq = new PriorityQueue<Integer>(20,
				new Comparator<Integer>() {
					// si les deux pairs ou impair, trie sur la taille croissante
					public int compare(Integer i, Integer j) {
						if ((i%2 == 0) &&(j%2 != 0) )
							return 1;	// i > j si i pair et j impair
						if ((j%2 == 0) &&(i%2 != 0) )
							return -1;	// j > i si j pair et i impair
						return i - j;	// sinon, si de m�me type (pair ou impair) par ordre croissant.
					}
				});

		// remplit avec des donn�es (le + grand : 18, le + petit : 1, apr�s le tri).
		for (int i = 0; i < 20; i++) {
			pq.offer(i);
		}

		// impression (extraction du plus petit �l�ment d'abord)
		for (int i = 0; i < 20; i++) {
			System.out.println(pq.poll());
		}
	}
}
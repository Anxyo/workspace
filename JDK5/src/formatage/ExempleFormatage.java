package formatage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ExempleFormatage {

	public static void main(String[] args) {
		String nomFichier;
		BufferedReader r = null;
		if (args.length == 0)
			nomFichier = ".\\src\\formatage\\ExempleFormatage.java";
		else
			nomFichier = args[0];

		try {
			File fichier = new File(nomFichier);
			FileReader fr = new FileReader(fichier);
			r = new BufferedReader(fr);

			String ligne;
			int i = 1;
			while ((ligne = r.readLine()) != null) {
				System.out.printf("Ligne %d: %s%n", i++, ligne);
			}

		} catch (Exception e) {
			System.err.printf(
					"Desole, impossible d'ouvrir le fichier '%s': %s",
					nomFichier, e.getMessage());
		}
		finally {
			try { r.close(); } catch (IOException e) {}
		}
	}
}
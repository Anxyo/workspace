package metier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * tester si:
 *  - les 4 op�rations fonctionnent;
 *  - la division de 0 par 0 donne NaN (Not A Number);
 *  - qu'une op�ration est bien rang�e dans l'historique;
 *  - la taille de l'historique suit le nombre des op�rations effectu�es;
 *  
 * @author Administrateur
 *
 */

public class TestCalc {
	
	private Calculette calculette;
	

	@Before
	public void initialisation() {
		this.calculette = new Calculette();
		System.out.println("Initialisation");
	}
	
	
	@After
	public void fermeture() {
		this.calculette = null;
		System.out.println("Shut down");
	}
	
	@Test
	public void testAddition() {
		double resultat = calculette.somme(2, 3);
		assertEquals("La somme doit valoir 5", 5.0, resultat, 0.0);	
	}
	
	@Test
	public void testSoustraction() {
		double resultat = calculette.soustraction(5, 4);
		assertEquals("La soustraction doit valoir 1", 1.0, resultat, 0.0);	
	}
	
	@Test
	public void testMultiplication() {
		double resultat = calculette.multiplication(2, 3);
		assertEquals("La * doit valoir 6", 6.0, resultat, 0.0);	
	}
	

	@Test
	public void testDivision() {
		double resultat = calculette.division(6, 2);
		assertEquals("La / doit valoir 3", 3.0, resultat, 0.0);	
	}
	
	@Test
	public void testZeroZero() {
		assertTrue("0/0 doit valoir NaN", Double.isNaN(0.0/0.0));
	}
	
	@Test
	public void testHistorique() {
		double resultat = calculette.somme(10, 10);
		Operation operation = calculette.getHistorique().get(0);
		assertTrue("Historique", operation.getOp1() == 10 
								&& operation.getOp2() == 10
								&& operation.getOperation() == '+'
								&& operation.getResultat() == resultat);
	}
	
	
	@Test
	public void testTailleHist() {
		calculette.division(10, 10);
		calculette.somme(10, 10);
		calculette.soustraction(10, 10);
		assertEquals("Taille historique", 3.0, calculette.getHistorique().size(), 0.0);
	}
}

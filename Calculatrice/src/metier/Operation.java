package metier;

public class Operation {
	
	private char operation;
	private double op1, op2, resultat;
	
	
	public Operation(char operation, double op1, double op2, double resultat) {
		super();
		this.operation = operation;
		this.op1 = op1;
		this.op2 = op2;
		this.resultat = resultat;
	}

	
	public char getOperation() {
		return operation;
	}

	public void setOperation(char operation) {
		this.operation = operation;
	}

	public double getOp1() {
		return op1;
	}

	public void setOp1(double op1) {
		this.op1 = op1;
	}

	public double getOp2() {
		return op2;
	}

	public void setOp2(double op2) {
		this.op2 = op2;
	}

	public double getResultat() {
		return resultat;
	}

	public void setResultat(double resultat) {
		this.resultat = resultat;
	}


	@Override
	public String toString() {
		return "Operation [operation=" + operation + ", op1=" + op1 + ", op2=" + op2 + ", resultat=" + resultat + "]";
	}
	
	
	
	

}

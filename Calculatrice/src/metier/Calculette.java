package metier;

import java.util.ArrayList;

public class Calculette {
	
	private ArrayList<Operation> historique;
	
	
	public Calculette() {
		historique = new ArrayList<Operation>();
	}
	
	public double somme(double a, double b) {
		historique.add(new Operation('+', a, b, a+b));
		return a + b;
	}
	
	public double soustraction(double a, double b) {
		historique.add(new Operation('-', a, b, a-b));
		return a - b;
	}
	
	public double multiplication(double a, double b) {
		historique.add(new Operation('*', a, b, a*b));
		return a * b;
	}
	
	public double division(double a, double b) {
		historique.add(new Operation('/', a, b, a/b));
		return a / b;
	}

	
	public ArrayList<Operation> getHistorique() {
		return historique;
	}
	
	
}

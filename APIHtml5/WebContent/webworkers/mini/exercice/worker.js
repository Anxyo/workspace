﻿var url, periode, min;

var xhr = new XMLHttpRequest();
xhr.onreadystatechange = callback;

onmessage = function (e) {
    url = e.data.url;
    periode = e.data.periode;
    min = e.data.min;
    console.log('Worker, message initial : url : ' + url +
                ', periode : ' + periode +
                ', min : ' + min);
    setInterval(function () {
        xhr.open('GET', url, true);
        xhr.send(null);
    }, periode * 1000);
}

function callback() {
    if (xhr.readyState === 4) {
        if (xhr.status === 200) {
            var r = Number(xhr.response);
            console.log('Worker, réponse ajax : ' + r);
            if (r >= min){
                postMessage(xhr.response);
            }
        }
    }
}
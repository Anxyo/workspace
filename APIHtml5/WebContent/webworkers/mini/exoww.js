onmessage = function(e){
	var obj = e.data;
	var t = genNb(obj.max);
	obj.nombres = t;
	setTimeout(function(){
		postMessage(obj);
	}, obj.tempo);
};

function genNb(max){
	var tab = [];
	for (var i = 0; i < 5; i++){
		tab.push(Math.floor(Math.random()*max));
	}
	return tab;
}

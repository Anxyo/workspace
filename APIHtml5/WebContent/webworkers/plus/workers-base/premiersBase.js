
self.addEventListener('message', handleMessages, false);

// Recherche du prochain nombre premier
function handleMessages(event) {
    n = event.data.valeur;

    while (true) {
        // retourne NaN si le nombre est trop grand
        if (!pasTropGrand(n)) {
            postMessage({ messageErreur : 'Ce nombre est trop grand.', valeur : NaN });
            break;
        }
        if(estPremier(n)) {
            postMessage({ valeur : n });
            break;
        }
        else
            ++n;
        // throw 10;  // d�commenter pour tester notre callbackErreur()
    }
}

function pasTropGrand(n) {
    return n <= 999999999999;
}

function estPremier(n) {
    if (n % 1 || n < 2) return false; 
    if (n == ppDiviseur(n)) return true;
    return false;
}

// Retourne le plus petit nombre qui divise p
function ppDiviseur(p) {
    if (isNaN(p) || !isFinite(p)) return NaN;
    if (p == 0) return 0;
    if (p % 1 || p * p < 2) return 1;
    if (p % 2 == 0) return 2;
    if (p % 3 == 0) return 3;
    if (p % 5 == 0) return 5;
    var q, m = Math.sqrt(p);  // calcul long
    for (var i = 7; i <= m; i++) {
        if (p % i == 0)
            return i;
    }
    return p;
}



var prochainEntier = 1;
var intervalle = 67;    // environ 15 par secondes
var etatExecution = 0;  // arreté, au début
var idTimer;            // l'id du timer retourné par setInterval()
var n = 0;              // compteur global

// enregistrement �v�nementiel
self.addEventListener('message', handlerMessages, false);

// routine événementielle
function handlerMessages(event) {
    etatExecution = event.data;
    if (etatExecution)
    {
        idTimer = setInterval(function () {
            n = prochainEntier;
            // Boucle de recherche de nombres premiers
            // On sort soit :
            // - si l'état d'exécution est modifié par l'utilisateur, depuis la page html
            //   (valeur event.data du message reçu)
            // - si on a trouvé un nombre premier
            // On recommence cette boucle à chaque intervalle timer (env.15 fois par seconde).
            while (true) {
                if (!etatExecution)     // boucle arretée en cas de message demandant l'arrêt
                    break;

                if (estPremier(n)) {
                    postMessage(n);
                    prochainEntier = ++n;
                    break;
                }
                ++n;
            }
        }, intervalle);
    }
    
    // si on a deamndé à s'arrêter et qu'un timer est actif, on le stoppe
    else if (idTimer) {
        clearInterval(idTimer);
        idTimer = null;
    }
};

function estPremier(valeur) {
    if (valeur % 1 || valeur < 2) return false; 
    if (valeur == ppDiviseur(valeur)) return true;
    return false;
}


// Retourne le plus petit nombre qui divise p
function ppDiviseur(p) {
    if (isNaN(p) || !isFinite(p)) return NaN;
    if (p == 0) return 0;
    if (p % 1 || p * p < 2) return 1;
    if (p % 2 == 0) return 2;
    if (p % 3 == 0) return 3;
    if (p % 5 == 0) return 5;
    var q, m = Math.sqrt(p);  // calcul long
    for (var i = 7; i <= m; i++) {
        if (p % i == 0)
            return i;
    }
    return p;
}

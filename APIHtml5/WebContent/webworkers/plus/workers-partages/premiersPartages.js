

var startInt = 1;
var interval = 100; // millisecondes
var idPrincipale = 'Principale';
var workers = {};

self.onconnect = connect;

function connect(event) {
    var monPort = event.ports[0];    // le port courant est toujours le 0

    monPort.status = Object();
    monPort.status.enCours = false;
    monPort.onmessage = function(event) { message(event, monPort) };
    monPort.postMessage({ status : monPort.status });
};

function log (port, m) {
    port.postMessage({ log : m });
}

// envoie l'état courant à la fenetre principale
function envoieEtatAPrincipale(monPort) {

 
    var etat = monPort.status;

    // on n'envoie pas à la fen�tre principale des infos sur elle même...
    if (monPort.id && (monPort.id == idPrincipale))  
        return;

    if (workers[idPrincipale]) {
        workers[idPrincipale].postMessage(
            {
                etatWorker:
                {
                    id: monPort.id,
                    enCours: etat.enCours
                }
            });
    }

}

function envoieInfosWorkers(monPort) {
    for( var idWorker in workers ) {

        if (idWorker == idPrincipale)  // pas d'information de status pour la fenetre principale
            continue;

        monPort.postMessage( // envoie un message de status pour chacun des workers
            {
                etatWorker: {
                    id: idWorker,
                    enCours: workers[idWorker].status.enCours
                }
            });
    }
}

function message(event, monPort) {
    var monStatus = monPort.status;
    switch (event.data.cmd) {

        case 'getWorkers':   // retourne le tableau des workers
            console.log("worker, message(), case 'getWorkers'");
            if (monPort.id != idPrincipale)
                break;
            envoieInfosWorkers(monPort);
            break;

        case 'id':  // enregistre l'ID et le port dans le tableau des workers
            if (event.data.id) {
                console.log("worker, message(), case 'id'");
                monPort.id = event.data.id;
                if (!workers[monPort.id])
                        workers[monPort.id] = monPort; // enregistre le port
                envoieEtatAPrincipale(monPort);
            }
            break;

        case 'enCours': // demande calcul nombres premiers
            if ((monStatus.enCours = event.data.value)) {
                monStatus.timer = setInterval( function() { calculPremiers(monPort); }, interval);
            } else {
                clearInterval(monStatus.timer);
            }
            monPort.postMessage({ id : monPort.id, status : monPort.status });
            envoieEtatAPrincipale(monPort);
            break;

        case 'status':  // demande d'info d'état
            monPort.postMessage({ status : monPort.status });
            envoieEtatAPrincipale(monPort);
            return;
    }
}

function calculPremiers(monPort) {
    n = monPort.nombrePremier ? monPort.nombrePremier : startInt;
    while(true) {
        if (!monPort.status.enCours)
            break;

        // retourne NaN si le nombre est trop grand
        if (!pasTropGrand(n)) {
            monPort.postMessage({ error: 'Le nombre est trop grand', nombrePremier : NaN});
            break;
        }
        
        if (estPremier(n)) {
            monPort.postMessage({ nombrePremier : n});
            monPort.nombrePremier = ++n;
            break;
        } else ++n;
    }
}

function pasTropGrand(n) {
    return n <= 999999999999;
}

function estPremier(n) {
    if (n % 1 || n < 2) return false;
    if (n == ppDiviseur(n)) return true;
    return false;
}

// Retourne le plus petit nombre qui divise p
function ppDiviseur(p) {
    if (isNaN(p) || !isFinite(p)) return NaN;
    if (p == 0) return 0;
    if (p % 1 || p * p < 2) return 1;
    if (p % 2 == 0) return 2;
    if (p % 3 == 0) return 3;
    if (p % 5 == 0) return 5;
    var q, m = Math.sqrt(p);  // calcul long
    for (var i = 7; i <= m; i++) {
        if (p % i == 0)
            return i;
    }
    return p;
}

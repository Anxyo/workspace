/**
*
*/

onmessage = function(e){
	var objet = e.data;
	var resultat;
	
	switch(objet.operateur){
	case '+':
		resultat = objet.operande1 + objet.operande2;
		break;
	case '-':
		resultat = objet.operande1 - objet.operande2;
		break;
	case '*':
		resultat = objet.operande1 * objet.operande2;
		break;
	case '/':
		resultat = objet.operande1 / objet.operande2;
		break;
	default:
		resultat = 'operateur non supporté';	
	}
	objet.resultat = resultat;
	postMessage(objet);
};
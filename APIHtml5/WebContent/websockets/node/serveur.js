/**
 * Petit serveur minimal utilisant le protocole WebSocket.
 * Nécessite l'utilisation du module socket.io.
 *
 * On notera que cette approche utilise nécessairement le support
 * des websockets et impose donc un navigateur récent.
 *
 */

var http = require("http");
var fs = require("fs");
var wsio = require("socket.io");

var appli = http.createServer(function(request, response){
    console.log("Connexion détectée : " + request.url);     // exécutée 2 fois à cause du favicon...
    var f = "./html/index.html";
    fs.exists(f, function (existe) {
        if (existe) {
            fs.readFile(f, function (err, data) {
                if (err) {
                    response.writeHead(500);
                    response.end("Erreur serveur !");
                    return;
                }
                response.writeHead(200, {"Content-Type": "text/html"});  // type MIME de la réponse
                response.end(data);
            });
        }
    });
});

var ws = wsio.listen(appli);
ws.on('connection', function(socket){
    var no = 0, no_recu = 0;
    socket.on('message', function(message){
        console.log('Reçu : ' + message + ' no : ' + ++no_recu);
        socket.emit('message','pong no ' + ++no);
    });
    socket.on('error', function(erreur){
        console.log("Erreur : " + erreur.stack);
        console.error("\nDéconnexion imprévue générée par le client !\n");
    });
    socket.on('close', function(){
        console.log("Connexion coupée !");
    });
});

appli.listen(8888);    // écoute sur le port 8888
console.log("Serveur démarré, en attente de connexion...");

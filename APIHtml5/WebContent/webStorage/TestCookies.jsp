<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
    // le cookie existe ?
    Cookie [] cookies = request.getCookies();
	Cookie cookie = null;
	if (cookies != null){
		for (int i = 0; i < cookies.length; i++){
			if (cookies[i].getName().equals("test")){
				cookie = cookies[i];
				break;
			}
		}
	}
    // Cookie non trouv�, on le cr�e
    if (cookie == null){
        // Cr�ation d'un cookie de nom "test" et de valeur "0"
        cookie = new Cookie("test", "0");
        // Expiration dans 10 mn (600 s)
        cookie.setMaxAge(600);
        // Ajout du cookie à la r�ponse
        response.addCookie(cookie);
        response.getWriter().write("Cookie g�n�r�.<br>");
    }
    else
    {
        // retrouve sa valeur
        String valeur = cookie.getValue();
        Integer val = Integer.parseInt(valeur) + 1;
        cookie.setValue(val.toString());
        // Ajout du cookie � la r�ponse
        response.addCookie(cookie);
        response.getWriter().write("Le cookie de nom 'test' a la valeur : " + valeur + "<br/>");
    }
%>
<script>
    window.onload = function () {
        document.getElementById('btnListe').onclick = function () {
            var div = document.getElementById('divCookies');
            div.innerHTML = document.cookie;
        };
    }
</script>
<input type="button" value="liste les cookies" id="btnListe" />
<div id="divCookies"></div>

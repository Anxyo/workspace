package dispatch;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author JM
 * D�finit les services apport�s par une route de traitement.
 */
public interface IRoute {
	void routeRequete(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException;
}

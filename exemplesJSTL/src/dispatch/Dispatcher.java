package dispatch;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * On veut utiliser un dispatcher qui, en fonction de l'url, va router vers des traitement diff�rents.
 * Normalement, on devrait utiliser un fichier de configuration, mais cela ajoute inutilement
 * de la complexit�. On va donc plut�t utiliser un Map, qui va associer.
 * Bien s�r, cela est inutilement compliqu�, mais montre comment des architectures MVC beaucoup plus
 * lourdes peuvent �tre construites.
 */
@WebServlet(name="/Dispatcher",urlPatterns="/Dispatcher/*")
public class Dispatcher extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private HashMap<String,String> routes;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Dispatcher() {
        super();
       
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		routes = new HashMap<String, String>();
		routes.put("base", "HelloJstl");
		routes.put("forme", "FormJstl");
		routes.put("ruptures", "RupturesSequence");
		routes.put("sql", "SQLStl");
		routes.put("xml", "XmlStl");
		System.out.println("Initialisation effectu�e...");
	}

	/**
	 * D�l�gue � doPost().
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * Identifie la commande li�e � l'Url et branche vers le gestionnaire correspondant
	 * enregistr� dans la Map. En cas de pb, branche vers pb.jsp.
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String chemin = req.getPathInfo();
		// si on ne demande rien, retour � index.jsp
		if ((chemin == null) || chemin.equals("/")) { 
		    RequestDispatcher d = req.getRequestDispatcher("/index.jsp");
		    d.forward(req, res);
		    return;
		}
		// quel est le nom de la commande pass�e apr�s le nom de la servlet ?
		System.out.println(chemin);
		String commande = chemin.substring(1, chemin.length());
		// du coup, quel est le nom de la classe de traitement ?
		String nomClasse = routes.get(commande);
		if (nomClasse == null) {	// la cagade
			RequestDispatcher d = req.getRequestDispatcher("/pb.jsp");
		    d.forward(req, res);
		} else {
			try {
			    Class<?> c = Class.forName("routes." + nomClasse);
			    IRoute ir = (IRoute) c.newInstance();
			    ir.routeRequete(req, res);	// appelle le gestionnaire sp�cifique
			}
			catch (ClassNotFoundException e) {
			    req.setAttribute("chemin", chemin);
			    req.setAttribute("message", "Classe non trouv�e !");
			    RequestDispatcher d = req.getRequestDispatcher("/pb.jsp");
			    d.forward(req, res);
			}
			// impossible d'instancier cette classe !
			catch (InstantiationException e) {
			    throw new ServletException(e);
			}
			// le constructeur n'est pas visible !
			catch (IllegalAccessException e) {
			    throw new ServletException(e);
			}
		}
		System.out.println(commande);
		System.out.println(nomClasse);
	}

}

package beans;

import java.util.List;

public class Personne {
	String nom;
	String prenom;
	int age;
	List<Adresse> adresses;
	public Personne() {}
	public Personne(String nom, String prenom, int age, List<Adresse> adresses) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.adresses = adresses;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public int getAge() {
		return age;
	}

	public List<Adresse> getAdresses() {
		return adresses;
	}
	
}

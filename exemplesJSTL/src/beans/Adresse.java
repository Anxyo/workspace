package beans;

public class Adresse {
	String noEtRue;
	String ville;
	int codePostal;
	String pays;
	public Adresse() {}
	public Adresse(String noEtRue, String ville, int codePostal, String pays) {
		super();
		this.noEtRue = noEtRue;
		this.ville = ville;
		this.codePostal = codePostal;
		this.pays = pays;
	}
	
	public String getNoEtRue() {
		return noEtRue;
	}
	public void setNoEtRue(String noEtRue) {
		this.noEtRue = noEtRue;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public int getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
}

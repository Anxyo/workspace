package routes;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dispatch.IRoute;

public class XmlStl implements IRoute{

	@Override
	public void routeRequete(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		    RequestDispatcher d = req.getRequestDispatcher("/xmlmini.jsp");
		    d.forward(req, res);
	}

}

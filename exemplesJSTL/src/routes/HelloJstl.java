package routes;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Adresse;
import beans.Personne;

import dispatch.IRoute;

public class HelloJstl implements IRoute {

	@Override
	public void routeRequete(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
			List<Adresse> adresses = new ArrayList<Adresse>();
			adresses.add(new Adresse("12 rue du Pont","Orl�ans",45000, "France"));
			adresses.add(new Adresse("15 rue Maritime","Ol�ron",17190, "France"));
			Personne p = new Personne("Dupont", "Jaques",27,adresses);
			req.setAttribute("personne", p);
		    RequestDispatcher d = req.getRequestDispatcher("/hellojstl.jsp");
		    d.forward(req, res);
	}

}

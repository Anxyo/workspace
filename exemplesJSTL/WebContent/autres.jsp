<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Autres usages de Jstl</title>
<link rel="stylesheet" type="text/css" href="styles.css" />
</head>
<body>
<h3>Autres usage de JSTL</h3>
<br/>
Les balises de formattage et de parsing permettent l'analyse et la conversion des dates et des nombres :
<br/>
<fmt:parseDate value="02/12/1932 8:00" pattern="dd/MM/yyyy hh:mm" var="naissancePapy"/>
<fmt:formatDate value="${naissancePapy}" type="date" var="dateNaissancePapy"/>
<fmt:formatDate value="${naissancePapy}" type="time" var="heureNaissancePapy"/>
<br/>
Papy est n&eacute; le  ${dateNaissancePapy} &agrave; ${heureNaissancePapy}.
<br/>
Les fonctions EL permettent le travail et la modification commode des chaines de caractères :
<br/>
<c:set value="Marc" var="personne1" />
<c:set value="Sophie" var="personne2" />
<c:set value="Anne" var="personne3" />
<ul>
<li>${fn:replace("Bravo X, vous &ecirc;tes la/le meilleur(e) d'entres nous !","X", personne1) }</li>
<li>${fn:replace("Bravo X, vous &ecirc;tes la/le meilleur(e) d'entres nous !","X", personne2) }</li>
<li>${fn:replace("Bravo X, vous &ecirc;tes la/le meilleur(e) d'entres nous !","X", personne3) }</li>
</ul>
</body>
</html>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Acces Database</title>
<link rel="stylesheet" type="text/css" href="../styles.css" />
</head>
<body>
	<h3>Requ&ecirc;te simple</h3>
	Connexion...
	<sql:setDataSource driver="com.mysql.jdbc.Driver"
		url="jdbc:mysql://localhost/test" user="root" password="arati"
		var="laDataSource" scope="session" />

	<br />Requete...
	<sql:query dataSource="${sessionScope.laDataSource}"
		sql="SELECT * FROM CLIENTS" var="resultat"></sql:query>

	<br />Affichage...
	<table>
		<c:forEach items="${resultat.rows}" var="ligne">
			<tr>
				<td>${ligne.r_sociale}</td>
				<td>${ligne.adresse}</td>
				<td>${ligne.ville}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>
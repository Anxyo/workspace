<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Affichage des donn�es de formulaire</title>
<link rel="stylesheet" type="text/css" href="styles.css" />
</head>
<body>
<h3>Donn&eacute;es de formulaire</h3>
Vous avez indiqu&eacute;es les valeurs suivantes (notez la syntaxe):

<c:set var="operande1" value = "${param.oper1}" />
<c:set var="operande2" value = "${param.oper2}" />
<c:set var="operateur" value = "${param.operateur}" />

<c:choose>
	<c:when test="${operateur == '+' }">
		<c:out value="${operande1 + operande2}" />
	</c:when>
	<c:when test="${operateur == '-' }">
		<c:out value="${operande1 - operande2}" />
	</c:when>
	<c:when test="${operateur == '*' }">
			<c:out value="${operande1 * operande2}" />
	</c:when>
	<c:when test="${operateur == '/' }">
			<c:out value="${operande1 / operande2}" />
	</c:when>	
	<c:otherwise> 
		<c:out value='Op�rateur non support�' />
	</c:otherwise>
</c:choose>
<br />

<!-- Pour intercepter une exception potentiellement lev�e dans la page -->
<c:catch var="infoPb">
	<c:set target="${monBeauBean}" property="nom" value="Duponx" />
</c:catch>
Type de l'exception lev�e : <c:out value="${infoPb}" />
	
<br/>
Je dois vous dire aussi que j'ai re�u un cookie de nom
${pageContext.request.cookies[0].name} et de valeur
${pageContext.request.cookies[0].value}.
</body>
</html>

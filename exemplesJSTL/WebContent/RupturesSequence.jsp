<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Ruptures de s&eacute;quence</title>
<link rel="stylesheet" type="text/css" href="../styles.css" />
</head>
<body>
	<h3>Utilisation de <code>if</code> et de <code>choose</code>.</h3><br/>
	<form action="../Dispatcher/ruptures" method="post" >
		<input type="checkbox" name="affiche" value="Afficher" />
		<c:if test="${param.affiche == 'Afficher'}">
			<input type="button" value="J'apparais conditionnellement" />
		</c:if>
		<input type="submit" value="envoi" />
	</form>

		<%-- Autant de when que l'on veut mais 1 seul otherwise --%>	
	<c:choose>
		<c:when test="${param.affiche == 'Afficher'}">
			La case est coch&eacute;e.
		</c:when>
		<c:otherwise>
			La case n'est pas coch&eacute;e.
		</c:otherwise>
	</c:choose>
	<p/>
	<%-- Les 2 boucles maintenant --%>
	<strong>Boucle <code>forEach</code> :</strong><br/>
	<c:forEach items="${headerValues}" var="entete">
		nom de l'entete : ${entete.key}<br/>
	</c:forEach>
	
	<br/><strong>Boucle <code>forTokens</code> :</strong><br/>
	<table>
		<c:set var="noTelephone" value="02-38-39-40-41"/>
		<c:forTokens items="${noTelephone}" delims="-" var="bloc">
		<tr><td>${bloc}</td></tr>
		</c:forTokens>
	</table>
</body>
</html>
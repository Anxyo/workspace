<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<link rel="stylesheet" type="text/css" href="../styles.css" />
<title>Hello JSTL !</title>
</head>
<body>
	
	<!-- ============================================ -->
	<br /> JSTL est divis&eacute; en 4 librairies :
	<ul>
		<li>Le noyau ("core") : affichage, ruptures de s&eacute;quences,
			formulaires etc.</li>
		<li>le traitement du xml.</li>
		<li>l'internationalisation ("i18n").</li>
		<li>l'acc&egrave;s aux bases de donn&eacute;es.</li>
	</ul>
	Dans l'impl&eacute;mentation de r�f�rence v.1.2, elles sont contenues dans deux librairies :
	<ul><li>jstl-api-1.2.jar</li><li>jstl-impl-1.2.jar</li></ul>
	<br />
	<br /> Le langage EL ("Expression Language") incorpor� dans JSTL est
	bien plus simple que Java et permet d'�valuer des expressions de port�e
	:
	<ul>
		<li>Page ("pageScope").</li>
		<li>Requete ("requestScope").</li>
		<li>Session ("sessionScope").</li>
		<li>Application ("applicationScope").</li>
	</ul>

	Voici quelques exemples de EL :
	<br />
	<!-- ============================================ -->
	<!-- Quelques constructions de base du langage EL -->
	<!-- ============================================ -->
	<!-- initialisations -->
	<c:set var="v1" value="12" />
	<c:set var="v2" value="15" />
	<c:set var="v3" value="0" />
	
	<c:out value="Hello JSTL !" />
	<br />
	<%-- Le lngage EL poss�de aussi de nombreux op�rateurs --%>
	<!--  expressions arithm�tiques minimales -->
	${v1 + v2}, ${v1 - v2 }, ${v1 /v2},${v1 /v3}
	
	<br />
	
	<!--  expressions booleenes -->
	${ v1 ge v2 ? v1 : v2}
	<br />
	
	<!-- autre notation -->
	<!-- autant de when que l'on veut mais un seul otherwise -->
	<c:choose> 
		<c:when test='${v1 ge v2 }'>  
			${v1}	
		</c:when>
		<c:otherwise>
			${v2}
		</c:otherwise>
	</c:choose>
	<br />
	<!-- pas d'evaluation, sort litt�ralement -->
	\${v1}
	<!-- on peut toujours utiliser des scriptlets... -->
	<%  
		int v = 15, s = 10;
		session.setAttribute("uneValeur",v+1);
	%>
	<%-- Cherche d'abord dans la page, puis dans la requete, dans la session et dans  l'application --%>
	${uneValeur}
	<%-- mais les locales ne sont pas visibles --%>
	${s}
	<br />
	${uneValeur == 5 }
	<br /> Une autre fa�on de mettre une variable dans la session (plus simple qu'avec un scriptlet) :
	<c:set var="test" value="${951%41}" scope="session" />
	<br />Pour la retrouver, l� encore : ${test} ou, plus efficacement : ${sessionScope.test} <br/>
	
	<!-- Acces a un objet, 1ere syntaxe -->
	${requestScope.personne["nom"]} <br />
	<!-- Acces a un objet, le plus simple -->
	${personne.nom} <br />
	<!-- On precise la portee : ici, requete, plus rapide -->
	${requestScope.personne.nom} , qui habite &agrave; 
	<!--  acces � la liste, utilisation des crochets-->
	${requestScope.personne.adresses[0].ville}, a comme adresses : <br />
	
	<!-- parcourt minimal de la collection -->
	<ul>
	<c:forEach items="${personne.adresses}" var="adresse"  varStatus="infos">
		<li>Adresse no ${infos.index + 1} : ${adresse.noEtRue}, ${adresse.codePostal}, ${adresse.ville}, ${adresse.pays}</li>
	</c:forEach>
	</ul>
</body>
</html>
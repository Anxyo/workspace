<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Traitement XML</title>
<link rel="stylesheet" type="text/css" href="../styles.css" />
</head>
<body>
	<h3>Balises XML</h3>
	On peut, avec ces tags, parser un document ou un fragment XML, �valuer
	des expressions XPATH, ou appliquer une feuille de style XSLT sur un
	document ou un fragment de document.

	<x:parse var="participants">
		<c:import url="./stagiaires.xml" />
	</x:parse>

	<x:set var="laStagiaire"
		select="$participants/stagiaires/stagiaire[@id=3]" />
	<br /> Nom :
	<x:out select="$laStagiaire/nom" />
	, Pr�nom :
	<x:out select="$laStagiaire/prenom" />
	<ul>
		<x:forEach select="$participants/stagiaires/stagiaire"
			var="leStagiaire">
			<li>Nom : <x:out select="$leStagiaire/nom" />, Pr�nom : <x:out
					select="$leStagiaire/prenom" />
			</li>
		</x:forEach>
	</ul>

	<br />La m�me chose, avec une transformation xslt :
	<br />
	<!-- Le fragment xslt minimal utilis� -->
	<c:set var="xsl">
		<?xml version="1.0" encoding="UTF-8"?>
		<xsl:stylesheet version="1.0"
			xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
			<xsl:template match="/">
				<ul>
					<xsl:apply-templates />
				</ul>
			</xsl:template>
			<xsl:template match="stagiaire">
				<li>Nom : <xsl:apply-templates select="nom" />, Pr�nom : <xsl:apply-templates
						select="prenom" />
				</li>
			</xsl:template>
		</xsl:stylesheet>
	</c:set>

	<!-- La transformation -->
	<x:transform doc="${participants}" xslt="${xsl}" />


</body>
</html>

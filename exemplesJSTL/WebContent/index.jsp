<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="styles.css" />
<script>
	function envoie(cible) {
		document.base.action += cible;
		document.base.submit();
	}
</script>
</head>
<body>
<h3>Exemples d'utilisation de JSTL</h3>
<p/>
<table align="center">
<tr><th colspan="2">Usage de base</th><th>Ruptures de s�quence</th><th>Balises SQL</th><th>Balises XML</th></tr>
<tr><td><a href='javascript:envoie("base")'> Hello Jstl !</a></td>
	<td><a href='javascript:envoie("forme")'> Formulaire Jstl</a></td>
	<td><a href='javascript:envoie("ruptures")'> Tests et boucles</a></td>
	<td><a href='javascript:envoie("sql")'> Requete Select</a></td>
	<td><a href='javascript:envoie("xml")'> Utilisation XPath</a></td>
</tr>
</table>

<form name="base" action="Dispatcher/" method="post">
</form>
</body>
</html>




















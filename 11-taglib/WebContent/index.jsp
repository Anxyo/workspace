<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/tlds/formation.tld" prefix="formation" %>    
    
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<h1>Mes balises à moi</h1>
	
	<p><formation:helloNantes /></p>
	
	<p><formation:hello ville="Brest" /></p>
	<p><formation:hello ville="Tours" /></p>
	<p><formation:hello ville="Monts" /></p>
	<p><formation:hello/></p>
	
	<p>15 + 7 = <formation:addition a="15" b="7" /></p>
	
	<formation:debug>
		<p>Ceci n'est affiché que si le paramètre debug est égal à true</p>
	</formation:debug>
	
	
	
</body>
</html>
package fr.formation.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class HelloTagLib extends TagSupport {
	private static final long serialVersionUID = 1L;

	private String ville = null;
	
	public void setVille(String v) {
		this.ville=v;
	}
	
	
	
	@Override
	public int doStartTag() throws JspException {
		
		String message = "bienvenue";
		if (this.ville == null) {
			message = message + " à la maison";
		}else
			message = message + " à " + ville;
			try {
				this.pageContext.getOut().println(message);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		return SKIP_BODY;
	}
}

package fr.formation.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class AdditionTagLib extends TagSupport {
	private static final long serialVersionUID = 1L;

	private Integer a = null;
	private Integer b = null;
	
	public void setA(Integer a) {
		this.a = a;
	}
	
	public void setB(Integer b) {
		this.b = b;
	}
	
	
	@Override
	public int doStartTag() throws JspException {
		
			try {
				this.pageContext.getOut().println(a+b);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		return SKIP_BODY;
	}
}

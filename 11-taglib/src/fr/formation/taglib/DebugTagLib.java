package fr.formation.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class DebugTagLib extends TagSupport {
	private static final long serialVersionUID = 1L;

	
	@Override
	public int doStartTag() throws JspException {
		
		String debug = this.pageContext.getRequest().getParameter("debug");
		
		if (debug != null && "true".equalsIgnoreCase(debug)) {
			return EVAL_BODY_INCLUDE;
		}
		else
			return SKIP_BODY;
	}
}

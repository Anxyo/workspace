package formation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		ApplicationContext cx = new ClassPathXmlApplicationContext("beans.xml");
		Personne p = (Personne)cx.getBean("p");
		System.out.println(p);
		Personne pp = (Personne)cx.getBean("p2");
		System.out.println(pp);
	}

}

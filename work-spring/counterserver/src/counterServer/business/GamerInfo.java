package counterServer.business;

public class GamerInfo {
	String name;
	int count;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	
	public GamerInfo() {
		super();
		this.name = "";
		this.count = 0;
	}

	public GamerInfo(String name) {
		super();
		this.name = name;
		this.count = 0;
	}

	public GamerInfo(String name, int count) {
		super();
		this.name = name;
		this.count = count;
	}
	
}

package counterServer.web;

import java.util.List;

import counterServer.business.GamerInfo;

public interface Dao {
	void setValue (String name, int value);
	int getValue (String name);
	List<GamerInfo> getValues ();
}

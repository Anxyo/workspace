package counterServer.web;

import java.awt.PageAttributes.MediaType;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;

import counterServer.business.GamerInfo;

@RestController
public class RestService {

	DaoImpl dao;
	boolean consolLog = false;
	@Autowired 
	private HttpServletRequest request;
	
	public RestService() {
		this.dao = new DaoImpl();
	}
	
	//  http://10.150.11.19:8080/CounterServer/set/gil/10
	@CrossOrigin(origins = "*")
	@RequestMapping("/set/{name}/{value}")
	public void set(@PathVariable (value="name") String name, @PathVariable (value="value") int value)
	{
		
		dao.setValue (name, value);
		ConsoleLog( "SET" + " - " + name  + " - " + value);
		// --------------------------
		//HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		//ServletRequestAttributes
		System.out.println(request.getRemoteAddr());
	}
	
	//  http://10.150.11.19:8080/CounterServer/get/gil
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/get/{name}", produces = "application/json")
	public int get (@PathVariable (value="name") String name)
	{
		int counter=0;
		counter = dao.getValue (name);
		ConsoleLog("GET" + " - " + name  + " - " + counter);
		return counter;
	}
	
	//  http://10.150.11.19:8080/CounterServer/get
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/get", produces = "application/json")
	public List<GamerInfo> getAll()
	{
		List<GamerInfo> values = new ArrayList<GamerInfo>();
		values = dao.getValues ();
		if (this.consolLog) {
			StringBuilder sb = new StringBuilder();
			sb.append("LIST");
			for (GamerInfo gamerInfo : values) {
				sb.append(" - " + gamerInfo.getName() + ": " + gamerInfo.getCount() );
			}
			ConsoleLog (sb.toString());
		}
		return values;
	}
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/admin/{command}", produces = "application/json")
	public void Admin(@PathVariable (value="command") String command)
	{
		switch(command) {
		case "display" : this.consolLog = true; break;
		case "nodisplay" : this.consolLog = false; break;
		}
	}	
	
	public void ConsoleLog(String message)
	{
		if (consolLog)
		{
			System.out.println(message);
		}
	}
	
}

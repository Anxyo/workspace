package counterServer.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import counterServer.business.GamerInfo;

public class DaoImpl implements  counterServer.web.Dao {
    HashMap<String, GamerInfo> counterList;

	public DaoImpl() {
		counterList = new HashMap<String, GamerInfo>();
	}
	
	@Override
	public void setValue(String name, int value) {
		GamerInfo gi = counterList.get(name);
		if (gi == null)
		{
			gi = new GamerInfo(name);
			counterList.put(name, gi);
		}
		gi.setCount(value);
	}

	@Override
	public int getValue(String name) {
		GamerInfo gi = counterList.get(name);
		if (gi == null)
		{
			return 0;
		}
		return gi.getCount();
	}

	@Override
	public List<GamerInfo> getValues() {
		// TODO Auto-generated method stub
		List<GamerInfo> gamerInfoList = new ArrayList<GamerInfo>();
		for (GamerInfo gi : counterList.values()) {
			gamerInfoList.add(gi);
		}
		return gamerInfoList;
	}

}

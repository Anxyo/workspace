package formation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {


	public static void main(String[] args) {
		
		ApplicationContext cx = new ClassPathXmlApplicationContext("beans.xml");
		ServiceDaoPersonne sdp = (ServiceDaoPersonne) cx.getBean("sdp");
		
		for(int i=0 ; i<100 ; i++){
			sdp.ajouter(new Personne("TOTO" + i, "toto" + i, i));
		}
		
		for(Personne p : sdp.lireTout()) {
			System.out.println(p);
		}
		
	}

}

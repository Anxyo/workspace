package formation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
@Qualifier("sdp")
public class ServiceDaoPersonneImpl implements  ServiceDaoPersonne{
	
	DataSource ds = null;
	JdbcTemplate jt = null;
	
	@Autowired
	public ServiceDaoPersonneImpl(DataSource ds) {
	
	jt = new JdbcTemplate(ds);
	
	}

	@Override
	public void ajouter(Personne p) {
		
		String req = "insert into Personnes (nom, prenom, age) values (?,?,?)";
		
			jt.update(req, new Object [] {
					p.getNom(),
					p.getPrenom(),
					p.getAge()
			});
	}

	@Override
	public List<Personne> lireTout() {
		String req = "select * from personnes";
		List<Personne> lp = jt.query(req, new PersonneMapper());
		for(Personne p : lp) {
			System.out.println(p);
		}
		return lp;
	}

}

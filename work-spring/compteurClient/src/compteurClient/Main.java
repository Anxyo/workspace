package compteurClient;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class Main {

	public static void main(String[] args) {
		
		Client client = Client.create();
        String nom = "florent";
        String url = "http://10.150.11.19:8080/counterserver/set/";
        ClientResponse reponse;
        WebResource webResource;
        int cpt = 0;
        
        try {
            webResource = client.resource( url + nom + "/" + cpt);
            reponse = webResource.get(ClientResponse.class);
            
            do {
				cpt++;
				 webResource = client.resource(url + nom + "/" + cpt);
				 reponse = webResource.get(ClientResponse.class);
				 System.out.println(cpt);
				 Thread.sleep(1);
			} while (true);

        } catch (Exception e) {
            e.printStackTrace();
        }
       

	}
}

package formation;

import java.util.List;

public interface ServiceDaoPersonne {

	void ajouter(Personne p);
	List<Personne> lireTout();
	
}

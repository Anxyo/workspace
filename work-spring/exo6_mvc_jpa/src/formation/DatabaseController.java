package formation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class DatabaseController {

	ApplicationContext cx = new ClassPathXmlApplicationContext("beans.xml");
	//DataSource ds = (DataSource) cx.getBean("ds");
	//JdbcTemplate jt = new JdbcTemplate(ds);
	ServiceDaoPersonne sdp = (ServiceDaoPersonne) cx.getBean("sdp");
	
	public DatabaseController() {

		System.out.println("base charg�e");
		
	}

	
	@RequestMapping("/")
	public String index() {
		return "index";	
	}
	
	
	
	@RequestMapping("/lister")
	public ModelAndView lister() {
		List<Personne> data = sdp.lireTout();
		return new ModelAndView("lister", "data", data);
	}
	
	@RequestMapping("/creer")
	public String creer() {
		return "ajouter";
	}
	
	@RequestMapping("/creerPersonne")
	public String creerPersonne(@RequestParam ("nom") String nom, @RequestParam ("prenom") String prenom, @RequestParam ("age") int age) {
		
		sdp.ajouter(new Personne(nom, prenom, age));
		return "index";
	}
	
}

package formation;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class Main {


	public static void main(String[] args) {
		ApplicationContext cx = new ClassPathXmlApplicationContext("beans.xml");
		DataSource ds = (DataSource) cx.getBean("ds");
		JdbcTemplate jt = new JdbcTemplate(ds);
		
		//Affichage de la base
		/*
		String req = "select * from personnes";
		List<Personne> lp = jt.query(req, new PersonneMapper());
		for(Personne p : lp) {
			System.out.println(p);
		}*/
		
		// Ajout en base
		/*
		String req = "insert into Personnes (nom, prenom, age) values (?,?,?)";
		for(int i = 0 ; i < 100 ; i++) {
			jt.update(req, new Object [] {
					"TOTO" + i,
					"toto" + i,
					i
			});
		}*/
		
		/*
		String req = "update personnes set nom='ZORRO' where age=?";
		jt.update(req, 10);
		*/
		
		
		String req = "delete from personnes where age=?";
		jt.update(req, 10);
	}

}

package tutorial.fullStack.presentation;



import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import tutorial.fullStack.business.model.Person;
import tutorial.fullStack.persistence.ServicePerson;
import tutorial.fullStack.persistence.ServicePersonBean;

public class Principale {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("ServicePerson");
		EntityManager em = emf.createEntityManager();
		
//		ServicePerson ie = new ServicePersonBean();
//		Person p = ie.creePerson("Gallimard", "Edition", 25);
//		List<Person> personList = ie.findAllPersons();
//		for (Person person : personList) {
//			System.out.println(person);
//		}
		
		

		em.close();
		emf.close();		
	}

}

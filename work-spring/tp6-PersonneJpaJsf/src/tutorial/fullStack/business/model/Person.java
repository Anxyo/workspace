package tutorial.fullStack.business.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Table(name = "personnes")
@NamedQuery(name="Commande.findAll", query="select p from Person p")
@Entity
public class Person {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "nom")
    private String lastName;
	@Column(name = "prenom")
    private String firstName;
    @Transient
    private String noSS;
	@Column(name = "age")
    private int age;
	    //private Date birthDate;
	    //private Sexe sexe;
	    //private String zipCode;
	    //private String city;
	    
	    
		public Person() {
			super();
			// TODO Auto-generated constructor stub
		}

		public Person(String lastName, String firstName, String noSS, int age /*Date dateNaissance*/) {
			super();
			this.lastName = lastName;
			this.firstName = firstName;
			this.noSS = noSS;
			this.age = age;
			//this.dateNaissance = dateNaissance;
		}

		
		
		
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getLastName() {
			return lastName;
		}
		public void setLastName(String nom) {
			this.lastName = nom;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String prenom) {
			this.firstName = prenom;
		}
		public String getNoSS() {
			return noSS;
		}
		public void setNoSS(String noSS) {
			this.noSS = noSS;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
//		public Date getBirthDate() {
//			return birthDate;
//		}
//		public void setBirthDate(Date birthDate) {
//			this.birthDate = birthDate;
//		}

	    
	    

}

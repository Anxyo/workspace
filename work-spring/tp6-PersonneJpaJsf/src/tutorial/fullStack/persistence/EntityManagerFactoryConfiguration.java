package tutorial.fullStack.persistence;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EntityManagerFactoryConfiguration {
	
	@Bean
	public EntityManagerFactory Create () {
		return Persistence.createEntityManagerFactory("ServicePerson");
	}

}

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Person List</title>
<style>

	td {
		border: 1px solid black;
		margin: 0px;
		padding: 0px;
	}
</style>


</head>
<body>
<h1>Person List </h1>

<table>
	<thead>
		<td>Nom</td>
		<td>Prenom</td>
		<td>Age</td>
	</thead>
<c:forEach var="personne" items="${personList}">
	<tr>
		<td>${personne.getLastName()}</td>
		<td>${personne.getFirstName()}</td>
		<td>${personne.getAge()}</td>
	</tr>
</c:forEach>
</table>
<form action="menu">
<input type="submit" value="retour menu" name="retour">
</form>
<%-- 
<ul>
<c:forEach var="personne" items="${personList}">
	<li>Nom: ${personne.getLastName()} - Prenom: ${personne.getFirstName()} - Age: ${personne.getAge()} </li>
</c:forEach>
</ul>
--%>

</body>
</html>
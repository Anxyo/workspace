<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1> Person Card </h1>

    <ul>
    <li> <label>person.firstName:</label> ${person.getFirstName() }</li>
    <li> <label>person.lastName:</label> ${person.getLastName() }</li>
    <li> <label>person.age:</label> ${person.getAge() }</li>
    <%-- <li> <label>person.birthDate:</label> ${person.birthDate }</li>  --%>
    <%-- <li> <label>person.adress.zipCode:</label> ${person.adress.zipCode }</li> --%>
    <%-- <li> <label>person.adress.city:</label> ${person.adress.city }</li> --%>
    </ul>

</body>
</html>
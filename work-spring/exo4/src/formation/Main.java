package formation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class Main {
    // Create an EntityManagerFactory when you start the application.
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("dbformations");
    
    public static void main(String[] args) {
    
        // remplir
    	create(1, "Formation Java");
	    create(2, "Formation C++");
	    create(3, "Formation Python");
    	//Lire les formations
    	/*
    	List<Formation> lf = readAll();
    	for (Formation f : lf)
    	{
    		System.out.println(f.getLibelle());
    	}*/
    	// Mettre � jour
    	/*
    	upate(1, "Formation Java JSP");
    	*/
    	// Supprimer
    	//delete(1);
    }
    
    public static void create(int id, String libelle) {
        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();

            // Create a new Formation object
            Formation frm = new Formation();
            frm.setId(id);
            frm.setLibelle(libelle);

            // Save the Formation object
            manager.persist(frm);

            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
        } finally {
            // Close the EntityManager
            manager.close();
        }
    }
    public static List<Formation> readAll() {

        List<Formation> formations= null;

        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();

            // Get a List of Formations
            //formations = manager.createQuery("SELECT f FROM formations f",Formation.class).getResultList();
            TypedQuery<Formation> query = manager.createNamedQuery("Formation.findAll", Formation.class); 	
            formations = query.getResultList();	
            
            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
        } finally {
            // Close the EntityManager
            manager.close();
        }
        return formations;
    }
    
    public static void delete(int id) {
        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();

            Formation frm = manager.find(Formation.class, id);

            // Delete the Formation
            manager.remove(frm);

            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
        } finally {
            // Close the EntityManager
            manager.close();
        }
    }
 
    public static void upate(int id, String libelle) {
        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            // Get a transaction
            transaction = manager.getTransaction();
            // Begin the transaction
            transaction.begin();

            // Get the Formation object
            Formation frm = manager.find(Formation.class, id);

            // Change the values
            frm.setLibelle(libelle);

            // Update the Formation
            manager.persist(frm);

            // Commit the transaction
            transaction.commit();
        } catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
        } finally {
            // Close the EntityManager
            manager.close();
        }
    }
   }
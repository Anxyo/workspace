package formation;

import javax.persistence.*;

/**
 * The persistent class for the formations database table.
 * 
 */
@Entity
@Table(name="formations")
@NamedQuery(name="Formation.findAll", query="SELECT f FROM Formation f")
public class Formation {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;
	private String libelle;
	public Formation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

}
package formation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class BoutonsController {

	List<HashMap<String, Object>> lescases = new ArrayList<HashMap<String, Object>>();
	Random rnd = new Random();
	
	public BoutonsController() {
		
		lescases.clear();
		
		for (int i = 0; i < 4; i++) {
			
			HashMap<String, Object> v = new HashMap<String, Object>();
			v.put("couleur", "gray");
			v.put("valeur", rnd.nextInt(20));
			lescases.add(v);
		}
		System.out.println(lescases);
		
	}

	@RequestMapping("/boutons")
	public ModelAndView selection(@RequestParam (value="selection") int valeur) {
		
		String couleur = "red";
		if(valeur % 2 == 0) couleur ="green";

		for (int i = 0; i < lescases.size(); i++) {
			if (lescases.get(i).containsValue(valeur)) {
				lescases.get(i).put("couleur", couleur);
				break;
			}
		}
		return new ModelAndView("index", "lacaseaff", lescases);
		
	}
	

}

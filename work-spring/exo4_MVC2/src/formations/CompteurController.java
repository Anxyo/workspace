package formations;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/bonjour")
public class CompteurController {

	public CompteurController() {
		
		System.out.println("CompteurController");
		
	}
	

	
	
	
	@RequestMapping({"/", "/compteur"})
	public ModelAndView compter(@RequestParam (value="valeur", defaultValue="0") int valeur) {
		valeur++;
		Map<String, Integer> data = new HashMap<String, Integer>();
		data.put("data", valeur);
		return new ModelAndView("compter", data);
	}

}

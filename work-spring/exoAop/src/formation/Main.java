package formation;
 
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
 
public class Main {
 
    public static void main(String[] args) {
        ApplicationContext appContext = new ClassPathXmlApplicationContext(
                new String[] { "Beans.xml" });
 
        Traitement t = (Traitement) appContext.getBean("traitement");
        t.faire();
    }  
}

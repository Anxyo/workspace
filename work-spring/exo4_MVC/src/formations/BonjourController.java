package formations;

import javax.websocket.server.PathParam;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/bonjour")
public class BonjourController {

	public BonjourController() {
		
		System.out.println("BonjourController");
		
	}
	
	@RequestMapping("/aqui")
	public String ditBonjour() {
		
		//return "Bonjour � toi Ho grand " + aqui;
		return "Bonjour";
	}
	
	@RequestMapping("/aqui2")
	@ResponseBody
	public String ditBonjour2(@PathParam ("aqui") String aqui) {
		
		return "<h1>Bonjour � toi Ho grand " + aqui + "</h1>";
		
	}

}

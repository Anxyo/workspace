<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>BatailleNavalev3</title>
</head>
<body>

	<h1>Bataille navale v3 2D</h1>

	<form action="selection">
		<table>
			<c:forEach var="ligne" items="${grille}">
				<tr>
					<c:forEach var="unecase" items="${ligne}">
						<td><input type="submit"
							style="width : 50px ; height : 50px ; background: ${unecase.couleur}; color:${unecase.couleur}"
							value="${unecase.ligne}-${unecase.colonne}" name="numcase">
						</td>
					</c:forEach>
				</tr>
			</c:forEach>
		</table>
	</form>

<form action="reset">
<input type="submit" value="reset">
</form>
</body>
</html>
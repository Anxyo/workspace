package formation;

import java.util.ArrayList;
import java.util.List;

public class Ocean {

	int maxLigne, maxColonne;
	int nbNavires;
	List<Navire> navires = new ArrayList<Navire>();

	public List<Navire> getNavires() {
		return navires;
	}

	public void setNavires(List<Navire> navires) {
		this.navires = navires;
	}

	public Ocean() {
		
	}
	
	public Ocean(int maxligne, int maxcolonne, int nbNavires) {
		this.maxLigne = maxligne;
		this.maxColonne = maxcolonne;
		this.nbNavires = nbNavires;
		
		for (int i = 0; i < nbNavires; i++) {
			
			navires.add(NavireFactory.newInstance(this));
		}
	}
	
	public void evaluerImpact(Tir tir) {
		
		for (Navire n : navires) {
	
			n.impact(tir);
		}
	}

	public int getMaxligne() {
		return maxLigne;
	}

	public void setMaxligne(int maxligne) {
		this.maxLigne = maxligne;
	}

	public int getMaxcolonne() {
		return maxColonne;
	}

	public void setMaxcolonne(int maxcolonne) {
		this.maxColonne = maxcolonne;
	}


	
	
}

package formation;

import java.util.Random;

public class NavireFactory {

	static Random rnd = new Random();
	public static Navire newInstance(Ocean ocean) {

		String orientation = "horizontal";
		if (rnd.nextBoolean()) {
			orientation = "vertical";
		}
		
		int taille = rnd.nextInt(ocean.getMaxcolonne()-2) + 2;
		int ligne = rnd.nextInt(ocean.getMaxligne()-taille);
		int colonne = rnd.nextInt(ocean.getMaxcolonne()-taille); 
		
		return new Navire(ligne, colonne, taille, orientation);
		
		
	}

}

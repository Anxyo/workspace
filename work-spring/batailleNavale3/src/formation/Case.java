package formation;

public class Case {

	
	private int ligne, colonne;
	private boolean etat;
	
	
	
	public Case(int ligne,int colonne, boolean e) {
		
		this.ligne = ligne;
		this.colonne = colonne;
		etat = e;
		
	}
	
	public Case() {
		ligne = colonne = -1;
		etat = true;
	}

	public int getLigne() {
		return ligne;
	}

	public void setLigne(int ligne) {
		this.ligne = ligne;
	}

	public int getColonne() {
		return colonne;
	}

	public void setColonne(int colonne) {
		this.colonne = colonne;
	}

	public boolean isEtat() {
		return etat;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}



	

	
}

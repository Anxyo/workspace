package formation;

import java.util.HashMap;
import java.util.LinkedList;

public class BatailleNavale {

	Ocean ocean;
	
	public BatailleNavale(int lmax, int cmax,int nbnv) {
		
		ocean = new Ocean(lmax, cmax, nbnv);
	}
	
	
	public void evaluerImpact(Tir tir){
		
		ocean.evaluerImpact(tir);
	}


	public void render(LinkedList<LinkedList<HashMap<String, Object>>> grille) {

		//Mettre � jour la grille par rapport aux �tats des navires
		for(Navire n : ocean.getNavires()) {
			
			String couleur = "orange";
			if(n.isSink()) couleur = "red";
			for(Case c : n.getCases()) {
				if (!c.isEtat()) {
					//Modifier la case de la grille
					grille.get(c.getLigne()).get(c.getColonne()).put("couleur", couleur);
				}
			}
			
		}
		
		
	}
	
	
	
}

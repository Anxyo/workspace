package formation;

public class Navire {

	int ligne, colonne, taille;
	String orientation = "";
	Case[] cases;

	public Case[] getCases() {
		return cases;
	}

	Navire(int ligne, int colonne, int taille, String orientation) {

		this.ligne = ligne;
		this.colonne = colonne;
		this.taille = taille;
		this.orientation = orientation;
		cases = new Case[taille];

		if (this.orientation.equals("horizontal")) {
			for (int i = 0; i < taille; i++) {
				cases[i] = new Case(ligne, colonne + i, true);
			}
		} else {
			for (int i = 0; i < taille; i++) {
				cases[i] = new Case(ligne + i, colonne, true);
			}
		}
		System.out.println(this);
	}

	public void impact(Tir tir) {

		for (Case c : cases) {
			if (c.getLigne() == tir.getLigne() && c.getColonne() == tir.getColonne()) {
				c.setEtat(false);
				break;
			}
		}
	}

	public boolean isSink() {

		for(Case c : cases) {
			if (c.isEtat()) {
				return false;
			}
		}
		return true;
	}

	public int getLigne() {
		return ligne;
	}

	public void setLigne(int ligne) {
		this.ligne = ligne;
	}

	public int getColonne() {
		return colonne;
	}

	public void setColonne(int colonne) {
		this.colonne = colonne;
	}

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}

	public String getOrientation() {
		return orientation;
	}

	public void setOrientation(String orientation) {
		this.orientation = orientation;
	}

	@Override
	public String toString() {
		return "Navire [ligne=" + ligne + ", colonne=" + colonne + ", taille=" + taille + ", orientation=" + orientation
				+ "]";
	}

}

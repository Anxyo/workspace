package formation;

import java.util.HashMap;
import java.util.LinkedList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class BatailleNavaleController {

	final int lignesMax = 8;
	final int colonnesMax = 8;
	BatailleNavale bt;
	LinkedList<LinkedList<HashMap<String, Object>>> grille = new LinkedList<LinkedList<HashMap<String, Object>>>();
	
	
	public BatailleNavaleController() {
		init();
		System.out.println(grille);

}

	private void init() {
		grille.clear();
		bt = new BatailleNavale(lignesMax, colonnesMax, 2);
		
		for (int l = 0; l < lignesMax; l++) {
			LinkedList<HashMap<String, Object>> ligne = new LinkedList<HashMap<String, Object>>();

			for (int c = 0; c < colonnesMax; c++) {

			HashMap<String, Object> unecase = new HashMap<String, Object>();
			unecase.put("ligne", l);
			unecase.put("colonne", c);
			unecase.put("couleur", "steelblue");
			
			ligne.addLast(unecase);
			}
			grille.add(ligne);
		}
	}
	
	@RequestMapping("/")
	public ModelAndView grille() {
		
		return new ModelAndView("index", "grille", grille);

	}
	
	@RequestMapping("/selection")
	public ModelAndView selection(@RequestParam ("numcase") String numcase) {
		
		String [] lc = numcase.split("-");
		int ligne = Integer.parseInt(lc[0]);
		int colonne = Integer.parseInt(lc[1]);
		System.out.println(String.format("%d -- %d", ligne, colonne));
		bt.evaluerImpact(new Tir(ligne, colonne));
		bt.render(grille);
		return new ModelAndView("index", "grille", grille);

		
	}
	
	@RequestMapping("/reset")
	public ModelAndView reset() {
		init();
		return new ModelAndView("index", "grille", grille); 
	}
	
	
}

package tutorial.fullStack.web.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import tutorial.fullStack.business.model.Person;
import tutorial.fullStack.persistence.ServicePerson;
import tutorial.fullStack.persistence.ServicePersonBean;


@Controller
@RequestMapping("/")
public class PersonController {
	
	@Autowired
	ServicePerson ie;
	

	@RequestMapping(value={"/", "menu"})
	public ModelAndView index (@RequestParam(value="action", defaultValue="") String action)
	{
		switch (action) {
		case "Creer" :
			return new ModelAndView("personCreation");
		case "Lister" :
			return list();
		default :
			return new ModelAndView("personMenu");
		}
		
	}

	@RequestMapping("/creation")
	public String creation (
				@RequestParam("firstName") String firstName, 
				@RequestParam("lastName") String lastName, 
				@RequestParam("age") int age)
	{

		
		Person p = ie.creePerson(firstName, lastName, age);
		return "personMenu";
	}

	@RequestMapping("/list")
	public ModelAndView list ()
	{
		List<Person> personList = ie.findAllPersons();
		for (Person person : personList) {
			System.out.println(person);
		}
		return new ModelAndView("personList", "personList", personList);
	}


//	@RequestMapping("/card")
//	public ModelAndView card ()
//	{
//		ModelAndView mv;
//		Personne personne = new Personne ("DAVID", "Raphael", "1234567890123", 10);
//		return new ModelAndView("personCard", "person", personne);
//	}
//
//	@RequestMapping("/edition")
//	public ModelAndView edition ()
//	{
//		ModelAndView mv;
//		Personne personne = new Personne ("DAVID", "Raphael", "1234567890123", 10);
//		return new ModelAndView("personCard", "person", personne);
//	}

}

package tutorial.fullStack.persistence;

import java.util.List;

import tutorial.fullStack.business.model.Person;

public interface ServicePerson {

	Person creePerson(String firstName, String lastName, int age);

	List<Person> findAllPersons();

}
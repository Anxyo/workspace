package formation;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class PersonneMapper implements RowMapper<Personne>{

	@Override
	public Personne mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Personne p = new Personne();
		p.setNom(rs.getString("nom"));
		p.setPrenom(rs.getString("prenom"));
		p.setAge(rs.getInt("age"));
		return p;
	}


}

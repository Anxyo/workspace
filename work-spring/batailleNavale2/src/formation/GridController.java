package formation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.jasper.tagplugins.jstl.core.When;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class GridController {

	List<HashMap<String, Object>> lescases = new ArrayList<HashMap<String, Object>>();
	List<HashMap<String, Object>> bateau = new ArrayList<HashMap<String, Object>>();

	Random rnd = new Random();
	
	int taille =  rnd.nextInt(5) + 1;
	int position = rnd.nextInt(30 - taille);
	boolean etat;
	
	public GridController() {
		
		lescases.clear();
		
		for (int i = 0; i < 30; i++) {
			HashMap<String, Object> v = new HashMap<String, Object>();
			v.put("couleur", "blue");
			v.put("valeur", i);
			lescases.add(v);
		}
		System.out.println(lescases);
		
		
		HashMap<String, Object> partieBateau = null;

		boolean [] etat = null;
		
		etat = new boolean [taille];
		
		for (int i = 0; i < taille; i++) {
			
			partieBateau = new HashMap<String, Object>();
			
			partieBateau.put("position", position+i);
			partieBateau.put("etat", false);
			bateau.add(partieBateau);
		}
		System.out.println(bateau);	
	}
	
	
	
	
	@RequestMapping({"/selection"})
	public ModelAndView selection(@RequestParam (value="selection", defaultValue = "0") int valeur) {
			
		String couleur;
		int touche = 0;
			
		for (int j = 0; j < taille; j++) {
			if(bateau.get(j).containsValue(valeur)) {
				etat = true;
				touche++;
				couleur = "orange";
				lescases.get(valeur).put("couleur", couleur);
				System.out.println(bateau.get(j).containsValue(valeur));
			}
			
			if(taille == touche) {
				couleur = "red";
				lescases.get(valeur).put("couleur", couleur);
			}
			
			
		}
		return new ModelAndView("index", "lacaseaff", lescases);

	}
	
	
	

}

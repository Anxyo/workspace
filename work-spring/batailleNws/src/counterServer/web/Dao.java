package counterServer.web;

import java.util.List;

import counterServer.business.GamerInfo;

public interface Dao {
	void setValue (String name, List<Integer> value);
	GamerInfo getValue (String name);
	List<GamerInfo> getValues ();
}

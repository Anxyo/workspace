package counterServer.business;

import java.util.ArrayList;
import java.util.List;

public class Bateau {

	List<MorceauBateau> bateauListe = new ArrayList<MorceauBateau>();

	boolean etat = false;

	public Bateau() {

	}

	public Bateau(List<Integer> numberList) {
		for (Integer numero : numberList) {
			MorceauBateau mb = new MorceauBateau(numero, false);
			bateauListe.add(mb);
		}
	}

	public List<MorceauBateau> getBateauListe() {
		return bateauListe;
	}

	public void setBateauListe(List<MorceauBateau> bateauListe) {
		this.bateauListe = bateauListe;
	}

	public boolean isEtat() {
		return etat;
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}

	public boolean containsNumero(int numero) {
		for (MorceauBateau bateau : bateauListe) {
			if (bateau.getValeurBat() == numero) {
				bateau.setEtat(true);
				checkBateauEtats();
				return true;
			}
		}
		return false;
	}

	public void checkBateauEtats() {
		int counter = 0;
		for (MorceauBateau bateau : bateauListe) {
			if (bateau.isEtat() == true) {
				counter++;
			}
		}

		if (counter == bateauListe.size()) {
			this.etat = true;
		}
	}

	public List<MorceauBateau> getCoulerList() {
		if (this.etat == true) {
			return this.bateauListe;
		} else {
			return null;
		}
	}

	@Override
	public String toString() {
		return "Bateau [bateauListe=" + bateauListe + ", etat=" + etat + "]";
	}
	
	
}

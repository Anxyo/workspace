package counterServer.business;

public class MorceauBateau {

	
	int valeurBat;
	boolean etat;
	
	public MorceauBateau(int valeurBat, boolean etat) {
		super();
		this.valeurBat = valeurBat;
		this.etat = etat;
	}
	
	public int getValeurBat() {
		return valeurBat;
	}
	
	public void setValeurBat(int valeurBat) {
		this.valeurBat = valeurBat;
	}
	
	public boolean isEtat() {
		return etat;
	}
	
	public MorceauBateau() {
		super();
	}

	public void setEtat(boolean etat) {
		this.etat = etat;
	}

	@Override
	public String toString() {
		return "MorceauBateau [valeurBat=" + valeurBat + ", etat=" + etat + "]";
	}
	
	
}

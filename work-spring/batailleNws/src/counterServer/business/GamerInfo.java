package counterServer.business;

import java.util.ArrayList;
import java.util.List;

public class GamerInfo {
	String name;
	List<Integer> bateauinfo;
	List<Bateau> bateaux = new ArrayList<Bateau> ();
	
	public List<Integer> getBateauinfo() {
		return bateauinfo;
	}
	
	public void setBateauinfo(List<Integer>  bateauinfo) {
		this.bateaux.add(new Bateau(bateauinfo));
		System.out.println(this.bateaux.size());
		this.bateauinfo = bateauinfo;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public GamerInfo(String name) {
		super();
		this.name = name;
	}
	
	public GamerInfo(String name, List<Integer> bateauinfo) {
		super();
		this.name = name;
		this.bateauinfo = bateauinfo;
	}

	public boolean findBateau(int numero) {
		
		for (int i = 0; i < bateaux.size(); i++) {
			System.out.println(bateaux.get(i));
			if (bateaux.get(i).containsNumero(numero)) {
				return true;
			}
			
		}
		return false;
	}

	public List<List<MorceauBateau>> getBateauCouleList() {
		System.out.println("call getBateauCouleList");
		List<List<MorceauBateau>> returnList = new ArrayList<List<MorceauBateau>>();
		for (int i = 0; i < bateaux.size(); i++) {
			Bateau b = bateaux.get(i);
			System.out.println(b);
			if (b.etat) {
				returnList.add(b.getBateauListe());
			}
		}
		return returnList;
	}

	@Override
	public String toString() {
		return "GamerInfo [name=" + name + ", bateauinfo=" + bateauinfo + ", bateaux=" + bateaux + "]";
	}

	
}

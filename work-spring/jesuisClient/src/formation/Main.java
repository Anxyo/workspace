package formation;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class Main {

	
	  public static String correspondant (String url)
	    {
	        Client client = Client.create();
	        String output = "";
	        try {
	            ClientResponse reponse;
	            WebResource webResource;
	            webResource = client.resource(url);
	            reponse = webResource.get(ClientResponse.class);
	            if (reponse.getStatus() != 200) {
	                throw new RuntimeException("Failed : HTTP error code : " + reponse.getStatus());
	            }
	            output= reponse.getEntity(String.class);
	           
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        return output;  
	        }
	
	
	
	public static void main(String[] args) {
		
		Client client = Client.create();
		String c = correspondant("http://10.150.11.24:8080/jesuis/annuaire/tous");
		System.out.println(c);
		
		Gson gson = new Gson();
        String[] noms =  gson.fromJson(c, String[].class);
        
        for (String nom : noms) {
			
    		try {
				String tous = correspondant("http://10.150.11.24:8080/jesuis/annuaire/" + nom);
				System.out.println(nom + " : " + tous);
			} catch (Exception e) {
				e.printStackTrace();
			}
	
		}
        
        
	}
}

package ws1;

import java.io.IOException;
import java.nio.ByteBuffer;

import javax.websocket.OnMessage;
import javax.websocket.PongMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/echo")
public class ServeurEcho {
	
	@OnMessage
	public void echoMessage(Session session, String msg, boolean last) {
		try {
	
			if (session.isOpen()) {
				session.getBasicRemote().sendText(msg, last);
			}
		} catch (IOException e) {
			try {
				session.close();
			} catch (IOException e1) {
				// on s'en fiche
			}
		}
	}

	@OnMessage
	public void echoMessage(Session session, ByteBuffer bb, boolean last) {
		try {
			if (session.isOpen()) {
				session.getBasicRemote().sendBinary(bb, last);
			}
		} catch (IOException e) {
			try {
				session.close();
			} catch (IOException e1) {
				// on s'en fiche
			}
		}
	}

	/**
	 * Traite un pong ('no-op').
	 *
	 * @param pm ignor�.
	 */
	@OnMessage
	public void echoPongMessage(PongMessage pm) {
		System.out.println("Pong !");
	}
}

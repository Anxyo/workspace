package metier;
import java.util.List;


public interface ServiceEmploye {
    Employe creeEmploye(int id, String nom, long salaire);
    void supprimeEmploye(int id);
    Employe augmenteSalaire(int id, long augmentation);
    public Employe findEmploye(int id);
	public List<Employe> findAllEmployes();
}

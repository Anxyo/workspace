package entites.blob;
import static javax.persistence.FetchType.LAZY;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Employe3 {
	@Id
    private int id;
    private String nom;
    private long salaire;
    
    @Basic(fetch=LAZY)
    @Lob
    @Column(name="IMG")
    private byte[] image;


    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + 
        " salaire: " + getSalaire() + " image : " + new String(getImage());
    }
}

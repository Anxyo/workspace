package entites.gen.id.sequence;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.SequenceGenerator;
import javax.persistence.Id;
// attention ce code n�cessite un SGBD compatible (comme ORACLE) !
// Ne fonctionne pas avec JavaDB et MySQL. 
@Entity
public class Employe9 {
	@SequenceGenerator(name="Emp_Gen_Seq", sequenceName="Emp_Seq")
    @Id @GeneratedValue(generator="Emp_Gen_Seq")
    private int id;
    private String nom;
    private long salaire;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + " salaire : " + getSalaire();
    }

}

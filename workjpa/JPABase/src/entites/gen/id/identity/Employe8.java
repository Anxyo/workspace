package entites.gen.id.identity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employe8 {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    
    private String nom;
    private long salaire;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + " salaire : " + getSalaire();
    }
}

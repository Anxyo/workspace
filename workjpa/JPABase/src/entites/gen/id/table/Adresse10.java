package entites.gen.id.table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
public class Adresse10 {
	@TableGenerator(name="Adresse_Gen",
            table="ID_GEN",
            pkColumnName="GEN_NOM",
            valueColumnName="GEN_VAL",
            pkColumnValue="Adresse_Gen",
            initialValue=10000,
            allocationSize=100)
    @Id @GeneratedValue(strategy=GenerationType.TABLE,
                        generator="Adresse_Gen")
    private int id;
	
    private String rue;
    private String ville;
    private String departement;
    private String codePostal;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getRue() {
        return rue;
    }
    
    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String departement) {
        this.departement = departement;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }
    public String toString() {
        return "Id adresse : " + getId() + 
               ", rue : " + getRue() +
               ", ville : " + getVille() +
               ", département : " + getDepartement() +
               ", code postal : " + getCodePostal();
    }

}

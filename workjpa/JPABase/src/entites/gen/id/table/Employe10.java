package entites.gen.id.table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;

@Entity
public class Employe10 {
	@TableGenerator(name="Emp_Gen", 
            table="ID_GEN",
            pkColumnName="GEN_NOM",
            valueColumnName="GEN_VAL")
    @Id @GeneratedValue(strategy=GenerationType.TABLE,
                        generator="Emp_Gen")
    private int id;
    private String nom;
    private long salaire;
    
    @OneToOne
    private Adresse10 adresse;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public Adresse10 getAdresse() {
        return adresse;
    }
    
    public void setAdresse(Adresse10 adresse) {
        this.adresse = adresse; 
    }
    
    public String toString() {
        return "Id Employe : " + getId() + " nom : " + getNom() + " salaire : " + getSalaire();
    }

}

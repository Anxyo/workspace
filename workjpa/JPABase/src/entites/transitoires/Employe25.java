package entites.transitoires;

import java.util.ResourceBundle;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Employe25 {
    @Id
    private int id;
    private String nom;
    private long salaire;
    // On peut aussi utiliser @Transient
    transient private String nomTraduit;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }

    public String toString() {
        if (nomTraduit == null) {
        	nomTraduit = 
                ResourceBundle.getBundle("RessourcesEmp").getString("Employe");
        }
        return nomTraduit + " id : " + getId() + " nom : " + getNom() + " salaire : " + getSalaire();
    }

}

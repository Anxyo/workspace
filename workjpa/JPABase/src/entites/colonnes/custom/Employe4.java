package entites.colonnes.custom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Employe4 {
	    @Id
	    @Column(name = "ID_EMP")
	    private int id;
	    
	    private String nom;
	    
	    @Column(name = "SAL")
	    private long salaire;
	    
	    
	    @Column(name = "COMM")
	    private String commentaires;

	    public int getId() {
	        return id;
	    }

	    public void setId(int id) {
	        this.id = id;
	    }

	    public String getNom() {
	        return nom;
	    }

	    public void setNom(String nom) {
	        this.nom = nom;
	    }

	    public long getSalaire() {
	        return salaire;
	    }

	    public void setSalaire(long salaire) {
	        this.salaire = salaire;
	    }

	    public String getCommentaires() {
	        return commentaires;
	    }

	    public void setCommentaires(String commentaires) {
	        this.commentaires = commentaires;
	    }

	    public String toString() {
	        return "Id employ� : " + getId() + " nom : " + getNom() +
	        " salaire : " + getSalaire() + " commentaires: " + getCommentaires();
	    }

}

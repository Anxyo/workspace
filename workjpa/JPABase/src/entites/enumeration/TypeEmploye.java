package entites.enumeration;

public enum TypeEmploye {
	EMPLOYE_PLEIN_TEMPS,
	EMPLOYE_TEMPS_PARTIEL,
	INTERIMAIRE
}

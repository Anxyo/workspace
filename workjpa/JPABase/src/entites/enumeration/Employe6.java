package entites.enumeration;

import static javax.persistence.EnumType.STRING;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class Employe6 {
    @Id
    private int id;
    private String nom;
    private long salaire;
    private TypeEmploye type;
    @Enumerated(STRING)
    private TypeEmploye typePrecedant;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }

    public TypeEmploye getType() {
        return type;
    }

    public void setType(TypeEmploye type) {
        this.typePrecedant = this.type;
        if (this.typePrecedant == null) {
            this.typePrecedant = type;
        }
        this.type = type;
    }

    public TypeEmploye getTypePrecedant() {
        return typePrecedant;
    }
    
    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + 
        " salaire : " + getSalaire() + " type : " + getType();
    }
}

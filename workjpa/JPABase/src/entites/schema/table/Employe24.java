package entites.schema.table;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="EMP", schema="HR")
public class Employe24 {
	    @Id
	    private int id;
	    private String nom;
	    private long salaire;

	    public int getId() {
	        return id;
	    }
	    
	    public void setId(int id) {
	        this.id = id;
	    }
	    
	    public String getNom() {
	        return nom;
	    }
	    
	    public void setNom(String nom) {
	        this.nom = nom;
	    }

	    public long getSalaire() {
	        return salaire;
	    }

	    public void setSalaire(long salaire) {
	        this.salaire = salaire;
	    }

	    public String toString() {
	        return "Id employ� : " + getId() + " nom : " + getNom() + " salaire : " + getSalaire();
	    }

}

package entites.dates;

import java.text.DateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Employe5 {
    @Id
    private int id;
    
    private String nom;
    private long salaire;
    
    @Temporal(TemporalType.DATE)
    private java.util.Calendar dateNaissance;
    
    @Temporal(TemporalType.DATE)
    @Column(name="DATE_DEB")
    private java.util.Date dateDebut;

    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }

    
    public java.util.Calendar getDateNaissance() {
        return dateNaissance;
    }

    public void setDob(java.util.Calendar dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public java.util.Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(java.util.Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + 
        " salaire : " + getSalaire() + 
        " date de naissance : " + DateFormat.getDateInstance().format(getDateNaissance().getTime()) + 
        " date de d�but : " + DateFormat.getDateInstance().format(getDateDebut());
    }

}

package services.schema.table;

import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import entites.schema.table.Employe24;

@Stateless(name="SEBSchema")
public class ServiceEmployeBean implements ServiceEmploye24 {
	@PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;
 
	public Employe24 creeEmploye(int id, String nom, long salaire) {
		 Employe24 emp = new Employe24();
	     emp.setId(id);
	     emp.setNom(nom);
	     emp.setSalaire(salaire);
	     em.persist(emp);
	     return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe24> findAllEmployes() {
	    Query query = em.createQuery("SELECT e FROM Employe24 e");
        return (Collection<Employe24>) query.getResultList();
	}

}

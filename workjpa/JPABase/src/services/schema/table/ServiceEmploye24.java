package services.schema.table;
import java.util.Collection;
import entites.schema.table.Employe24;

public interface ServiceEmploye24 {
	public Employe24 creeEmploye(int id, String nom, long salaire);
    public Collection<Employe24> findAllEmployes();

}

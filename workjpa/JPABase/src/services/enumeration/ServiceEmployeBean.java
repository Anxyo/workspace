package services.enumeration;
import java.util.Collection;
import entites.enumeration.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SEBEnum")
public class ServiceEmployeBean implements ServiceEmploye6 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe6 creeEmploye(int id, String nom, long salaire, TypeEmploye type) {
        Employe6 emp = new Employe6();
        emp.setId(id);
        emp.setNom(nom);
        emp.setSalaire(salaire);
        emp.setType(type);
        em.persist(emp);
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe6> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe6 e");
        return (Collection<Employe6>) query.getResultList();
	}

}

package services.enumeration;

import java.util.Collection;

import entites.enumeration.Employe6;
import entites.enumeration.TypeEmploye;

public interface ServiceEmploye6 {
    public Employe6 creeEmploye(int id, String nom, long salaire, TypeEmploye type);
    public Collection<Employe6> findAllEmployes();
}

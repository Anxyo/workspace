package services.transitoires;
import entites.transitoires.*;

import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SEBTransient")
public class ServiceEmployeBean implements ServiceEmploye25 {

    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;
   
	public Employe25 creeEmploye(int id, String nom, long salaire) {
		Employe25 emp = new Employe25();
        emp.setId(id);
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);     
        return emp;
	}


	@SuppressWarnings("unchecked")
	public Collection<Employe25> findAllEmployes() {
		Query query = em.createQuery("SELECT e FROM Employe25 e");
        return (Collection<Employe25>) query.getResultList();	}

}

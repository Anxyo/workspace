package services.transitoires;
import java.util.Collection;

import entites.transitoires.*;

public interface ServiceEmploye25 {
	    public Employe25 creeEmploye(int id, String nom, long salaire);
	    public Collection<Employe25> findAllEmployes();

}

package services.gen.id.sequence;

import java.util.Collection;

import entites.gen.id.sequence.Employe9;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

//attention ce code nécessite un SGBD compatible (comme ORACLE) !
//Ne fonctionne pas avec JavaDB et MySQL. 

@Stateless(name="SEBSequence")
public class ServiceEmployeBean implements ServiceEmploye9 {
	@PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe9 creeEmploye(String nom, long salaire) {
        Employe9 emp = new Employe9();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);
        
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe9> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe9 e");
        return (Collection<Employe9>) query.getResultList();
	}

}

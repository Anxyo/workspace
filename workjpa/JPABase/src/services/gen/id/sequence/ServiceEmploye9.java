package services.gen.id.sequence;
import entites.gen.id.sequence.*;
import java.util.Collection;

//attention ce code nécessite un SGBD compatible (comme ORACLE) !
//Ne fonctionne pas avec JavaDB et MySQL. 


public interface ServiceEmploye9 {
	public Employe9 creeEmploye(String nom, long salaire);
    public Collection<Employe9> findAllEmployes();
}

package services.gen.id.table;

import java.util.Collection;

import entites.gen.id.table.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SEBIdTable")
public class ServiceEmployeBean implements ServiceEmploye10 {
	 @PersistenceContext(unitName="ServiceEmploye")
	 protected EntityManager em;


	public Employe10 creeEmploye(String nom, long salaire, String rue, String ville, String departement, String codePostal) {
		
		Employe10 emp = new Employe10();
    	emp.setNom(nom);
    	emp.setSalaire(salaire);
    
    	Adresse10 adr = new Adresse10();
    	adr.setRue(rue);
    	adr.setVille(ville);
    	adr.setDepartement(departement);
    	adr.setCodePostal(codePostal);
    	emp.setAdresse(adr);

    	em.persist(emp);
    	em.persist(adr);
    
    	return emp;
	}


	@SuppressWarnings("unchecked")
	public Collection<Employe10> findAllEmployes() {
		 Query query = em.createQuery("SELECT e FROM Employe10 e");
	     return (Collection<Employe10>) query.getResultList();
	}

}

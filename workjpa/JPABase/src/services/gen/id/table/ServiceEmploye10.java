package services.gen.id.table;
import java.util.Collection;
import entites.gen.id.table.*;

public interface ServiceEmploye10 {
 public Employe10 creeEmploye(String nom, long salaire, String rue, String ville, String departement, String codePostal);
    public Collection<Employe10> findAllEmployes();

}

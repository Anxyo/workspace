package services.gen.id.identity;
import java.util.Collection;
import entites.gen.id.identity.*;

public interface ServiceEmploye8 {
    public Employe8 creeEmploye(String nom, long salaire);
    public Collection<Employe8> findAllEmployes();
}

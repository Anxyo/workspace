package services.gen.id.identity;

import java.util.Collection;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entites.gen.id.identity.Employe8;

@Stateless(name="SEBIdentite")
public class ServiceEmployeBean implements ServiceEmploye8 {

	@PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe8 creeEmploye(String nom, long salaire) {
        Employe8 emp = new Employe8();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);       
        return emp;
	}


	@SuppressWarnings("unchecked")
	public Collection<Employe8> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe8 e");
        return (Collection<Employe8>) query.getResultList();
	}

}

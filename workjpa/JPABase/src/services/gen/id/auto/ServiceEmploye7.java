package services.gen.id.auto;
import java.util.Collection;
import entites.gen.id.auto.*;

public interface ServiceEmploye7 {
    public Employe7 creeEmploye(String nom, long salaire);
    public Collection<Employe7> findAllEmployes();

}

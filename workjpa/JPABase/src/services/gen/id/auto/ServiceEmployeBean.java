package services.gen.id.auto;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entites.gen.id.auto.Employe7;

@Stateless(name="SEBAuto")
public class ServiceEmployeBean implements ServiceEmploye7 {

    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe7 creeEmploye(String nom, long salaire) {
        Employe7 emp = new Employe7();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);       
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe7> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe7 e");
        return (Collection<Employe7>) query.getResultList();
	}

}

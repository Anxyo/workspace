package services.acces.champs;

import java.util.Collection;
import entites.acces.champs.Employe2;

public interface ServiceEmploye2 {
	 public Employe2 creeEmploye(int id, String nom, long salaire);
	 public Collection<Employe2> findAllEmployes();

}

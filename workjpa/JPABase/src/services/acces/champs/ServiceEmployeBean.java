package services.acces.champs;

import java.util.Collection;
import entites.acces.champs.Employe2;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SEBChamps")
public class ServiceEmployeBean implements ServiceEmploye2 {
	@PersistenceContext(unitName="ServiceEmploye")
     protected EntityManager em;

	public Employe2 creeEmploye(int id, String nom, long salaire) {
        Employe2 emp = new Employe2();
        emp.setId(id);
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);       
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe2> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe2 e");
        return (Collection<Employe2>) query.getResultList();
	}

}

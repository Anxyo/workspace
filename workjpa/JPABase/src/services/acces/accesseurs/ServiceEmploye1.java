package services.acces.accesseurs;

import java.util.Collection;
import entites.acces.accesseurs.Employe1;

public interface ServiceEmploye1 {
	 public Employe1 creeEmploye(int id, String nom, long salaire);
	 public Collection<Employe1> findAllEmployes();

}

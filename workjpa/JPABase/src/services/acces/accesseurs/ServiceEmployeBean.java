package services.acces.accesseurs;
import java.util.Collection;

import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.persistence.Query;

import entites.acces.accesseurs.*;

@Stateless(name="SEBAccesseurs")
public class ServiceEmployeBean implements ServiceEmploye1 {
	 
	@PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe1 creeEmploye(int id, String nom, long salaire) {
        Employe1 emp = new Employe1();
        emp.setId(id);
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);       
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe1> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe1 e");
        return (Collection<Employe1>) query.getResultList();
	}

	
}

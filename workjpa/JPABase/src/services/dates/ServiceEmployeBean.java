package services.dates;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entites.dates.Employe5;

@Stateless(name="SEBDates")
public class ServiceEmployeBean implements ServiceEmploye5 {
  	   @PersistenceContext(unitName="ServiceEmploye")
	   protected EntityManager em;

	public Employe5 creeEmploye(int id, String nom, long salaire, Calendar dateNaissance) {
        Employe5 emp = new Employe5();
        emp.setId(id);
        emp.setNom(nom);
        emp.setSalaire(salaire);
        emp.setDob(dateNaissance);
        emp.setDateDebut(new Date());
        em.persist(emp);       
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe5> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe5 e");
        return (Collection<Employe5>) query.getResultList();
	}
}

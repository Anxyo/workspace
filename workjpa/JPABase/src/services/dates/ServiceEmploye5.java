package services.dates;

import java.util.Calendar;
import java.util.Collection;
import entites.dates.*;

public interface ServiceEmploye5 {
    public Employe5 creeEmploye(int id, String nom, long salaire, Calendar dateNaissance);
    public Collection<Employe5> findAllEmployes();
}

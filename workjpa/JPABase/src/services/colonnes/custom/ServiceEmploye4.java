package services.colonnes.custom;

import java.util.Collection;
import entites.colonnes.custom.*;

public interface ServiceEmploye4 {
	public Employe4 creeEmploye(int id, String nom, long salaire, String commentaires);
    public Collection<Employe4> findAllEmployes();

}

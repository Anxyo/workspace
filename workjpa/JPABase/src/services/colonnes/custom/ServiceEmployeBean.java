package services.colonnes.custom;


import java.util.Collection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entites.colonnes.custom.Employe4;

@Stateless(name="SEBColCustom")
public class ServiceEmployeBean implements ServiceEmploye4 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe4 creeEmploye(int id, String nom, long salaire,String commentaires) {
        Employe4 emp = new Employe4();
        emp.setId(id);
        emp.setNom(nom);
        emp.setSalaire(salaire);
        emp.setCommentaires(commentaires);
        em.persist(emp);
        return emp;
	}


	@SuppressWarnings("unchecked")
	public Collection<Employe4> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe4 e");
        return (Collection<Employe4>) query.getResultList();
	}

}

package services.blob;

import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entites.blob.Employe3;

@Stateless(name="SEBBlob")
public class ServiceEmployeBean implements ServiceEmploye3 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe3 creeEmploye(int id, String nom, long salaire, byte[] image) {
        Employe3 emp = new Employe3();
        emp.setId(id);
        emp.setNom(nom);
        emp.setSalaire(salaire);
        emp.setImage(image);
        em.persist(emp);       
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe3> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe3 e");
        return (Collection<Employe3>) query.getResultList();
	}
}

package services.blob;
import java.util.Collection;
import entites.blob.*;

public interface ServiceEmploye3 {
    public Employe3 creeEmploye(int id, String nom, long salaire, byte[] image);
    public Collection<Employe3> findAllEmployes();
}

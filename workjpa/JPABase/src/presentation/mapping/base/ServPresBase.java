package presentation.mapping.base;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Vector;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entites.acces.accesseurs.Employe1;
import entites.acces.champs.Employe2;
import entites.colonnes.custom.Employe4;
import entites.schema.table.Employe24;

import services.acces.accesseurs.ServiceEmploye1;
import services.acces.champs.ServiceEmploye2;
import services.colonnes.custom.ServiceEmploye4;
import services.schema.table.ServiceEmploye24;
import util.Utilitaires;

/**
 * Ce servlet g�re l'invocation des 4 exemples :
 *  - package services.acces.accesseurs 
 *  - package services.acces.champs
 *  - package services.schema.table
 *  - package services.colonnes.custom 
 */
public class ServPresBase extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
    // injection des EJBs
	@EJB(beanName="SEBAccesseurs")ServiceEmploye1 service1;
	@EJB(beanName="SEBChamps")ServiceEmploye2 service2;
	@EJB(beanName="SEBSchema")ServiceEmploye24 service24;
	@EJB(beanName="SEBColCustom")ServiceEmploye4 service4;
	
    public ServPresBase() {
        super();
        // g�n�ration des tables
        Utilitaires util = new Utilitaires();
        Vector<String> v = new Vector<String>();
        v.add("DROP TABLE EMPLOYE1");
        v.add("CREATE TABLE EMPLOYE1 (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, PRIMARY KEY (ID))");
        v.add("DROP TABLE EMPLOYE2");
        v.add("CREATE TABLE EMPLOYE2 (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, PRIMARY KEY (ID))");
        v.add("DROP TABLE HR.EMP");
        v.add("CREATE TABLE HR.EMP (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, PRIMARY KEY (ID))");
        v.add("DROP TABLE EMPLOYE4");
        v.add("CREATE TABLE EMPLOYE4 (ID_EMP INTEGER NOT NULL, NOM VARCHAR(255), SAL BIGINT, COMM VARCHAR(255), PRIMARY KEY (ID_EMP))");
        util.initSQL("jdbc/__default", v);
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilitaires util = new Utilitaires();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		util.debutPage(out);
		// traite la requ�te
		String action = request.getParameter("action");
		if (action == null) {
			// ne fait rien...
		} else if (action.equals("Creation1")) {
			Employe1 emp = service1.creeEmploye(util.parseInt(request
					.getParameter("idCreation1")), request.getParameter("nom1"), util
					.parseLong(request.getParameter("salaire1")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		} else if (action.equals("Rechercher1")) {
			Collection<Employe1> emps = service1.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe1 emp : emps) {
					out.println(emp + "<br/>");
				}
			}
		} else if (action.equals("Creation2")) {
			Employe2 emp = service2.creeEmploye(util.parseInt(request
					.getParameter("idCreation2")), request.getParameter("nom2"), util
					.parseLong(request.getParameter("salaire2")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		} else if (action.equals("Rechercher2")) {
			Collection<Employe2> emps = service2.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe2 emp : emps) {
					out.println(emp + "<br/>");
				}
			}
		} else if (action.equals("Creation24")) {
			Employe24 emp = service24.creeEmploye(util.parseInt(request
					.getParameter("idCreation24")), request.getParameter("nom24"), util
					.parseLong(request.getParameter("salaire24")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		} else if (action.equals("Rechercher24")) {
			Collection<Employe24> emps = service24.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe24 emp : emps) {
					out.println(emp + "<br/>");
				}
			}
		} else if (action.equals("Creation4")) {
			Employe4 emp = service4.creeEmploye(
					util.parseInt(request.getParameter("idCreation4")), 
					request.getParameter("nom4"), 
					util.parseLong(request.getParameter("salaire4")), 
					request.getParameter("commentaires4"));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		} else if (action.equals("Rechercher4")) {
			Collection<Employe4> emps = service4.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe4 emp : emps) {
					out.println(emp + "<br/>");
				}
			}
		}
		util.finPage(out);
	        

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}
	


}

package presentation.mapping.cleprim;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Vector;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import entites.gen.id.auto.Employe7;
import entites.gen.id.identity.Employe8;
import entites.gen.id.sequence.Employe9;
import entites.gen.id.table.Employe10;
import services.gen.id.auto.ServiceEmploye7;
import services.gen.id.identity.ServiceEmploye8;
import services.gen.id.sequence.ServiceEmploye9;
import services.gen.id.table.ServiceEmploye10;
import util.Utilitaires;

/**
 * Ce servlet g�re l'invocation des exemples :
 *  - package services.gen.id.auto
 *  - package services.gen.id.identity
 *  - package services.gen.id.sequence
 *  - package services.gen.id.table 
 */
public class ServPresClePrim extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// les EJBs inject�s
	@EJB(beanName="SEBAuto") 
	ServiceEmploye7 service7;
	@EJB(beanName="SEBIdentite") ServiceEmploye8 service8;
	@EJB(beanName="SEBSequence") ServiceEmploye9 service9;
	@EJB(beanName="SEBIdTable") ServiceEmploye10 service10;
	
//	@Override
//	public void init() throws ServletException {
//	    genSQL();
//		super.init();
//	}
	
    public ServPresClePrim() {
        super();
       genSQL();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilitaires util = new Utilitaires();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		util.debutPage(out);
		// traite la requ�te
		String action = request.getParameter("action");
		if (action == null) {
			// ne fait rien...
		} else if (action.equals("Creation7")) {
			Employe7 emp = service7.creeEmploye(request.getParameter("nom7"), util.parseLong(request.getParameter("salaire7")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		} else if (action.equals("Rechercher7")) {
			Collection<Employe7> emps = service7.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe7 emp : emps) {
					out.println(emp + "<br/>");
				}
			}
		} else if (action.equals("Creation8")) {
			Employe8 emp = service8.creeEmploye(request.getParameter("nom8"), util.parseLong(request.getParameter("salaire8")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		} else if (action.equals("Rechercher8")) {
			Collection<Employe8> emps = service8.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe8 emp : emps) {
					out.println(emp + "<br/>");
				}
			}
		} else if (action.equals("Creation9")) {
			Employe9 emp = service9.creeEmploye(request.getParameter("nom9"), util.parseLong(request.getParameter("salaire9")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		} else if (action.equals("Rechercher9")) {
			Collection<Employe9> emps = service9.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe9 emp : emps) {
					out.println(emp + "<br/>");
				}
			}
		} else if (action.equals("Creation10")) {
			Employe10 emp = service10.creeEmploye(	request.getParameter("nom10"),util.parseLong(request.getParameter("salaire10")), 
													request.getParameter("rue10"), request.getParameter("ville10"),
													request.getParameter("departement10"),request.getParameter("codepostal10"));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp + " " + emp.getAdresse());
		} else if (action.equals("Rechercher10")) {
			Collection<Employe10> emps = service10.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe10 emp : emps) {
					out.println(emp + " " + emp.getAdresse() + "<br/>");
				}
			}
		}
		util.finPage(out);
	        
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}
	
	private void genSQL() {
		// g�n�ration des tables
        Utilitaires util = new Utilitaires();
        Vector<String> v = new Vector<String>();
    
        v.add("DROP TABLE EMPLOYE7");
        v.add("DROP TABLE SEQUENCE");
        v.add("CREATE TABLE SEQUENCE (SEQ_NAME VARCHAR(64), SEQ_COUNT INTEGER)");
        v.add("CREATE TABLE EMPLOYE7 (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, PRIMARY KEY (ID))");
        v.add("INSERT INTO SEQUENCE (SEQ_NAME, SEQ_COUNT ) VALUES('SEQ_GEN',1)");
        
        v.add("DROP TABLE EMPLOYE8");
        v.add("CREATE TABLE EMPLOYE8 (ID INTEGER  NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), NOM VARCHAR(255), SALAIRE BIGINT, PRIMARY KEY (ID))");
        
        // attention les 4 requetes suivantes n�cessitent un SGBD compatible (comme ORACLE) !
        // Ne fonctionne pas avec JavaDB et MySQL. 
        v.add("DROP SEQUENCE Emp_Seq");
        v.add("DROP TABLE EMPLOYE9");
        v.add("CREATE TABLE EMPLOYE9 (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, PRIMARY KEY (ID))");
        v.add("CREATE SEQUENCE Emp_Seq MINVALUE 1 START WITH 100 INCREMENT BY 50");
        
        v.add("DROP TABLE EMPLOYE10");
        v.add("DROP TABLE ADRESSE10");
        v.add("DROP TABLE ID_GEN");
        v.add("CREATE TABLE ADRESSE10 (ID INTEGER NOT NULL, VILLE VARCHAR(255), DEPARTEMENT VARCHAR(255), RUE VARCHAR(255), CODEPOSTAL VARCHAR(255), PRIMARY KEY (ID))");
        v.add("CREATE TABLE EMPLOYE10 (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, ADRESSE_ID INTEGER, CONSTRAINT ADRESSE_FK FOREIGN KEY (ADRESSE_ID) REFERENCES ADRESSE10 (ID), PRIMARY KEY (ID))");
        v.add("CREATE TABLE ID_GEN (GEN_NOM VARCHAR(80), GEN_VAL INTEGER, PRIMARY KEY (GEN_NOM))");
        v.add("INSERT INTO ID_GEN (GEN_NOM, GEN_VAL) VALUES ('Emp_Gen', 0)");
        v.add("INSERT INTO ID_GEN (GEN_NOM, GEN_VAL) VALUES ('Adresse_Gen', 10000)");
        util.initSQL("jdbc/__default", v);
	}

}

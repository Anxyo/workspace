package presentation.mapping.bizaroide;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Vector;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entites.blob.Employe3;
import entites.dates.Employe5;
import entites.enumeration.Employe6;
import entites.enumeration.TypeEmploye;
import entites.transitoires.Employe25;

import services.blob.ServiceEmploye3;
import services.dates.ServiceEmploye5;
import services.enumeration.ServiceEmploye6;
import services.transitoires.ServiceEmploye25;
import util.Utilitaires;

// Ce servlet g�re l'invocation de :
// - entites.dates
// - entites.enumeration
// - entites.blob
// - entites.transitoires

public class ServPresBizaroide extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	// injection des EJBs
	@EJB(beanName="SEBDates")ServiceEmploye5 service5;
	@EJB(beanName="SEBEnum")ServiceEmploye6 service6;
	@EJB(beanName="SEBBlob")ServiceEmploye3 service3;
	@EJB(beanName="SEBTransient")ServiceEmploye25 service25;
	
    public ServPresBizaroide() {
        super();
        // g�n�ration des tables
        Utilitaires util = new Utilitaires();
        Vector<String> v = new Vector<String>();
        // - entites.blob
        v.add("DROP TABLE EMPLOYE3");
        v.add("CREATE TABLE EMPLOYE3 (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, IMG BLOB, PRIMARY KEY (ID))");
        // - entites.dates
        v.add("DROP TABLE EMPLOYE5");
        v.add("CREATE TABLE EMPLOYE5 (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, DATENAISSANCE DATE, DATE_DEB DATE, PRIMARY KEY (ID))");
        // - entites.enumeration
        v.add("DROP TABLE EMPLOYE6");
        v.add("CREATE TABLE EMPLOYE6 (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, TYPE INTEGER, TYPEPRECEDANT VARCHAR(64), PRIMARY KEY (ID))");
        // - entites.transitoires
        v.add("DROP TABLE EMPLOYE25");
        v.add("CREATE TABLE EMPLOYE25 (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, PRIMARY KEY (ID))");
        util.initSQL("jdbc/__default", v);
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilitaires util = new Utilitaires();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		util.debutPage(out);
		// traite la requ�te
		String action = request.getParameter("action");
		if (action == null) {
			// ne fait rien...
		} else if (action.equals("Test 3")) {	// blob
			// on simule une image en r�cup�rant les octets d'une chaine
            Employe3 emp = service3.creeEmploye(3,"Jean Lefebvre", 12000,"Je suis un bloc d'octets".getBytes());                   
            out.println("Employ&eacute; cr&eacute;&eacute;... Attention, ne marche qu'une fois (conflit d'id!)... Employ&eacute; : " + emp);
		} else if (action.equals("Test 5")) {	// dates
			Calendar ddn = Calendar.getInstance();
			ddn.set(14,7,1919);
			Employe5 emp = service5.creeEmploye(3,"Lino Ventura", 25000,ddn);                   
            out.println("Employ&eacute; cr&eacute;&eacute;... Attention, ne marche qu'une fois (conflit d'id!)... Employ&eacute; : " + emp);
		} else if (action.equals("Test 6")) {	// enum
			Employe6 emp = service6.creeEmploye(3,"Bernard Blier",20000,TypeEmploye.EMPLOYE_PLEIN_TEMPS);                   
            out.println("Employ&eacute; cr&eacute;&eacute;... Attention, ne marche qu'une fois (conflit d'id!)... Employ&eacute; : " + emp);
		} else if (action.equals("Test 25")){   // transient
			Employe25 emp = service25.creeEmploye(3,"Francis Blanche",25000);
            out.println("Employ&eacute; cr&eacute;&eacute;... Attention, ne marche qu'une fois (conflit d'id!)... Employ&eacute; : " + emp);			
		}
		util.finPage(out);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

}

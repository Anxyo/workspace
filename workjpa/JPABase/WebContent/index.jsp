<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>Exemples JPA de base</title>
  </head>
  <body>
    <h1>Exemples JPA de base</h1>
    <ul>
    <li><a href="invocation1.jsp">Invocation du code de mapping de base</a></li>
    <li><a href="invocation2.jsp">Invocation de mappings 'bizaroides'</a></li>
    <li><a href="invocation3.jsp">Start&eacute;gies de g&eacute;n&eacute;ration de cl&eacute; primaire</a></li>
    </ul>
  </body>
</html> 

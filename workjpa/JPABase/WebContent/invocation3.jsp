<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Start&eacute;gies de g&eacute;n&eacute;ration de cl&eacute; primaire</title>
</head>
<body>
<center>
<h2>Strat&eacute;gies de g&eacute;n&eacute;ration de cl&eacute; primaire</h2>
</center>

<hr />
<form action="ServPresClePrim" method="POST">
<h3>Cr&eacute;er un employ&eacute; avec g&eacute;n&eacute;ration d'Id AUTO (package  entites.gen.id.auto)</h3>
<table>
	<tbody>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom7" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire7" />(entier long)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation7" /></td>
		</tr>

	</tbody>
</table>


<h4>Rechercher tous les employ&eacute;s </h4>
<input name="action" type="submit" value="Rechercher7" />
<hr />
<h3>Cr&eacute;er un employ&eacute; avec g&eacute;n&eacute;ration d'Id via une colonne IDENTITY (package  entites.gen.id.identity)</h3>
<table>
	<tbody>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom8" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire8" />(entier long)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation8" /></td>
		</tr>

	</tbody>
</table>


<h4>Rechercher tous les employ&eacute;s </h4>
<input name="action" type="submit" value="Rechercher8" />
<hr />

<h3>Cr&eacute;er un employ&eacute; avec g&eacute;n&eacute;ration d'Id via une SEQUENCE (package  entites.gen.id.sequence)<br>
(attention ce code ne fonctionne ni avec Derby, ni avec MySQL. Oracle,  par contre, supporte les s�quences.)</h3>
<table>
	<tbody>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom9" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire9" />(entier long)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation9" /></td>
		</tr>

	</tbody>
</table>

<h4>Rechercher tous les employ&eacute;s</h4>
<input name="action" type="submit" value="Rechercher9" />
<hr />

<h3>Cr&eacute;er un employ&eacute; avec g&eacute;n&eacute;ration d'Id via une TABLE (package  entites.gen.id.table)</h3>
<table>
	<tbody>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom10" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire10" />(entier long)</td>
		</tr>
		<tr>
			<td>Rue :</td>
			<td><input type="text" name="rue10" />(une chaine)</td>
		</tr>
		<tr>
			<td>Ville :</td>
			<td><input type="text" name="ville10" />(une chaine)</td>
		</tr>
		<tr>
			<td>Departement :</td>
			<td><input type="text" name="departement10" />(une chaine)</td>
		</tr>
		<tr>
			<td>Code postal :</td>
			<td><input type="text" name="codepostal10" />(une chaine)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation10" /></td>
		</tr>
	</tbody>
</table>


<h4>Rechercher tous les employ&eacute;s</h4>
<input name="action" type="submit" value="Rechercher10" />
<hr />


</form>
</body>
</html>
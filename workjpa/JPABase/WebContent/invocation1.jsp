<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Invocation du code de mapping de base</title>
</head>
<body>
<center>
<h2>CODE DE MAPPING DE BASE</h2>
</center>

<hr />
<form action="ServPresBase" method="POST">
<h3>Cr&eacute;er un employ&eacute; avec mapping accesseurs (package  entites.acces.accesseurs)</h3>
<table>
	<tbody>
		<tr>
			<td>Id :</td>
			<td><input type="text" name="idCreation1" />(un entier)</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom1" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire1" />(entier long)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation1" /></td>
		</tr>

	</tbody>
</table>


<h4>Rechercher tous les employ&eacute;s </h4>
<input name="action" type="submit" value="Rechercher1" />
<hr />
<h3>Cr&eacute;er un employ&eacute; avec mapping champs (package  entites.acces.champs)</h3>
<table>
	<tbody>
		<tr>
			<td>Id :</td>
			<td><input type="text" name="idCreation2" />(un entier)</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom2" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire2" />(entier long)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation2" /></td>
		</tr>

	</tbody>
</table>


<h4>Rechercher tous les employ&eacute;s</h4>
<input name="action" type="submit" value="Rechercher2" />
<hr />
<h3>Cr&eacute;er un employ&eacute; avec utilisation d'un schema sp&eacute;cifique (package  entites.schema.table)</h3>
<table>
	<tbody>
		<tr>
			<td>Id :</td>
			<td><input type="text" name="idCreation24" />(un entier)</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom24" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire24" />(entier long)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation24" /></td>
		</tr>

	</tbody>
</table>


<h4>Rechercher tous les employ&eacute;s</h4>
<input name="action" type="submit" value="Rechercher24" />
<hr />
<h3>Cr&eacute;er un employ&eacute; avec mapping de colonnes custom (package  entites.colonnes.custom)</h3>
<table>
	<tbody>
		<tr>
			<td>Id :</td>
			<td><input type="text" name="idCreation4" />(un entier)</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom4" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire4" />(entier long)</td>
		</tr>
				<tr>
			<td>Commentaires :</td>
			<td><input type="text" name="commentaires4" />(Chaine)</td>
		</tr>
		
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation4" /></td>
		</tr>

	</tbody>
</table>


<h4>Rechercher tous les employ&eacute;s</h4>
<input name="action" type="submit" value="Rechercher4" />
<hr />


</form>
</body>
</html>
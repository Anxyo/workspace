package presentation.relations.univaluees;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Vector;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;


import entites.relations.many.to.one.Departement13;
import entites.relations.many.to.one.Employe13;
import entites.relations.many.to.one_jointure.Departement14;
import entites.relations.many.to.one_jointure.Employe14;
import entites.relations.one.to.one.bidirect.Bureau20;
import entites.relations.one.to.one.bidirect.Employe20;
import entites.relations.one.to.one.cleprim.Bureau21;
import entites.relations.one.to.one.cleprim.Employe21;
import entites.relations.one.to.one.unidirect.Bureau23;
import entites.relations.one.to.one.unidirect.Employe23;

import services.relations.many.to.one.ServiceDepartement13;
import services.relations.many.to.one.ServiceEmploye13;
import services.relations.many.to.one_jointure.ServiceDepartement14;
import services.relations.many.to.one_jointure.ServiceEmploye14;
import services.relations.one.to.one.bidirect.ServiceBureau20;
import services.relations.one.to.one.bidirect.ServiceEmploye20;
import services.relations.one.to.one.cleprim.ServiceBureau21;
import services.relations.one.to.one.cleprim.ServiceEmploye21;
import services.relations.one.to.one.unidirect.ServiceBureau23;
import services.relations.one.to.one.unidirect.ServiceEmploye23;
import util.Utilitaires;



public class ServPresRelUni extends HttpServlet {
	private static final long serialVersionUID = 1L;
	// injection des EJBs
	// relation many to one, package entites.relations.many.to.one
	@EJB(beanName="SEBManyToOne") ServiceEmploye13 service13;
	@EJB(beanName="SDBManyToOne") ServiceDepartement13 serviceDept13;
	
	// relation many to on, avec colonne explicite de jointure, package entites.relations.many.to.one_jointure
	@EJB(beanName="SEBManyToOneJointure") ServiceEmploye14 service14;
	@EJB(beanName="SDBManyToOneJointure") ServiceDepartement14 serviceDept14;

	// relation one to one unidirectionnelle, package entites.relations.one.to.one.unidirect
	@EJB(beanName="SEBOneToOneUnidirect") ServiceEmploye23 service23;
	@EJB(beanName="SBBOneToOneUnidirect") ServiceBureau23 serviceBur23;
	
	// relation one to one bidirectionnelle, package entites.relations.one.to.one.bidirect
	@EJB(beanName="SEBOneToOneBidirect") ServiceEmploye20 service20;
	@EJB(beanName="SBBOneToOneBidirect") ServiceBureau20 serviceBur20;
	
	// relation one to one utilisant une cl� prim. identique, package entites.relations.one.to.one.cleprim
	@EJB(beanName="SEBOneToOneClePrim") ServiceEmploye21 service21;
	@EJB(beanName="SBBOneToOneClePrim") ServiceBureau21 serviceBur21;
	
	// on veut utiliser explicitement JTA depuis ce servlet
    @Resource UserTransaction tx;

//     Ce servlet permet l'invocation des 5 relation univalu�es (de x vers 1)  :
//     - relation many to one, package entites.relations.many.to.one
//     - relation many to on, avec colonne explicite de jointure, package entites.relations.many.to.one_jointure
//     - relation one to one unidirectionnelle, package entites.relations.one.to.one.unidirect
//     - relation one to one bidirectionnelle, package entites.relations.one.to.one.bidirect
//     - relation one to one utilisant une cl� prim. identique, package entites.relations.one.to.one.cleprim

    public ServPresRelUni() {
        super();
       // genSQL();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilitaires util = new Utilitaires();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		util.debutPage(out);
		// traite la requ�te
		String action = request.getParameter("action");
		if (action == null) {
			// ne fait rien..
		} 
		// relation many to one, package entites.relations.many.to.one
		else if (action.equals("Creation Emp13")) {
			Employe13 emp = service13.creeEmploye(request.getParameter("nom13"), 
												util.parseLong(request.getParameter("salaire13")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		} 
		else if (action.equals("Creation Dep13")) {
			Departement13 dep = serviceDept13.creeDepartement(request.getParameter("nomDep13"));
			out.println("D&eacute;partement : " + dep);
		}
		else if (action.equals("Association13")) {
			Employe13 emp = service13.setDepartementEmploye(util.parseInt(request.getParameter("idEmp13")),util.parseInt(request.getParameter("idDep13")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		}
		else if (action.equals("Rechercher13")) {
			Collection<Employe13> emps = service13.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe13 emp : emps) {
					out.println(emp +"<br/>");
				}
			}
		} 
		// relation many to on, avec colonne explicite de jointure, package entites.relations.many.to.one_jointure
		else if (action.equals("Creation Emp14")) {
			Employe14 emp = service14.creeEmploye(request.getParameter("nom14"), 
												util.parseLong(request.getParameter("salaire14")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		} 
		else if (action.equals("Creation Dep14")) {
			Departement14 dep = serviceDept14.creeDepartement(request.getParameter("nomDep14"));
			out.println("D&eacute;partement : " + dep);
		}
		else if (action.equals("Association14")) {
			Employe14 emp = service14.setDepartementEmploye(util.parseInt(request.getParameter("idEmp14")),util.parseInt(request.getParameter("idDep14")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		}
		else if (action.equals("Rechercher14")) {
			Collection<Employe14> emps = service14.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe14 emp : emps) {
					out.println(emp +"<br/>");
				}
			}
		}
		// relation one to one unidirectionnelle, package entites.relations.one.to.one.unidirect
		else if (action.equals("Creation Emp23")) {
			Employe23 emp = service23.creeEmploye(request.getParameter("nom23"), 
												util.parseLong(request.getParameter("salaire23")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		} 
		else if (action.equals("Creation Bureau23")) {
			Bureau23 bur = serviceBur23.creeBureau(	util.parseInt(request.getParameter("nomBat23")), 
													request.getParameter("nomEmp23"));
			out.println("Bureau : " + bur);
		}
		else if (action.equals("Association23")) {
			Employe23 emp = service23.setBureauEmploye(util.parseInt(request.getParameter("idEmp23")),util.parseInt(request.getParameter("idBur23")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		}
		else if (action.equals("Rechercher23")) {
			Collection<Employe23> emps = service23.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe23 emp : emps) {
					out.println(emp +"<br/>");
				}
			}
		}
		
		// relation one to one bidirectionnelle, package entites.relations.one.to.one.bidirect
		else if (action.equals("Creation Emp20")) {
			Employe20 emp = service20.creeEmploye(request.getParameter("nom20"), 
												util.parseLong(request.getParameter("salaire20")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		} 
		else if (action.equals("Creation Bureau20")) {
			Bureau20 bur = serviceBur20.creeBureau(	util.parseInt(request.getParameter("nomBat20")), 
													request.getParameter("nomEmp20"));
			out.println("Bureau : " + bur);
		}
		else if (action.equals("Association20")) {
			Employe20 emp = service20.setBureauEmploye(util.parseInt(request.getParameter("idEmp20")),util.parseInt(request.getParameter("idBur20")));
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
		}
		else if (action.equals("Rechercher20")) {
			Collection<Employe20> emps = service20.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe20 emp : emps) {
					out.println(emp +"<br/>");
				}
			}
		}
		// relation one to one utilisant une cl� prim. identique, package entites.relations.one.to.one.cleprim
		else if (action.equals("Creation et association")) {
			// les cr�ations et la connexion doivent se faire de fa�on transactionnelle :
			// tout est bon ou rien n'est bon...
			try {
				tx.begin();
				Employe21 emp = service21.creeEmploye(util.parseInt(request.getParameter("idEmp21")),
																	request.getParameter("nom21"), 
																	util.parseLong(request.getParameter("salaire21")));
				Bureau21 bur = serviceBur21.creeBureau(	emp,	util.parseInt(request.getParameter("nomBat21")), 
														request.getParameter("nomEmp21"));
				tx.commit();
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
			out.println("Bureau : " + bur);
			}
			catch (Exception exc){
				throw new ServletException(exc);	// pour le container...
			}
		} 
		else if (action.equals("Rechercher21")) {
			Collection<Employe21> emps = service21.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe21 emp : emps) {
					out.println(emp +"<br/>");
				}
			}
		}
		util.finPage(out, 4);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}
	
	@SuppressWarnings("unused")
	private void genSQL() {
		// g�n�ration des tables
        Utilitaires util = new Utilitaires();
        Vector<String> v = new Vector<String>();
        // relation many to one, package entites.relations.many.to.one
        v.add("DROP TABLE EMPLOYE13");
        v.add("DROP TABLE DEPARTEMENT13");
        v.add("CREATE TABLE DEPARTEMENT13 (ID INTEGER  NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), NOM VARCHAR(255), PRIMARY KEY (ID))");
        v.add("CREATE TABLE EMPLOYE13 (ID INTEGER  NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), NOM VARCHAR(255), SALAIRE BIGINT, DEPARTEMENT_ID INTEGER, PRIMARY KEY (ID), CONSTRAINT DEPT_FK FOREIGN KEY (DEPARTEMENT_ID) REFERENCES DEPARTEMENT13 (ID))");
        // relation many to on, avec colonne explicite de jointure, package entites.relations.many.to.one_jointure
        v.add("DROP TABLE EMPLOYE14");
        v.add("DROP TABLE DEPARTEMENT14");
        v.add("CREATE TABLE DEPARTEMENT14 (ID INTEGER  NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),NOM VARCHAR(255), PRIMARY KEY (ID))");
        v.add("CREATE TABLE EMPLOYE14 (ID INTEGER  NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), NOM VARCHAR(255), SALAIRE BIGINT, ID_DEPT INTEGER, PRIMARY KEY (ID), CONSTRAINT DEPT14_FK FOREIGN KEY (ID_DEPT) REFERENCES DEPARTEMENT14 (ID))");
    	// relation one to one unidirectionnelle, package entites.relations.one.to.one.unidirect
        v.add("DROP TABLE EMPLOYE23");
        v.add("DROP TABLE BUREAU23");
        v.add("CREATE TABLE BUREAU23 (ID INTEGER  NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), BATIMENT INTEGER, EMPLACEMENT VARCHAR(255), PRIMARY KEY (ID))");
        v.add("CREATE TABLE EMPLOYE23 (ID INTEGER  NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), NOM VARCHAR(255), SALAIRE BIGINT, ID_BUREAU INTEGER, PRIMARY KEY (ID), CONSTRAINT BUREAU_FK FOREIGN KEY (ID_BUREAU) REFERENCES BUREAU23(ID))");
        // relation one to one bidirectionnelle, package entites.relations.one.to.one.bidirect
        v.add("DROP TABLE EMPLOYE20");
        v.add("DROP TABLE BUREAU20");
        v.add("CREATE TABLE BUREAU20 (ID INTEGER  NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), BATIMENT INTEGER, EMPLACEMENT VARCHAR(255), PRIMARY KEY (ID))");
        v.add("CREATE TABLE EMPLOYE20 (ID INTEGER  NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), NOM VARCHAR(255), SALAIRE BIGINT, ID_BUREAU INTEGER, PRIMARY KEY (ID), CONSTRAINT BUREAU20_FK FOREIGN KEY (ID_BUREAU) REFERENCES BUREAU20(ID))");
        v.add("INSERT INTO BUREAU20 (BATIMENT, EMPLACEMENT) VALUES (1, 'Bureau A17')");
        v.add("INSERT INTO BUREAU20 (BATIMENT , EMPLACEMENT) VALUES (2, 'Bureau B12')");
        v.add("INSERT INTO EMPLOYE20 (NOM, SALAIRE, BUREAU_ID) VALUES ('Michelle', 30000, 1)");
        v.add("INSERT INTO EMPLOYE20 (NOM, SALAIRE, BUREAU_ID) VALUES ('Jean Marc', 37000, 1)");
        // relation one to one utilisant une cl� prim. identique, package entites.relations.one.to.one.cleprim
        v.add("DROP TABLE EMPLOYE21");
        v.add("DROP TABLE BUREAU21");
        v.add("CREATE TABLE BUREAU21 (ID INTEGER  NOT NULL, BATIMENT INTEGER, EMPLACEMENT VARCHAR(255), PRIMARY KEY (ID))");
        v.add("CREATE TABLE EMPLOYE21 (ID INTEGER  NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, PRIMARY KEY (ID))");     
        util.initSQL("jdbc/__default", v);
	}
}

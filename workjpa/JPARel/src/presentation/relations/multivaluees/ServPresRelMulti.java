package presentation.relations.multivaluees;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entites.relations.many.to.many.bidirect.Employe11;
import entites.relations.many.to.many.bidirect.Projet11;
import entites.relations.many.to.many.table.jointure.Employe12;
import entites.relations.many.to.many.table.jointure.Projet12;

import entites.relations.one.to.many.bidirect.Departement15;
import entites.relations.one.to.many.bidirect.Employe15;
import entites.relations.one.to.many.liste.Departement16;
import entites.relations.one.to.many.liste.Employe16;
import entites.relations.one.to.many.map.Departement17;
import entites.relations.one.to.many.map.Employe17;
import entites.relations.one.to.many.targetentity.Departement18;
import entites.relations.one.to.many.targetentity.Employe18;
import entites.relations.one.to.many.unidirect.Employe19;
import entites.relations.one.to.many.unidirect.Telephone19;

import services.relations.many.to.many.bidirect.ServiceEmploye11;
import services.relations.many.to.many.bidirect.ServiceProjet11;
import services.relations.many.to.many.table.jointure.ServiceEmploye12;
import services.relations.many.to.many.table.jointure.ServiceProjet12;
import services.relations.one.to.many.bidirect.ServiceDepartement15;
import services.relations.one.to.many.bidirect.ServiceEmploye15;
import services.relations.one.to.many.liste.ServiceDepartement16;
import services.relations.one.to.many.liste.ServiceEmploye16;
import services.relations.one.to.many.map.ServiceDepartement17;
import services.relations.one.to.many.map.ServiceEmploye17;
import services.relations.one.to.many.targetentity.ServiceDepartement18;
import services.relations.one.to.many.targetentity.ServiceEmploye18;
import services.relations.one.to.many.unidirect.ServiceEmploye19;
import services.relations.one.to.many.unidirect.ServiceTelephone19;


public class ServPresRelMulti extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	// injection des EJBs
	// - package entites.relations.one.to.many.bidirect
	@EJB(beanName="SEBOneToManyBidirect")ServiceEmploye15 service15;
	@EJB(beanName="SDBOneToManyBidirect")ServiceDepartement15 serviceDep15;
	// - package entites.relations.one.to.many.targetentity
	@EJB(beanName="SEBOneToManyTE")ServiceEmploye18 service18;
	@EJB(beanName="SDBOneToManyTE")ServiceDepartement18 serviceDep18;
	// - package entites.relations.many.to.many.bidirect
	@EJB(beanName="SEBManyToMany")ServiceEmploye11 service11;
	@EJB(beanName="SPBManyToMany")ServiceProjet11 serviceProj11;
	// - package entites.relations.many.to.many.table.jointure
	@EJB(beanName="SEBManyToManyJointure")ServiceEmploye12 service12;
	@EJB(beanName="SPBManyToManyJointure")ServiceProjet12 serviceProj12;
	// - package entites.relations.one.to.many.unidirect
	@EJB(beanName="SEBOneToManyUni")ServiceEmploye19 service19;
	@EJB(beanName="STBOneToManyUni")ServiceTelephone19 serviceTel19;
	// - package entites.relations.one.to.many.liste
	@EJB(beanName="SEBOneToManyListe")ServiceEmploye16 service16;
	@EJB(beanName="SDBOneToManyListe")ServiceDepartement16 serviceDep16;
	// - package entites.relations.one.to.many.map
	@EJB(beanName="SEBOneToManyMap")ServiceEmploye17 service17;
	@EJB(beanName="SDBOneToManyMap")ServiceDepartement17 serviceDep17;
    
    public ServPresRelMulti() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		debutPage(out);
		// traite la requ�te
		String action = request.getParameter("action");
		if (action == null) {
			// ne fait rien..
		} 
		// - package entites.relations.one.to.many.bidirect (Employes et Departements)
		else if (action.equals("Test 15")) {
			Employe15 emp1 = service15.creeEmploye("Paul Dupont",2500);
			Employe15 emp2 = service15.creeEmploye("Raoul Brugnard",3000);
			Departement15 dep = serviceDep15.creeDepartement("Marketing");
			service15.setDepartementEmploye(emp1.getId(),dep.getId());
			service15.setDepartementEmploye(emp2.getId(),dep.getId());
			Collection<Employe15> emps = service15.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe15 emp : emps) {
					out.println(emp + "dans le d&eacute;partement : " + emp.getDepartement() + "<br/>");
				}
			}
		}
		// - package entites.relations.one.to.many.targetentity (Employes et Departements)
		else if (action.equals("Test 18")) {
			Employe18 emp1 = service18.creeEmploye("Pierre Dubois",2500);
			Employe18 emp2 = service18.creeEmploye("Evelyne Pillet",3000);
			Departement18 dep = serviceDep18.creeDepartement("Finance");
			service18.setDepartementEmploye(emp1.getId(),dep.getId());
			service18.setDepartementEmploye(emp2.getId(),dep.getId());
			Collection<Employe18> emps = service18.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe18 emp : emps) {
					out.println(emp + " dans le d&eacute;partement : " + emp.getDepartement() + "<br/>");
				}
			}
		} 
		
		// - package entites.relations.many.to.many.bidirect (Employes et Projets)
		
		else if (action.equals("Test 11")) {
			Employe11 emp1 = service11.creeEmploye("Francis Blanche",4000);
			Employe11 emp2 = service11.creeEmploye("Pierre Dac",4000);
			Employe11 emp3 = service11.creeEmploye("Coluche",4000);
			Employe11 emp4 = service11.creeEmploye("Pierre Desproges",4000);
			Projet11 proj1 = serviceProj11.creeProjet("Faire rire le management");
			Projet11 proj2 = serviceProj11.creeProjet("Sourire des facheux");
			service11.ajouteProjetEmploye(emp1.getId(), proj1.getId());
			service11.ajouteProjetEmploye(emp2.getId(), proj1.getId());
			service11.ajouteProjetEmploye(emp4.getId(), proj1.getId());
			service11.ajouteProjetEmploye(emp3.getId(), proj2.getId());			
			service11.ajouteProjetEmploye(emp4.getId(), proj2.getId());
			
			Collection<Projet11> projs  = serviceProj11.findAllProjets();
			if (projs.isEmpty()) {
				out.println("Aucun projet trouv&eacute;");
			} else {
				out.println("Projets trouv&eacute;s :</br>");
				for (Projet11 p : projs) {
					out.println(p + " avec les employ&eacute;s : " + p.getEmployes() + "<br/>");
				}
			}
		} 
		
		// - package entites.relations.many.to.many.table.jointure (Employes et Projets)
		else if (action.equals("Test 12")) {
			Employe12 emp1 = service12.creeEmploye("Francis Blanche",4000);
			Employe12 emp2 = service12.creeEmploye("Pierre Dac",4000);
			Employe12 emp3 = service12.creeEmploye("Coluche",4000);
			Employe12 emp4 = service12.creeEmploye("Pierre Desproges",4000);
			Projet12 proj1 = serviceProj12.creeProjet("Faire rire le management");
			Projet12 proj2 = serviceProj12.creeProjet("Sourire des facheux");
			
			service12.ajouteProjetEmploye(emp1.getId(), proj1.getId());
			service12.ajouteProjetEmploye(emp2.getId(), proj1.getId());
			service12.ajouteProjetEmploye(emp4.getId(), proj1.getId());
			service12.ajouteProjetEmploye(emp3.getId(), proj2.getId());			
			service12.ajouteProjetEmploye(emp4.getId(), proj2.getId());
			
			Collection<Projet12> projs  = serviceProj12.findAllProjets();
			if (projs.isEmpty()) {
				out.println("Aucun projet trouv&eacute;");
			} else {
				out.println("Projets trouv&eacute;s :</br>");
				for (Projet12 p : projs) {
					out.println(p + " avec les employ&eacute;s : " + p.getEmployes() + "<br/>");
				}
			}
		}
		
		// - package entites.relations.one.to.many.unidirect (Employes et Telephones)
		else if (action.equals("Test 19")) {
			Employe19 emp = service19.creeEmploye("Paul Eluard",4500);
			Telephone19 tel1 = serviceTel19.creeNoTelephone("02 44 55 66 88","fixe");
			Telephone19 tel2 = serviceTel19.creeNoTelephone("06 91 54 76 84","mobile");
			service19.ajouteNumeroEmploye( emp.getId(), tel1.getId());
			service19.ajouteNumeroEmploye( emp.getId(), tel2.getId());
			Collection<Employe19> emps  = service19.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute;s trouv&eacute;s :</br>");
				for (Employe19 e : emps) {
					out.println(e + "avec les t&eacute;l&eacute;phones : " + e.getNumeros() + "<br/>");
				}
			}
			Collection<Telephone19> tels  = serviceTel19.findAllTelephones();
			if (tels.isEmpty()) {
				out.println("Aucun t&eacute;l&eacute;phone trouv&eacute;");
			} else {
				out.println("T&eacute;l&eacute;phones trouv&eacute;s :</br>");
				for (Telephone19 t : tels) {
					out.println(t + "<br/>");
				}
			}
		} 
		
		// - package entites.relations.one.to.many.liste (Employes et Departements)
		else if (action.equals("Test 16")) {
			Employe16 emp1 = service16.creeEmploye("Rene Char",8200);
			Employe16 emp2 = service16.creeEmploye("Philippe Soupault",9000);
			Departement16 dep = serviceDep16.creeDepartement("Edition");
			service16.setDepartementEmploye(emp1.getId(),dep.getId());
			service16.setDepartementEmploye(emp2.getId(),dep.getId());
			Collection<Employe16> emps = service16.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe16 emp : emps) {
					out.println(emp + "dans le d&eacute;partement : " + emp.getDepartement() + "<br/>");
				}
			}
			Collection<Departement16> deps = serviceDep16.findAllDepartements();
			if (deps.isEmpty()) {
				out.println("Aucun d&eacute;partement trouv&eacute;");
			} else {
				out.println("D&eacute;partements trouv&eacute;s :</br>");
				for (Departement16 dep1 : deps) {
					out.println(dep1 + " avec " + dep1.getEmployes().size() + "employ&eacute;s<br/>");
				}
			}
		}
		
		// - package entites.relations.one.to.many.map (Employes et Departements)
		else if (action.equals("Test 17")) {
			Employe17 emp1 = service17.creeEmploye("Max Jacob",15000);
			Employe17 emp2 = service17.creeEmploye("Andr� Breton",10000);
			Departement17 dep1 = serviceDep17.creeDepartement("Po�sie");
			service17.setDepartementEmploye(emp1.getId(),dep1.getId());
			service17.setDepartementEmploye(emp2.getId(),dep1.getId());
			Collection<Employe17> emps = service17.findAllEmployes();
			if (emps.isEmpty()) {
				out.println("Aucun employ&eacute; trouv&eacute;");
			} else {
				out.println("Employ&eacute; trouv&eacute; :</br>");
				for (Employe17 emp : emps) {
					out.println(emp + "dans le d&eacute;partement : " + emp.getDepartement() + "<br/>");
				}
			}
			Collection<Departement17> deps = serviceDep17.findAllDepartements();
			if (deps.isEmpty()) {
				out.println("Aucun d&eacute;partement trouv&eacute;");
			} else {
				out.println("D&eacute;partements trouv&eacute;s :</br>");
				for (Departement17 dep : deps) {
					out.println(dep + " avec " + dep.getEmployes().size() + "employ&eacute;s<br/>");
				}
			}
		} 
		finPage(out);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}
	
    public void debutPage(PrintWriter out) throws IOException{
	    out.println("<html>");
        out.println("<head><title>R&eacute;sultats</title></head>");
        out.println("<center><h1>R&eacute;sultats</h1></center>");
        out.println("<hr/>");		
	}
    public void finPage(PrintWriter out) throws IOException{
		out.println("<hr/>");
	    out.println("</body>");    
	    out.println("</html>");
		out.close();
	}
}

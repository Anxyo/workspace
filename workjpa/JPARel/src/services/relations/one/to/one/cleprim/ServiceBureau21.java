package services.relations.one.to.one.cleprim;
import entites.relations.one.to.one.cleprim.*;
import java.util.Collection;

public interface ServiceBureau21 {
	public Bureau21 creeBureau(Employe21 emp, int bat, String emplct);
    public Collection<Bureau21> findAllBureaux();
}

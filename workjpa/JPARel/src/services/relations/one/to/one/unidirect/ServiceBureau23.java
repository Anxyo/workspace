package services.relations.one.to.one.unidirect;
import java.util.Collection;
import entites.relations.one.to.one.unidirect.*;

public interface ServiceBureau23 {
	public Bureau23 creeBureau(int batiment, String emplacement);
    public Collection<Bureau23> findAllBureaux();

}

package services.relations.one.to.one.lazy;
import java.util.Collection;

import entites.relations.one.to.one.lazy.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SBBOneToOneLazy")
public class ServiceBureauBean implements ServiceBureau22 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Bureau22 creeBureau(int batiment, String emplacement) {
        Bureau22 b = new Bureau22();
        b.setBatiment(batiment);
        b.setEmplacement(emplacement);
        em.persist(b);
        return b;
	}

	@SuppressWarnings("unchecked")
	public Collection<Bureau22> findAllBureaux() {
        Query query = em.createQuery("SELECT b FROM Bureau22 b");
        return (Collection<Bureau22>) query.getResultList();
	}

}

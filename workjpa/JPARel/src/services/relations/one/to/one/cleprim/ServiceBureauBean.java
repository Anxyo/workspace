package services.relations.one.to.one.cleprim;
import java.util.Collection;

import entites.relations.one.to.one.cleprim.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SBBOneToOneClePrim")
public class ServiceBureauBean implements ServiceBureau21 {
	@PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;


	public Bureau21 creeBureau(Employe21 emp, int bat, String emplct) {
		Bureau21 b = new Bureau21();
        b.setId(emp.getId());	// la ligne importante
        b.setBatiment(bat);
        b.setEmplacement(emplct);
        
        emp.setBureau(b);
        b.setEmploye(emp);
        em.persist(b);
        return b;
	}

	@SuppressWarnings("unchecked")
	public Collection<Bureau21> findAllBureaux() {
        Query query = em.createQuery("SELECT b FROM Bureau21 b");
        return (Collection<Bureau21>) query.getResultList();
	}

}

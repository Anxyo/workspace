package services.relations.one.to.one.bidirect;
import entites.relations.one.to.one.bidirect.*;
import java.util.Collection;

public interface ServiceEmploye20 {
	public Employe20 creeEmploye(String nom, long salaire);
    public Employe20 setBureauEmploye(int idEmp, int idBureau);
    public Collection<Employe20> findAllEmployes();
}

package services.relations.one.to.one.cleprim;
import entites.relations.one.to.one.cleprim.*;

import java.util.Collection;
public interface ServiceEmploye21 {
	public Employe21 creeEmploye(int id, String nom, long salaire);
    public Collection<Employe21> findAllEmployes();

}

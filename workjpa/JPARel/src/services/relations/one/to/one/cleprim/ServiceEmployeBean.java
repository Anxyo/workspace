package services.relations.one.to.one.cleprim;
import java.util.Collection;

import entites.relations.one.to.one.cleprim.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SEBOneToOneClePrim")
public class ServiceEmployeBean implements ServiceEmploye21 {

@PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe21 creeEmploye(int id, String nom, long salaire) {
        Employe21 emp = new Employe21();
        emp.setId(id);
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);
        return emp;
	}


	@SuppressWarnings("unchecked")
	public Collection<Employe21> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe21 e");
        return (Collection<Employe21>) query.getResultList();
	}

}

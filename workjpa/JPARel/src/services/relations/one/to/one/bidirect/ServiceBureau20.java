package services.relations.one.to.one.bidirect;
import java.util.Collection;
import entites.relations.one.to.one.bidirect.*;

public interface ServiceBureau20 {
	public Bureau20 creeBureau(int batiment, String emplacement);
    public Collection<Bureau20> findAllBureaux();

}

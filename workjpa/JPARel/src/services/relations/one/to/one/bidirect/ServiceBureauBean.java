package services.relations.one.to.one.bidirect;

import java.util.Collection;

import entites.relations.one.to.one.bidirect.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(name="SBBOneToOneBidirect")
public class ServiceBureauBean implements ServiceBureau20 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;
    
    public Bureau20 creeBureau(int batiment, String emplacement) {
	     Bureau20 b = new Bureau20();
	        b.setBatiment(batiment);
	        b.setEmplacement(emplacement);
	        em.persist(b);
	        return b;
	}

	@SuppressWarnings("unchecked")
	public Collection<Bureau20> findAllBureaux() {
        Query query = em.createQuery("SELECT b FROM Bureau20 b");
        return (Collection<Bureau20>) query.getResultList();
	}

}

package services.relations.one.to.one.unidirect;
import entites.relations.one.to.one.unidirect.*;
import java.util.Collection;

public interface ServiceEmploye23 {
	public Employe23 creeEmploye(String nom, long salaire);
    public Employe23 setBureauEmploye(int idEmp, int idBureau);
    public Collection<Employe23> findAllEmployes();
}

package services.relations.one.to.one.unidirect;

import java.util.Collection;

import entites.relations.one.to.one.unidirect.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SEBOneToOneUnidirect")
public class ServiceEmployeBean implements ServiceEmploye23 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

  
	public Employe23 creeEmploye(String nom, long salaire) {
        Employe23 emp = new Employe23();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);        
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe23> findAllEmployes() {
       Query query = em.createQuery("SELECT e FROM Employe23 e");
       return (Collection<Employe23>) query.getResultList();
 	}

	public Employe23 setBureauEmploye(int idEmp, int idBureau) {
		 Employe23 emp = em.find(Employe23.class, idEmp);
	     Bureau23 b = em.find(Bureau23.class, idBureau);
	     emp.setBureau(b);
	     return emp;
	}

}

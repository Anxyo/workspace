package services.relations.one.to.one.lazy;
import java.util.Collection;

import entites.relations.one.to.one.lazy.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SEBOneToOneLazy")
public class ServiceEmployeBean implements ServiceEmploye22 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe22 creeEmploye(String nom, long salaire) {
        Employe22 emp = new Employe22();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);       
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe22> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe22 e");
        return (Collection<Employe22>) query.getResultList();
	}


	public Employe22 setBureauEmploye(int idEmp, int idBureau) {
	       Employe22 emp = em.find(Employe22.class, idEmp);
	        Bureau22 b = em.find(Bureau22.class, idBureau);
	        emp.setBureau(b);
	        return emp;
	}

}

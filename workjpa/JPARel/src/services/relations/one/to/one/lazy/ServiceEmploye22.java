package services.relations.one.to.one.lazy;
import entites.relations.one.to.one.lazy.*;
import java.util.Collection;

public interface ServiceEmploye22 {
public Employe22 creeEmploye(String nom, long salaire);
    public Employe22 setBureauEmploye(int idEmp, int idBureau);
    public Collection<Employe22> findAllEmployes();
}

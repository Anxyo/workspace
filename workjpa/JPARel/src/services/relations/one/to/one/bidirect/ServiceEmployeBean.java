package services.relations.one.to.one.bidirect;

import java.util.Collection;

import entites.relations.one.to.one.bidirect.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SEBOneToOneBidirect")
public class ServiceEmployeBean implements ServiceEmploye20 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

  
	public Employe20 creeEmploye(String nom, long salaire) {
        Employe20 emp = new Employe20();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);        
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe20> findAllEmployes() {
       Query query = em.createQuery("SELECT e FROM Employe20 e");
       return (Collection<Employe20>) query.getResultList();
 	}

	public Employe20 setBureauEmploye(int idEmp, int idBureau) {
		 Employe20 emp = em.find(Employe20.class, idEmp);
	     Bureau20 b = em.find(Bureau20.class, idBureau);
	     emp.setBureau(b);
	     b.setEmploye(emp);	// les deux cot�s
	     return emp;
	}

}

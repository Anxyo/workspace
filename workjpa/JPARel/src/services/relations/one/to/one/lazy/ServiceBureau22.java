package services.relations.one.to.one.lazy;
import entites.relations.one.to.one.lazy.*;
import java.util.Collection;

public interface ServiceBureau22 {
	public Bureau22 creeBureau(int batiment, String emplacement);
    public Collection<Bureau22> findAllBureaux();
}

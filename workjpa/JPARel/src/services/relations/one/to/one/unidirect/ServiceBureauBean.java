package services.relations.one.to.one.unidirect;

import java.util.Collection;

import entites.relations.one.to.one.unidirect.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(name="SBBOneToOneUnidirect")
public class ServiceBureauBean implements ServiceBureau23 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;
    
    public Bureau23 creeBureau(int batiment, String emplacement) {
	     Bureau23 b = new Bureau23();
	        b.setBatiment(batiment);
	        b.setEmplacement(emplacement);
	        em.persist(b);
	        return b;
	}

	@SuppressWarnings("unchecked")
	public Collection<Bureau23> findAllBureaux() {
        Query query = em.createQuery("SELECT b FROM Bureau23 b");
        return (Collection<Bureau23>) query.getResultList();
	}

}

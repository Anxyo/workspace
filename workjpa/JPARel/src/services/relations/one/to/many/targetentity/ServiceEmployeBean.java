package services.relations.one.to.many.targetentity;
import java.util.Collection;

import entites.relations.one.to.many.targetentity.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SEBOneToManyTE")
public class ServiceEmployeBean implements ServiceEmploye18 {
	@PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

    
	public Employe18 creeEmploye(String nom, long salaire) {
        Employe18 emp = new Employe18();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);
        return emp;
	}


	@SuppressWarnings("unchecked")
	public Collection<Employe18> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe18 e");
        return (Collection<Employe18>) query.getResultList();
	}


	public Employe18 setDepartementEmploye(int idEmp, int idDept) {
        Employe18 emp = em.find(Employe18.class, idEmp);
        Departement18 dept = em.find(Departement18.class, idDept);
        dept.ajouteEmploye(emp);
        return emp;
	}

}

package services.relations.one.to.many.unidirect;

import entites.relations.one.to.many.unidirect.*;
import java.util.Collection;

public interface ServiceEmploye19 {
    public Employe19 creeEmploye(String nom, long salaire);
    public Employe19 ajouteNumeroEmploye(int idEmp, int idTel);
    public Collection<Employe19> findAllEmployes();
}

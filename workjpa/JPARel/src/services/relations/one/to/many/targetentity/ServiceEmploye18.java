package services.relations.one.to.many.targetentity;
import entites.relations.one.to.many.targetentity.*;
import java.util.Collection;

public interface ServiceEmploye18 {
    public Employe18 creeEmploye(String nom, long salaire);
    public Employe18 setDepartementEmploye(int idEmp, int idDept);
    public Collection<Employe18> findAllEmployes();
}

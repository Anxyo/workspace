package services.relations.one.to.many.liste;
import java.util.Collection;

import entites.relations.one.to.many.liste.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(name="SEBOneToManyListe")
public class ServiceEmployeBean implements ServiceEmploye16 {
	
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe16 creeEmploye(String nom, long salaire) {
        Employe16 emp = new Employe16();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);       
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe16> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe16 e");
        return (Collection<Employe16>) query.getResultList();
	}


	public Employe16 setDepartementEmploye(int idEmp, int idDept) {
        Employe16 emp = em.find(Employe16.class, idEmp);
        Departement16 dept = em.find(Departement16.class, idDept);
        dept.ajouteEmploye(emp);
        return emp;
	}

}

package services.relations.one.to.many.unidirect;
import java.util.Collection;

import entites.relations.one.to.many.unidirect.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="STBOneToManyUni")
public class ServiceTelephoneBean implements ServiceTelephone19 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

 	public Telephone19 creeNoTelephone(String num, String type) {
        Telephone19 tel = new Telephone19();
        tel.setNumero(num);
        tel.setType(type);
        em.persist(tel);       
        return tel;
	}


	@SuppressWarnings("unchecked")
	public Collection<Telephone19> findAllTelephones() {
        Query query = em.createQuery("SELECT t FROM Telephone19 t");
        return (Collection<Telephone19>) query.getResultList();
	}

}

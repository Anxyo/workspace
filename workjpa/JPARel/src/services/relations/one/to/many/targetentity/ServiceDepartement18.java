package services.relations.one.to.many.targetentity;
import entites.relations.one.to.many.targetentity.*;
import java.util.Collection;

public interface ServiceDepartement18 {
    public Departement18 creeDepartement(String nom);
    public Collection<Departement18> findAllDepartements();
}

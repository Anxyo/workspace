package services.relations.one.to.many.map;
import java.util.Collection;

import entites.relations.one.to.many.map.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(name="SEBOneToManyMap")
public class ServiceEmployeBean implements ServiceEmploye17 {
	@PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe17 creeEmploye(String nom, long salaire) {
        Employe17 emp = new Employe17();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);
        return emp;
	}


	@SuppressWarnings("unchecked")
	public Collection<Employe17> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe17 e");
        return (Collection<Employe17>) query.getResultList();
	}


	public Employe17 setDepartementEmploye(int idEmp, int idDept) {
        Employe17 emp = em.find(Employe17.class, idEmp);
        Departement17 dept = em.find(Departement17.class, idDept);
        dept.ajouteEmploye(emp);
        emp.setDepartement(dept);
        return emp;
	}

}

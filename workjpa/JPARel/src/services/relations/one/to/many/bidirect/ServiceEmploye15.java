package services.relations.one.to.many.bidirect;
import entites.relations.one.to.many.bidirect.*;
import java.util.Collection;

public interface ServiceEmploye15 {
	public Employe15 creeEmploye(String nom, long salaire);
    public Employe15 setDepartementEmploye(int idEmp, int idDept);
    public Collection<Employe15> findAllEmployes();

}

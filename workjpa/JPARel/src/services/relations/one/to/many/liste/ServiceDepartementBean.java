package services.relations.one.to.many.liste;
import java.util.Collection;

import entites.relations.one.to.many.liste.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SDBOneToManyListe")
public class ServiceDepartementBean implements ServiceDepartement16 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Departement16 creeDepartement(String nom) {
        Departement16 dept = new Departement16();
        dept.setNom(nom);
        em.persist(dept);
        return dept;
	}


	@SuppressWarnings("unchecked")
	public Collection<Departement16> findAllDepartements() {
        Query query = em.createQuery("SELECT d FROM Departement16 d");
        return (Collection<Departement16>) query.getResultList();
	}

}

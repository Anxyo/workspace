package services.relations.one.to.many.bidirect;

import entites.relations.one.to.many.bidirect.*;
import java.util.Collection;

public interface ServiceDepartement15 {
	public Departement15 creeDepartement(String nom);
    public Collection<Departement15> findAllDepartements();
}

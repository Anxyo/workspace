package services.relations.one.to.many.unidirect;

import entites.relations.one.to.many.unidirect.*;
import java.util.Collection;

public interface ServiceTelephone19 {
    public Telephone19 creeNoTelephone(String num, String type);
    public Collection<Telephone19> findAllTelephones();
}

package services.relations.one.to.many.unidirect;

import java.util.Collection;

import entites.relations.one.to.many.unidirect.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SEBOneToManyUni")
public class ServiceEmployeBean implements ServiceEmploye19 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe19 ajouteNumeroEmploye(int idEmp, int idTel) {
	      Employe19 emp = em.find(Employe19.class, idEmp);
	        Telephone19 tel = em.find(Telephone19.class, idTel);
	        emp.ajouteTelephone(tel);
	        return emp;
	}

	
	public Employe19 creeEmploye(String nom, long salaire) {
	      Employe19 emp = new Employe19();
	        emp.setNom(nom);
	        emp.setSalaire(salaire);
	        em.persist(emp);
	        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe19> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe19 e");
        return (Collection<Employe19>) query.getResultList();
	}

}

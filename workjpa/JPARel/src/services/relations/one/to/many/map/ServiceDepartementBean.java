package services.relations.one.to.many.map;
import java.util.Collection;

import entites.relations.one.to.many.map.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SDBOneToManyMap")
public class ServiceDepartementBean implements ServiceDepartement17 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

    public Departement17 creeDepartement(String nom) {
        Departement17 dept = new Departement17();
        dept.setNom(nom);
        em.persist(dept);        
        return dept;
	}

	@SuppressWarnings("unchecked")
	public Collection<Departement17> findAllDepartements() {
        Query query = em.createQuery("SELECT d FROM Departement17 d");
        return (Collection<Departement17>) query.getResultList();
	}

}

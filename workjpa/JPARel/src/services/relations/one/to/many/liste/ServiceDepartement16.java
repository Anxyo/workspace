package services.relations.one.to.many.liste;
import entites.relations.one.to.many.liste.*;
import java.util.Collection;

public interface ServiceDepartement16 {
	public Departement16 creeDepartement(String nom);
    public Collection<Departement16> findAllDepartements();
}

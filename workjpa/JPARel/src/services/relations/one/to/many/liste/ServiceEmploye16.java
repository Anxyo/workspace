package services.relations.one.to.many.liste;
import entites.relations.one.to.many.liste.*;
import java.util.Collection;

public interface ServiceEmploye16 {
    public Employe16 creeEmploye(String nom, long salaire);
    public Employe16 setDepartementEmploye(int idEmp, int idDept);
    public Collection<Employe16> findAllEmployes();
}

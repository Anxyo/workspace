package services.relations.one.to.many.bidirect;
import java.util.Collection;

import entites.relations.one.to.many.bidirect.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SDBOneToManyBidirect")
public class ServiceDepartementBean implements ServiceDepartement15 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;


	public Departement15 creeDepartement(String nom) {
        Departement15 dept = new Departement15();
        dept.setNom(nom);
        em.persist(dept);
        return dept;
	}


	@SuppressWarnings("unchecked")
	public Collection<Departement15> findAllDepartements() {
        Query query = em.createQuery("SELECT d FROM Departement15 d");
        return (Collection<Departement15>) query.getResultList();
	}

}

package services.relations.one.to.many.targetentity;
import java.util.Collection;

import entites.relations.one.to.many.targetentity.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SDBOneToManyTE")
public class ServiceDepartementBean implements ServiceDepartement18 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Departement18 creeDepartement(String nom) {
        Departement18 dept = new Departement18();
        dept.setNom(nom);
        em.persist(dept);       
        return dept;
	}


	@SuppressWarnings("unchecked")
	public Collection<Departement18> findAllDepartements() {
        Query query = em.createQuery("SELECT d FROM Departement18 d");
        return (Collection<Departement18>) query.getResultList();
	}

}

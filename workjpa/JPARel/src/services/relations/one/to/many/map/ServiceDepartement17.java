package services.relations.one.to.many.map;
import entites.relations.one.to.many.map.*;
import java.util.Collection;

public interface ServiceDepartement17 {
    public Departement17 creeDepartement(String nom);
    public Collection<Departement17> findAllDepartements();
}

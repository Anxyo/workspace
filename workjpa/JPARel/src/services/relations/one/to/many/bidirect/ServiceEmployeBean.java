package services.relations.one.to.many.bidirect;
import java.util.Collection;

import entites.relations.one.to.many.bidirect.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(name="SEBOneToManyBidirect")
public class ServiceEmployeBean implements ServiceEmploye15 {
	@PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe15 creeEmploye(String nom, long salaire) {
        Employe15 emp = new Employe15();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe15> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe15 e");
        return (Collection<Employe15>) query.getResultList();
	}

	public Employe15 setDepartementEmploye(int idEmp, int idDept) {
        Employe15 emp = em.find(Employe15.class, idEmp);
        Departement15 dept = em.find(Departement15.class, idDept);
        dept.ajouteEmploye(emp);
        return emp;
	}

}

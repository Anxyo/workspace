package services.relations.one.to.many.map;
import entites.relations.one.to.many.map.*;
import java.util.Collection;

public interface ServiceEmploye17 {
    public Employe17 creeEmploye(String nom, long salaire);
    public Employe17 setDepartementEmploye(int idEmp, int idDept);
    public Collection<Employe17> findAllEmployes();
}

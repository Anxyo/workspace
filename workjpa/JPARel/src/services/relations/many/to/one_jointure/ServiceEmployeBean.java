package services.relations.many.to.one_jointure;
import java.util.Collection;

import entites.relations.many.to.one_jointure.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless(name="SEBManyToOneJointure")
public class ServiceEmployeBean implements ServiceEmploye14 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;
  
	public Employe14 creeEmploye(String nom, long salaire) {
        Employe14 emp = new Employe14();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe14> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe14 e");
        return (Collection<Employe14>) query.getResultList();
	}

	public Employe14 setDepartementEmploye(int idEmp, int idDept) {
        Employe14 emp = em.find(Employe14.class, idEmp);
        Departement14 dept = em.find(Departement14.class, idDept);
        emp.setDepartement(dept);
        return emp;
	}

}

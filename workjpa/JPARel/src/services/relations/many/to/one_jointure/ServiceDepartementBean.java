package services.relations.many.to.one_jointure;
import java.util.Collection;

import entites.relations.many.to.one_jointure.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SDBManyToOneJointure")
public class ServiceDepartementBean implements ServiceDepartement14 {
	
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Departement14 creeDepartement(String nom) {
        Departement14 dept = new Departement14();
        dept.setNom(nom);
        em.persist(dept);        
        return dept;
	}

	@SuppressWarnings("unchecked")
	public Collection<Departement14> findAllDepartements() {
        Query query = em.createQuery("SELECT d FROM Departement14 d");
        return (Collection<Departement14>) query.getResultList();
	}

}

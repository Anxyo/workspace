package services.relations.many.to.one_jointure;
import entites.relations.many.to.one_jointure.*;
import java.util.Collection;

public interface ServiceEmploye14 {
    public Employe14 creeEmploye(String nom, long salaire);
    public Employe14 setDepartementEmploye(int idEmp, int idDept);
    public Collection<Employe14> findAllEmployes();

}

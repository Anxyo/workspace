package services.relations.many.to.one_jointure;
import entites.relations.many.to.one_jointure.*;
import java.util.Collection;

public interface ServiceDepartement14 {
	public Departement14 creeDepartement(String nom);
    public Collection<Departement14> findAllDepartements();

}

package services.relations.many.to.one;
import java.util.Collection;
import entites.relations.many.to.one.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
@Stateless(name="SEBManyToOne")
public class ServiceEmployeBean implements ServiceEmploye13{

	@PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe13 creeEmploye(String nom, long salaire) {
	    Employe13 emp = new Employe13();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);
        return emp;	
        }


	@SuppressWarnings("unchecked")
	public Collection<Employe13> findAllEmployes() {
		  Query query = em.createQuery("SELECT e FROM Employe13 e");
	        return (Collection<Employe13>) query.getResultList();
	}


	public Employe13 setDepartementEmploye(int idEmp, int idDept) {
        Employe13 emp = em.find(Employe13.class, idEmp);
        Departement13 dept = em.find(Departement13.class, idDept);
        emp.setDepartement(dept);
        return emp;
	}

}

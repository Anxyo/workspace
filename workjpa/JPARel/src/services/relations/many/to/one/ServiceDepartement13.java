package services.relations.many.to.one;
import entites.relations.many.to.one.*;
import java.util.Collection;

public interface ServiceDepartement13 {
	public Departement13 creeDepartement(String nom);
    public Collection<Departement13> findAllDepartements();

}

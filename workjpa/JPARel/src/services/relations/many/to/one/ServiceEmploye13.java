package services.relations.many.to.one;
import entites.relations.many.to.one.*;
import java.util.Collection;

public interface ServiceEmploye13 {
    public Employe13 creeEmploye(String nom, long salaire);
    public Employe13 setDepartementEmploye(int idEmp, int idDept);
    public Collection<Employe13> findAllEmployes();

}

package services.relations.many.to.one;
import java.util.Collection;

import javax.ejb.Stateless;
import entites.relations.many.to.one.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SDBManyToOne")
public class ServiceDepartementBean implements ServiceDepartement13 {
	@PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Departement13 creeDepartement(String nom) {
        Departement13 dept = new Departement13();
        dept.setNom(nom);
        em.persist(dept);    
        return dept;
	}


	@SuppressWarnings("unchecked")
	public Collection<Departement13> findAllDepartements() {
        Query query = em.createQuery("SELECT d FROM Departement13 d");
        return (Collection<Departement13>) query.getResultList();
	}

}

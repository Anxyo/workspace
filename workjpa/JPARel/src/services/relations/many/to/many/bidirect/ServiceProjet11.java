package services.relations.many.to.many.bidirect;
import entites.relations.many.to.many.bidirect.*;
import java.util.Collection;

public interface ServiceProjet11 {
    public Projet11 creeProjet(String nom);
    public Collection<Projet11> findAllProjets();
}

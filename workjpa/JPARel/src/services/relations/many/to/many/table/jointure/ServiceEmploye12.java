package services.relations.many.to.many.table.jointure;
import entites.relations.many.to.many.table.jointure.*;
import java.util.Collection;

public interface ServiceEmploye12 {
    public Employe12 creeEmploye(String nom, long salaire);
    public Employe12 ajouteProjetEmploye(int idEmp, int idProj);
    public Collection<Employe12> findAllEmployes();
}

package services.relations.many.to.many.table.jointure;
import entites.relations.many.to.many.table.jointure.*;
import java.util.Collection;

public interface ServiceProjet12 {
    public Projet12 creeProjet(String nom);
    public Collection<Projet12> findAllProjets();
}

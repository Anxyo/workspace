package services.relations.many.to.many.table.jointure;

import java.util.Collection;
import entites.relations.many.to.many.table.jointure.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SPBManyToManyJointure")
public class ServiceProjetBean implements ServiceProjet12 {

    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Projet12 creeProjet(String nom) {
        Projet12 proj = new Projet12();
        proj.setNom(nom);
        em.persist(proj);
        
        return proj;
	}


	@SuppressWarnings("unchecked")
	public Collection<Projet12> findAllProjets() {
        Query query = em.createQuery("SELECT p FROM Projet12 p");
        return (Collection<Projet12>) query.getResultList();
	}

}

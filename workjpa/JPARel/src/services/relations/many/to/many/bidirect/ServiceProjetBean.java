package services.relations.many.to.many.bidirect;
import java.util.Collection;

import entites.relations.many.to.many.bidirect.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SPBManyToMany")
public class ServiceProjetBean implements ServiceProjet11 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;


	public Projet11 creeProjet(String nom) {
        Projet11 proj = new Projet11();
        proj.setName(nom);
        em.persist(proj);
        
        return proj;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Projet11> findAllProjets() {
        Query query = em.createQuery("SELECT p FROM Projet11 p");
        return (Collection<Projet11>) query.getResultList();
	}

}

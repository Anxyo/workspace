package services.relations.many.to.many.bidirect;
import entites.relations.many.to.many.bidirect.*;
import java.util.Collection;

public interface ServiceEmploye11 {
    public Employe11 creeEmploye(String nom, long salaire);
    public Employe11 ajouteProjetEmploye(int empId, int projId);
    public Collection<Employe11> findAllEmployes();
}

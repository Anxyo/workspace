package services.relations.many.to.many.table.jointure;
import java.util.Collection;

import entites.relations.many.to.many.table.jointure.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SEBManyToManyJointure")
public class ServiceEmployeBean implements ServiceEmploye12 {
 @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;
    
	public Employe12 ajouteProjetEmploye(int idEmp, int idProj) {
        Employe12 emp = em.find(Employe12.class, idEmp);
        Projet12 proj = em.find(Projet12.class, idProj);
        proj.ajouteEmploye(emp);
        return emp;
	}


	public Employe12 creeEmploye(String nom, long salaire) {
        Employe12 emp = new Employe12();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);       
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe12> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe12 e");
        return (Collection<Employe12>) query.getResultList();
	}

}

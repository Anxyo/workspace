package services.relations.many.to.many.bidirect;
import java.util.Collection;

import entites.relations.many.to.many.bidirect.*;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless(name="SEBManyToMany")
public class ServiceEmployeBean implements ServiceEmploye11 {

    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe11 ajouteProjetEmploye(int idEmp, int idProj) {
        Employe11 emp = em.find(Employe11.class, idEmp);
        Projet11 proj = em.find(Projet11.class, idProj);
        proj.ajouteEmploye(emp);
        return emp;
	}

	@Override
	public Employe11 creeEmploye(String nom, long salaire) {
        Employe11 emp = new Employe11();
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);
        
        return emp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Employe11> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe11 e");
        return (Collection<Employe11>) query.getResultList();
	}

}

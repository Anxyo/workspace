package util;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.naming.InitialContext;
import javax.sql.DataSource;
/**
 * Factorisation de quelques routines utiles
 *
 */
public class Utilitaires {
	/**
	 * 
	 * @param nomDataSource : la source de donn�es � utiliser pour la connexion
	 * @param ordresSQL : un Vector d'ordres SQL 
	 */
	public void initSQL(String nomDataSource, Vector<String> ordresSQL){
    	Connection cnx = null;
    	Statement st = null;
    	// connexion
    	try {
    		InitialContext initialContext = new InitialContext();
    		DataSource dataSource = (DataSource) initialContext.lookup(nomDataSource);
    		cnx = dataSource.getConnection();
    		System.out.println("Connexion effectu�e : " + (!cnx.isClosed()));
    		   	
    	}
    	catch (Exception exc) {
    		System.out.println("Pb Connexion : " + exc.getMessage());
    		return;
    	}
    	// ex�cution des requ�tes
    	for (int i = 0; i < ordresSQL.size(); i++) {
    		String rqt = ordresSQL.elementAt(i);
    		try {
    			st = cnx.createStatement();
    			st.executeUpdate(rqt);
    		}
    		catch (SQLException exc) {
    			System.out.println("Erreur d'ex�cution de la requete : " + rqt + "(" + exc.getMessage()+ ")");
    		}
    	}
    	
    	try {
        	cnx.close();
        	System.out.println("Connexion referm�e");
    	} 
    	catch (Exception exc) {}
    }
	
	public int parseInt(String intString) {
        try {
            return Integer.parseInt(intString);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
	    
    public long parseLong(String longString) {
        try {
            return Long.parseLong(longString);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
    
    public void debutPage(PrintWriter out) throws IOException{
	    out.println("<html>");
        out.println("<head><title>R&eacute;sultats</title></head>");
        out.println("<center><h1>R&eacute;sultats</h1></center>");
        out.println("<hr/>");		
	}
    
    public void finPage(PrintWriter out, int noPageJsp) throws IOException{
	    out.println("</body>");    
	    out.println("</html>");
		out.close();
	}
}


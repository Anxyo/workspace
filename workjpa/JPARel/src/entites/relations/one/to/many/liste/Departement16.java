package entites.relations.one.to.many.liste;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Departement16 {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    @OneToMany(mappedBy="departement")
    @OrderBy("nom ASC")
    private List<Employe16> employes;

    public Departement16() {
        employes = new ArrayList<Employe16>();
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nomDept) {
        this.nom = nomDept;
    }
    
    public void ajouteEmploye(Employe16 employe) {
        if (!getEmployes().contains(employe)) {
            getEmployes().add(employe);
            if (employe.getDepartement() != null) {
                employe.getDepartement().getEmployes().remove(employe);
            }
            employe.setDepartement(this);
        }
    }
    
    public Collection<Employe16> getEmployes() {
        return employes;
    }

    public String toString() {
        return "Id département : " + getId() + 
               ", nom : " + getNom();
    }
}

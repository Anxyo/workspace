package entites.relations.one.to.many.unidirect;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Employe19 {
@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    private long salaire;
    
    @OneToMany
    @JoinTable(name="EMP_TEL19", 
          joinColumns=@JoinColumn(name="ID_EMP"),
          inverseJoinColumns=@JoinColumn(name="ID_TEL"))
    private Collection<Telephone19> numeros;

    public Employe19() {
        numeros = new ArrayList<Telephone19>();
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }

    public void ajouteTelephone(Telephone19 no) {
        if (!getNumeros().contains(no)) {
        	getNumeros().add(no);
        }
    }
    
    public Collection<Telephone19> getNumeros() {
        return numeros;
    }
    
    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + 
               " a " + getNumeros().size() + " num�ros";
    }
}

package entites.relations.one.to.many.bidirect;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Departement15 {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    @OneToMany(mappedBy="departement")
    private Collection<Employe15> employes;

    public Departement15() {
        employes = new ArrayList<Employe15>();
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nomDept) {
        this.nom = nomDept;
    }
    
    public void ajouteEmploye(Employe15 employe) {
        if (!getEmployes().contains(employe)) {
            getEmployes().add(employe);
            if (employe.getDepartement() != null) {
                employe.getDepartement().getEmployes().remove(employe);
            }
            employe.setDepartement(this);
        }
    }
    
    public Collection<Employe15> getEmployes() {
        return employes;
    }

    public String toString() {
        return "Id département : " + getId() + 
               ", nom : " + getNom();
    }

}

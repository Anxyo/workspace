package entites.relations.one.to.many.bidirect;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Employe15 {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    private long salaire;
    
    @ManyToOne
    private Departement15 departement;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public Departement15 getDepartement() {
        return departement;
    }
    
    public void setDepartement(Departement15 dep) {
        this.departement = dep;
    }

    public String toString() {
        return "Id employe : " + getId() + " nom : " + getNom() + 
               " membre de " + getDepartement();
    }

}

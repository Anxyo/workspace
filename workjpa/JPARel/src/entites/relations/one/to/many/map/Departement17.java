package entites.relations.one.to.many.map;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Departement17 {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    @OneToMany(mappedBy="departement")
    @MapKey(name="nom")
    private Map<String, Employe17> employes;

    public Departement17() {
        employes = new HashMap<String, Employe17>();
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nomDept) {
        this.nom = nomDept;
    }
    
    public void ajouteEmploye(Employe17 employe) {
        if (!getEmployes().containsKey(employe.getNom())) {
            getEmployes().put(employe.getNom(), employe);
            if (employe.getDepartement() != null) {
                employe.getDepartement().getEmployes().remove(employe.getNom());
            }
            employe.setDepartement(this);
        }
    }
    
    public Map<String,Employe17> getEmployes() {
        return employes;
    }

    public String toString() {
        return "Id département : " + getId() + 
               ", nom : " + getNom();
    }
}

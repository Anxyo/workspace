package entites.relations.one.to.many.unidirect;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Telephone19 {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    
    @Column(name="NUM")
    private String numero;
    private String type;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNumero() {
        return numero;
    }
    
    public void setNumero(String no) {
        this.numero = no;
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String typeTel) {
        this.type = typeTel;
    }

    public String toString() {
        return "Id t�l. : " + getId() + 
               ", no : " + getNumero() +
               ", type: " + getType();
    }
}

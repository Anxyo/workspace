package entites.relations.one.to.many.map;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Employe17 {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    private long salaire;
    
    @ManyToOne
    private Departement17 departement;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public Departement17 getDepartement() {
        return departement;
    }
    
    public void setDepartement(Departement17 departement) {
        this.departement = departement;
    }

    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + 
               " membre de " + getDepartement();
    }
}

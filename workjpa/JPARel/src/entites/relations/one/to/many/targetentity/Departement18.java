package entites.relations.one.to.many.targetentity;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Departement18 {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    @SuppressWarnings("unchecked")
	@OneToMany(targetEntity=Employe18.class, mappedBy="departement")
    private Collection employes;				// type g�n�rique souhaitable, mais non indispensable

    public Departement18() {
        employes = new ArrayList<Employe18>();
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nomDept) {
        this.nom = nomDept;
    }
    
    @SuppressWarnings("unchecked")
	public void ajouteEmploye(Employe18 employe) {
        if (!getEmployes().contains(employe)) {
            getEmployes().add(employe);
            if (employe.getDepartement() != null) {
                employe.getDepartement().getEmployes().remove(employe);
            }
            employe.setDepartement(this);
        }
    }
    
    @SuppressWarnings("unchecked")
	public Collection getEmployes() {
        return employes;
    }

    public String toString() {
        return "Id d�partment : " + getId() + 
               ", nom : " + getNom();
    }

}

package entites.relations.one.to.many.liste;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Employe16 {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    private long salaire;
    
    @ManyToOne
    private Departement16 departement;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public Departement16 getDepartement() {
        return departement;
    }
    
    public void setDepartement(Departement16 dep) {
        this.departement = dep;
    }

    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + 
               " membre de " + getDepartement();
    }
}

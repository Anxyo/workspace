package entites.relations.one.to.one.unidirect;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(schema="RELATIONS")
public class Bureau23 {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private int batiment;
    private String emplacement;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public int getBatiment() {
        return batiment;
    }

    public void setBatiment(int bat) {
        this.batiment = bat;
    }
    
    public String getEmplacement() {
        return emplacement;
    }
    
    public void setEmplacement(String emplct) {
        this.emplacement = emplct;
    }

    public String toString() {
        return "Id bureau : " + getId() + " batiment : " + getBatiment() +
               ", emplacement : " + getEmplacement();
    }


}

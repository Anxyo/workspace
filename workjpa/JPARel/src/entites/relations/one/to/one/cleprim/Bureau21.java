package entites.relations.one.to.one.cleprim;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Bureau21 {
    @Id
    private int id;
    private int batiment;
    private String emplacement;
    @OneToOne
    @PrimaryKeyJoinColumn
    private Employe21 employe;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public int getBatiment() {
        return batiment;
    }

    public void setBatiment(int b) {
        this.batiment = b;
    }
    
    public String getEmplacement() {
        return emplacement;
    }
    
    public void setEmplacement(String nomEmplact) {
        this.emplacement = nomEmplact;
    }

    public Employe21 getEmploye() {
        return employe;
    }

    public void setEmploye(Employe21 employe) {
        this.employe = employe;
    }
    
    public String toString() {
        return "Id bureau : " + getId() + " batiment : " + getBatiment() +
               ", emplacement : " + getEmplacement();
    }

}

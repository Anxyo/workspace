package entites.relations.one.to.one.lazy;
import static javax.persistence.FetchType.LAZY;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Employe22 {
@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    private long salaire;
    
    @OneToOne(fetch=LAZY)
    private Bureau22 bureau;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String n) {
        this.nom = n;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public Bureau22 getBureau() {
        return bureau;
    }
    
    public void setBureau(Bureau22 b) {
        this.bureau = b;
    }

    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + 
               " with " + getBureau();
    }

}

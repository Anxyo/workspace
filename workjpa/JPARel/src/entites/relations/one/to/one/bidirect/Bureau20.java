package entites.relations.one.to.one.bidirect;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(schema="RELATIONS")
public class Bureau20 {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private int batiment;
    private String emplacement;
    
    @OneToOne(mappedBy="bureau")
    private Employe20 employe;

	public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public Employe20 getEmploye() {
		return employe;
	}

	public void setEmploye(Employe20 employe) {
		this.employe = employe;
	}

    
    public int getBatiment() {
        return batiment;
    }

    public void setBatiment(int bat) {
        this.batiment = bat;
    }
    
    public String getEmplacement() {
        return emplacement;
    }
    
    public void setEmplacement(String emplct) {
        this.emplacement = emplct;
    }

    public String toString() {
        return "Id bureau : " + getId() + " batiment : " + getBatiment() +
               ", emplacement : " + getEmplacement();
    }

}

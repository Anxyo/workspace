package entites.relations.one.to.one.cleprim;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Employe21 {
    @Id
    private int id;
    private String nom;
    private long salaire;
    
    @OneToOne(mappedBy="employe") 
    private Bureau21 bureau;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public Bureau21 getBureau() {
        return bureau;
    }
    
    public void setBureau(Bureau21 bureau) {
        this.bureau = bureau;
    }

    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + 
               " bureau :  " + getBureau();
    }

}

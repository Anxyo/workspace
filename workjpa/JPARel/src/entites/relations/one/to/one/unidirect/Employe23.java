package entites.relations.one.to.one.unidirect;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Employe23 {
	 @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	    private int id;
	    private String nom;
	    private long salaire;
	    
	    @OneToOne 
	    @JoinColumn(name="ID_BUREAU") 
	    private Bureau23 bureau;

	    public int getId() {
	        return id;
	    }
	    
	    public void setId(int id) {
	        this.id = id;
	    }
	    
	    public String getNom() {
	        return nom;
	    }
	    
	    public void setNom(String nom) {
	        this.nom = nom;
	    }

	    public long getSalaire() {
	        return salaire;
	    }

	    public void setSalaire(long salaire) {
	        this.salaire = salaire;
	    }
	    
	    public Bureau23 getBureau() {
	        return bureau;
	    }
	    
	    public void setBureau(Bureau23 bureau) {
	        this.bureau = bureau;
	    }

	    public String toString() {
	        return "Id employ� : " + getId() + " nom : " + getNom() + 
	               " bureau : " + getBureau();
	    }

}

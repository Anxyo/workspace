package entites.relations.one.to.one.bidirect;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Employe20 {
	 @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	    private int id;
	    private String nom;
	    private long salaire;
	    
	    @OneToOne 
	    @JoinColumn(name="ID_BUREAU")      //g�n�re la clef �trang�re. 
	    private Bureau20 bureau;

	    public int getId() {
	        return id;
	    }
	    
	    public void setId(int id) {
	        this.id = id;
	    }
	    
	    public String getNom() {
	        return nom;
	    }
	    
	    public void setNom(String nom) {
	        this.nom = nom;
	    }

	    public long getSalaire() {
	        return salaire;
	    }

	    public void setSalaire(long salaire) {
	        this.salaire = salaire;
	    }
	    
	    public Bureau20 getBureau() {
	        return bureau;
	    }
	    
	    public void setBureau(Bureau20 bureau) {
	        this.bureau = bureau;
	    }

	    public String toString() {
	        return "Id employ� : " + getId() + " nom : " + getNom() + 
	               " bureau : " + getBureau();
	    }

}

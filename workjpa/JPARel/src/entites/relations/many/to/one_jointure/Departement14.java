package entites.relations.many.to.one_jointure;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Departement14 {
	
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nomDept) {
        this.nom = nomDept;
    }

    public String toString() {
        return "Id département : " + getId() + 
               ", nom : " + getNom();
    }
}

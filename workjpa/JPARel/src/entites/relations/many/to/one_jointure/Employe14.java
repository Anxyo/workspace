package entites.relations.many.to.one_jointure;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Employe14 {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    private long salaire;
    
    @ManyToOne
    @JoinColumn(name="ID_DEPT")
    private Departement14 departement;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public Departement14 getDepartement() {
        return departement;
    }
    
    public void setDepartement(Departement14 departement) {
        this.departement = departement;
    }

    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + 
               " membre de " + getDepartement();
    }

}

package entites.relations.many.to.many.table.jointure;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Employe12 {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    private long salaire;
    
    @ManyToMany 
    @JoinTable(name="EMP_PROJ12", 
          joinColumns=@JoinColumn(name="ID_EMP"),
          inverseJoinColumns=@JoinColumn(name="ID_PROJ"))
    private Collection<Projet12> projets;
    
    
    public Employe12() {
        projets = new ArrayList<Projet12>();
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public void ajouteProjet(Projet12 projet) {
        if (!getProjets().contains(projet)) {
            getProjets().add(projet);
        }
        if (!projet.getEmployes().contains(this)) {
            projet.getEmployes().add(this);
        }
    }

    public Collection<Projet12> getProjets() {
        return projets;
    }

    public String toString() {
        return "Id employ� : " + getId() + ", nom : " + getNom() + 
               ", participe  � " + getProjets().size() + " projets";
    }

}

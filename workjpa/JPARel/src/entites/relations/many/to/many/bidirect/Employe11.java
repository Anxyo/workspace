package entites.relations.many.to.many.bidirect;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="TEST3", schema="RELATIONS")
public class Employe11 {
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    private long salaire;
    
    @ManyToMany
    private Collection<Projet11> projets;

    public Employe11() {
        projets = new ArrayList<Projet11>();
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public void ajouteProjet(Projet11 projet) {
        if (!getProjets().contains(projet)) {
            getProjets().add(projet);
        }
        if (!projet.getEmployes().contains(this)) {
            projet.getEmployes().add(this);
        }
    }

    public Collection<Projet11> getProjets() {
        return projets;
    }

    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + 
               ", participe � " + getProjets().size() + " projets";
    }
}

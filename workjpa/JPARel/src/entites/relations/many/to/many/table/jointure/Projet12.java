package entites.relations.many.to.many.table.jointure;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Projet12 {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    protected int id;
    protected String nom;
    @ManyToMany(mappedBy="projets")
    private Collection<Employe12> employes;

    public Projet12() {
        employes = new ArrayList<Employe12>();
    }

    public int getId() {
        return id;
    }
    
    public void setId(int noProjet) {
        this.id = noProjet;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nomProjet) {
        this.nom = nomProjet;
    }
    
    public Collection<Employe12> getEmployes() {
        return employes;
    }
    
    public void ajouteEmploye(Employe12 emp) {
        if (!getEmployes().contains(emp)) {
            getEmployes().add(emp);
        }
        if (!emp.getProjets().contains(this)) {
            emp.getProjets().add(this);
        }
    }
    
    public String toString() {
        return "Id projet : " + getId() + ", nom : " + getNom() +
               " trait� par " + getEmployes().size() + " employ�s";
    }
}

package entites.relations.many.to.many.bidirect;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(schema="RELATIONS")
public class Projet11 {
@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    protected int id;
    protected String nom;
    @ManyToMany(mappedBy="projets")
    private Collection<Employe11> employes;

    public Projet11() {
        employes = new ArrayList<Employe11>();
    }

    public int getId() {
        return id;
    }
    
    public void setId(int noProjet) {
        this.id = noProjet;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setName(String nomProjet) {
        this.nom = nomProjet;
    }
    
    public Collection<Employe11> getEmployes() {
        return employes;
    }
    
    public void ajouteEmploye(Employe11 employe) {
        if (!getEmployes().contains(employe)) {
            getEmployes().add(employe);
        }
        if (!employe.getProjets().contains(this)) {
            employe.getProjets().add(this);
        }
    }
    
    public String toString() {
        return "La projet d'id : " + getId() + ", de nom : " + getNom() +
               ", est trait� par " + getEmployes().size() + " employ�s";
    }

}

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>GlassFish JSP Page</title>
</head>
<body>

<center>
<h2>RELATIONS MULTIVALUEES</h2>
</center>

<hr />
<form action="ServPresRelMulti" method="POST">
<h3>Relation one to many bidirectionelle (package
entites.relations.one.to.many.bidirect)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Test 15" /></td>
		</tr>
	</tbody>
</table>

<hr />
<h3>Relation one to many bidirectionelle non typée (package
entites.relations.one.to.many.targetentity)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Test 18" /></td>
		</tr>
	</tbody>
</table>

<hr />
<h3>Relation many to many bidirectionelle (package
entites.relations.many.to.many.bidirect)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Test 11" /></td>
		</tr>
	</tbody>
</table>

<hr />
<h3>Relation many to many avec table de jointure (package
entites.relations.many.to.many.table.jointure)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Test 12" /></td>
		</tr>
	</tbody>
</table>

<hr />
<h3>Relation one to many unidirectionnelle (package
entites.relations.one.to.many.unidirect)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Test 19" /></td>
		</tr>
	</tbody>
</table>

<hr />

<h3>Relation one to many utilisant une liste (package
entites.relations.one.to.many.liste)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Test 16" /></td>
		</tr>
	</tbody>
</table>

<hr />
<h3>Relation one to many utilisant un map (package
entites.relations.one.to.many.map)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Test 17" /></td>
		</tr>
	</tbody>
</table>

<hr />
</form>
</html>

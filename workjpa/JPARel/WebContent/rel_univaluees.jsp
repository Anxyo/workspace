<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Relations univalu&eacute;es</title>
</head>
<body>
<center>
<h2>RELATIONS UNIVALUEES</h2>
</center>

<hr />
<form action="ServPresRelUni" method="POST">
<h3>Relation many to one (package entites.relations.many.to.one)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align="center">Employe</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom13" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire13" />(entier long)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation Emp13" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center">Departement</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nomDep13" />(une chaine)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation Dep13" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center">Associer un Employe � un
			Departement</td>
		</tr>
		<tr>
			<td>ID Employe :</td>
			<td><input type="text" name="idEmp13" />(un entier)</td>
		</tr>
		<tr>
			<td>ID Departement :</td>
			<td><input type="text" name="idDep13" />(un entier)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Association13" /></td>
		</tr>
	</tbody>
</table>

<h4>Rechercher tous les employ&eacute;s</h4>
<input name="action" type="submit" value="Rechercher13" />
<hr />
<h3>Relation many to one avec jointure (package
entites.relations.many.to.one_jointure)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align="center">Employe</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom14" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire14" />(entier long)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation Emp14" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center">Departement</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nomDep14" />(une chaine)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation Dep14" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center">Associer un Employe � un
			Departement</td>
		</tr>
		<tr>
			<td>ID Employe :</td>
			<td><input type="text" name="idEmp14" />(un entier)</td>
		</tr>
		<tr>
			<td>ID Departement :</td>
			<td><input type="text" name="idDep14" />(un entier)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Association14" /></td>
		</tr>
	</tbody>
</table>

<h4>Rechercher tous les employ&eacute;s</h4>
<input name="action" type="submit" value="Rechercher14" />
<hr />
<h3>Relation one to one unidirectionnelle (package
entites.relations.one.to.one.unidirect)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align="center">Employe</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom23" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire23" />(entier long)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation Emp23" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center">Bureau</td>
		</tr>
		<tr>
			<td>Batiment :</td>
			<td><input type="text" name="nomBat23" />(un entier)</td>
		</tr>
		<tr>
			<td>Emplacement :</td>
			<td><input type="text" name="nomEmp23" />(une chaine)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation Bureau23" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center">Associer un Employe � un
			Bureau</td>
		</tr>
		<tr>
			<td>ID Employe :</td>
			<td><input type="text" name="idEmp23" />(un entier)</td>
		</tr>
		<tr>
			<td>ID Bureau :</td>
			<td><input type="text" name="idBur23" />(un entier)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Association23" /></td>
		</tr>
	</tbody>
</table>

<h4>Rechercher tous les employ&eacute;s</h4>
<input name="action" type="submit" value="Rechercher23" />
<hr />
<h3>Relation one to one bidirectionnelle (package
entites.relations.one.to.one.bidirect)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align="center">Employe</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom20" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire20" />(entier long)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation Emp20" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center">Bureau</td>
		</tr>
		<tr>
			<td>Batiment :</td>
			<td><input type="text" name="nomBat20" />(un entier)</td>
		</tr>
		<tr>
			<td>Emplacement :</td>
			<td><input type="text" name="nomEmp20" />(une chaine)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation Bureau20" /></td>
		</tr>
		<tr>
			<td colspan="2" align="center">Associer un Employe � un
			Bureau</td>
		</tr>
		<tr>
			<td>ID Employe :</td>
			<td><input type="text" name="idEmp20" />(un entier)</td>
		</tr>
		<tr>
			<td>ID Bureau :</td>
			<td><input type="text" name="idBur20" />(un entier)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Association20" /></td>
		</tr>
	</tbody>
</table>

<h4>Rechercher tous les employ&eacute;s</h4>
<input name="action" type="submit" value="Rechercher20" />
<hr />
<h3>Relation one to one avec partage de cl� primaire (package
entites.relations.one.to.one.cleprim)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align="center">Employe</td>
		</tr>
		<tr>
			<td>Id :</td>
			<td><input type="text" name="idEmp21" />(un entier)</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom21" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire21" />(entier long)</td>
		</tr>
		<tr>
			<td colspan="2" align="center">Bureau</td>
		</tr>
		<tr>
			<td>Batiment :</td>
			<td><input type="text" name="nomBat21" />(un entier)</td>
		</tr>
		<tr>
			<td>Emplacement :</td>
			<td><input type="text" name="nomEmp21" />(une chaine)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation et association" /></td>
		</tr>
		
	</tbody>
</table>

<h4>Rechercher tous les employ&eacute;s</h4>
<input name="action" type="submit" value="Rechercher21" />

</form>
</body>
</html>
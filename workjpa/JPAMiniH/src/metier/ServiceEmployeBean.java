package metier;


import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Stateless
public class ServiceEmployeBean implements ServiceEmploye {
	
	@PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;     //injection de dépendance

    public Employe creeEmploye(int id, String nom, long salaire) {
        Employe emp = new Employe(id);
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);
        return emp;
    }

    public void supprimeEmploye(int id) {
        Employe emp = findEmploye(id);
        if (emp != null) {
            em.remove(emp);
        }
    }

    public Employe augmenteSalaire(int id, long augmentation) {
        Employe emp = em.find(Employe.class, id);
        if (emp != null) {
            emp.setSalaire(emp.getSalaire() + augmentation);
        }
        return emp;
    }

    public Employe findEmploye(int id) {
        return em.find(Employe.class, id);
    }


	public List<Employe> findAllEmployes() {
        TypedQuery<Employe> rqt = em.createQuery("SELECT e FROM Employe e", Employe.class);
        return  rqt.getResultList();
    }
}


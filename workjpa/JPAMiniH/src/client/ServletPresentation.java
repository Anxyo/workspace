package client;

import java.io.IOException;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Collection;

import metier.ServiceEmploye;
import metier.Employe;

public class ServletPresentation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	// injection du bean stateless
	@EJB ServiceEmploye service;

	
	public ServletPresentation(){
		super();
		//initSQL();
	}
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
	        PrintWriter out = response.getWriter();
	        debutPage(out);
	        // traite la requete
	        String action = request.getParameter("action");
	        if (action == null) {
	            // rien � faire, dans ce cas
	        } else if (action.equals("Creation")) {
	            Employe emp = service.creeEmploye(
	                    parseInt(request.getParameter("idCreation")),
	                    request.getParameter("nom"),
	                    parseLong(request.getParameter("salaire")));
	            out.println("Employ� cr�� : " + emp);
	            
	        } else if (action.equals("Suppression")) {
	            String id = request.getParameter("idSupp");
	            service.supprimeEmploye(parseInt(id));
	            out.println("Employe d'id : " + id + " supprim&eacute;");
	        } else if (action.equals("Mise a jour")) {
	            String id = request.getParameter("idAugment");
	            Employe emp = service.augmenteSalaire(
	                    parseInt(id),
	                    parseLong(request.getParameter("augmentation")));
	            out.println("Mis &agrave; jour : " + emp);
	        } else if (action.equals("Cherche")) {
	            Employe emp = service.findEmploye(
	                    parseInt(request.getParameter("idRecherche")));
	            out.println("Trouv&eacute; : " + emp);
	        } else if (action.equals("Rechercher")) {
	            Collection<Employe> emps = service.findAllEmployes();
	            if (emps.isEmpty()) {
	                out.println("Aucun employ&eacute; trouv&eacute;");
	            } else {
	                out.println("Employ&eacute; trouv&eacute;s : </br>");
	                for (Employe emp : emps) {
	                    out.print(emp + "<br/>");
	                }
	            }
	        }
	        finPage(out);
	        
	    }
	    
	private void debutPage(PrintWriter out) throws IOException{
	    out.println("<html>");
        out.println("<head><title>R&eacute;sultats</title></head>");
        out.println("<center><h1>R&eacute;sultats</h1></center>");
        out.println("<hr/>");
       
		
	}

	private void finPage(PrintWriter out) throws IOException{
		out.println("<hr/>");
	    out.println("<a href=\"index.jsp\">Page pr&eacute;c&eacute;dante<a/>");
	    out.println("</body>");    
	    out.println("</html>");
		out.close();
	}

	private int parseInt(String intString) {
        try {
            return Integer.parseInt(intString);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
	    
    private long parseLong(String longString) {
        try {
            return Long.parseLong(longString);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

	 private void initSQL(){
	    	String rqtSupprimeTable = "DROP TABLE EMPLOYE";
	    	String rqtCreeTable = "CREATE TABLE EMPLOYE (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, NOMBRE INTEGER, PRIMARY KEY (ID))";
	    	Connection cnx = null;
	    	Statement st = null;
	    	// connexion
	    	try {
	    		InitialContext initialContext = new InitialContext();
	    		DataSource dataSource = (DataSource) initialContext.lookup("java:app/jdbc/ServiceEmployeDS");
	    		cnx = dataSource.getConnection();
	    		System.out.println("Connexion effectu�e : " + (!cnx.isClosed()));
	    		   	
	    	}
	    	catch (Exception exc) {
	    		System.out.println("Pb Connexion : " + exc.getMessage());
	    		return;
	    	}
	    	
	    	// ex�cution des requ�tes
	    	try {
	    		st = cnx.createStatement();
	    		st.executeUpdate(rqtSupprimeTable);
	    	}
	    	catch (SQLException exc) {
	    		System.out.println("La table EMPLOYE n'existait pas : " + exc.getMessage());
	    	}
	    	try {
	    		st.executeUpdate(rqtCreeTable);
	    		System.out.println("La table EMPLOYE a bien �t� cr��e");
	    	}
	    	catch (SQLException exc) {
	    		System.out.println("Erreur de cr�ation de table : " + exc.getMessage());
	    	}
	    	try {
	        	cnx.close();
	        	System.out.println("Connexion referm�e");
	    	} 
	    	catch (Exception exc) {}
	    }
}

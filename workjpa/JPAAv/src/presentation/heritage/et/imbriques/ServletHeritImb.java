package presentation.heritage.et.imbriques;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entites.heritage.jointure.EmployePleinTemps31;
import entites.heritage.jointure.EmployeTempsPartiel31;
import entites.heritage.table.unique.EmployePleinTemps32;
import entites.heritage.table.unique.EmployeTempsPartiel32;
import entites.heritage.tpcc.EmployePleinTemps33;
import entites.heritage.tpcc.EmployeTempsPartiel33;
import entites.objets.imbriques.Employe40;

import services.heritage.jointure.ServiceEmploye31;
import services.heritage.table.unique.ServiceEmploye32;
import services.heritage.tpcc.ServiceEmploye33;
import services.objets.imbriques.ServiceEmploye40;
import util.Utilitaires;

public class ServletHeritImb extends HttpServlet {
	private static final long serialVersionUID = 1L;
	// injection EJBs
	@EJB(beanName = "SEBObjImb")
	ServiceEmploye40 service40;
	
	@EJB(beanName = "ESBHerTableUnique")
	ServiceEmploye32 service32;
	
	@EJB(beanName = "ESBHerJointure")
	ServiceEmploye31 service31;
	
	@EJB(beanName = "ESBHerTPCC")
	ServiceEmploye33 service33;

	public ServletHeritImb() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		Utilitaires util = new Utilitaires();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		util.debutPage(out);
		// traite la requ�te
		String action = request.getParameter("action");
		if (action == null) {
			// ne fait rien...
		} 
		
		else if (action.equals("Test 40")) {
			Employe40 emp = service40.creeEmployeEtAdresse(1, "Dupont Paul",
					2500, "24 Grande Rue", "Nice", "Alpes Maritimes", "06150");
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
			out.println("<br/>Attention ! Ne marche qu'une fois ! ");
		} 
		
		
		else if (action.equals("Creation employe plein temps32")) {
			EmployePleinTemps32 emp = new EmployePleinTemps32();
			emp.setId(1);
			emp.setNom("Raoul TableUnique-plein temps");
			emp.setDateDebut(new Date());
			emp.setSalaire(4000);
			emp.setRetraite(0);
			emp.setDureeVacances(45);
			service32.creeEmploye(emp);
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
			out.println("<br/>Attention ! Ne marche qu'une fois ! ");
		} else if (action.equals("Creation employe temps partiel32")) {
			EmployeTempsPartiel32 emp = new EmployeTempsPartiel32();
			emp.setId(2);
			emp.setNom("Raoul TableUnique-temps partiel");
			emp.setDateDebut(new Date());
			emp.setTauxHeure(50);
			emp.setDureeVacances(45);
			service32.creeEmploye(emp);
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
			out.println("<br/>Attention ! Ne marche qu'une fois ! ");
		}
		
		else if (action.equals("Creation employe plein temps31")) {
			EmployePleinTemps31 emp = new EmployePleinTemps31();
			emp.setId(1);
			emp.setNom("Raoul Jointure-plein temps");
			emp.setDateDebut(new Date());
			emp.setSalaire(4000);
			emp.setRetraite(0);
			emp.setDureeVacances(45);
			service31.creeEmploye(emp);
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
			out.println("<br/>Attention ! Ne marche qu'une fois ! ");
		} else if (action.equals("Creation employe temps partiel31")) {
			EmployeTempsPartiel31 emp = new EmployeTempsPartiel31();
			emp.setId(2);
			emp.setNom("Raoul Jointure-temps partiel");
			emp.setDateDebut(new Date());
			emp.setTauxHeure(50);
			emp.setDureeVacances(45);
			service31.creeEmploye(emp);
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
			out.println("<br/>Attention ! Ne marche qu'une fois ! ");
		}
		
		else if (action.equals("Creation employe plein temps33")) {
			EmployePleinTemps33 emp = new EmployePleinTemps33();
			emp.setId(1);
			emp.setNom("Raoul TableParClasseConcrete-plein temps");
			emp.setDateDebut(new Date());
			emp.setSalaire(4000);
			emp.setRetraite(450);
			emp.setDureeVacances(45);
			service33.creeEmploye(emp);
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
			out.println("<br/>Attention ! Ne marche qu'une fois ! ");
		} else if (action.equals("Creation employe temps partiel33")) {
			EmployeTempsPartiel33 emp = new EmployeTempsPartiel33();
			emp.setId(2);
			emp.setNom("Raoul TableParClasseConcrete-temps partiel");
			emp.setDateDebut(new Date());
			emp.setTauxHeure(50);
			emp.setDureeVacances(45);
			service33.creeEmploye(emp);
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
			out.println("<br/>Attention ! Ne marche qu'une fois ! ");
		}
		util.finPage(out);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

package presentation.detachees.et.cascade;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import services.detachees.lazy1.ServiceEmploye28;


public class ServletTxNonDetaches extends HttpServlet {
	private static final long serialVersionUID = 1L;
		@Resource UserTransaction tx;
	    @EJB(beanName="SEBParcourtTX") ServiceEmploye28 service28;

    public ServletTxNonDetaches() {
        super();
      

    }

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  service28.init();
		 try {
	            tx.begin();
	            List emps = service28.findAll();
	            request.setAttribute("employes", emps);
	            getServletContext().getRequestDispatcher("/WEB-INF/listeEmployes.jsp")
	                               .forward(request, response);
	        } catch (Exception e) {
	            throw new ServletException(e);
	        } finally {
	            try {
	                tx.commit();
	            } catch (Exception e) {
	                throw new ServletException(e);
	            }
	        }

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

}

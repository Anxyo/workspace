package presentation.detachees.et.cascade;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import entites.cascade.persist.Employe26;
import entites.cascade.remove.Employe27;
import entites.detachees.merge.Employe30;
import services.cascade.persist.ServiceEmploye26;
import services.cascade.remove.ServiceEmploye27;
import services.detachees.lazy2.ServiceEmploye29;
import services.detachees.merge.ServiceEmploye30;
import util.Utilitaires;

public class ServletDetCasc extends HttpServlet {
	private static final long serialVersionUID = 1L;
	// injection des EJBs
	@EJB(beanName = "SEBCascPersiste")
	ServiceEmploye26 service26;
	
	@EJB(beanName = "SEBCascRemove")
	ServiceEmploye27 service27;
	
	@EJB(beanName = "SEBLazyParcourt")
	ServiceEmploye29 service29;
	
	@EJB(beanName = "SEBMerge")
	ServiceEmploye30 service30;

	public ServletDetCasc() {
		super();
	
	}
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		Utilitaires util = new Utilitaires();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		util.debutPage(out);
		// traite la requ�te
		String action = request.getParameter("action");
		if (action == null) {
			// ne fait rien...
		} 
		
		else if (action.equals("Test 26")) {
			Employe26 emp = service26.creeEmployeAdresse(2, "Dupont Paul", 2501, "24 Grande Rue","Nice","Alpes maritimes", "06150");
			out.println("Employ&eacute cr&eacute;&eacute; " + emp);
			out.println("<br/>Attention ! Ne marche qu'une fois ! ");
		}
		
		else if (action.equals("Test 27")) {
			service27.init();
			out.println("Avant la suppression de l'employ&eacute; d'id 1, il y a " + service27.findAllTelephones().size() + " t&eacute;l&eacute;phones..");
			Employe27 emp = service27.findEmploye(1);
			if (emp == null) {
				out.println("<br/>Employ&eacute; d'id 1 non trouv&eacute; ! ");
			}
			else {
				service27.supprEmploye(1);
			}
			out.println("Apr&egrave; la suppression de l'employ&eacute; d'id 1, il y a " + service27.findAllTelephones().size() + " t&eacute;l&eacute;phones..");
			out.println("<br/>Attention ! Ne marche qu'une fois (ou rechargez le servlet) ! ");
		}
		
		else if (action.equals("Test 29")) {
			service29.init();
			List emps = service29.findAll();
	        request.setAttribute("employes", emps);
	        getServletContext().getRequestDispatcher("/WEB-INF/listeEmployes.jsp")
	                           .forward(request, response);

		}
		
		else if (action.equals("Test 30")) {
                Employe30 emp = new Employe30();
                emp.setId(1);
                emp.setNom("Nicephor Niepxe");
                service30.creeEmploye(emp);
                Collection col = service30.findAllEmployes();
                for (Object o : col)
                    out.print(o + "<br/>");
                Employe30 emp1 = new Employe30();
                emp1.setId(1);
                emp1.setNom("Nicephor Niepce");
                service30.majEmploye(emp1);
                Collection col1 = service30.findAllEmployes();
                for (Object o : col1)
                    out.print(o + "<br/>");
                
                Employe30 emp2 = new Employe30();
                emp2.setId(1);
                emp2.setNom("Fr�res Lumi�re");
                service30.majEmployeIncorrecte(emp2);
                Collection col2 = service30.findAllEmployes();
                for (Object o : col2)
                    out.print(o + "<br/>");
                out.println("<br/>Attention ! Ne marche qu'une fois ! ");
		} 
		util.finPage(out);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

package entites.utilisateur;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="UTILISATEUR", schema="AVANCE")
public class Utilisateur {
    @Id
    private String nom;
    private String motPasse;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMotPasse() {
        return motPasse;
    }
    
    public void setPassword(String name) {
        this.motPasse = name;
    }
    
    public String toString() {
        return "Utilisateur : " + getNom() +
               " mot de passe : " + getMotPasse();
    }
}

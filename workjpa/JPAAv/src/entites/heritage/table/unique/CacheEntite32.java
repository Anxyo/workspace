package entites.heritage.table.unique;

public abstract class CacheEntite32 {
    private long heureCreation;

    public CacheEntite32() {
        heureCreation = System.currentTimeMillis();
    }

    public long getAgeCache() {
        return System.currentTimeMillis() - heureCreation;
    }
}

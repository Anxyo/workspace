package entites.heritage.table.unique;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name="EmpTP")
@DiscriminatorValue("EmpTP")
public class EmployeTempsPartiel32 extends EmployeStd32 {
    @Column(name="TX_HEURE")
    private float tauxHeure;

    public float getTauxHeure() {
        return tauxHeure;
    }

    public void setTauxHeure(float tx) {
        this.tauxHeure = tx;
    }

    public String toString() {
        return "Id employ� temps partiel : " + getId() + " nom : " + getNom();
    }
}

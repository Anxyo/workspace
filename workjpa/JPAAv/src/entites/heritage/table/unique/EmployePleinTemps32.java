package entites.heritage.table.unique;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("EmpPT")
public class EmployePleinTemps32 extends EmployeStd32 {
    private long salaire;
    private long retraite;
    
    public long getRetraite() {
        return retraite;
    }
    
    public void setRetraite(long r) {
        this.retraite = r;
    }
    
    public long getSalaire() {
        return salaire;
    }
    
    public void setSalaire(long s) {
        this.salaire = s;
    }

    public String toString() {
        return "Id employ� plein temps : " + getId() + " nom : " + getNom();
    }
}

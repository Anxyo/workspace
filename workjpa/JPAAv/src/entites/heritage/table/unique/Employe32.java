package entites.heritage.table.unique;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity 
@Table(name="EMP32")
@Inheritance
@DiscriminatorColumn(name="TYPE_EMP")
public abstract class Employe32 extends CacheEntite32 {
    @Id private int id;
    private String nom;
    @Temporal(TemporalType.DATE)
    @Column(name="DATE_DEB")
    private Date dateDeb;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDateDebut() {
        return dateDeb;
    }

    public void setDateDebut(Date d) {
        this.dateDeb = d;
    }

    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom();
    }
}

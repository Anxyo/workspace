package entites.heritage.table.unique;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Contractant32 extends Employe32 {
    @Column(name="TX_JOUR")
    private int tauxJour;
    private int duree;
    
    public int getTauxJour() {
        return tauxJour;
    }
    
    public void setTauxJour(int tx) {
        this.tauxJour = tx;
    }
    
    public int getDuree() {
        return duree;
    }
    
    public void setDuree(int d) {
        this.duree = d;
    }

    public String toString() {
        return "Id contractant : " + getId() + ", nom : " + getNom();
    }
}

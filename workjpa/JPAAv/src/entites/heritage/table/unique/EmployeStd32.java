package entites.heritage.table.unique;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class EmployeStd32 extends Employe32 {
    private int dureeVacances;

    public int getDureeVacances() {
        return dureeVacances;
    }

    public void setDureeVacances(int v) {
        this.dureeVacances = v;
    }
}

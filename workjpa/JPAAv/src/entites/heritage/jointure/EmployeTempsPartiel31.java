package entites.heritage.jointure;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="EMP_TP31")
@DiscriminatorValue("3")
public class EmployeTempsPartiel31 extends EmployeStd31 {
    @Column(name="TX_HEURE")
    private float tauxHeure;

    public float getTauxHeure() {
        return tauxHeure;
    }

    public void setTauxHeure(float tx) {
        this.tauxHeure = tx;
    }

    public String toString() {
        return "Id d'employ� � temps partiel : " + getId() + ", nom : " + getNom();
    }
}

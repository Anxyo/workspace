package entites.heritage.jointure;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class EmployeStd31 extends Employe31 {
    private int dureeVacances;

    public int getDureeVacances() {
        return dureeVacances;
    }

    public void setDureeVacances(int vacation) {
        this.dureeVacances = vacation;
    }
}

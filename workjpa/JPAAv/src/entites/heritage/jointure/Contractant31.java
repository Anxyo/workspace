package entites.heritage.jointure;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="EMP_CONTRACT31")
@DiscriminatorValue("1")
public class Contractant31 extends Employe31 {
    @Column(name="TX_JOUR")
    private int tauxJour;
    private int duree;
    
    public int getTauxJour() {
        return tauxJour;
    }
    
    public void setTauxJour(int txJour) {
        this.tauxJour = txJour;
    }
    
    public int getDuree() {
        return duree;
    }
    
    public void setDuree(int d) {
        this.duree = d;
    }

    public String toString() {
        return "Id contractant : " + getId() + ", nom : " + getNom();
    }
}

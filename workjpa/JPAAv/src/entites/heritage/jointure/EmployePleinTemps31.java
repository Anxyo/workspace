package entites.heritage.jointure;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="EMP_PT31")
@DiscriminatorValue("2")
public class EmployePleinTemps31 extends EmployeStd31 {
    private long salaire;
    private long retraite;
    
    public long getRetraite() {
        return retraite;
    }
    
    public void setRetraite(long r) {
        this.retraite = r;
    }
    
    public long getSalaire() {
        return salaire;
    }
    
    public void setSalaire(long s) {
        this.salaire = s;
    }

    public String toString() {
        return "Id d'employ� � plein temps : " + getId() + " nom : " + getNom();
    }
}

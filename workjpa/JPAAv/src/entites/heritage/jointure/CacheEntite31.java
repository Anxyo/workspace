package entites.heritage.jointure;

public abstract class CacheEntite31 {
    private long heureCreation;

    public CacheEntite31() {
        heureCreation = System.currentTimeMillis();
    }

    public long getAgeCache() {
        return System.currentTimeMillis() - heureCreation;
    }
}

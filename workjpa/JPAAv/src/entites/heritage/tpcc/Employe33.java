package entites.heritage.tpcc;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Employe33 extends CacheEntite33 {
    @Id private int id;
    private String nom;
    @Temporal(TemporalType.DATE)
    @Column(name="DATE_DEB")
    private Date dateDebut;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date d) {
        this.dateDebut = d;
    }

    public String toString() {
        return "Id employ� : " + getId() + ", nom : " + getNom();
    }
}

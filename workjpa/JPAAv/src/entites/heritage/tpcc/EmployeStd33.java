package entites.heritage.tpcc;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class EmployeStd33 extends Employe33 {
    private int dureeVacances;
    @ManyToOne
    private Employe33 leChef;

    public int getDureeVacances() {
        return dureeVacances;
    }

    public void setDureeVacances(int v) {
        this.dureeVacances = v;
    }

    public Employe33 getLeChef() {
        return leChef;
    }

    public void setLeChef(Employe33 chef) {
        this.leChef = chef;
    }
}

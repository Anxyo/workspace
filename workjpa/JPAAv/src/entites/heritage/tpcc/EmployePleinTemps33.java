package entites.heritage.tpcc;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity @Table(name="EMP_PT33")
public class EmployePleinTemps33 extends EmployeStd33 {
    private long salaire;
    @Column(name="RETRAITE")
    private long retraite;
    
    public long getRetraite() {
        return retraite;
    }
    
    public void setRetraite(long cotis) {
        this.retraite = cotis;
    }
    
    public long getSalaire() {
        return salaire;
    }
    
    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }

    public String toString() {
        return "Id employ� temps plein : " + getId() + ", nom : " + getNom();
    }
}

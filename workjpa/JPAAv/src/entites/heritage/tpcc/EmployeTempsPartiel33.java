package entites.heritage.tpcc;

import javax.persistence.AssociationOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="EMP_TP33")
@AssociationOverride(name="leChef", 
                     joinColumns=@JoinColumn(name="LE_CHEF"))
public class EmployeTempsPartiel33 extends EmployeStd33 {
    @Column(name="TX_HEURE")
    private float tauxHeure;

    public float getTauxHeure() {
        return tauxHeure;
    }

    public void setTauxHeure(float tx) {
        this.tauxHeure = tx;
    }

    public String toString() {
        return "Id employ� temps partiel : " + getId() + ", nom : " + getNom();
    }
}

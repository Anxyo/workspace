package entites.heritage.tpcc;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="CONTRACTANT33")
@AttributeOverrides({
    @AttributeOverride(name="nom", column=@Column(name="NOM_FAMILLE")),
    @AttributeOverride(name="dateDeb", column=@Column(name="DATE_DEBUT_CTR"))
})
public class Contractant33 extends Employe33 {
    @Column(name="TX_JOUR")
    private int tauxJour;
    private int duree;

    
    public int getTauxJour() {
        return tauxJour;
    }
    
    public void setTauxJour(int tx) {
        this.tauxJour = tx;
    }
    
    public int getDuree() {
        return duree;
    }
    
    public void setDuree(int d) {
        this.duree = d;
    }

    public String toString() {
        return "Id contractant : " + getId() + " nom : " + getNom();
    }
}

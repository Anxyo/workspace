package entites.heritage.tpcc;

public abstract class CacheEntite33 {
    private long heureCreation;

    public CacheEntite33() {
    	heureCreation = System.currentTimeMillis();
    }

    public long getAgeCache() {
        return System.currentTimeMillis() - heureCreation;
    }
}

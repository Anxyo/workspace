package entites.objets.imbriques;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="AVANCE")
public class Employe40 {
    @Id private int id;
    private String nom;
    private long salaire;
    @Embedded private Adresse40 adresse;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String n) {
        this.nom = n;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }

    public Adresse40 getAdresse() {
        return adresse;
    }
    
    public void setAdresse(Adresse40 adresse) {
        this.adresse = adresse; 
    }

    public String toString() {
        return "Id employ� : " + getId() + ", nom : " + getNom() +
               ", adresse : " + getAdresse();
    }
}

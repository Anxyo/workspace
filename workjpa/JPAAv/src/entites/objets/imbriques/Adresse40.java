package entites.objets.imbriques;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Adresse40 {
    private String rue; 
    private String ville; 
    private String departement;
    @Column(name="CODE_POSTAL")
    private String codePostal;

    public String getRue() {
        return rue;
    }
    
    public void setRue(String r) {
        this.rue = r;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String v) {
        this.ville = v;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String d) {
        this.departement = d;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String cp) {
        this.codePostal = cp;
    }
    public String toString() {
        return "rue : " + getRue() +
               ", ville : " + getVille() +
               ", département : " + getDepartement() +
               ", code postal : " + getCodePostal();
    }

}

package entites.cascade.persist;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Employe26 {
    @Id
    private int id;
    private String nom;
    
    @ManyToOne(cascade=CascadeType.PERSIST)
    Adresse26 adresse;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse26 getAdresse() {
        return adresse;
    }
    
    public void setAddress(Adresse26 adresse) {
        this.adresse = adresse; 
    }

    public String toString() {
        return "Id employe : " + getId() + " nom : " + getNom() +
               " adresse : " + getAdresse();
    }
}

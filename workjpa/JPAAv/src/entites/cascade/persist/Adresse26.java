package entites.cascade.persist;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Adresse26 {
    @Id
    private int id;
    private String rue;
    private String ville;
    private String departement;
    private String codePostal;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getRue() {
        return rue;
    }
    
    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String dep) {
        this.departement = dep;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String cp) {
        this.codePostal = cp;
    }
    public String toString() {
        return "Id adresse : " + getId() + 
               ", rue : " + getRue() +
               ", ville : " + getVille() +
               ", dept : " + getDepartement() +
               ", code postal : " + getCodePostal();
    }

}

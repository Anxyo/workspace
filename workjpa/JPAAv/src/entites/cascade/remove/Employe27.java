package entites.cascade.remove;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Employe27 {
    @Id
    private int id;
    private String nom;
    
    @OneToOne(cascade={CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name="ID_BUREAU") 
    Bureau27 bureau;

    @OneToMany(cascade={CascadeType.PERSIST, CascadeType.REMOVE},
               mappedBy="employe")
    Collection<Telephone27> telephones;

    public Employe27() {
        telephones = new ArrayList<Telephone27>();
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public Bureau27 getBureau() {
        return bureau;
    }
    
    public void setBureau(Bureau27 b) {
        this.bureau = b;
    }
    
    public Collection getTelephones() {
    	return telephones;
    }

    public String toString() {
        return "Id employ� : " + getId() + " nom : " + getNom() + 
               ", bureau : " + getBureau();
    }
}

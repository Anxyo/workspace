package entites.cascade.remove;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Entity
public class Bureau27 {
    @Id
    private int id;
    private int batiment;
    private String emplacement;
    @OneToOne(mappedBy="bureau")
    private Employe27 employe;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public int getBatiment() {
        return batiment;
    }

    public void setBatiment(int b) {
        this.batiment = b;
    }
    
    public String getEmplacement() {
        return emplacement;
    }
    
    public void setEmplacement(String e) {
        this.emplacement = e;
    }

    public Employe27 getEmploye() {
        return employe;
    }

    public void setEmploye(Employe27 employe) {
        this.employe = employe;
    }
    
    public String toString() {
        return "Id bureau : " + getId() + " batiment : " + getBatiment() +
               ", emplacement : " + getEmplacement();
    }
}

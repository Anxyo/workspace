package entites.cascade.remove;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Telephone27 {
    @Id
    private long id;
    private String numero;
    private String type;
    @OneToOne 
    Employe27 employe;
    
    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    public String getNumero() {
        return numero;
    }
    
    public void setNumero(String no) {
        this.numero = no;
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public Employe27 getEmploye() {
        return employe;
    }

    public void setEmploye(Employe27 employe) {
        this.employe = employe;
    }

    public String toString() {
        return "Id telephone : " + getId() + 
               ", no : " + getNumero() +
               ", type : " + getType();
    }
}

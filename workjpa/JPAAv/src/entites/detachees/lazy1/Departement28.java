package entites.detachees.lazy1;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Departement28 {
    @Id
    private int id;
    private String nom;
    @OneToMany(mappedBy="departement")
    private Collection<Employe28> employes;

    public Departement28() {
        employes = new ArrayList<Employe28>();
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public Collection<Employe28> getEmployes() {
        return employes;
    }

    public String toString() {
        return "Departement no : " + getId() + 
               ", nom : " + getNom();
    }
}

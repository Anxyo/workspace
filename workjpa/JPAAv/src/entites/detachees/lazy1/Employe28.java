package entites.detachees.lazy1;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Employe28 {
    @Id
    private int id;
    private String nom;
    private long salaire;
    
    @ManyToOne
    private Departement28 departement;
    

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String n) {
        nom = n;
    }
    public long getSalaire() {
        return salaire;
    }

    public Departement28 getDepartement() {
        return departement;
    }

    public void setDepartement(Departement28 dept) {
        this.departement = dept;
    }
    
    public String toString() {
        return "Employe : " + getId() + 
               ", nom : " + getNom() +
               ", salaire: " + getSalaire();
    }
}

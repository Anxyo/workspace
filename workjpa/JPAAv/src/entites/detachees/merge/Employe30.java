package entites.detachees.merge;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Employe30 {
    @Id
    private int id;
    private String nom;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date dernierAcces;
    

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDernierAcces() {
        return dernierAcces;
    }
    
    public void setDernierAcces(Date da) {
        this.dernierAcces = da;
    }
    
    public String toString() {
        return "Employe : " + getId() + 
               ", nom : " + getNom() +
               ", dernier acc�s : " + getDernierAcces();
    }
}

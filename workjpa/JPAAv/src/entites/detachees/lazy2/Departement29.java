package entites.detachees.lazy2;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Departement29 {
    @Id
    private int id;
    private String nom;
    @OneToMany(mappedBy="departement")
    private Collection<Employe29> employes;

    public Departement29() {
        employes = new ArrayList<Employe29>();
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    public Collection<Employe29> getEmployes() {
        return employes;
    }

    public String toString() {
        return "Departement no : " + getId() + 
               ", nom : " + getNom();
    }
}

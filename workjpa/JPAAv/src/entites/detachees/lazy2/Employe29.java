package entites.detachees.lazy2;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Employe29 {
    @Id
    private int id;
    private String nom;
    private long salaire;
    
    @ManyToOne(fetch=FetchType.LAZY)
    private Departement29 departement;
    

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String n) {
        nom = n;
    }

    public long getSalaire() {
        return salaire;
    }

    public Departement29 getDepartement() {
        return departement;
    }

    public void setDepartement(Departement29 dept) {
        this.departement = dept;
    }
    
    public String toString() {
        return "Employe " + getId() + 
               ", nom : " + getNom() +
               ", salaire : " + getSalaire();
    }
}

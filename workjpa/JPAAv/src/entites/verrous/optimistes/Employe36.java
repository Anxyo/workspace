package entites.verrous.optimistes;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity 
public class Employe36 {
    @Id private int id;
    
    @Version
    private int version;
    
    private String nom;
    private long salaire;
    private int nbJoursVacances;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }
    
    public void setVersion(int version) {
        this.version = version;
    }

    public String getNom() {
        return nom;
    }
    
    public void setNom(String n) {
        this.nom = n;
    }
    
    public long getSalaire() {
        return salaire;
    }
    
    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public int getNbJoursDeVacances() {
        return nbJoursVacances;
    }

    public void setNbJoursDeVacances(int nb) {
        this.nbJoursVacances = nb;
    }

    public String toString() {
        return "Id employ� : " + getId() + ", nom : " + getNom() +
            ", nb de jours de vacances : " + getNbJoursDeVacances();
    }
}

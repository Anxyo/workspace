package entites.verrous.optimistes;

import javax.ejb.ApplicationException;

@ApplicationException
public class ChangementConcurentException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	public ChangementConcurentException() { super(); }
}


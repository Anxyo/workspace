package entites.verrous.lecture;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="EMP35")
public class Employe35 {
    @Id
    private int id;
    @Version private int version;
    private String nom;
    private long salaire;
    
    @ManyToOne
    private Departement35 departement;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getNom() {
        return nom;
    }
    
    public void setNom(String n) {
        this.nom = n;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public Departement35 getDepartement() {
        return departement;
    }
    
    public void setDepartement(Departement35 d) {
        this.departement = d;
    }

    public String toString() {
        return "Id employ� : " + getId() + ", nom : " + getNom() + 
               ", membre de  " + getDepartement();
    }
}

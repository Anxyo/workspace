package entites.verrous.lecture;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Departement35 {
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    private String nom;
    @OneToMany(mappedBy="departement")
    private Collection<Employe35> employes;

    public Departement35() {
        employes = new ArrayList<Employe35>();
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String n) {
        this.nom = n;
    }
    
    public void ajouteEmploye(Employe35 emp) {
        if (!getEmployes().contains(emp)) {
            getEmployes().add(emp);
            if (emp.getDepartement() != null) {
                emp.getDepartement().getEmployes().remove(emp);
            }
            emp.setDepartement(this);
        }
    }
    
    public void supprEmploye(Employe35 employe) {
        getEmployes().remove(employe);
    }
    
    public Collection<Employe35> getEmployes() {
        return employes;
    }

    public String toString() {
        return "Id département : " + getId() + 
               ", nom : " + getNom();
    }
}

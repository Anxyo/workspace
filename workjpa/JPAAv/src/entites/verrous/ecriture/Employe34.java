package entites.verrous.ecriture;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.persistence.OneToMany;

@Entity
public class Employe34 {
    @Id
    private int id;
    @Version 
    private int version;
    private String nom;
    private float cout;
    @OneToMany(mappedBy="employe")
    private Collection<Badge34> badges;

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public float getCout() {
        return cout;
    }

    public void setCout(float salaire) {
        this.cout = salaire;
    }
    
    public Collection<Badge34> getBadges() {
        return badges;
    }
    
    public void addUniform(Badge34 b) {
        getBadges().add(b);
    }
    
    public void setBadges(Collection<Badge34> b) {
        this.badges = b;
    }

    public String toString() {
        return "Id employ� : " + getId() + ", nom: " + getNom();
    }
}

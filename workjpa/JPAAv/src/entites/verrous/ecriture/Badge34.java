package entites.verrous.ecriture;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Badge34 {
    @Id
    private int id;
    private String couleur;
    @ManyToOne
    private Employe34 employe;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getCouleur() {
        return couleur;
    }
    
    public void setCouleur(String c) {
        this.couleur = c;
    }
    
    public Employe34 getEmploye() {
        return employe;
    }
    
    public void setEmploye(Employe34 e) {
        this.employe = e;
    }
}

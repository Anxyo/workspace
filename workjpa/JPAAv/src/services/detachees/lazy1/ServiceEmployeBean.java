package services.detachees.lazy1;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entites.detachees.lazy1.Departement28;
import entites.detachees.lazy1.Employe28;

@Stateless(name = "SEBParcourtTX")
public class ServiceEmployeBean implements ServiceEmploye28 {
	@PersistenceContext(unitName = "ServiceEmploye")
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public List findAll() {
		return em.createQuery("SELECT e FROM Employe28 e").getResultList();
	}

	// ajoute quelques donn�es
	public void init() {
		Employe28 emp = em.find(Employe28.class, 1);
		if (emp == null) {		// une fois seulement
			Employe28 emp1 = new Employe28();
			Employe28 emp2 = new Employe28();
			Employe28 emp3 = new Employe28();
			emp1.setId(1);
			emp1.setNom("Max");
			emp2.setNom("Robert");
			emp2.setId(2);
			emp3.setId(3);
			emp3.setNom("Nicole");
			Departement28 d1 = new Departement28();
			Departement28 d2 = new Departement28();
			d1.setId(1);
			d1.setNom("Informatique");
			d2.setId(2);
			d2.setNom("Ressources humaines");
			emp1.setDepartement(d1);
			emp2.setDepartement(d1);
			emp3.setDepartement(d2);
			em.persist(emp1);
			em.persist(emp2);
			em.persist(emp3);
			em.persist(d1);
			em.persist(d2);
		}
	}
}

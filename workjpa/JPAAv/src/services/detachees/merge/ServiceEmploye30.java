package services.detachees.merge;

import java.util.Collection;
import entites.detachees.merge.Employe30;


public interface ServiceEmploye30 {
    public void majEmploye(Employe30 emp);
    public void majEmployeIncorrecte(Employe30 emp);
    public Collection<Employe30> findAllEmployes();
    public void creeEmploye(Employe30 emp);
}

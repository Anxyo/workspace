package services.detachees.merge;

import java.util.Collection;
import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import entites.detachees.merge.Employe30;


@Stateless(name="SEBMerge")
public class ServiceEmployeBean implements ServiceEmploye30 {
    @PersistenceContext(unitName="ServiceEmploye")
    EntityManager em;



	@SuppressWarnings("unchecked")
	public Collection<Employe30> findAllEmployes() {
        return (Collection<Employe30>) em.createQuery(
        "SELECT e FROM Employe30 e").getResultList();
	}


	public void majEmploye(Employe30 emp) {
		 if (em.find(Employe30.class, emp.getId()) == null) {
	            throw new IllegalArgumentException("Id d'employ� inconnu : " + emp.getId());
	        }
	        Employe30 empGere = em.merge(emp);
	        empGere.setDernierAcces(new Date());
	}


	public void majEmployeIncorrecte(Employe30 emp) {
        if (em.find(Employe30.class, emp.getId()) == null) {
            throw new IllegalArgumentException("Id d'employ� inconnu : " + emp.getId());
        }
        em.merge(emp);
        emp.setDernierAcces(new Date());  // pb !
	}


	@Override
	public void creeEmploye(Employe30 emp) {
		em.persist(emp);
	}
}

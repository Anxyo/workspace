package services.detachees.lazy2;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entites.detachees.lazy1.Departement28;
import entites.detachees.lazy1.Employe28;
import entites.detachees.lazy2.Departement29;
import entites.detachees.lazy2.Employe29;


@Stateless(name="SEBLazyParcourt")
public class EmployeeServiceBean implements ServiceEmploye29 {
    @PersistenceContext(unitName="ServiceEmploye")
    private EntityManager em;
    
    @SuppressWarnings("unchecked")
	public List findAll() {
        List<Employe29> emps = em.createQuery("SELECT e FROM Employe29 e")
                      			.getResultList();
        for (Employe29 emp : emps) {
            if (emp.getDepartement() != null) {
                emp.getDepartement().getNom();
            }
        }
        return emps;
    }


	public void init() {
		Employe29 emp = em.find(Employe29.class, 1);
		if (emp == null) {		// une fois seulement
			Employe29 emp1 = new Employe29();
			Employe29 emp2 = new Employe29();
			Employe29 emp3 = new Employe29();
			emp1.setId(1);
			emp1.setNom("Max");
			emp2.setNom("Robert");
			emp2.setId(2);
			emp3.setId(3);
			emp3.setNom("Nicole");
			Departement29 d1 = new Departement29();
			Departement29 d2 = new Departement29();
			d1.setId(1);
			d1.setNom("Informatique");
			d2.setId(2);
			d2.setNom("Ressources humaines");
			emp1.setDepartement(d1);
			emp2.setDepartement(d1);
			emp3.setDepartement(d2);
			em.persist(emp1);
			em.persist(emp2);
			em.persist(emp3);
			em.persist(d1);
			em.persist(d2);
		}
		
	}
}

package services.verrous.lecture;

import java.util.Collection;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entites.verrous.lecture.Departement35;
import entites.verrous.lecture.Employe35;
import entites.verrous.lecture.FeuilleDePaie;


@Stateless
public class ServiceEmployeBean implements ServiceEmploye35 {
    @PersistenceContext(unitName="ServiceEmploye")
    private EntityManager em;

    public FeuilleDePaie genereFeuilleDePaie(List<Integer> idsDept) {
        FeuilleDePaie etat = new FeuilleDePaie();
        long total = 0;
        for (Integer idDept : idsDept) {
            long totalDept = totalSalairesDep(idDept);
            etat.ajouteLigne(idDept, totalDept);
            total += totalDept;
        }
        etat.ajouteLigneTotal(total);
        return etat;
    }

    protected long totalSalairesDep(int idDept) {
        long total = 0;
        Departement35 dept = em.find(Departement35.class, idDept);
        for (Employe35 emp : dept.getEmployes()) {
            em.lock(emp, LockModeType.READ);
            total += emp.getSalaire();
        }
        return total;
    }

    public void changeEmployeDeDepartment(int idDept, int idEmp) {
        Employe35 emp = em.find(Employe35.class, idEmp);
        emp.getDepartement().supprEmploye(emp);
        Departement35 dept = em.find(Departement35.class, idDept);
        dept.ajouteEmploye(emp);
        emp.setDepartement(dept);
    }

    @SuppressWarnings("unchecked")
	public Collection<Employe35> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe35 e");
        return (Collection<Employe35>) query.getResultList();
    }
}

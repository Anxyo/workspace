package services.verrous.lecture;

import java.util.Collection;
import java.util.List;

import entites.verrous.lecture.Employe35;
import entites.verrous.lecture.FeuilleDePaie;

public interface ServiceEmploye35 {
    public FeuilleDePaie genereFeuilleDePaie(List<Integer> idsDept);
    public void changeEmployeDeDepartment(int idDept, int idEmp);
    public Collection<Employe35> findAllEmployes();
}

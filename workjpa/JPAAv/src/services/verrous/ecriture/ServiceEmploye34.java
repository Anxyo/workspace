package services.verrous.ecriture;

import entites.verrous.ecriture.Badge34;

public interface ServiceEmploye34 {
    public Badge34 getBadge(int id);
    public void donneBadge(int id, Badge34 leBadge);
}

package services.verrous.ecriture;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import entites.verrous.ecriture.Badge34;
import entites.verrous.ecriture.Employe34;

@Stateful(name="SEBVerrEcr")
public class ServiceEmployeBean implements ServiceEmploye34 {
    @PersistenceContext(unitName="ServiceEmploye",
                        type=PersistenceContextType.EXTENDED)
    private EntityManager em;


	public void donneBadge(int id, Badge34 leBadge) {
		 Employe34 emp = em.find(Employe34.class, id);
	        em.lock(emp, LockModeType.WRITE); // mettre en commentaires pour voire l'�crasement de donn�es
	        emp.addUniform(leBadge);
	        leBadge.setEmploye(emp);
	}

	public Badge34 getBadge(int id) {
		  return em.find(Badge34.class, id);
	}
}

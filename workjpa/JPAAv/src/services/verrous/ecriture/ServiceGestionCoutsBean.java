package services.verrous.ecriture;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import entites.verrous.ecriture.Employe34;

@Stateful
public class ServiceGestionCoutsBean implements ServiceGestionCouts34 {
    static final Float COUT_BADGE = 4.7f;

    @PersistenceContext(unitName="ServiceEmploye",
                        type=PersistenceContextType.EXTENDED)
    protected EntityManager em;

    public void calculeCoutEchange(int id) {
        Employe34 emp = em.find(Employe34.class, id);
        Float cout = emp.getBadges().size() * COUT_BADGE;
        emp.setCout(emp.getCout() + cout); // pas terrble comme calcul !
                                           // mais force une mise � jour pour voir le verrou
    }
}


package services.verrous.optimistes;

public interface ServicePersonnel36 {
    public void modifConges(int id, int nbJours);
    public void modifCongesVerrou(int id, int nbJours);
}

package services.verrous.optimistes;

import java.util.Collection;
import entites.verrous.optimistes.Employe36;


public interface ServiceEmploye36 {
    public Employe36 creeEmploye(int id, String nom, int vacances);
    public void supprJoursConges(int id, int nbJours);
    public void supprJoursCongesVerrou(int id, int nbJours);
    public void modifSalEmploye(int id, long salaire);
    public Collection<Employe36> findAllEmployes();
}

package services.verrous.optimistes;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import entites.verrous.optimistes.ChangementConcurentException;


@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class EmpServiceClientBean implements ServicePersonnel36 {
    @EJB protected ServiceEmploye36 servicePers;

    public void modifConges(int id, int nb) {
        try { 
            servicePers.supprJoursConges(id, nb);
        } catch (ChangementConcurentException ccx) {
            System.out.println("Collision avec une autre modification - R�essaye..."); 
            servicePers.supprJoursConges(id, nb);
        }
    }

	public void modifCongesVerrou(int id, int nbJours) {
		 try { 
	            servicePers.supprJoursCongesVerrou(id, nbJours);
	        } catch (ChangementConcurentException ccx) {
	            System.out.println("Collision avec une autre modification - R�essaye..."); 
	            servicePers.supprJoursCongesVerrou(id, nbJours);
	        }
	}

}
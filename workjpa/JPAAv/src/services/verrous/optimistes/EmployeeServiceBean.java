package services.verrous.optimistes;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entites.verrous.optimistes.ChangementConcurentException;
import entites.verrous.optimistes.Employe36;


@Stateless
public class EmployeeServiceBean implements ServiceEmploye36 {
    @PersistenceContext(unitName="ServiceEmploye")
    private EntityManager em;

    protected void execFlush() {
        try { 
            em.flush(); 
        } catch (OptimisticLockException exc) {
            throw new ChangementConcurentException();
        }
    } 
    

	public Employe36 creeEmploye(int id, String nom, int vacances) {
        Employe36 emp = new Employe36();
        emp.setId(id);
        emp.setNom(nom);
        emp.setNbJoursDeVacances(vacances);
        em.persist(emp);
        return emp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Employe36> findAllEmployes() {
		  Query query = em.createQuery("SELECT e FROM Employe36 e");
	        return (Collection<Employe36>) query.getResultList();	
	}


	public void modifSalEmploye(int id, long salaire) {
		 Employe36 emp = em.find(Employe36.class, id);
	        emp.setSalaire(salaire);
	        // ... 
	        execFlush();
	}


	public void supprJoursConges(int id, int nbJours) {
		   Employe36 emp = em.find(Employe36.class, id);
	       emp.setNbJoursDeVacances(emp.getNbJoursDeVacances() - nbJours);
	        // ... 
	        execFlush();
	}


	public void supprJoursCongesVerrou(int id, int nbJours) {
        Employe36 emp = em.find(Employe36.class, id);
        emp.setVersion(emp.getVersion()-1); // force verrou optimiste
        emp.setNbJoursDeVacances(emp.getNbJoursDeVacances() - nbJours);
        // ... 
        execFlush();
	}
}


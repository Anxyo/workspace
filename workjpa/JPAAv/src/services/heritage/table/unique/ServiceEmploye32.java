package services.heritage.table.unique;

import java.util.Collection;
import entites.heritage.table.unique.Employe32;


public interface ServiceEmploye32 {
    public void creeEmploye(Employe32 emp);
    public Collection<Employe32> findAllEmployes();
}

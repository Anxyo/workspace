package services.heritage.table.unique;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import entites.heritage.table.unique.Employe32;


@Stateless(name="ESBHerTableUnique")
public class EmployeeServiceBean implements ServiceEmploye32 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;



	public void creeEmploye(Employe32 emp) {
		 em.persist(emp);	
	}

	
	@SuppressWarnings("unchecked")
	public Collection<Employe32> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe32 e");
        return (Collection<Employe32>) query.getResultList();
	}
}

package services.heritage.jointure;

import java.util.Collection;
import entites.heritage.jointure.Employe31;

public interface ServiceEmploye31 {
    public void creeEmploye(Employe31 emp);
    public Collection<Employe31> findAllEmployes();
}

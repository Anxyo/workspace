package services.heritage.jointure;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entites.heritage.jointure.Employe31;

@Stateless(name="ESBHerJointure")
public class EmployeeServiceBean implements ServiceEmploye31 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public void creeEmploye(Employe31 emp) {
		  em.persist(emp);
	}

    @SuppressWarnings("unchecked")
	public Collection<Employe31> findAllEmployes() {
		  Query query = em.createQuery("SELECT e FROM Employe31 e");
	        return (Collection<Employe31>) query.getResultList();
	 }
}

package services.heritage.tpcc;

import java.util.Collection;
import entites.heritage.tpcc.Employe33;


public interface ServiceEmploye33 {
    public void creeEmploye(Employe33 emp);
    public Collection<Employe33> findAllEmployes();
}

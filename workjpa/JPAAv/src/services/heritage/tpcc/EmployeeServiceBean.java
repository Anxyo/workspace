package services.heritage.tpcc;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import entites.heritage.tpcc.Employe33;

@Stateless(name="ESBHerTPCC")
public class EmployeeServiceBean implements ServiceEmploye33 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;



@SuppressWarnings("unchecked")
public Collection<Employe33> findAllEmployes() {
     Query query = em.createQuery("SELECT e FROM Employe33 e");
     return (Collection<Employe33>) query.getResultList();
}

@Override
public void creeEmploye(Employe33 emp) {
  	em.persist(emp);
 }
}

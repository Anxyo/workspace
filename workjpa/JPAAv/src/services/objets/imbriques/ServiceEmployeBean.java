package services.objets.imbriques;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entites.objets.imbriques.Adresse40;
import entites.objets.imbriques.Employe40;


@Stateless(name="SEBObjImb")
public class ServiceEmployeBean implements ServiceEmploye40 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	@Override
	public Employe40 creeEmployeEtAdresse(int id, String nom, long salaire,
			String rue, String ville, String dept, String codePostal) {
	    Employe40 emp = new Employe40();
        emp.setId(id);
        emp.setNom(nom);
        emp.setSalaire(salaire);
        Adresse40 adr = new Adresse40();
        adr.setRue(rue);
        adr.setVille(ville);
        adr.setDepartement(dept);
        adr.setCodePostal(codePostal);
        emp.setAdresse(adr);
        em.persist(emp);
        return emp;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Employe40> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe40 e");
        return (Collection<Employe40>) query.getResultList();
	}
}

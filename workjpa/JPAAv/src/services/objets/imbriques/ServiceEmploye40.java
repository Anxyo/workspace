package services.objets.imbriques;

import java.util.Collection;

import entites.objets.imbriques.Employe40;


public interface ServiceEmploye40 {
    public Employe40 creeEmployeEtAdresse(int id, String nom, long salaire,
                      String rue, String ville, String dept, String codePostal);
    public Collection<Employe40> findAllEmployes();
}

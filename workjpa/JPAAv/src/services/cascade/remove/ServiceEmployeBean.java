package services.cascade.remove;

import java.util.Collection;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entites.cascade.remove.Bureau27;
import entites.cascade.remove.Employe27;
import entites.cascade.remove.Telephone27;

@Stateless(name = "SEBCascRemove")
public class ServiceEmployeBean implements ServiceEmploye27 {
	@PersistenceContext(unitName = "ServiceEmploye")
	EntityManager em;

	public void supprEmploye(int idEmp) {
		Employe27 emp = em.find(Employe27.class, idEmp);
		em.remove(emp);
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe27> findAllEmployes() {
		return (Collection<Employe27>) em.createQuery(
				"SELECT e FROM Employe27 e").getResultList();
	}

	@SuppressWarnings("unchecked")
	public Collection<Telephone27> findAllTelephones() {
		return (Collection<Telephone27>) em.createQuery(
				"SELECT t FROM Telephone27 t").getResultList();
	}

	@SuppressWarnings("unchecked")
	public void init() {
		Employe27 emp1;
		if ((emp1 = em.find(Employe27.class, 1)) == null) {
			emp1 = new Employe27();
			emp1.setId(1);
			emp1.setNom("Brugnard Georges");
			Bureau27 b = new Bureau27();
			b.setId(1);
			b.setBatiment(1);
			b.setEmplacement("pr�s du radiateur");
			
			b.setEmploye(emp1);
			emp1.setBureau(b);
			
			Telephone27 t = new Telephone27();
			t.setId(1);
			t.setNumero("06 55 66 77 88");
			t.setType("portable");
			
			t.setEmploye(emp1);
			emp1.getTelephones().add(t);
			em.persist(emp1);
		}
	}
		

	public Employe27 findEmploye(int idEmp) {
		return em.find(Employe27.class, idEmp);
	}
}

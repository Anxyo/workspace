package services.cascade.remove;

import java.util.Collection;

import entites.cascade.remove.Employe27;
import entites.cascade.remove.Telephone27;


public interface ServiceEmploye27 {
    public void supprEmploye(int idEmp);
    public Employe27 findEmploye(int idEMp);
    public Collection<Telephone27> findAllTelephones();
    public Collection<Employe27> findAllEmployes();
    public void init();
}

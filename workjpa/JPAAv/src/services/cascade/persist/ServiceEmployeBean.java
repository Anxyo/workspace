package services.cascade.persist;

import java.util.Collection;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import entites.cascade.persist.Adresse26;
import entites.cascade.persist.Employe26;


@Stateless(name="SEBCascPersiste")
public class ServiceEmployeBean implements ServiceEmploye26 {
    @PersistenceContext(unitName="ServiceEmploye")
    protected EntityManager em;

	public Employe26 creeEmployeAdresse(int id, String nom, int idAdr,
			String rue, String ville, String dep, String cp) {
	    Employe26 emp = new Employe26();
        emp.setId(id);
        emp.setNom(nom);
        Adresse26 adr = new Adresse26();
        adr.setId(idAdr);
        adr.setRue(rue);
        adr.setVille(ville);
        adr.setDepartement(dep);
        adr.setCodePostal(cp);
        emp.setAddress(adr);
        em.persist(emp);
        return emp;
	}

	@SuppressWarnings("unchecked")
	public Collection<Employe26> findAllEmployes() {
        Query query = em.createQuery("SELECT e FROM Employe26 e");
        return (Collection<Employe26>) query.getResultList();
	}
}

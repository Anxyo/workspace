package services.cascade.persist;

import java.util.Collection;
import entites.cascade.persist.Employe26;



public interface ServiceEmploye26 {
    public Employe26 creeEmployeAdresse(int id, String nom,
                      int idAdr, String rue, String ville, String dep, String cp);
    public Collection<Employe26> findAllEmployes();
}

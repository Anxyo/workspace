package login;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import util.Utilitaires;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entites.utilisateur.Utilisateur;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	 @PersistenceUnit(unitName="ServiceEmploye")
     EntityManagerFactory emf;

    public LoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
	        PrintWriter out = response.getWriter();
	        Utilitaires util = new Utilitaires();
	        util.debutPage(out);
	        
	        String idUtil = request.getParameter("utilisateur");

	        // OK ?
	        EntityManager em = emf.createEntityManager();
	        try {
	            if (idUtil != null) {
	                Utilisateur utilisateur = em.find(Utilisateur.class, idUtil);
	                if (utilisateur == null) {
	                    // Non trouv� !
	                    out.println("Utilisateur avec l'id : " + idUtil + " non trouv� !<br> ");
	                } else {
	                    out.println("OK, utilisateur trouv&eacute; : " + utilisateur + "</br>");
	                }
	            }
	            out.println("Uilisateurs :<br> ");
	            listeCollection(em.createQuery("SELECT u FROM Utilisateur u").getResultList(), out);
	        } finally {
	            em.close();
	        }
	        util.finPage(out);
	}
    
	@SuppressWarnings("unchecked")
	private void listeCollection(Collection c, PrintWriter out) {
        for (Object o : c) {
            out.print(o + "<br/>");
        }
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

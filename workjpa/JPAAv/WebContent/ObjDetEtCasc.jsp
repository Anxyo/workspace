<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<center>
<h2>Objets d&eacute;tach&eacute;s,  cascades et chargements diff&eacute;r&eacute;s</h2>
</center>
<hr />
<form action="ServletDetCasc" method="POST">
<h3>em.persist() et cascade   (package entites.cascade.persist)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Test 26" /></td>
		</tr>
	</tbody>
</table>
<hr />
<h3>em.remove et  cascade (package entites.cascade.remove)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Test 27" /></td>
			
			
		</tr>
	</tbody>
</table>
<hr>
<h3>Parcourt des collections lazy dans une transaction (package entites.detachees.lazy1)</h3>
 <a href="ServletTxNonDetaches">Suivez ce lien pour la d&eacute;mo...</a>

<hr>
<h3>Parcourt explicte des collections lazy (package entites.detachees.lazy2)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Test 29" /></td>
			
			
		</tr>
	</tbody>
</table>
<hr />
<h3>Utilisation de em.merge()  (package entites.detachees.merge)</h3>
<table>
	<tbody>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Test 30" /></td>
		</tr>
	</tbody>
</table>
</form>
<hr />

</body>
</html>
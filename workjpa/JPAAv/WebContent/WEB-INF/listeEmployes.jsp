<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
  <head>
    <title>Liste les employes</title>
  </head>
  <body>
    <table>
      <thead>
        <tr>
          <th>Nom</th>
          <th>D&eacute;partement</th>
        </tr>
      </thead>
      <tbody>
        <c:forEach items="${employes}" var="emp">
          <tr>
            <td><c:out value="${emp.nom}"/></td>
            <td><c:out value="${emp.departement.nom}"/></td>
          </tr>
        </c:forEach>
      </tbody>
    </table>
  </body>
</html>

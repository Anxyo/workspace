<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Requetes</title>
</head>
<body>
<h1 align='center'>Requetes JPQL</h1>
<form action="ServletExemplesRqt" method="post">
<table align="center">
	<tr>
		<td align='center'>
		<img src='images/modele.png'/>
		</td>
	</tr></table>
<table align="center">
	<tr>
		<td align='center'>
		<h3>Requête Dynamique</h3>
		</td>
	</tr>
	<tr>
		<td><textarea name='rqt' rows='5' cols='80' ></textarea></td>
	</tr>
	<tr>
		<td align='center'><input type="submit" name="type" value="Requete dynamique" /></td>
	</tr>
</table>






<table align="center">
	<tr>
		<td align='center'>
		<h3>Requêtes SELECT</h3>
		</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="SELECT1" checked/>SELECT e
		FROM Employe e</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="SELECT2" />SELECT d FROM Departement d</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="SELECT3" />SELECT OBJECT(d) FROM Departement d</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="SELECT4" />SELECT e.nom FROM Employe e</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="SELECT5" />SELECT e.departement FROM Employe e</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="SELECT6" />SELECT DISTINCT e.departement FROM Employe e</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="SELECT7" />SELECT d.employes FROM Departement d</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="SELECT8" />SELECT e.nom, e.salaire FROM Employe e</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="SELECT9" />SELECT NEW DetailsEmploye(e.nom, e.salaire, e.departement.nom) FROM Employe e</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="SELECT10" />SELECT p FROM Projet p WHERE p.employes IS NOT EMPTY</td>
	</tr>
	<tr>
		<td align='center'><input type="submit" name="action" value="Exécuter" /></td>
	</tr>
</table>

<table align="center">
	<tr>
		<td align='center'>
		<h3>Requêtes FROM</h3>
		</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="FROM1" />SELECT p 
	                                 FROM Employe e JOIN e.telephones p</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="FROM2" />SELECT DISTINCT t FROM Employe e, IN(e.telephones) t</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="FROM3" />SELECT t.numero FROM Employe e JOIN e.telephones t</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="FROM4" />SELECT d  FROM Employe e JOIN e.departement d</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="FROM5" />SELECT e.departement FROM Employe e</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="FROM6" />SELECT DISTINCT p 
	                                 FROM Departement d JOIN d.employes e JOIN e.projets p</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="FROM7" />SELECT e, d  FROM Employe e LEFT JOIN e.departement d</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="FROM8" />SELECT e FROM Employe e JOIN FETCH e.adresse</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="FROM9" />SELECT d, e FROM Departement d LEFT JOIN d.employes e</td>
	</tr>
	<tr>
		<td align='center'><input type="submit" name="action" value="Exécuter" /></td>
	</tr>
</table>
<table align="center">
	<tr>
		<td align='center'>
		<h3>Requêtes WHERE</h3>
		</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE1" />SELECT e FROM Employe e 
	                                 WHERE e.salaire BETWEEN 40000 AND 45000</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE2" />SELECT e FROM Employe e WHERE e.salaire &gt;= 40000 AND e.salaire &lt;= 45000</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE3" />SELECT d FROM Departement d WHERE d.nom LIKE 'Mark%'</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE4" />SELECT e FROM Employe e WHERE e.salaire = (SELECT MAX(e2.salaire) FROM Employe e2)</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE5" />SELECT e FROM Employe e WHERE EXISTS (SELECT p FROM Telephone p WHERE p.employe = e AND p.type = 'Portable')</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE6" />SELECT e  FROM Employe e WHERE EXISTS (SELECT p FROM e.Telephones p WHERE p.type = 'Portable'</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE7" />SELECT e FROM Employe e WHERE e.adresse.codePostal IN ('45000', '41000')</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE8" />SELECT e FROM Employe e WHERE e.diriges IS NOT EMPTY</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE9" />SELECT e FROM Employe e WHERE e MEMBER OF e.diriges</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE10" />SELECT e FROM Employe e WHERE NOT EXISTS (SELECT t
	                                 							FROM e.telephones t WHERE t.type = 'Portable')</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE11" />SELECT e FROM Employe e WHERE e.diriges IS NOT EMPTY AND 
	                                 							e.salaire &lt; ALL (SELECT d.salaire FROM e.diriges d)</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE12" />SELECT d FROM Departement d WHERE SIZE(d.employes) = 2</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="WHERE13" />SELECT d FROM Departement d WHERE (SELECT COUNT(e) FROM d.employes e) = 2</td>
	</tr>
	<tr>
		<td align='center'><input type="submit" name="action" value="Exécuter" /></td>
	</tr> 
	</table>
	
	
	
	
	<table align="center">
	<tr>
		<td align='center'>
		<h3>Requêtes ORDER BY</h3>
		</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="ORDERBY1" />SELECT e FROM Employe e JOIN e.departement d 
	                    ORDER BY d.nom, e.nom DESC</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="ORDERBY2" />SELECT e.nom FROM Employe e 
	                    ORDER BY e.salaire DESC</td>
	</tr>
	<tr>
		<td align='center'><input type="submit" name="action" value="Exécuter" /></td>
	</tr> 
	</table>
	
	
	
	
	
	<table align="center">
	<tr>
		<td align='center'>
		<h3>Requêtes d'Agr&eacute;gation</h3>
		</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="AG1" />SELECT AVG(e.salaire) FROM Employe e</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="AG2" />SELECT d.nom, AVG(e.salaire) FROM Departement d JOIN d.employes e 
	                    GROUP BY d.nom</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="AG3" />SELECT d.nom, AVG(e.salaire) FROM Departement d JOIN d.employes e
	                    GROUP BY d.nom</td>
	</tr>
	<tr>
		<td align='center'><input type="submit" name="action" value="Exécuter" /></td>
	</tr></table>
	
	
	
	<table align="center">
	<tr>
		<td align='center'>
		<h3>Requêtes de mise &agrave; jour</h3>
		</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="UPD1" />UPDATE Employe e SET e.salaire = 60000 
	                    WHERE e.salaire = 55000</td>
	</tr>
	<tr>
		<td><input type="radio" name="type" value="DEL1" />DELETE FROM Employe e WHERE e.departement IS NULL</td>
	</tr>
	<tr>
		<td align='center'><input type="submit" name="action" value="Exécuter" /></td>
	</tr></table>
	
</form>
</body>
</html>

package entites.types.requetes;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = "CommandeClient.findAll", 
            query = "select o from CommandeClient o")
@Table(name = "COMMANDE_CLIENT",schema="JPQL")
public class CommandeClient
  implements Serializable
{
  private String ville;
  @Column(name = "DATE_CREATION")
  private Timestamp dateCreation;
  @Id
  @Column(nullable = false)
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  private String region;
  private String etatCommande;
  private String rue1;
  private String rue2;
  private Long version;
  @Column(name = "CODE_POSTAL")
  private String codePostal;
  @ManyToOne
  @JoinColumn(name = "ID_CLIENT", referencedColumnName = "ID")
  private Client client;

  public CommandeClient() {
  }

  public String getVille() {
    return ville;
  }

  public void setVille(String ville) {
    this.ville = ville;
  }

  public Timestamp getDateCreation() {
    return dateCreation;
  }

  public void setDateCreation(Timestamp dateCreation) {
    this.dateCreation = dateCreation;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getEtatCommande() {
    return etatCommande;
  }

  public void setEtatCommande(String etatCommande) {
    this.etatCommande = etatCommande;
  }

  public String getRue1() {
    return rue1;
  }

  public void setRue1(String rue1) {
    this.rue1 = rue1;
  }

  public String getRue2() {
    return rue2;
  }

  public void setRue2(String rue2) {
    this.rue2 = rue2;
  }

  public Long getVersion() {
    return version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  public String getCodePostal() {
    return codePostal;
  }

  public void setCodePostal(String codePostal) {
    this.codePostal = codePostal;
  }

  public Client getClient() {
    return client;
  }

  public void setClient(Client client) {
    this.client = client;
  }
}

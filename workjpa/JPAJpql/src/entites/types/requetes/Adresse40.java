package entites.types.requetes;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@NamedQuery(name = "Adresse40.findAll", query = "select o from Adresse40 o")
@SequenceGenerator(name = "GenerateurSequenceId", 
                   sequenceName = "GEN_SEQ_ID", initialValue = 100, 
                   allocationSize = 20)
@Table(name = "ADRESSE40", schema="JPQL")
public class Adresse40 implements Serializable
{
  private String ville;
  @Id
  @Column(nullable = false)
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  private String region;
  private String rue1;
  private String rue2;
  private Long version;
  @Column(name = "CODE_POSTAL")
  private Long codePostal;
  @OneToMany(mappedBy = "adresseExpedition")
  private List<Client> clientsAdresseExpedition;
  @OneToMany(mappedBy = "adresseFacturation")
  private List<Client> clientsAdresseFacturation;

  public Adresse40() {
  }

  public String getVille() {
    return ville;
  }

  public void setVille(String ville) {
    this.ville = ville;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getRue1() {
    return rue1;
  }

  public void setRue1(String rue) {
    this.rue1 = rue;
  }

  public String getRue2() {
    return rue2;
  }

  public void setRue2(String rue) {
    this.rue2 = rue;
  }

  public Long getVersion() {
    return version;
  }

  public void setVersion(Long version) {
    this.version = version;
  }

  public Long getCodePostal() {
    return codePostal;
  }

  public void setCodePostal(Long codePostal) {
    this.codePostal = codePostal;
  }

  public List<Client> getClientsAdresseExpedition() {
    return clientsAdresseExpedition;
  }

  public void setClientsAdresseExpedition(List<Client> listeClients) {
    this.clientsAdresseExpedition = listeClients;
  }

  public Client ajouteClient(Client client) {
    getClientsAdresseExpedition().add(client);
    client.setAdresseExpedition(this);
    return client;
  }

  public Client supprimeClient(Client client) {
    getClientsAdresseExpedition().remove(client);
    client.setAdresseExpedition(null);
    return client;
  }

  public List<Client> getClientsAdresseFacturation() {
    return clientsAdresseFacturation;
  }

  public void setClientsAdresseFacturation(List<Client> listeClients) {
    this.clientsAdresseFacturation = listeClients;
  }

  public Client ajouteClient1(Client client) {
    getClientsAdresseFacturation().add(client);
    client.setAdresseFacturation(this);
    return client;
  }

  public Client supprimeClient1(Client client) {
    getClientsAdresseFacturation().remove(client);
    client.setAdresseFacturation(null);
    return client;
  }
}

package entites.types.requetes;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@NamedQueries({
  @NamedQuery(name = "Client.findAll", 
              query = "select o from Client o"), 
  @NamedQuery(name = "Client.findByEmail", 
              query = "select o from Client o where o.email = :email")
})
@Table(name = "CLIENT", schema="JPQL")
public class Client implements Serializable
{
  private String email;
  @Id
  @Column(nullable = false)
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Long id;
  @OneToMany(mappedBy = "client")
  private List<CommandeClient> listeCommandes;
  @ManyToOne(cascade = { CascadeType.ALL })
  @JoinColumn(name = "ADRESSE_EXPEDITION", referencedColumnName = "ID")
  private Adresse40 adresseExpedition;
  @ManyToOne(cascade = { CascadeType.ALL })
  @JoinColumn(name = "ADRESSE_FACTURATION", referencedColumnName = "ID")
  private Adresse40 adresseFacturation;

  public Client() {
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public List<CommandeClient> getListeCommandes() {
    return listeCommandes;
  }

  public void setListeCommandes(List<CommandeClient> listeCommandes) {
    this.listeCommandes = listeCommandes;
  }

  public CommandeClient ajouteCommande(CommandeClient commande) {
    getListeCommandes().add(commande);
    commande.setClient(this);
    return commande;
  }

  public CommandeClient supprimeCommande(CommandeClient commande) {
    getListeCommandes().remove(commande);
    commande.setClient(null);
    return commande;
  }

  public Adresse40 getAdresseExpedition() {
    return adresseExpedition;
  }

  public void setAdresseExpedition(Adresse40 adresse) {
    this.adresseExpedition = adresse;
  }

  public Adresse40 getAdresseFacturation() {
    return adresseFacturation;
  }

  public void setAdresseFacturation(Adresse40 adresse) {
    this.adresseFacturation = adresse;
  }
}

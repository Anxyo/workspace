package exemples.requetes;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(schema="JPQL")
public class Departement {
    @Id
    private int id;
    private String nom;
    @OneToMany(mappedBy="departement")
    private Set<Employe> employes = new HashSet<Employe>();

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String n) {
        this.nom = n;
    }
    
    public Set<Employe> getEmployes() {
        return employes;
    }

    public String toString() {
        return "No département : " + getId() + 
               ", nom : " + getNom();
    }
}

package exemples.requetes;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(schema="JPQL")
public class Telephone {
    @Id
    private long id;
    private String numero;
    private String type;
    @OneToOne Employe employe;
    
    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    public String getNumero() {
        return numero;
    }
    
    public void setNumero(String no) {
        this.numero = no;
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String typeTel) {
        this.type = typeTel;
    }
    
    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe emp) {
        this.employe = emp;
    }

    public String toString() {
        return "Id t�l�phone : " + getId() + 
               ", no : " + getNumero() +
               ", type : " + getType();
    }
}

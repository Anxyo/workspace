package exemples.requetes;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema="JPQL")
public class Adresse {
    @Id
    private int id;
    private String rue;
    private String ville;
    private String departement;
    private String codePostal;
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getRue() {
        return rue;
    }
    
    public void setRue(String r) {
        this.rue = r;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String v) {
        this.ville = v;
    }

    public String getDepartement() {
        return departement;
    }

    public void setDepartement(String d) {
        this.departement = d;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String cp) {
        this.codePostal = cp;
    }
    public String toString() {
        return "Id adresse : " + getId() + 
               ", rue : " + getRue() +
               ", ville : " + getVille() +
               ", département : " + getDepartement() +
               ", code postal : " + getCodePostal();
    }

}

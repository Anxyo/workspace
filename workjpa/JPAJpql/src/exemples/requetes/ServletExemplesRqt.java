package exemples.requetes;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import util.Utilitaires;
public class ServletExemplesRqt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 @PersistenceUnit(unitName="ServiceEmploye")
	    private EntityManagerFactory emf;
	    
	    @Resource
	    private UserTransaction tx;

    public ServletExemplesRqt() {
        super();
        initSQL();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html");
	        PrintWriter out = response.getWriter();
	        debutHTML(out);
	         
	        
	       
	        String requete = request.getParameter("type");
	        // requete dynamique
	        if (requete.equals("Requete dynamique")) {
	            String queryString = request.getParameter("rqt");
	            execRqt(queryString, out);
	        } 
	        // une des requetes cabl�es
	        else if (requete.equals("SELECT1")) {
	            execRqt("SELECT e " +
	                                 "FROM Employe e", out);
	        } else if (requete.equals("SELECT2")) {
	            execRqt("SELECT d  " +
	                                 "FROM Departement d", out);
	        } else if (requete.equals("SELECT3")) {
	            execRqt("SELECT OBJECT(d)  " +
	                                 "FROM Departement d", out);
	        } else if (requete.equals("SELECT4")) {
	            execRqt("SELECT e.nom  " +
	                                 "FROM Employe e", out);
	        } else if (requete.equals("SELECT5")) {
	            execRqt("SELECT e.departement  " +
	                                 "FROM Employe e", out);
	        } else if (requete.equals("SELECT6")) {
	            execRqt("SELECT DISTINCT e.departement  " +
	                                 "FROM Employe e", out);
	        } else if (requete.equals("SELECT7")) {
	            execRqt("SELECT d.employes  " +
	                                 "FROM Departement d", out);
	        } else if (requete.equals("SELECT8")) {
	            execRqt("SELECT e.nom, e.salaire  " +
	                                 "FROM Employe e", out);
	        } else if (requete.equals("SELECT9")) {
	            execRqt("SELECT NEW exemples.requetes.DetailsEmploye(e.nom, e.salaire, e.departement.nom)  " +
	                                 "FROM Employe e", out);
	        } else if (requete.equals("SELECT10")) {
	            execRqt("SELECT p  " +
	                                 "FROM Projet p " +
	                                 "WHERE p.employes IS NOT EMPTY", out);
	        } 
	        
	        
	        // FROM
	        else if (requete.equals("FROM1")) {		//
	            execRqt("SELECT p  " +
	                                 "FROM Employe e JOIN e.telephones p", out);
	        } 
	        else if (requete.equals("FROM2")) {		//
	            execRqt("SELECT DISTINCT t  " +
	                                 "FROM Employe e, IN(e.telephones) t", out);
	        } 
	        else if (requete.equals("FROM3")) {		
	            execRqt("SELECT t.numero  " +
	                                 "FROM Employe e JOIN e.telephones t", out);
	        } 
	        else if (requete.equals("FROM4")) {
	            execRqt("SELECT d  " +
	                                 "FROM Employe e JOIN e.departement d", out);
	        } else if (requete.equals("FROM5")) {
	            execRqt("SELECT e.departement  " +
	                                 "FROM Employe e", out);
	        } 
	        else if (requete.equals("FROM6")) {
	            execRqt("SELECT DISTINCT p  " +
	                                 "FROM Departement d JOIN d.employes e JOIN e.projets p", out);
	        } else if (requete.equals("FROM7")) {
	            execRqt("SELECT e, d  " +
	                                 "FROM Employe e LEFT JOIN e.departement d", out);
	        } else if (requete.equals("FROM8")) {
	            execRqt("SELECT e  " +
	                                 "FROM Employe e JOIN FETCH e.adresse", out);
	        } else if (requete.equals("FROM9")) {
	            execRqt("SELECT d, e  " +
	                                 "FROM Departement d LEFT JOIN d.employes e", out);
	        } 
	        
	        // WHERE
	        else if (requete.equals("WHERE1")) {
	            execRqt("SELECT e  " +
	                                 "FROM Employe e " +
	                                 "WHERE e.salaire BETWEEN 40000 AND 45000", out);
	        } else if (requete.equals("WHERE2")) {
	            execRqt("SELECT e  " +
	                                 "FROM Employe e " +
	                                 "WHERE e.salaire >= 40000 AND e.salaire <= 45000", out);
	        } else if (requete.equals("WHERE3")) {
	            execRqt("SELECT d  " +
	                                 "FROM Departement d " +
	                                 "WHERE d.nom LIKE 'Mark%'", out);
	        }
	        
        
	        else if (requete.equals("WHERE4")) {
	            execRqt("SELECT e  " +
	                                 "FROM Employe e " +
	                                 "WHERE e.salaire = (SELECT MAX(e2.salaire) FROM Employe e2)", out);
	        } else if (requete.equals("WHERE5")) {
	            execRqt("SELECT e  " +
	                                 "FROM Employe e " +
	                                 "WHERE EXISTS (SELECT p FROM Telephone p WHERE p.employe = e AND p.type = 'Portable')", out);
	        } else if (requete.equals("WHERE6")) {
	            execRqt("SELECT e  " +
	                                 "FROM Employe e " +
	                                 "WHERE EXISTS (SELECT p FROM e.telephones p WHERE p.type = 'Portable')", out);
	        } else if (requete.equals("WHERE7")) {
	            execRqt("SELECT e  " +
	                                 "FROM Employe e " +
	                                 "WHERE e.adresse.codePostal IN ('45000', '41000')", out);
	        } 
	         else if (requete.equals("WHERE8")) {
	            execRqt("SELECT e " +
	                                 "FROM Employe e " +
	                                 "WHERE e.diriges IS NOT EMPTY", out);
	        } else if (requete.equals("WHERE9")) {
	            execRqt("SELECT e " +
	                                 "FROM Employe e " +
	                                 "WHERE e MEMBER OF e.diriges", out);
	        } else if (requete.equals("WHERE10")) {
	            execRqt("SELECT e " +
	                                 "FROM Employe e " +
	                                 "WHERE NOT EXISTS (SELECT t " +
	                                 "FROM e.telephones t " +
	                                 "WHERE t.type = 'Portable')", out);
	        } else if (requete.equals("WHERE11")) {
	            execRqt("SELECT e " +
	                                 "FROM Employe e " +
	                                 "WHERE e.diriges IS NOT EMPTY AND " +
	                                 "e.salaire < ALL (SELECT d.salaire " +
	                                 "FROM e.diriges d)", out);
	        } else if (requete.equals("WHERE12")) {
	            execRqt("SELECT d " +
	                    "FROM Departement d " +
	                    "WHERE SIZE(d.employes) = 2", out);
	        } else if (requete.equals("WHERE13")) {
	            execRqt("SELECT d " +
	                    "FROM Departement d " +
	                    "WHERE (SELECT COUNT(e) " +
	                    "FROM d.employes e) = 2", out);
	        } 
	        
	        // ORDER BY
	        else if (requete.equals("ORDERBY1")) {
	            execRqt("SELECT e " +
	                    "FROM Employe e JOIN e.departement d " +
	                    "ORDER BY d.nom, e.nom DESC", out);
	        } else if (requete.equals("ORDERBY2")) {
	            execRqt("SELECT e.nom " +
	                    "FROM Employe e " +
	                    "ORDER BY e.salaire DESC", out);
	        } 
	        
	        
	        // AGREGATION
	        else if (requete.equals("AG1")) {
	            execRqt("SELECT AVG(e.salaire) " +
	                    "FROM Employe e", out);
	        } else if (requete.equals("AG2")) {
	            execRqt("SELECT d.nom, AVG(e.salaire) " +
	                    "FROM Departement d JOIN d.employes e " +
	                    "GROUP BY d.nom", out);
	        } 
	        else if (requete.equals("AG3")) {
	            execRqt("SELECT d.nom, AVG(e.salaire) " +
	                    "FROM Departement d JOIN d.employes e " +
	                    "GROUP BY d.nom", out);
	        } 
	        
	                
	        //UPDATE, DELETE 
	        else if (requete.equals("UPD1")) {
	            execUpd("UPDATE Employe e " +
	                    "SET e.salaire = 60000 " +
	                    "WHERE e.salaire = 55000", out);
	        } else if (requete.equals("DEL1")) {
	            execUpd("DELETE FROM Employe e " +
	                    "WHERE e.departement IS NULL", out);
	        }
	        
	        finHTML(out);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}
	private void execRqt(String queryString, PrintWriter out) {
        EntityManager em = emf.createEntityManager();
        try {
            Query query = em.createQuery(queryString);
            afficheResultats(queryString, query.getResultList(), out);
        } finally {
            em.close();
        }
    }

    private void execUpd(String queryString, PrintWriter out) {
        EntityManager em = emf.createEntityManager();
        try {
            tx.begin();
            em.joinTransaction();
            em.createQuery(queryString).executeUpdate();
            tx.commit();
            out.println("<b>JPQL : </b>" + queryString + " </br>Ex&eacute;cut&eacute;.");
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            em.close();
        }
    }
    
    
	private void afficheResultats(String rqt, List<?> resultat, PrintWriter out) {
        out.println("<table><tbody>");
        out.println("<tr><td><b>JPQL : </b>" + rqt + "</td></tr>");
        out.println("<tr><td><b>Resultat : </b></td></tr>");
        if (resultat.isEmpty()) {
            out.println("<tr><td>Pas de r&eacute;sultats...</td></tr>");
        } else {
            for (Object o : resultat) {
                out.println("<tr><td>" + resultatEnChaine(o) + "</td></tr>");
            }
        }
        out.println("</tbody></table>");
    }

    // soit des tableaux (que l'on transforme en List)
    // soit directement des listes
    private String resultatEnChaine(Object o) {
        if (o instanceof Object[]) {
            return Arrays.asList((Object[])o).toString();
        } else {
            return String.valueOf(o);
        }
    }
    
    
    private void debutHTML(PrintWriter out) throws IOException {  
        out.println("<html>");
        out.println("<head><title>Requetes JPQL</title></head>");
        out.println("<body>");
    }
    
    
    private void finHTML(PrintWriter out) throws IOException {
        out.println("<hr/>");
        out.println("</body>");
        out.println("</html>");
        out.close();
    }

	
	
	
	
	private void initSQL() {
	Utilitaires util = new Utilitaires();
	Vector<String> v = new Vector<String>();
	v.add("SET SCHEMA JPQL");
	v.add("DROP TABLE DEPARTEMENT");
	v.add("SET SCHEMA JPQL");
	v.add("DROP TABLE PROJET");
	v.add("SET SCHEMA JPQL");
	v.add("DROP TABLE PROJET_EMPLOYE");
	v.add("SET SCHEMA JPQL");
	v.add("DROP TABLE EMPLOYE");
	v.add("SET SCHEMA JPQL");
	v.add("DROP TABLE TELEPHONE");
	v.add("SET SCHEMA JPQL");
	v.add("DROP TABLE ADRESSE");
	v.add("SET SCHEMA JPQL");
	v.add("CREATE TABLE DEPARTEMENT (ID INTEGER NOT NULL, NOM VARCHAR(255), PRIMARY KEY (ID))");
	v.add("SET SCHEMA JPQL");
	v.add("CREATE TABLE PROJET (ID INTEGER NOT NULL, DTYPE VARCHAR(31), NOM VARCHAR(255), PRIMARY KEY (ID))");
	v.add("SET SCHEMA JPQL");
	v.add("CREATE TABLE PROJET_EMPLOYE (EMPLOYES_ID INTEGER, PROJETS_ID INTEGER)");
	v.add("SET SCHEMA JPQL");
	v.add("CREATE TABLE EMPLOYE (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, DATEDEBUT DATE, ADRESSE_ID INTEGER, DEPARTEMENT_ID INTEGER, MANAGER_ID INTEGER, PRIMARY KEY (ID))");
	v.add("SET SCHEMA JPQL");
	v.add("CREATE TABLE TELEPHONE (ID BIGINT NOT NULL, NUMERO VARCHAR(255), TYPE VARCHAR(255), EMPLOYE_ID INTEGER, PRIMARY KEY (ID))");
	v.add("SET SCHEMA JPQL");
	v.add("CREATE TABLE ADRESSE (ID INTEGER NOT NULL, VILLE VARCHAR(255), DEPARTEMENT VARCHAR(255), RUE VARCHAR(255), CODEPOSTAL VARCHAR(255), PRIMARY KEY (ID))");
	v.add("SET SCHEMA JPQL");
	v.add("INSERT INTO ADRESSE (ID, VILLE, DEPARTEMENT, RUE, CODEPOSTAL) VALUES (1, 'Paris', 'Seine', '123 Rue de Maubeuge', '75012')");
	v.add("INSERT INTO ADRESSE (ID, VILLE, DEPARTEMENT, RUE, CODEPOSTAL)  VALUES (2, 'Roissy', 'Seine Saint Denis', '12 rue du d�part', '94500')");
	v.add("INSERT INTO ADRESSE (ID, VILLE, DEPARTEMENT, RUE, CODEPOSTAL)  VALUES (3, 'Orl�ans', 'Loiret', '12 rue Emile Zola', '45000')");
	v.add("INSERT INTO ADRESSE (ID, VILLE, DEPARTEMENT, RUE, CODEPOSTAL) VALUES (4, 'Malesherbes', 'Loiret', '15 rue Jean Mermoz', '45330')");
	v.add("INSERT INTO ADRESSE (ID, VILLE, DEPARTEMENT, RUE, CODEPOSTAL)  VALUES (5, 'Bordeaux', 'Gironde', '12 cours Louis Pasteur', '33100')");
	v.add("INSERT INTO ADRESSE (ID, VILLE, DEPARTEMENT, RUE, CODEPOSTAL)  VALUES (6, 'Nice', 'Alpes maritimes', '15 promenade des Anglais', '06300')");
	v.add("INSERT INTO ADRESSE (ID, VILLE, DEPARTEMENT, RUE, CODEPOSTAL)  VALUES (7, 'Lille', 'Nord', '12 rue Jospin', '59000')");
	v.add("INSERT INTO ADRESSE (ID, VILLE, DEPARTEMENT, RUE, CODEPOSTAL)  VALUES (8, 'Brest', 'Finist�re', '12 rue du Goeland', '29200')");
	v.add("INSERT INTO ADRESSE (ID, VILLE, DEPARTEMENT, RUE, CODEPOSTAL)  VALUES (9, 'Toulouse', 'Haute Garonne', '4 rue Nougaro', '31000')");
	v.add("INSERT INTO ADRESSE (ID, VILLE, DEPARTEMENT, RUE, CODEPOSTAL)  VALUES (10, 'Tours', 'Indre et Loire', '17 rue du Duc', '37000')");

	v.add("INSERT INTO DEPARTEMENT (ID, NOM) VALUES (1, 'Technique')");
	v.add("INSERT INTO DEPARTEMENT (ID, NOM) VALUES (2, 'Support')");
	v.add("INSERT INTO DEPARTEMENT (ID, NOM) VALUES (3, 'Comptabilit�')");
	v.add("INSERT INTO DEPARTEMENT (ID, NOM) VALUES (4, 'Marketing')");
	v.add("INSERT INTO DEPARTEMENT (ID, NOM) VALUES (5, 'Direction')");
	v.add("INSERT INTO DEPARTEMENT (ID, NOM) VALUES (6, 'Informatique')");
	v.add("INSERT INTO DEPARTEMENT (ID, NOM) VALUES (7, 'Entretien')");
	v.add("INSERT INTO DEPARTEMENT (ID, NOM) VALUES (8, 'Finances')");

	v.add("INSERT INTO PROJET (ID, NOM, DTYPE) VALUES (1, 'Projet 2', 'PC')");
	v.add("INSERT INTO PROJET (ID, NOM, DTYPE) VALUES (2, 'Version 1', 'PC')");
	v.add("INSERT INTO PROJET (ID, NOM, DTYPE) VALUES (3, 'Test Version 2', 'PQ')");
	v.add("INSERT INTO PROJET (ID, NOM, DTYPE) VALUES (4, 'Impl. Version 3', 'PQ')");

	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (1, 'Jean', 55000, {d '2001-01-01'}, 1, 2, 9)");
	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (2, 'Robert', 53000, {d '2001-05-23'}, 2, 2, 9)");
	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (3, 'Pierre', 40000, {d '2002-08-06'}, 3, 2, 9)");
	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (4, 'Frank', 41000, {d '2003-02-17'}, 4, 1, 10)");
	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (5, 'Marc', 60000, {d '2004-11-14'}, 5, 1, 10)");
	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (6, 'Paule', 62000, {d '2005-08-18'}, 6, 1, 10)");
	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (7, 'Stephanie', 54000, {d '2006-06-07'}, 7, 1, 10)");
	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (8, 'Clara', 45000, {d '1999-08-11'}, 8, 1, NULL)");
	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (9, 'Sarah', 52000, {d '2002-04-26'}, 9, 2, 10)");
	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (10, 'Fran�oise', 59000, {d '2003-04-16'}, 10, 1, NULL)");
	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (11, 'Marcus', 35000, {d '1995-07-22'}, NULL, NULL, NULL)");
	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (12, 'Paul', 36000, {d '1995-07-22'}, NULL, 3, 11)");
	v.add("INSERT INTO EMPLOYE (ID, NOM, SALAIRE, DATEDEBUT, ADRESSE_ID, DEPARTEMENT_ID, MANAGER_ID) VALUES (13, 'Jaques', 43000, {d '1995-07-22'}, NULL, 3, NULL)");

	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (1, '0111111111', 'Bureau', 1)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (2, '0222222222', 'Domicile', 1)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (3, '0333333333', 'Bureau', 2)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (4, '0444444444', 'Bureau', 3)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (5, '0612345678', 'Portable', 3)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (6, '0145546996', 'Bureau', 4)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (7, '0114523669', 'Domicile', 4)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (8, '0358985621', 'Bureau', 5)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (9, '0647859658', 'Portable', 5)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (10, '0425859647', 'Bureau', 6)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (11, '0345789658', 'Domicile', 6)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (12, '0617758965', 'Portable', 6)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (13, '0285964785', 'Bureau', 7)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (14, '0245789654', 'Domicile', 7)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (15, '0325631458', 'Bureau', 8)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (16, '0345789658', 'Bureau', 9)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (17, '0125367485', 'Domicile', 9)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (18, '0614523696', 'Portable', 9)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (19, '0125636985', 'Bureau', 10)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (20, '0214523696', 'Domicile', 10)");
	v.add("INSERT INTO TELEPHONE (ID, NUMERO, TYPE, EMPLOYE_ID) VALUES (21, '0685967447', 'Portable', 10)");

	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (1, 1)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (2, 2)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (2, 3)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (3, 1)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (3, 2)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (3, 3)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (4, 1)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (5, 2)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (5, 3)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (6, 1)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (6, 2)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (7, 3)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (8, 1)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (8, 2)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (9, 3)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (9, 1)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (10, 1)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (10, 2)");
	v.add("INSERT INTO PROJET_EMPLOYE (EMPLOYES_ID, PROJETS_ID) VALUES (10, 3)");
	util.initSQL("jdbc/__default", v);
	}

}

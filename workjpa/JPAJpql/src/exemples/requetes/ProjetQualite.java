package exemples.requetes;

import javax.persistence.Entity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Table;

@Entity
@Table(schema="JPQL")
@DiscriminatorValue("PQ")
public class ProjetQualite extends Projet {
}

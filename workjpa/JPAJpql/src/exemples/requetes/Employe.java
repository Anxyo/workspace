package exemples.requetes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="Employe")
@Table(schema="JPQL")
public class Employe {
    @Id
    private int id;
    private String nom;
    private long salaire;
    @Temporal(TemporalType.DATE)
    private Date dateDebut;
    
    @OneToOne
    private Adresse adresse;
    
    @OneToMany(mappedBy="employe")
    private Collection<Telephone> telephones = new ArrayList<Telephone>();
    
    @ManyToOne
    private Departement departement;
    
    @ManyToOne
    private Employe manager;
    
    @OneToMany(mappedBy="manager")
    private Collection<Employe> diriges = new ArrayList<Employe>();
    
    @ManyToMany(mappedBy="employes")
    private Collection<Projet> projets = new ArrayList<Projet>();

    public int getId() {
        return id;
    }
    
    public void setId(int no) {
        this.id = no;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String n) {
        this.nom = n;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long s) {
        this.salaire = s;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dd) {
        this.dateDebut = dd;
    }
    
    public Collection<Telephone> getTelephones() {
        return telephones;
    }
    
    public void ajouteNumero(Telephone tel) {
        if (!getTelephones().contains(tel)) {
            getTelephones().add(tel);
            if (tel.getEmploye() != null) {
                tel.getEmploye().getTelephones().remove(tel);
            }
            tel.setEmploye(this);
        }
    }
    
    public Departement getDepartement() {
        return departement;
    }
    
    public void setDepartement(Departement dep) {
        if (this.departement != null) {
            this.departement.getEmployes().remove(this);
        }
        this.departement = dep;
        this.departement.getEmployes().add(this);
    }
    
    public Collection<Employe> getDiriges() {
        return diriges;
    }
    
    public void ajouteDirige(Employe emp) {
        if (!getDiriges().contains(emp)) {
            getDiriges().add(emp);
            if (emp.getManager() != null) {
                emp.getManager().getDiriges().remove(emp);
            }
            emp.setManager(this);
        }
    }
    
    public Employe getManager() {
        return manager;
    }
    
    public void setManager(Employe manager) {
        this.manager = manager;
    }

    public Collection<Projet> getProjets() {
        return projets;
    }
    
    public void ajouteProjet(Projet p) {
        if (!getProjets().contains(p)) {
            getProjets().add(p);
        }
        if (!p.getEmployees().contains(this)) {
            p.getEmployees().add(this);
        }
    }
    
    public Adresse getAdresse() {
        return adresse;
    }
    
    public void setAdresse(Adresse a) {
        this.adresse = a; 
    }
    
    public String toString() {
        return "Id employ� : " + getId() + 
               ", nom : " + getNom() +
               ", salaire : " + getSalaire() +
               ", t�l�phones : " + getTelephones() +
               ", id du manager : " + ((getManager() == null) ? null : getManager().getId()) +
               ", id du d�partement : " + ((getDepartement() == null) ? null : getDepartement().getId());
    }

}

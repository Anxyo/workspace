package exemples.requetes;

// cette classe n'est PAS une entit�
public class DetailsEmploye {
    private String nom;
    private long salaire;
    private String nomDept;
    
    public DetailsEmploye(String n, long s, String nd) {
        this.nom = n;
        this.salaire = s;
        this.nomDept = nd;
    }
    
    public String toString() {
        return "D�tails employ� : nom : " + nom +
               ", salair : " + salaire +
               ", nom du d�pt : " + nomDept;
    }
}

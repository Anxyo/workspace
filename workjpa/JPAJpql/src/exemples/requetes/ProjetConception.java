package exemples.requetes;

import javax.persistence.Entity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Table;

@Entity
@DiscriminatorValue("PC")
@Table(schema="JPQL")
public class ProjetConception extends Projet {
}

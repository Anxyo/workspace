package exemples.requetes;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(schema="JPQL")
@Inheritance
public class Projet {
    @Id
    protected int id;
    protected String nom;
    @ManyToMany
    @JoinTable(name="PROJET_EMPLOYE", schema="JPQL",
            joinColumns=@JoinColumn(name="EMPLOYES_ID"),
            inverseJoinColumns=@JoinColumn(name="PROJETS_ID"))
    protected Collection<Employe> employes = new ArrayList<Employe>();
    
    public int getId() {
        return id;
    }
    
    public void setId(int projectNo) {
        this.id = projectNo;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String np) {
        this.nom = np;
    }
    
    public Collection<Employe> getEmployees() {
        return employes;
    }
    
    public void ajouteEmploye(Employe emp) {
        if (!getEmployees().contains(emp)) {
            getEmployees().add(emp);
        }
        if (!emp.getProjets().contains(this)) {
            emp.getProjets().add(this);
        }
    }
    
    public String toString() {
        return "Id projet : " + getId() + 
                ", nom : " + getNom();
    }
}

package types.requetes;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entites.types.requetes.Adresse40;
import entites.types.requetes.Client;

import services.types.requetes.IGestionCommandeClient;


public class ServletExRqtNommees extends HttpServlet {
	private static final long serialVersionUID = 1L;
       // injection ejb
	@EJB IGestionCommandeClient service1;
   
    public ServletExRqtNommees() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    response.setContentType("text/html");
	    PrintWriter out = response.getWriter();
	    debutHTML(out);
		Adresse40 adresse = new Adresse40();
		adresse.setVille("Orleans");
		adresse.setRegion("Centre");
		adresse.setCodePostal(45000L);
		Client client = new Client();
		client.setEmail("toto@toto.com");
		client.setAdresseExpedition(adresse);
		service1.persisteEntite(client);
		for (Client clt:
			service1.rqtClientFindByEmail("toto@toto.com")) {
			out.println("L'ID client est : " + clt.getId());
		}
		finHTML(out);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}
    private void debutHTML(PrintWriter out) throws IOException {
        out.println("<html>");
        out.println("<head><title>Invocation requ�tes nomm�es</title></head>");
        out.println("<body>");
    }
    
    
    private void finHTML(PrintWriter out) throws IOException {
        out.println("<hr/>");
        out.println("</body>");
        out.println("</html>");
        out.close();
    }
}

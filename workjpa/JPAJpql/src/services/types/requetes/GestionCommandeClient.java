package services.types.requetes;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entites.types.requetes.Adresse40;
import entites.types.requetes.Client;
import entites.types.requetes.CommandeClient;

@Stateless(mappedName="GestionCommandeClient")
public class GestionCommandeClient implements IGestionCommandeClient
{
  @PersistenceContext(unitName = "ServiceEmploye")
  private EntityManager em;

  public GestionCommandeClient() {
  }

  public Object mergeEntite(Object entite) {
    return em.merge(entite);
  }

  public Object persisteEntite(Object entite) {
    em.persist(entite);
    return entite;
  }

  /** <code>select o from Client </code> */
  @SuppressWarnings("unchecked")
public List<Client> queryClientFindAll() {
    return em.createNamedQuery("Client.findAll").getResultList();
  }

  public void supprimeClient(Client client) {
    client = em.find(Client.class, client.getId());
    em.remove(client);
  }

  /** <code>select o from CommandeClient o</code> */
  @SuppressWarnings("unchecked")
public List<CommandeClient> rqtCommandeClientFindAll() {
    return em.createNamedQuery("CommandeClient.findAll").getResultList();
  }

  public void supprimeCommandeClient(CommandeClient commande) {
    commande = em.find(CommandeClient.class, commande.getId());
    em.remove(commande);
  }

  /** <code>select o from Adresse o</code> */
  @SuppressWarnings("unchecked")
public List<Adresse40> rqtAdresseFindAll() {
    return em.createNamedQuery("Adresse.findAll").getResultList();
  }

  public void supprimeAdresse(Adresse40 adresse) {
    adresse = em.find(Adresse40.class, adresse.getId());
    em.remove(adresse);
  }

  /** <code>select o from Client o where email = :email</code> */
  @SuppressWarnings("unchecked")
public List<Client> rqtClientFindByEmail(Object email) {
    return em.createNamedQuery("Client.findByEmail").setParameter("email", 
                                                                    email).getResultList();
  }
}

package services.types.requetes;

import java.util.List;
import javax.ejb.Remote;

import entites.types.requetes.Adresse40;
import entites.types.requetes.Client;
import entites.types.requetes.CommandeClient;

@Remote
public interface IGestionCommandeClient
{
  Object mergeEntite(Object entity);
  Object persisteEntite(Object entity);
  List<Client> queryClientFindAll();
  void supprimeClient(Client customer);
  List<CommandeClient> rqtCommandeClientFindAll();
  void supprimeCommandeClient(CommandeClient customerOrder);
  List<Adresse40> rqtAdresseFindAll();
  void supprimeAdresse(Adresse40 address);
  List<Client> rqtClientFindByEmail(Object email);
}

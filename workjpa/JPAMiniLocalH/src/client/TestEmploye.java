package client;



import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import metier.Employe;
import service.ServiceEmploye;

public class TestEmploye {

    public static void main(String[] args) {
    
    EntityManagerFactory emf = 
            Persistence.createEntityManagerFactory("ServiceEmploye");
    EntityManager em = emf.createEntityManager();
    ServiceEmploye service = new ServiceEmploye(em);
    
    //  cr�e et persiste un employ� (contexte tx requis)
    em.getTransaction().begin();
    Employe emp = service.creeEmploye(158, "Pierre Dupont", 45000);
    em.getTransaction().commit();
    System.out.println("Enregistr� : " + emp);
    
    // retrouve un employ� sp�cifique (pas de contexte tx requis)
    emp = service.findEmploye(158);
    System.out.println("Trouv� : " + emp);
    
    // retrouve tous les employ�s (pas de contexte tx requis)
    Collection<Employe> emps = service.findAllEmployes();
    for (Employe e : emps) 
        System.out.println("Employ� trouv� : " + e);
    
    // mise � jour de l'employ� (contexte tx requis)
    em.getTransaction().begin();
    emp = service.augmenteSalaire(158, 1000);
    em.getTransaction().commit();
    System.out.println("Mis � jour : " + emp);

    // supression (contexte tx requis)
    em.getTransaction().begin();
    service.supprimeEmploye(158);
    em.getTransaction().commit();
    System.out.println("Employ� 158 supprim�");        
    
    // fermeture de l'EM et de l'EMF
    em.close();
    emf.close();
    }
        
}


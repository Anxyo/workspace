package metier;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

// pour lancer cet exemple, ajouter au chemin des classes les bibliothèques :
// - C:\GlassFish-Tools-Bundle-For-Eclipse-1.2\glassfishv3\glassfish\modules\javax.persistence.jar
// - C:\GlassFish-Tools-Bundle-For-Eclipse-1.2\glassfishv3\javadb\lib\derbyclient.jar

@Entity
public class Employe {
    @Id
    private int id;
    private String nom;
    private long salaire;

    public Employe() {}
    public Employe(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getSalaire() {
        return salaire;
    }

    public void setSalaire(long salaire) {
        this.salaire = salaire;
    }
    
    public String toString() {
        return "Id employé : " + getId() + " nom : " + getNom() + " salaire : " + getSalaire();
    }
}



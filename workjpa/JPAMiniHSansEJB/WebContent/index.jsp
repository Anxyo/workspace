<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Gestion d'un employé</title>
</head>
<body>

<center>
<h2>GESTION D'UN EMPLOYE</h2>
</center>

<hr />
<form action="ServletPresentation" method="POST">
<h3>Cr&eacute;er un employ&eacute;</h3>
<table>
	<tbody>
		<tr>
			<td>Id :</td>
			<td><input type="text" name="idCreation" />(un entier)</td>
		</tr>
		<tr>
			<td>Nom :</td>
			<td><input type="text" name="nom" />(une chaine)</td>
		</tr>
		<tr>
			<td>Salaire:</td>
			<td><input type="text" name="salaire" />(entier long)</td>
		</tr>
		<tr>
			<td colspan="2" align='center'><input name="action"
				type="submit" value="Creation" /></td>
		</tr>

	</tbody>
</table>
<hr />

<h3>Supprimer un employ&eacute;</h3>
<table>
	<tbody>
		<tr>
			<td>Id:</td>
			<td><input type="text" name="idSupp" />(un entier)</td>
		</tr>
		<tr>
			<td colspan='2' align='center'><input name="action"
				type="submit" value="Suppression" /></td>
		</tr>
	</tbody>
</table>
<hr />
<h3>Mettre &agrave; jour employ&eacute;</h3>
<table>
	<tbody>
		<tr>
			<td>Id :</td>
			<td><input type="text" name="idAugment" />(un entier)</td>
		</tr>
		<tr>
			<td>Augmentation :</td>
			<td><input type="text" name="augmentation" />(entier long)</td>
		</tr>
		<tr>
			<td colspan='2' align='center'><input name="action"
				type="submit" value="Mise a jour" /></td>
		</tr>
	</tbody>
</table>

<hr />
<h3>Rechercher un employ&eacute;</h3>
<table>
	<tbody>

		<tr>
			<td>Id:</td>
			<td><input type="text" name="idRecherche" />(un entier)</td>
		</tr>
		<tr>
			<td colspan='2' align='center'><input name="action"
				type="submit" value="Cherche" /></td>
		</tr>
	</tbody>
</table>
<hr />

<h3>Rechercher tous les employ&eacute;s</h3>
<input name="action" type="submit" value="Rechercher" />
<hr />
</form>
</body>
</html>

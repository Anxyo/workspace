package metier;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;


public class ServiceEmployeBean implements ServiceEmploye {
	
    protected EntityManager em;

    public ServiceEmployeBean(EntityManager em) {
    	this.em = em;
    }
    
    public Employe creeEmploye(int id, String nom, long salaire) {
    	em.getTransaction().begin();			// d�but de transaction
        Employe emp = new Employe(id);
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);
        em.getTransaction().commit();			// commit
        return emp;
    }

    public void supprimeEmploye(int id) {
    	em.getTransaction().begin();			// d�but de transaction
    	Employe emp = findEmploye(id);
        if (emp != null) {
            em.remove(emp);
        }
        em.getTransaction().commit();			// commit
    }

    public Employe augmenteSalaire(int id, long augmentation) {
    	em.getTransaction().begin();			// d�but de transaction
        Employe emp = em.find(Employe.class, id);
        if (emp != null) {
            emp.setSalaire(emp.getSalaire() + augmentation);
        }
        em.getTransaction().commit();			// commit
        return emp;
    }

    public Employe findEmploye(int id) {
        return em.find(Employe.class, id);
    }


	public List<Employe> findAllEmployes() {
        TypedQuery<Employe> rqt = em.createQuery("SELECT e FROM Employe e", Employe.class);
        return  rqt.getResultList();
    }
}


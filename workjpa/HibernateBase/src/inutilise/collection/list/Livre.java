package inutilise.collection.list;
import java.util.*;

public class Livre {
  private int id;
  private String titre;
  private List <Chapitre> chapitres;

  public Livre(){
  }

  public Livre(String titre) {
    this.titre = titre;
  }

  public void setId(int i) {
    id = i;
  }

  public int getId() {
    return id;
  }

  public void setTitre(String n) {
    titre = n;
  }

  public String getTitre() {
    return titre;
  }

  public void setChapitres(List<Chapitre> l) {
    chapitres = l;
  }

  public List<Chapitre> getChapitres() {
    return chapitres;
  }
}
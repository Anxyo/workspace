package inutilise.collection.list;


public class Chapitre {
  private int id;
  private String info;

  public Chapitre(){
  }

  public Chapitre(String info) {
    this.info = info;
  }

  public void setId(int i) {
    id = i;
  }

  public int getId() {
    return id;
  }

  public void setInfo(String n) {
    info = n;
  }

  public String getInfo() {
    return info;
  }
}
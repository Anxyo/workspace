package inutilise.collection.list;
import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.Transaction;

import commun.HibernateHelper;
import commun.JdbcUtil;

public class Main {
   public static void main(String[] args) throws Exception {
	   
      JdbcUtil.execUpdate("create table grouptable (id int,name varchar);");    
      JdbcUtil.execUpdate("create table story (id int,info varchar,idx int,parent_id int);");    

      Session session = HibernateHelper.getSession();

      Livre sp = new Livre("Doroth�e la volage");
     
      ArrayList<Chapitre> list = new ArrayList<Chapitre>();
      list.add(new Chapitre("I - Jack rencontre Doroth�e"));
      list.add(new Chapitre("II - Doroth�e rencontre Max"));
      sp.setChapitres(list);

      Transaction transaction = null;

      try {
           transaction = session.beginTransaction();
           session.save(sp);
           transaction.commit();
      } catch (Exception e) { 
           if (transaction != null) {
             transaction.rollback();
             throw e;
           }
      }  finally { 
           session.close();
           HibernateHelper.getSessionFactory().close();
      }
      JdbcUtil.execRequete("select * from LIVRES");
      JdbcUtil.execRequete("select * from CHAPITRES");      
   }
}

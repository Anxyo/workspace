package transactions.rollback;
import java.util.ArrayList;
import java.util.List;

public class Fournisseur
{
    private int id;
    private String nom;
    private List<Produit> produits = new ArrayList<Produit>();
    private int noVersion;
    
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getNom()
    {
        return nom;
    }
    public void setNom(String nom)
    {
        this.nom = nom;
    }
    public List<Produit> getProduits()
    {
        return produits;
    }
    public void setProduits(List<Produit> produits)
    {
        this.produits = produits;
    }
	public void setNoVersion(int version) {
		this.noVersion = version;
	}
	public int getNoVersion() {
		return noVersion;
	}
    
}

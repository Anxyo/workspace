package transactions.rollback;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import commun.HibernateHelper;
import commun.JdbcUtil;


public class Main {
	
	public static void main(String[] args) {
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`PRODUITS`;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`FOURNISSEURS`;");
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
		
		String rqt = "CREATE TABLE  `test`.`FOURNISSEURS` (`ID` int(11) NOT NULL,`VERSION` int(11),`NOM` varchar(255) default NULL," + 
					"PRIMARY KEY  (`ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		JdbcUtil.execUpdate(rqt);
		rqt = 	"CREATE TABLE  `test`.`PRODUITS` (`ID` int(11) NOT NULL,`VERSION` int(11), `NOM` varchar(255) default NULL," +
				"`DESCRIPTION` varchar(255) default NULL,`PRIX` double default NULL,`ID_FOURNISSEUR` int(11) default NULL," +
				" PRIMARY KEY  (`ID`), KEY `FKF2D1D7EAEE411957` (`ID_FOURNISSEUR`), CONSTRAINT `FKF2D1D7EAEE411957` " +
				"FOREIGN KEY (`ID_FOURNISSEUR`) REFERENCES `fournisseurs` (`ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		JdbcUtil.execUpdate(rqt);
		
        SessionFactory factory = HibernateHelper.getSessionFactory();
        Produit p = null;
        Session session = null;
        try {
        	session = factory.openSession();
        	Transaction tx = session.beginTransaction();
        	creeData(session);
        	tx.commit(); // �crit r�ellement les donn�es dans la base
        	// les donn�es sont bien l� ?
        	// HibernateUtil.execRequete("select * from Fournisseurs");
        	// HibernateUtil.execRequete("select * from Produits");

        	// la session reste ouverte ici...
        
        		
        	// on commence par retrouver le produit avec une requete HQL
        	System.out.println("D�but de transaction :");
        	tx = session.beginTransaction();	// nouvelle connexion
        	p = (Produit)session.createQuery("from Produit where nom = 'Produit 1'").uniqueResult();       
        	System.out.println("Nom du produit extrait de la base : " + p.getNom());
        	System.out.println("Changement- de l'intitul� de p...");
        	p.setNom("Cartouche HP 45");
        	System.out.println("Intitule de p avant le rollback : " + p.getNom());
        
        
        	tx.rollback();	// impact sur la base, pas sur l'objet m�moire
        	//tx.commit();	// essayer �galement
        
        	// o� en est la base ?
        	JdbcUtil.execRequete("select * from Fournisseurs");
        	JdbcUtil.execRequete("select * from Produits");

        	TransactionStatus transactionStatus = session.getTransaction().getStatus();
        	
            if(transactionStatus.equals(TransactionStatus.ROLLED_BACK)){
            	System.out.println("Le rollback de la transaction a �t� fait");
            }
        	else
        		System.out.println("Le rollback de la transaction n'a PAS �t� fait");
        	System.out.println("Intitule de p (valeur m�moire) apr�s le rollback : " + p.getNom());
      
        	// il faut red�marrer une transaction
        	tx = session.beginTransaction();
        	System.out.println("Avant le rafraichissement de p ...");
        	session.refresh(p);	// recharge les donn�es de la base dans l'objet m�moire 
        	System.out.println("Intitul� de p apr�s le refresh() : " + p.getNom());
        	tx.commit();
        	System.out.println("Intitul� de p apr�s le commit : " + p.getNom());
		
        	// �tat de la base � la fin de tout cela
        	JdbcUtil.execRequete("select * from Fournisseurs");
        	JdbcUtil.execRequete("select * from Produits");
        }
        catch (HibernateException he){
        	System.out.println("Exception hibernate : " + he);
        }
        finally {
        	session.close();	// fermeture de la session
        	HibernateHelper.getSessionFactory().close();
        }
    }
	
	static public void afficheProduits(List<Produit> liste){
        Iterator<Produit> iter = liste.iterator();
        System.out.println("\n\n");
        if (!iter.hasNext()){
            System.out.println("Pas de produit � afficher !");
            
        }
        else {
        	for (Produit p : liste) {
        		String msg = p.getFournisseur().getNom() + "\t";
        		msg += p.getNom() + "\t";
        		msg += p.getPrix() + "\t";
        		msg += p.getDescription();
        		System.out.println(msg);
        	}
        }
        System.out.println("\n\n");
    }


	private static void creeData(Session session){
        
        Fournisseur fourn1 = new Fournisseur();
        fourn1.setNom("Fournisseur 1");
        session.save(fourn1);
        
        Fournisseur fourn2 = new Fournisseur();
        fourn2.setNom("Fournisseur 2");
        session.save(fourn2);        
        
        Produit produit1 = new Produit("Produit 1","Description du produit 1", 2.10);
        produit1.setFournisseur(fourn1);
        fourn1.getProduits().add(produit1);
        session.save(produit1);
        
        Produit produit2 = new Produit("Produit 2","Description du produit 2", 11.45);
        produit2.setFournisseur(fourn1);
        fourn1.getProduits().add(produit2);        
        session.save(produit2);
        
        Produit produit3 = new Produit("Produit 3", "Description du produit 3", 2.25);
        produit3.setFournisseur(fourn2);
        fourn2.getProduits().add(produit3);
        session.save(produit3);
	}
}
package many.to.one.mini;

import org.hibernate.Session;
import org.hibernate.Transaction;

import commun.HibernateHelper;
import commun.JdbcUtil;

public class Main {
	
	
	public static void main(String[] args) {
		// dans cet ordre...
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`enfants`;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`parents`;");
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
		   
		// et dans cet ordre...
		String strRequete = "CREATE TABLE  `test`.`parents` (`ID` bigint(20) NOT NULL auto_increment,`nom` varchar(255) default NULL," +
							" PRIMARY KEY  (`ID`)) ENGINE=InnoDB;";
		JdbcUtil.execUpdate(strRequete);
		
		strRequete 		  =  "CREATE TABLE  `test`.`enfants` (" +
							"`ID` bigint(20) NOT NULL, `prenom` varchar(20) default NULL, `ID_PARENT` bigint(20) NOT NULL," +
							" PRIMARY KEY  (`ID`), KEY `FKCB2121A93E773583` (`ID_PARENT`), CONSTRAINT `FKCB2121A93E773583` FOREIGN KEY (`ID_PARENT`) " +
							" REFERENCES `parents` (`ID`)) ENGINE=InnoDB;";
		JdbcUtil.execUpdate(strRequete);
		Session session = HibernateHelper.getSession();
		Transaction tx = session.beginTransaction();

        Parent p = new Parent("Papa");
        p.ajouteEnfant(new Enfant("Marc"));
        p.ajouteEnfant(new Enfant("Sophie"));

        session.save(p);	// normalement sauvegarde toute la famille...
        tx.commit();
        
		session.close();
		HibernateHelper.getSessionFactory().close();

		// la seconde de v�rit�...
		System.out.println("===========================================================================");
		JdbcUtil.execRequete("select * from PARENTS");
		System.out.println("===========================================================================");
        JdbcUtil.execRequete("select * from ENFANTS");        
        System.out.println("===========================================================================");
		
	}
	
}
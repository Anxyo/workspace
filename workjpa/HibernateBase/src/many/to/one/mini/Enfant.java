package many.to.one.mini;

public class Enfant {

private Long id;
private String prenom;
private Parent parent;

public Enfant(String prenom){
	this.prenom = prenom;
}

public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public Parent getParent() {
	return parent;
}
public void setParent(Parent parent) {
	this.parent = parent;
}
public String getPrenom() {
	return prenom;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}

}

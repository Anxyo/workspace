package many.to.one.mini;
import java.util.*;

public class Parent {
	private Long id;
	private String nom;
	private Set<Enfant> enfants;
	
	public Parent(String nom){	// init minimale
		this.nom = nom;
		enfants = new HashSet<Enfant>();
	}
	
	// proprietes
	public Set<Enfant> getEnfants() {
		return enfants;
	}
	public void setEnfants(Set <Enfant> enfants) {
		this.enfants = enfants;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	// m�thode utilitaire
	void ajouteEnfant(Enfant e){
		// connexion des objets
		enfants.add(e);
		e.setParent(this);
	}
	
}

package many.to.many;

import java.util.HashSet;

import org.hibernate.Session;
import org.hibernate.Transaction;

import commun.HibernateHelper;
import commun.JdbcUtil;

public class Main {
	
	
	public static void main(String[] args) {
		creeSchema();
		// Il nous faut toute une population d'objets
		// d'abord 2 formations
		Formation laFormation = new Formation();
        laFormation.setIntitule("Introduction � Hibernate");
       
        // Deux stagiaires
        Stagiaire stagiaire1 = new Stagiaire("Prenom stagiaire1", "nom stagiaires1");
        Stagiaire stagiaire2 = new Stagiaire("Prenom stagiaire2", "nom stagiaires2");
        
        // un centre
        Centre leCentre = new Centre("Orl�ans");
        
        // on connecte tout �a...
        // d'abord les stagiaires et les formations
        HashSet <Stagiaire> hs_stagiaires1 = new HashSet <Stagiaire>();
        HashSet <Formation> hs_formation1 = new HashSet <Formation>();
        HashSet <Formation> hs_formation2 = new HashSet <Formation>();
        // cot� formation
        hs_stagiaires1.add(stagiaire1);
        hs_stagiaires1.add(stagiaire2);
        laFormation.setStagiaires(hs_stagiaires1);
        // cot� stagiaires
        hs_formation1.add(laFormation);
        hs_formation2.add(laFormation);
        stagiaire1.setStages(hs_formation1);
        stagiaire2.setStages(hs_formation2);
        // Ici, on a donc 1 formation et deux stagiaires de connect�s
        // il ne reste plus que le Centre ouf !
        laFormation.setCentre(leCentre);       		
        
		// et maintenant on tente de sauvegarder tout cela
		Session session = HibernateHelper.getSession();
		Transaction tx = session.beginTransaction();
        session.save(laFormation);
        tx.commit();
		session.close();
		HibernateHelper.getSessionFactory().close();
	
        JdbcUtil.execRequete("select * from STAGIAIRES");
        JdbcUtil.execRequete("select * from FORMATIONS_STAGIAIRES");
        JdbcUtil.execRequete("select * from FORMATIONS");       
        JdbcUtil.execRequete("select * from CENTRES");
		
	}
	
	public static void creeSchema() {
		// commence par supprimer les tables qui existeraient
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`formations_stagiaires`;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`formations`;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`stagiaires`;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`centres`;");
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
		
		String rqt = null;
		// cr�e les 4 tables des entites		
		rqt = "CREATE TABLE  `test`.`centres` (`id` bigint(20) NOT NULL,`nom` varchar(255) default NULL,`adresse` varchar(255) default NULL, PRIMARY KEY  (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		JdbcUtil.execUpdate(rqt);
		rqt = "CREATE TABLE  `test`.`formations` (`id` bigint(20) NOT NULL,`intitule` varchar(100) default NULL,`DATE_DEBUT` date default NULL,`duree` int(11) default NULL,`ID_CENTRE` bigint(20) default NULL,PRIMARY KEY  (`id`),KEY `FK304079E21013A665` (`ID_CENTRE`),CONSTRAINT `FK304079E21013A665` FOREIGN KEY (`ID_CENTRE`) REFERENCES `centres` (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		JdbcUtil.execUpdate(rqt);
		rqt = "CREATE TABLE  `test`.`stagiaires` (`uid` bigint(20) NOT NULL,`prenom` varchar(20) default NULL,`nom` varchar(20) default NULL,PRIMARY KEY  (`uid`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		JdbcUtil.execUpdate(rqt);		
		// cr�e les 2 tables de jointure (assez hideux comme ordres SQL, mais �a marche...)		
		rqt = "CREATE TABLE  `test`.`formations_stagiaires` (`ID_FORMATION` bigint(20) NOT NULL,`ID_STAGIAIRE` bigint(20) NOT NULL," +
				"PRIMARY KEY  (`ID_FORMATION`,`ID_STAGIAIRE`), KEY `FK912F50336E387AB` (`ID_STAGIAIRE`), KEY `FK912F50333625453` (`ID_FORMATION`)," + 
				"CONSTRAINT `FK912F50333625453` FOREIGN KEY (`ID_FORMATION`) REFERENCES `formations` (`id`),CONSTRAINT `FK912F50336E387AB` FOREIGN KEY (`ID_STAGIAIRE`) REFERENCES `stagiaires` (`uid`)" + 
				") ENGINE=InnoDB DEFAULT CHARSET=latin1;";	
		JdbcUtil.execUpdate(rqt);				
	}
	
	
}
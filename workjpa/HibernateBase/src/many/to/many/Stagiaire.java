package many.to.many;
import java.util.HashSet;
import java.util.Set;

public class Stagiaire {

    private Long id;
    private String prenom;
    private String nom;
    private Set <Formation> stages = new HashSet<Formation>();

    public Stagiaire() {
    }

    public Stagiaire(String prenom, String nom) {
        setPrenom(prenom);
        setNom(nom);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Formation> getStages() {
        return this.stages;
    }

    public void setStages(Set<Formation> stages) {
        this.stages = stages;
    }

   

}

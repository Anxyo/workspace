package many.to.many;
public class Centre {

    private Long id;
    private String nom;
    private String adresse;
    public Centre(){}	// nécessaire
    public Centre(String nomCentre){
    	nom = nomCentre;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}

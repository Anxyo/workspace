package many.to.many;
import java.util.*;

public class Formation {

	private Long id;
	private String intitule;
	private Date dateDebut;
	private int duree;
	private Set <Stagiaire> stagiaires;
    private Centre centre;

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String nom) {
        this.intitule = nom;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public Set<Stagiaire> getStagiaires() {
        return stagiaires;
    }

    public void setStagiaires(Set<Stagiaire> stagiaires) {
        this.stagiaires = stagiaires;
    }

	public Centre getCentre() {
		return centre;
	}

	public void setCentre(Centre centre) {
		this.centre = centre;
	}

	

}

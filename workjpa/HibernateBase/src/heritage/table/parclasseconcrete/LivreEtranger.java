package heritage.table.parclasseconcrete;

import java.util.*;

public class LivreEtranger extends Livre { 

	  private String langue; 
	  
	  
	  public LivreEtranger() {
	  }

	  public LivreEtranger(String title, String auteur, Date dateAchat, double prix, String langue) {
	    super(title, auteur, dateAchat, prix);

	    this.langue = langue;
	  }

	  public void setLangue(String s) {
	    langue = s;
	  }

	  public String getLangue() {
	    return langue;
	  }

	}

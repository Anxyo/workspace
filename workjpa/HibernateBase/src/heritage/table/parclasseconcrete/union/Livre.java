package heritage.table.parclasseconcrete.union;

import java.util.*;

public class Livre {
    int id;
    String titre; 
    String auteur;
    Date	dateAchat; 
    double prix;

    public Livre() {
    }

    public Livre(String titre, String auteur, Date dateAchat, double prix) {
      this.titre = titre;
      this.auteur = auteur;
      this.dateAchat = dateAchat;
      this.prix = prix;
    }

    public void setId(int id) { 
      this.id = id;
    }

    public int getId(){ 
      return id;
    }

    public void setTitre(String titre) { 
      this.titre = titre;
    }

    public String getTitre() { 
      return titre;
    }

    public void setAuteur(String auteur) { 
       this.auteur = auteur;
    }

    public String getAuteur() { 
      return auteur;
    }

   

    public Date getDateAchat() {
		return dateAchat;
	}

	public void setDateAchat(Date dateAchat) {
		this.dateAchat = dateAchat;
	}

	public void setPrix(double prix) { 
      this.prix = prix;
    }

    public double getPrix() { 
      return prix;
    }
  }
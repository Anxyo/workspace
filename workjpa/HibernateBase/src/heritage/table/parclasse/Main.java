package heritage.table.parclasse;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import commun.HibernateHelper;
import commun.JdbcUtil;

public class Main {
   public static void main(String[] args) throws Exception {
	  JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
      JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`livres_et`;");
      JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`livres_tn`;");
      JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`livres`;");
      JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
      
      String rqt = "CREATE TABLE  `test`.`livres` (`id` int(11) NOT NULL,`titre` varchar(255) default NULL,`auteur` varchar(255) default NULL," +
       				"`dateAchat` date default NULL,`prix` double default NULL, PRIMARY KEY  (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
      JdbcUtil.execUpdate(rqt);
      rqt = "CREATE TABLE  `test`.`livres_et` (`id` int(11) NOT NULL,`langue` varchar(255) default NULL, PRIMARY KEY  (`id`)," +
        	"KEY `FKF8BF88877E9DF698` (`id`),CONSTRAINT `FKF8BF88877E9DF698` FOREIGN KEY (`id`) REFERENCES `livres` (`id`)" +
        	") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
      JdbcUtil.execUpdate(rqt);
      rqt = "CREATE TABLE  `test`.`livres_tn` (`id` int(11) NOT NULL,`noExemplaire` varchar(255) default NULL, PRIMARY KEY  (`id`)," + 
        	"KEY `FKF8BF8A527E9DF698` (`id`), CONSTRAINT `FKF8BF8A527E9DF698` FOREIGN KEY (`id`) REFERENCES `livres` (`id`)" + 
        	") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
      JdbcUtil.execUpdate(rqt);
      
      Session session = HibernateHelper.getSession();
      Transaction tx = session.beginTransaction();
      Livre leLivre = new Livre("M�moire d'un �ne", "Cadichon", new Date(), 9.99);
      LivreTirageNumerote secd = new LivreTirageNumerote("Estampes japonaises", "Shung Hia Yen", new Date(), 99.99, "Japonais");
      LivreEtranger icd = new LivreEtranger("My Taylor is Very Rich", "Elisabeth Taylor", new Date(), 9.99, "Anglais");

      session.save(leLivre);
      session.save(secd);
      session.save(icd);

      tx.commit();
      session.close();
	  HibernateHelper.getSessionFactory().close();
	  
      JdbcUtil.execRequete("select * from LIVRES");
      JdbcUtil.execRequete("select * from LIVRES_ET");
      JdbcUtil.execRequete("select * from LIVRES_TN");
   }
}

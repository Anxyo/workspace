package heritage.table.unique;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;

import commun.HibernateHelper;
import commun.JdbcUtil;

public class Main {
   public static void main(String[] args) throws Exception {
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
	   JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`livres`;");
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
	   
	   String rqt = "CREATE TABLE  `test`.`livres` (`id` int(11) NOT NULL,`TYPE_LIVRE` varchar(255) NOT NULL," + 
	     			"`titre` varchar(255) default NULL,`auteur` varchar(255) default NULL,`dateAchat` date default NULL," +
	     			"`prix` double default NULL,`noExemplaire` varchar(255) default NULL,`langue` varchar(255) default NULL," +
	     			" PRIMARY KEY  (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
      JdbcUtil.execUpdate(rqt);    

      Session session = HibernateHelper.getSession();
      Transaction tx = session.beginTransaction();
      Livre leLivre = new Livre("La guerre des mondes", "HG Wells", new Date(), 9.99);
      LivreTirageNumerote ltn = new LivreTirageNumerote("J'apprend le papou m�di�val", "Raoul Skornik", new Date(), 19.99, "0001A");
      LivreEtranger le = new LivreEtranger("My Way", "Frank Sinatra", new Date(), 29.99, "Anglais");

      session.save(leLivre);
      session.save(ltn);
      session.save(le);
      
      tx.commit();      
      session.close();
      HibernateHelper.getSessionFactory().close();
      JdbcUtil.execRequete("select * from LIVRES");
   }
}

package heritage.table.unique;
import java.util.*;

public class LivreTirageNumerote extends Livre { 

  private String noExemplaire;
  
  public LivreTirageNumerote() {
  }

    public LivreTirageNumerote(String titre, String auteur, Date dateAchat, double prix, String noExemplaire) {
      super(titre, auteur, dateAchat, prix);

      this.noExemplaire = noExemplaire;
    }

  public void setNoExemplaire(String s) {
    noExemplaire = s;
  }

  public String getNoExemplaire() {
    return noExemplaire;
  }
}

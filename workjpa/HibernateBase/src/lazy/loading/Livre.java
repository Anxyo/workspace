package lazy.loading;
import java.util.*;

public class Livre {
  private int id;
  private String nom;
  private Collection<Chapitre> chapitres = new ArrayList<Chapitre>();

  public Livre(){	  
  }

  public Livre(String name) {
    this.nom = name;
  }

  public void setId(int i) {
    id = i;
  }

  public int getId() {
    return id;
  }

  public void setNom(String n) {
    nom = n;
  }

  public String getNom() {
    return nom;
  }

  public void setChapitres(Collection<Chapitre> l) {
    chapitres = l;
  }

  public Collection<Chapitre> getChapitres() {
    return chapitres;
  }  
}
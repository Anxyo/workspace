package hql.select;

import java.util.*;

public class Fournisseur
{
    private int id;
    private String nom;
    private Collection<Produit> produits = new ArrayList<Produit>();
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Collection<Produit> getProduits() {
		return produits;
	}
	public void setProduits(Collection<Produit> produits) {
		this.produits = produits;
	}
}

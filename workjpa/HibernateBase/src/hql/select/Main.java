package hql.select;

import java.util.*;
import commun.*;

import org.hibernate.*;

public class Main {
	
	public static void main(String[] args) {
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`produits`;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`fournisseurs`;");
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
		
		String rqt = "CREATE TABLE  `test`.`fournisseurs` (`ID` int(11) NOT NULL,`NOM` varchar(255) default NULL," + 
					"PRIMARY KEY  (`ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		JdbcUtil.execUpdate(rqt);
		rqt = 	"CREATE TABLE  `test`.`produits` (`ID` int(11) NOT NULL, `NOM` varchar(255) default NULL," +
				"`DESCRIPTION` varchar(255) default NULL,`PRIX` double default NULL,`ID_FOURNISSEUR` int(11) default NULL," +
				" PRIMARY KEY  (`ID`), KEY `FKF2D1D7EAEE411957` (`ID_FOURNISSEUR`), CONSTRAINT `FKF2D1D7EAEE411957` " +
				"FOREIGN KEY (`ID_FOURNISSEUR`) REFERENCES `fournisseurs` (`ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		JdbcUtil.execUpdate(rqt);
		
		Session session = HibernateHelper.getSession();
		Transaction tx = session.beginTransaction();
		creeData(session);
		 
		// au minimum
		Query q = session.createQuery("from Produit p");
		List<?> l = q.list();
		afficheListeProduits(l);
		
		// avec une jointure
		q = session.createQuery("select f.nom, p.nom from Fournisseur f join f.produits p where p.fournisseur = 1");
		l = q.list();
		System.out.println("Liste des Produits du Fournisseur 1");
		System.out.println("==================================");
		for(Object elt : l){
			Object [] t = (Object [])elt;  // chacun des elt est un tableau d'Object
			System.out.println(t[0] + "  " + t[1]);
		}
		System.out.println("==================================");

		// retrouve une requete nomm�e dans le fichier de mapping
		q = session.getNamedQuery("prixHQL");
		l = q.list();
		System.out.println("Liste des prix :");
		System.out.println("=================");
		for(Object elt : l){
			System.out.println("Prix : " + elt);
		}
		System.out.println("=================");
		tx.commit();
		session.close();
		HibernateHelper.getSessionFactory().close();
		
		JdbcUtil.execRequete("select * from Fournisseurs");
		JdbcUtil.execRequete("select * from Produits");
	}

	
	// Routines utilitaires
	///////////////////////
	static public void afficheListeFournisseurs(List<?> list) {
    	// on pourrait faire un foreach mais bon...
        Iterator<?> iter = list.iterator();
        if (!iter.hasNext()) {
            System.out.println("Pas de fournisseurs � lister !");
            return;
        }        
        while (iter.hasNext()) {
            Fournisseur fourn = (Fournisseur) iter.next();
            String msg = fourn.getNom();
            System.out.println(msg);
        }
    }

	static public void afficheListeProduits(List<?> list) {
    	// on pourrait faire un foreach mais bon...
        Iterator<?> iter = list.iterator();
        if (!iter.hasNext()) {
            System.out.println("Pas de produits � lister !");
            return;
        }        
        while (iter.hasNext()) {
            Produit p = (Produit) iter.next();
            String msg = p.getNom();
            System.out.println(msg);
        }
    }

	private static void creeData(Session s){
        
        Fournisseur fourn1 = new Fournisseur();
        fourn1.setNom("Fournisseur 1");
        s.save(fourn1);
        
        Fournisseur fourn2 = new Fournisseur();
        fourn2.setNom("Fournisseur 2");
        s.save(fourn2);        
        
        Produit produit1 = new Produit("Produit 1","Description du produit 1", 2.10);
        produit1.setFournisseur(fourn1);
        fourn1.getProduits().add(produit1);
        s.save(produit1);
        
        Produit produit2 = new Produit("Produit 2","Description du produit 2", 11.45);
        produit2.setFournisseur(fourn1);
        fourn1.getProduits().add(produit2);        
        s.save(produit2);
        
        Produit produit3 = new Produit("Produit 3", "Description du produit 3", 2.25);
        produit3.setFournisseur(fourn2);
        fourn2.getProduits().add(produit3);
        s.save(produit3);        
	}
	
}
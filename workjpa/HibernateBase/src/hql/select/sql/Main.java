package hql.select.sql;

import java.util.Iterator;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.type.StandardBasicTypes;

import commun.HibernateHelper;
import commun.JdbcUtil;


public class Main {
	
	public static void main(String[] args) {
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`produits`;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`fournisseurs`;");
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
		
		String rqt = "CREATE TABLE  `test`.`fournisseurs` (`ID` int(11) NOT NULL,`NOM` varchar(255) default NULL," + 
					"PRIMARY KEY  (`ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		JdbcUtil.execUpdate(rqt);
		rqt = 	"CREATE TABLE  `test`.`produits` (`ID` int(11) NOT NULL, `NOM` varchar(255) default NULL," +
				"`DESCRIPTION` varchar(255) default NULL,`PRIX` double default NULL,`ID_FOURNISSEUR` int(11) default NULL," +
				" PRIMARY KEY  (`ID`), KEY `FKF2D1D7EAEE411957` (`ID_FOURNISSEUR`), CONSTRAINT `FKF2D1D7EAEE411957` " +
				"FOREIGN KEY (`ID_FOURNISSEUR`) REFERENCES `fournisseurs` (`ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		JdbcUtil.execUpdate(rqt);
		
		Session session = HibernateHelper.getSession();
		creeData(session);
		
		String sql = "select avg(produits.prix) as prixMoyen from Produits produits";
		SQLQuery r = session.createSQLQuery(sql);
		r.addScalar("prixMoyen",StandardBasicTypes.DOUBLE);
		List<?> resultats = r.list();
		affichePrix(resultats);
		
        String sqlAvecAlias = "select {f.*} from Fournisseurs f";
        SQLQuery query = session.createSQLQuery(sqlAvecAlias);
        query.addEntity("f", Fournisseur.class);
        resultats = query.list();
        afficheFournisseurs(resultats);
		session.close();
		HibernateHelper.getSessionFactory().close();
        
        JdbcUtil.execRequete("select * from Fournisseurs");
        JdbcUtil.execRequete("select * from Produits");

	}
    static public void afficheFournisseurs(List<?> liste) {
        Iterator<?> iter = liste.iterator();
        if (!iter.hasNext()) {
            System.out.println("Pas de fournisseur � afficher !");
            return;
        }        
        while (iter.hasNext()) {
            Fournisseur fourn = (Fournisseur) iter.next();
            System.out.println("Nom du fournisseur : " + fourn.getNom());
        }
    }

    static public void affichePrix(List<?> liste) {
        Iterator<?> iter = liste.iterator();
        if (!iter.hasNext()) {
            System.out.println("Pas de prix � afficher !");
            return;
        }        
        while (iter.hasNext()) {
            Double d = (Double) iter.next();
            System.out.println("Prix : " + d);
        }
    }
 


	private static void creeData(Session session){

        Transaction t = session.beginTransaction();
		Fournisseur f1 = new Fournisseur();
        f1.setNom("Fournisseur 1");
        session.save(f1);
        
        Fournisseur f2 = new Fournisseur();
        f2.setNom("Fournisseur 2");
        session.save(f2);        
        
        Produit p1 = new Produit("Produit 1","Desription du produit 1", 2.25);
        p1.setFournisseur(f1);
        f1.getProduits().add(p1);
        session.save(p1);
        
        Produit p2 = new Produit("Produit 2","Description du produit 2", 22.0);
        p2.setFournisseur(f1);
        f1.getProduits().add(p2);        
        session.save(p2);
        
        Produit p3 = new Produit("Produit 3", "Description du produit 3", 35.0);
        p3.setFournisseur(f2);
        f2.getProduits().add(p3);
        session.save(p3);
        t.commit();
	}
}
package hql.select.sql;
public class Produit
{
    private int id;
    private Fournisseur fournisseur;
    
    private String nom;
    private String description;
    private double prix;
    
    public Produit()
    {
        super();
    }
    
    public Produit(String nom, String description, double prix)
    {
        super();
        this.nom = nom;
        this.description = description;
        this.prix = prix;
    }
    
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getNom()
    {
        return nom;
    }
    public void setNom(String nom)
    {
        this.nom = nom;
    }
 
    public Fournisseur getFournisseur()
    {
        return fournisseur;
    }
    public void setFournisseur(Fournisseur fournisseur)
    {
        this.fournisseur = fournisseur;
    }
    
    public double getPrix()
    {
        return prix;
    }
    public void setPrix(double prix)
    {
        this.prix = prix;
    }
}

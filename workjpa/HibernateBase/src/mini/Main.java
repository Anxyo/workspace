package mini;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;
import commun.HibernateHelper;
import commun.JdbcUtil;

public class Main {
	
	
	public static void main(String[] args) {
		
		// Cr�ation la table dont on a besoin
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`evenements`;");
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
		
		String rqtCreation = "CREATE TABLE  `test`.`evenements` (`EVENT_ID` bigint(20) NOT NULL auto_increment, ";
		rqtCreation += "`EVENT_DATE` datetime default NULL, `EVENT_NOM` varchar(255) default NULL, PRIMARY KEY  (EVENT_ID))";
		rqtCreation += " ENGINE=InnoDB;";
		JdbcUtil.execUpdate(rqtCreation);
		
		// Noter que la m�me chose peut �tre faite avec l'utilitaire SchemaExport Hibernate
		
		// ouverture ressources hibernate
		Session session = HibernateHelper.getSession();
		Transaction tx = session.beginTransaction();
		
		Evenement evnt = new Evenement();
		evnt.setNom("Un �v�nement ordinaire");
		evnt.setDate(new Date());
		session.save(evnt);
		tx.commit();
		
		// fermeture ressources hibernate		
		session.close();
		//HibernateHelper2.getSessionFactory().close();

        // maintenant qu'il a �t� �crit, on le recharge

		// r�-ouverture ressources hibernate
		session = HibernateHelper.getSession();
		tx = session.beginTransaction();
		
		// on le retrouve par sa cl� primaire (que l'on est suppos� connaitre...)
		Evenement e = (Evenement)session.get(Evenement.class, new Long(1));
		System.out.println(e);	
		tx.commit();
		
		// fermeture ressources hibernate		
		session.close();
		HibernateHelper.getSessionFactory().close();
		
		// v�rification des donn�es dans la table EVENEMENTS
		JdbcUtil.execRequete("select * from evenements;");
	}
}
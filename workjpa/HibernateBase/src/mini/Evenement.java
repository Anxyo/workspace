package mini;

import java.util.Date;

public class Evenement {
	private Long id;

	private String nom;

	private Date date;

	Evenement() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String toString() {
		return "ID :" + id + "\nNom :" + nom + "\nDate :" + date;
	}
}
package commun;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;


public class JdbcUtil {

    static Connection cnx; 
    static Statement st;

    
	public static void execUpdate(String sql) {
		try {
			// Charge le driver
			Class.forName("com.mysql.jdbc.Driver");
			//System.out.println("Driver charg�");
			// Connexion � la base
			String url = "jdbc:mysql://localhost/test";
			cnx = DriverManager.getConnection(url, "root", "arati");
			//System.out.println("Connexion effectu�e");
			st = cnx.createStatement();
			st.executeUpdate(sql);
			cnx.close();
		} catch (Exception e) {
			System.err.println("Exception !");
			e.printStackTrace();
			System.exit(0);	// on s'en va !
		}
	}
	// teste le contenu des tables
	public static void execRequete(String sql) {
		try {
			// Charge le driver
			Class.forName("com.mysql.jdbc.Driver");
			//System.out.println("Driver charg�");
			// Connexion � la base
			String url = "jdbc:mysql://localhost/test";
			cnx = DriverManager.getConnection(url, "root", "arati");
			//System.out.println("Connexion effectu�e");
			st = cnx.createStatement();
			ResultSet rs = st.executeQuery(sql);
			JdbcUtil.afficheResultSet(rs);
			cnx.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// affiche de fa�on g�n�rique le contenu d'un RS
    public static void afficheResultSet(ResultSet rs) throws Exception {
    
    ResultSetMetaData metadata = rs.getMetaData();

	int nbCols = metadata.getColumnCount();
	String[] titres = new String[nbCols]; 
	//int[] largeurs = new int[nbCols];
	//int[] posCols = new int[nbCols];
	//int largeurLigne;

	for (int i = 0; i < nbCols; i++) {
    	titres[i] = metadata.getColumnLabel(i + 1); // retrouve le nom de colonne
	    System.out.print(titres[i]+"  ");
	}
	System.out.println("------------------------------------------------------------------------");

	while (rs.next()) {
		for (int i = 0; i < nbCols; i++) {
			Object v = rs.getObject(i + 1);
			if(v == null){
			    System.out.print("       ");
			}else{
			    System.out.print(v.toString().trim()+"   ");
			}
			
		}
	    System.out.println("       ");
	}
}
    
}
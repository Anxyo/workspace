package commun;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateHelper {
	private static SessionFactory factory;

	private HibernateHelper() {
	}
	
	public static Session getSession() {
		return getSessionFactory().openSession();
	}

	public static SessionFactory getSessionFactory() throws HibernateException {
		if ((factory != null)&&(factory.isClosed())) {
			factory = null;		// pour ne pas travailler avec une factory dont les ressources ont �t� lib�r�es par un appel � close()
		}
		if (factory == null) {
			final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
					.configure() // configures settings from hibernate.cfg.xml
					.build();
			try {
				factory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
			}
			catch (Exception e) {
				// The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
				// so destroy it manually.
				StandardServiceRegistryBuilder.destroy( registry );
			}
		}  
		return factory;
	}

}


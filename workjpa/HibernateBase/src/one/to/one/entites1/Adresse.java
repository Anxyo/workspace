package one.to.one.entites1;

public class Adresse {
private Long id;
private String rue;
private String ville;
private String zip;
private Formateur habitant;

public Formateur getHabitant() {
	return habitant;
}
public void setHabitant(Formateur habitant) {
	this.habitant = habitant;
}

public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}

public String getRue() {
	return rue;
}
public void setRue(String rue) {
	this.rue = rue;
}
public String getVille() {
	return ville;
}
public void setVille(String ville) {
	this.ville = ville;
}
public String getZip() {
	return zip;
}
public void setZip(String zip) {
	this.zip = zip;
}

}

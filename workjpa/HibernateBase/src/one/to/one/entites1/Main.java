package one.to.one.entites1;

import org.hibernate.Session;
import org.hibernate.Transaction;

import commun.HibernateHelper;
import commun.JdbcUtil;

public class Main {
   public static void main(String[] args) throws Exception {
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
	   JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`formateurs`;");
	   JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`adresses`;");
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
	   
	   String createRqt = "CREATE TABLE  adresses (uid bigint(20) NOT NULL, rue varchar(40) default NULL, ville varchar(30) default NULL, "
	  					 + " zip varchar(5) default NULL,  PRIMARY KEY (uid)) ENGINE=InnoDB;";	  
      JdbcUtil.execUpdate(createRqt);   

      // Grace � FOREIGN KEY ... REFERENCE : impossible de cr�er des Adresses si elles ne sont pas li�es � un Formateur 
      // (liaison bidirectionnelle demand�e par le mapping via property-ref="adressePerso"
	  createRqt = "CREATE TABLE  formateurs (uid bigint(20) NOT NULL, prenom varchar(20) default NULL,nom varchar(20) default NULL," 
		  				+ "ID_ADRESSE bigint(20) default NULL, PRIMARY KEY  (uid), UNIQUE KEY ID_ADRESSE (ID_ADRESSE), "
		  				+ "KEY `FK303EBF68C385D5BD` (`ID_ADRESSE`), CONSTRAINT `FK303EBF68C385D5BD` FOREIGN KEY (`ID_ADRESSE`) REFERENCES `adresses` (`uid`) "
		  				+ ") ENGINE=InnoDB;";
	  JdbcUtil.execUpdate(createRqt);  
      Session session = HibernateHelper.getSession();
      
      // init de nos objets
      Formateur f = new Formateur();
      f.setNom("G");
      f.setPrenom("JM");
      Adresse adr = new Adresse();
      adr.setRue("12 rue du Bois");
      adr.setVille("Orl�ans");
      adr.setZip("45000");
      f.setAdressePerso(adr);
      // code hibernate
      Transaction tx = null;
      try {
           tx = session.beginTransaction();
           session.save(f);
           tx.commit();
      } catch (Exception e) { 
           if (tx != null) {
             tx.rollback();
             throw e;		// pour voir
           }
      }  finally { 
           session.close();
      }
      
      session = HibernateHelper.getSession();
      f = null;
      tx = null;
      try {
           tx = session.beginTransaction();
           f = (Formateur)session.get(Formateur.class, new Long(1));
           System.out.println("Rue : " + f.getAdressePerso().getRue());
           tx.commit();
      } catch (Exception e) { 
           if (tx != null) {
             tx.rollback();
             throw e;		// pour voir
           }
      }  finally { 
    	  session.close();
      }
      HibernateHelper.getSessionFactory().close();	
      
      JdbcUtil.execRequete("select * from FORMATEURS");  // toujours pour voir
      JdbcUtil.execRequete("select * from ADRESSES");  // toujours pour voir      
      
   }
}

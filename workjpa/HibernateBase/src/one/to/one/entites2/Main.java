package one.to.one.entites2;

import org.hibernate.Session;
import org.hibernate.Transaction;
import commun.HibernateHelper;
import commun.JdbcUtil;

public class Main {
   public static void main(String[] args) throws Exception {
	   // init de la base, si besoin est (ordre � respecter � cause des contraintes de cl� �trang�re)
	   // d'abord suppression des tables si elles existent
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
	   JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`adresses`;");
	   JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`formateurs`;");
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");

	   String createRqt = "CREATE TABLE  `test`.`formateurs` (" +
	     "`uid` bigint(20) NOT NULL," + 
	     "`prenom` varchar(20) default NULL," +
	     "`nom` varchar(20) default NULL," +
	     " PRIMARY KEY  (`uid`)" +
	   ") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	   JdbcUtil.execUpdate(createRqt);

	   // doit etre fait dans un 2d temps car r�f�rence la table FORMATEURS
	   createRqt = "CREATE TABLE  `test`.`adresses` (" +
	        			"`uid` bigint(20) NOT NULL," + 
	        			"`rue` varchar(40) default NULL," +
	        			"`ville` varchar(30) default NULL," + 
	        			"`zip` varchar(5) default NULL," +
	        			"PRIMARY KEY  (`uid`)," +
	        			" KEY `FKFD34D5A4F61042AE` (`uid`)," +
	        			" CONSTRAINT `FKFD34D5A4F61042AE` FOREIGN KEY (`uid`) REFERENCES `formateurs` (`uid`) " +
	        			") ENGINE=InnoDB DEFAULT CHARSET=latin1;";   
       JdbcUtil.execUpdate(createRqt);   
      
      // le code utile...
      Session session = HibernateHelper.getSession();
      
      // le code utilis� est le m�me que celui de l'exemple one.to.one.entite1 puisque le r�sultat recherch�
      // est le m�me : une relation 1 � 1 bidirectionnelle entre entit�s.
      
      // init de nos objets
      Formateur f = new Formateur();
      f.setNom("G");
      f.setPrenom("JM");
      Adresse adr = new Adresse();
      adr.setRue("12 rue du Bois");
      adr.setVille("Orl�ans");
      adr.setZip("45000");
      adr.setHabitant(f);	// ne pas oublier car habitant sert pour la g�n�ration de la cl� primaire de Adresse
      f.setAdressePerso(adr);
      // code hibernate
      Transaction tx = null;
      try {
           tx = session.beginTransaction();
           session.save(f);
           tx.commit();
      } catch (Exception e) { 
           if (tx != null) {
             tx.rollback();
             throw e;		// pour voir
           }
      }  finally { 
           session.close();
      }
      
      session = HibernateHelper.getSession();
      f = null;
      tx = null;
      try {
           tx = session.beginTransaction();
           f = (Formateur)session.get(Formateur.class, new Long(1));
           System.out.println("Rue : " + f.getAdressePerso().getRue());
           tx.commit();
      } catch (Exception e) { 
           if (tx != null) {
             tx.rollback();
             throw e;		// pour voir
           }
      }  finally { 
    	  session.close();
      }
      HibernateHelper.getSessionFactory().close();	
    
      // dans l'affichage qui suit on contate bien l'�galit� des cl�s primaires
      JdbcUtil.execRequete("select * from FORMATEURS");  // toujours pour voir
      JdbcUtil.execRequete("select * from ADRESSES");  // toujours pour voir      
   }
}

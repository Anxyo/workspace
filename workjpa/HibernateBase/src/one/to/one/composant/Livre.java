package one.to.one.composant;


public class Livre {
  private int id;
  private String nom;
  private Couverture couverture;

  public Livre(){
  }

  public Livre(String name) {
    this.nom = name;
  }

  public void setId(int i) {
    id = i;
  }

  public int getId() {
    return id;
  }

  public void setNom(String n) {
    nom = n;
  }

  public String getNom() {
    return nom;
  }

public Couverture getCouverture() {
	return couverture;
}

public void setCouverture(Couverture couverture) {
	this.couverture = couverture;
}

}
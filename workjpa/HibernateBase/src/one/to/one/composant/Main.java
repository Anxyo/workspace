package one.to.one.composant;

import org.hibernate.Session;
import org.hibernate.Transaction;

import commun.HibernateHelper;
import commun.JdbcUtil;

public class Main {
   public static void main(String[] args) throws Exception {
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
	  JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`LIVRES`;");
	  JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
	  
	  String createRqt = "CREATE TABLE  LIVRES (id int(11) NOT NULL, NOM varchar(255) default NULL,";
	  createRqt += " titre varchar(255) default NULL, commentaire varchar(255) default NULL, PRIMARY KEY (id) ) ENGINE=InnoDB;";
      JdbcUtil.execUpdate(createRqt);    
   
      Session session = HibernateHelper.getSession();
      // init de nos objets
      Livre l = new Livre("La vie sans code, c'est possible !");
      Couverture c = new Couverture();
      c.setCommentaire("L'un des meilleurs livres de la rentr�e...");
      c.setTitre("Le titre de la 4eme de couverture");
      l.setCouverture(c);
      c.setLivre(l);	// navigagtion oblige. Rarement utilis� pour les composants !
      
      // code hibernate
      Transaction tx = null;
      try {
           tx = session.beginTransaction();
           session.save(l);
           tx.commit();
      } catch (Exception e) { 
           if (tx != null) {
             tx.rollback();
             throw e;		// pour voir
           }
      }  finally { 
           session.close();
      }
      
      JdbcUtil.execRequete("select * from LIVRES");  // toujours pour voir
      
   }
}

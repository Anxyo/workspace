package one.to.many.bag;

// manque cruel d'imagination
public class Chapitre {
  private int id;
  private String info;
  private Livre livre;

  public Livre getLivre() {
	return livre;
}

public void setLivre(Livre livre) {
	this.livre = livre;
}

public Chapitre(){
  }

  public Chapitre(String info) {
    this.info = info;
  }

  public void setId(int i) {
    id = i;
  }

  public int getId() {
    return id;
  }

  public void setInfo(String n) {
    info = n;
  }

  public String getInfo() {
    return info;
  }
  public String toString(){
	return "id = " + this.id + ", info = " + this.info;  
  }
}
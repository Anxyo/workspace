package one.to.many.bag;
import org.hibernate.*;

import commun.HibernateHelper;
import commun.JdbcUtil;

public class Main {
   public static void main(String[] args) throws Exception {
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
	   JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`chapitres`;");
	   JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`livres`;");
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
		
	   String rqt1 = "CREATE TABLE  `test`.`chapitres` (`id` int(11) NOT NULL," +
	     			"`info` varchar(255) default NULL,`ID_LIVRE` int(11) NOT NULL,`id_parent` int(11) default NULL," +
	     			"PRIMARY KEY  (`id`), KEY `FK6288A197CCAAD0` (`id_parent`),KEY `FK6288A1BB34F4EA` (`ID_LIVRE`)," +
	     			"CONSTRAINT `FK6288A1BB34F4EA` FOREIGN KEY (`ID_LIVRE`) REFERENCES `livres` (`id`) " +
	     			") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	   String rqt2 = "CREATE TABLE  `test`.`livres` (`id` int(11) NOT NULL,`nom` varchar(255) default NULL," +
	     			 "PRIMARY KEY  (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
      JdbcUtil.execUpdate(rqt2);    
      JdbcUtil.execUpdate(rqt1);    

      Session session = HibernateHelper.getSession();
      // le livre
      Livre leLivre = new Livre("Hibernate pour les nuls");
      // et ses quelques chapitres (le bag est peut etre pas la meilleure id�e car pas d'ordre pr�serv�
      // ce qui pour des chapitres peut etre g�nant, mais bon...).
      Chapitre chap1 = new Chapitre("Chapitre 1");
      chap1.setLivre(leLivre);						// bidirectionnel !
      Chapitre chap2 = new Chapitre("Chapitre 2");
      chap2.setLivre(leLivre);						// bidirectionnel !
      leLivre.getChapitres().add(chap1);
      leLivre.getChapitres().add(chap2);

      Transaction transaction = null;
      try {
           transaction = session.beginTransaction();
           session.save(leLivre);
           transaction.commit();
      } catch (Exception e) { 
           if (transaction != null) {
             transaction.rollback();
             throw e;
           }
      }  finally { 
           session.close();
      }
      HibernateHelper.getSessionFactory().close();
      JdbcUtil.execRequete("SELECT * FROM LIVRES");
      JdbcUtil.execRequete("SELECT * FROM CHAPITRES");      
   }
}

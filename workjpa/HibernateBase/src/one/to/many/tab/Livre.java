package one.to.many.tab;

public class Livre {
  private int id;
  private String nom;
  private Chapitre[] chapitres;

  public Livre(){
  }

  public Livre(String name) {
    this.nom = name;
  }

  public void setId(int i) {
    id = i;
  }

  public int getId() {
    return id;
  }

  public void setNom(String n) {
    nom = n;
  }

  public String getNom() {
    return nom;
  }

  public void setChapitres(Chapitre[] l) {
    chapitres = l;
  }

  public Chapitre[] getChapitres() {
    return chapitres;
  }
}
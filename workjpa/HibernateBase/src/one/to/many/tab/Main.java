package one.to.many.tab;

import org.hibernate.Session;
import org.hibernate.Transaction;

import commun.HibernateHelper;
import commun.JdbcUtil;

public class Main {
   public static void main(String[] args) throws Exception {
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
	   JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`chapitres`;");
	   JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`livres`;");
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
	   
	   String rqt2 = "CREATE TABLE  `test`.`chapitres` (`id` int(11) NOT NULL,`info` varchar(255) default NULL,`ID_PARENT` int(11) default NULL," + 
	     				"`IDX` int(11) default NULL, PRIMARY KEY  (`id`), KEY `FK6288A1FC1D509D` (`ID_PARENT`), CONSTRAINT `FK6288A1FC1D509D` FOREIGN KEY (`ID_PARENT`) REFERENCES `livres` (`id`)" +
	     				") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	   
	   String rqt1 = "CREATE TABLE  `test`.`livres` (`id` int(11) NOT NULL,`nom` varchar(255) default NULL,PRIMARY KEY  (`id`)" + 
	   					") ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	   
      JdbcUtil.execUpdate(rqt1);    
      JdbcUtil.execUpdate(rqt2);    

      Session session = HibernateHelper.getSession();

      Livre leLivre = new Livre("Hibernate pour les nuls");    
      leLivre.setChapitres(new Chapitre[]{new Chapitre("Chapitre 1"), new Chapitre("Chapitre 2")});
      Transaction transaction = null;

      try {
           transaction = session.beginTransaction();
           session.save(leLivre);
           transaction.commit();
      } catch (Exception e) { 
           if (transaction != null) {
             transaction.rollback();
             throw e;
           }
      }  finally { 
           session.close();
      }
      HibernateHelper.getSessionFactory().close();
      JdbcUtil.execRequete("select * from LIVRES");
      JdbcUtil.execRequete("select * from CHAPITRES");      
   }
}

package one.to.many.list;
import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.Transaction;

import commun.HibernateHelper;
import commun.JdbcUtil;

public class Main {
   public static void main(String[] args) throws Exception {
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
	   JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`chapitres`;");
	   JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`livres`;");
	   JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
	   
	   String rqt1 = "CREATE TABLE  `test`.`livres` (`id` int(11) NOT NULL,`nom` varchar(255) default NULL," +
	     				"PRIMARY KEY  (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
	   String rqt2 = "CREATE TABLE  `test`.`chapitres` (`id` int(11) NOT NULL,`info` varchar(255) default NULL," +
	     				"`parent_id` int(11) default NULL,`idx` int(11) default NULL, PRIMARY KEY  (`id`), KEY `FK6288A1B0C337F0` (`parent_id`)," +
	     				" CONSTRAINT `FK6288A1B0C337F0` FOREIGN KEY (`parent_id`) REFERENCES `livres` (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
      JdbcUtil.execUpdate(rqt1);    
      JdbcUtil.execUpdate(rqt2);    

      Session session = HibernateHelper.getSession();

      Livre leLivre = new Livre("Hibernate pour les nuls");
     
      ArrayList<Chapitre> list = new ArrayList<Chapitre>();
      list.add(new Chapitre("Chapitre 1"));
      list.add(new Chapitre("Chapitre 2"));
      leLivre.setChapitres(list);

      Transaction transaction = null;

      try {
           transaction = session.beginTransaction();
           session.save(leLivre);
           transaction.commit();
      } catch (Exception e) { 
           if (transaction != null) {
             transaction.rollback();
             throw e;
           }
      }  finally { 
           session.close();
      }
      HibernateHelper.getSessionFactory().close();
      JdbcUtil.execRequete("select * from LIVRES");
      JdbcUtil.execRequete("select * from CHAPITRES");      
   }
}

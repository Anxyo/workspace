package one.to.many.list;
import java.util.*;

public class Livre {
  private int id;
  private String nom;
   private List<Chapitre> chapitres;

  public Livre(){
  }

  public Livre(String name) {
    this.nom = name;
  }

  public void setId(int i) {
    id = i;
  }

  public int getId() {
    return id;
  }

  public void setNom(String n) {
    nom = n;
  }

  public String getNom() {
    return nom;
  }

  public void setChapitres(List<Chapitre> l) {
    chapitres = l;
  }

  public List<Chapitre> getChapitres() {
    return chapitres;
  }
}
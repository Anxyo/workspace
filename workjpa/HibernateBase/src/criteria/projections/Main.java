package criteria.projections;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;

import commun.HibernateHelper;
import commun.JdbcUtil;


public class Main {
	
	public static void main(String[] args) {
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 0;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`produits`;");
		JdbcUtil.execUpdate("DROP TABLE IF EXISTS `test`.`fournisseurs`;");
		JdbcUtil.execUpdate("SET FOREIGN_KEY_CHECKS = 1;");
		
		String rqt = "CREATE TABLE  `test`.`fournisseurs` (`ID` int(11) NOT NULL,`NOM` varchar(255) default NULL," + 
					"PRIMARY KEY  (`ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		JdbcUtil.execUpdate(rqt);
		rqt = 	"CREATE TABLE  `test`.`produits` (`ID` int(11) NOT NULL, `NOM` varchar(255) default NULL," +
				"`DESCRIPTION` varchar(255) default NULL,`PRIX` double default NULL,`ID_FOURNISSEUR` int(11) default NULL," +
				" PRIMARY KEY  (`ID`), KEY `FKF2D1D7EAEE411957` (`ID_FOURNISSEUR`), CONSTRAINT `FKF2D1D7EAEE411957` " +
				"FOREIGN KEY (`ID_FOURNISSEUR`) REFERENCES `fournisseurs` (`ID`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		JdbcUtil.execUpdate(rqt);
		
		Session session = HibernateHelper.getSession();
		Transaction tx = session.beginTransaction();
		creeData(session);
		
        Criteria crit = session.createCriteria(Produit.class);
        ProjectionList projList = Projections.projectionList();
        projList.add(Projections.max("prix"));
        projList.add(Projections.min("prix"));
        projList.add(Projections.avg("prix"));
        projList.add(Projections.countDistinct("description"));
        crit.setProjection(projList);
        List<?> l = crit.list();
        afficheListe(l);
		
        tx.commit();
		session.close();
		HibernateHelper.getSessionFactory().close();
		
		JdbcUtil.execRequete("select * from Fournisseurs");
		JdbcUtil.execRequete("select * from Produits");
	}
	static public void afficheListe(List<?> liste){
        Iterator<?> iter = liste.iterator();
        if (!iter.hasNext()){
            System.out.println("Pas de donn�es � afficher !");
            return;
        }
        // 1 seule ligne, mais bon...
        for (Object o : liste) {
        	Object [] t = (Object [])o;
        	System.out.printf("\n\n  max : %5.2f, min : %5.2f, moy.: %5.2f, nbre: %d\n\n", t[0],t[1],t[2],t[3]);
        }
    }

private static void creeData(Session s){
        

        Fournisseur fourn1 = new Fournisseur();
        fourn1.setNom("Fournisseur 1");
        s.save(fourn1);
        
        Fournisseur fourn2 = new Fournisseur();
        fourn2.setNom("Fournisseur 2");
        s.save(fourn2);        
        
        Produit produit1 = new Produit("Produit 1","Description du produit 1", 2.10);
        produit1.setFournisseur(fourn1);
        fourn1.getProduits().add(produit1);
        s.save(produit1);
        
        Produit produit2 = new Produit("Produit 2","Description du produit 2", 11.45);
        produit2.setFournisseur(fourn1);
        fourn1.getProduits().add(produit2);        
        s.save(produit2);
        
        Produit produit3 = new Produit("Produit 3", "Description du produit 3", 2.25);
        produit3.setFournisseur(fourn2);
        fourn2.getProduits().add(produit3);
        s.save(produit3);        
	}
	
}
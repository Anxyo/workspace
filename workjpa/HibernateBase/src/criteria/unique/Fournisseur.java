package criteria.unique;
import java.util.ArrayList;
import java.util.List;

public class Fournisseur
{
    private int id;
    private String nom;
    private List<Produit> produits = new ArrayList<Produit>();
    
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getNom()
    {
        return nom;
    }
    public void setNom(String name)
    {
        this.nom = name;
    }
    public List<Produit> getProduits()
    {
        return produits;
    }
    public void setProduits(List<Produit> products)
    {
        this.produits = products;
    }
}

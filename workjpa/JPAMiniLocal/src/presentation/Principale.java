package presentation;
// Avec Glassfish v3 :
// ------------------
// Pour lancer cet exemple, ajouter au chemin des classes les biblioth�ques :

//-  Dans le r�pertoire C:\GlassFish-Tools-Bundle-For-Eclipse-1.2\glassfishv3\glassfish\modules :
//      	javax.persistence.jar, org.eclipse.persistence.core.jar, org.eclipse.persistence.jpa.jar,
//			org.eclipse.persistence.antlr.jar, org.eclipse.persistence.asm.jar.

//-  Dans le r�pertoire C:\GlassFish-Tools-Bundle-For-Eclipse-1.2\glassfishv3\javadb\lib :
//           derbyclient.jar

// Avec Glassfish v.2
// ------------------
// ajouter au chemin des classes le jar : c:\glassfish\lib\toplink-essentials.jar 
// et activer la configuration adapt�e dans persistence.xml. 


import java.util.Collection;
import java.sql.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import metier.Employe;
import service.ServiceEmploye;

public class Principale {

    public static void main(String[] args) {
        initSQL();
    	EntityManagerFactory emf = 
            Persistence.createEntityManagerFactory("ServiceEmploye");
    EntityManager em = emf.createEntityManager();
    ServiceEmploye service = new ServiceEmploye(em);
    
    //  cr�e et persiste un employ� (contexte transactionnel requis)
    em.getTransaction().begin();
    Employe emp = service.creeEmploye(158, "Pierre Dupont", 45000);
    em.getTransaction().commit();
    System.out.println("Enregistr� : " + emp);
    
    // retrouve un employ� sp�cifique (pas de contexte transactionnel requis)
    emp = service.findEmploye(158);
    System.out.println("Trouv� : " + emp);
    
    // retrouve tous les employ�s (pas de contexte transactionnel requis)
    Collection<Employe> emps = service.findAllEmployes();
    for (Employe e : emps) 
        System.out.println("Employ� trouv� : " + e);
    
    // mise � jour de l'employ� (contexte transactionnel requis)
    em.getTransaction().begin();
    emp = service.augmenteSalaire(158, 1000);
    em.getTransaction().commit();
    System.out.println("Mis � jour : " + emp);

    // supression (contexte tx requis)
    em.getTransaction().begin();
    service.supprimeEmploye(158);
    em.getTransaction().commit();
    System.out.println("Employ� 158 supprim�");        
    
    // fermeture de l'EM et de l'EMF
    em.close();
    emf.close();
    }
    
    /**
     * Le r�le de cette m�thode est de cr�er la table EMPLOYE afin que le code JPA puisse fonctionner.
     * Elle utilise une syntaxe JDBC traditionnelle. On aurait pu �galement utiliser une t�che ant...
     */
    private static void initSQL(){
    	String nomDriver = "org.apache.derby.jdbc.ClientDriver";
    	String urlJdbc = "jdbc:derby://localhost:1527/TestDB;create=true";
    	String rqtSupprimeTable = "DROP TABLE EMPLOYE";
    	String rqtCreeTable = "CREATE TABLE EMPLOYE (ID INTEGER NOT NULL, NOM VARCHAR(255), SALAIRE BIGINT, NOMBRE INTEGER, PRIMARY KEY (ID))";
    	Connection cnx = null;
    	Statement st = null;
    	// chargement driver
    	try {
    		Class.forName(nomDriver).newInstance();   	
    	}
    	catch (Exception exc) {
    		System.out.println("Driver non trouve : " + exc.getMessage());
    	}
    	
    	// connexion
    	try {
    		   	cnx = DriverManager.getConnection(urlJdbc,"APP","APP");
    		   	System.out.println("Connexion effectu�e : " + (!cnx.isClosed()));
    	}
    	catch (SQLException exc) {
    		System.out.println("Pb Connexion : " + exc.getMessage());
    		return;
    	}
    	
    	// ex�cution des requ�tes
    	try {
    		st = cnx.createStatement();
    		st.executeUpdate(rqtSupprimeTable);
    	}
    	catch (SQLException exc) {
    		System.out.println("La table EMPLOYE n'existait pas : " + exc.getMessage());
    	}
    	try {
    		st.executeUpdate(rqtCreeTable);
    		System.out.println("La table EMPLOYE a bien �t� cr��e");
    	}
    	catch (SQLException exc) {
    		System.out.println("Erreur de cr�ation de table : " + exc.getMessage());
    	}
    	try {
        	cnx.close();
        	System.out.println("Connexion referm�e");
    	} 
    	catch (Exception exc) {}
    }
    
}


package service;

import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import metier.Employe;


public class ServiceEmploye {
    protected EntityManager em;

    public ServiceEmploye(EntityManager em) {
        this.em = em;
    }

    public Employe creeEmploye(int id, String nom, long salaire) {
        Employe emp = new Employe(id);
        emp.setNom(nom);
        emp.setSalaire(salaire);
        em.persist(emp);
        return emp;
    }

    public void supprimeEmploye(int id) {
        Employe emp = findEmploye(id);
        if (emp != null) {
            em.remove(emp);
        }
    }

    public Employe augmenteSalaire(int id, long augmentation) {
        Employe emp = em.find(Employe.class, id);
        if (emp != null) {
            emp.setSalaire(emp.getSalaire() + augmentation);
        }
        return emp;
    }

    public Employe findEmploye(int id) {
        return em.find(Employe.class, id);
    }

    @SuppressWarnings("unchecked")
	public Collection<Employe> findAllEmployes() {
        Query rqt = em.createQuery("SELECT e FROM Employe e");
        return (Collection<Employe>) rqt.getResultList();
    }
}


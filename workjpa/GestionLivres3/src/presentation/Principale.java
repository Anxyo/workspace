package presentation;

import static java.lang.System.out;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import commun.JPAHelper;
import metier.Client;
import metier.Editeur;
import service.GestionCommande;
import service.GestionEdition;
import service.ICommandes;
import service.IEdition;

public class Principale {

	public static void main(String[] args) {
		// objet complexe, lourd et threadsafe
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("EditeurLivres");
		// objet et non thread-safe
		EntityManager em1 = emf.createEntityManager();
		EntityManager em2 = emf.createEntityManager();
		
		IEdition ie = new GestionEdition(em2);
		Editeur ed = ie.creeEditeur("Gallimard", "12 rue de la Lune");
		ie.publieLivre(ed, "L'�tranger", "0321127420", "Albert Camus",
				new Date(), 20.50, new String[] {"Chapitre 1", "Chapitre 2", "Chapitre 3" }, 
									new int[] {	34, 42, 55 });
		ie.publieLivre(ed, "La peste", "0321127450", "Albert Camus", new Date(), 
				22.50, new String[] {"Chapitre 1", "Chapitre 2", "Chapitre 3" }, 
					   new int[] { 34, 53, 75 });
		List<Editeur> liste1 = ie.listeEditeurs();
		for (Editeur unEditeur : liste1) {
			out.println(unEditeur);
		}

		out.println("=============================================================================");
		// gestion des clients
		ICommandes ic = new GestionCommande(em1);
		Client clt = ic.creeClient("Lamy", "Marc", "12 rue des amis",
				"blamy@gmail.com");
		Client clt2 = ic.creeClient("Lamyx", "Marcx", "12 rue des amix",
				"blamyx@gmail.com");
		ic.creeCommande(clt, new Date(System.currentTimeMillis()), 34.50);
		ic.creeCommande(clt, new Date(System.currentTimeMillis()), 43.0);
		ic.creeCommande(clt2, new Date(System.currentTimeMillis()), 76.0);

		List<Client> liste2 = ic.listeClients();
		for (Client unClient : liste2) {
			out.println(unClient);
		}
		//JPAHelper.closeEMF();
		em1.close();
		em2.close();
		emf.close();
	}
}

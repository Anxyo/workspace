package metier;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@NamedQuery(name="Editeur.FindAll", query="select e from Editeur e")
@Entity
public class Editeur {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String nom, adresse;
	
	@OneToMany(	mappedBy="editeur", 
				cascade=CascadeType.ALL, 
				fetch=FetchType.EAGER)
	private Set<Livre> livres;
	
	public Editeur(){
		livres = new HashSet<Livre>();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public Set<Livre> getLivres() {
		return livres;
	}
	public void setLivres(Set<Livre> livres) {
		this.livres = livres;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Editeur id " + id + ", nom : " + nom + ", adresse " + adresse + "\n");
		for (Livre l : livres){
			sb.append(l);
		}
		return sb.toString();
	}
}

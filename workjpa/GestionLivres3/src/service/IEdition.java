package service;

import java.util.Date;
import java.util.List;

import metier.Editeur;
import metier.Livre;

public interface IEdition {
	// cr�ation d'un �diteur
	Editeur creeEditeur(String nom, String adresse);
	
	// publier un livre
	Livre publieLivre(	Editeur e, String titre, String isbn, 
						String auteur, Date datePub, Double prix, 
						String [] titresChapitres, int [] nbPagesChapitre);
	
	// lister les livres d'un �diteur
	List<Livre> listeLivres(Editeur e);
	
	// lister les �diteurs
	List<Editeur> listeEditeurs();
}

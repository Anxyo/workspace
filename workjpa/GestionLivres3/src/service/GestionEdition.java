package service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import metier.Chapitre;
import metier.Editeur;
import metier.Livre;

public class GestionEdition implements IEdition{
	//List<Editeur> listeEditeurs = new ArrayList<Editeur>();
	EntityManager em;
	
	public GestionEdition(EntityManager em) {
		this.em = em;
	}
	
	@Override
	public Editeur creeEditeur(String nom, String adresse) {
		Editeur ed = new Editeur();
		ed.setNom(nom);
		ed.setAdresse(adresse);
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		em.persist(ed);
		tx.commit();
		return ed;
	}

	@Override
	public Livre publieLivre(Editeur e, String titre, String isbn,
			String auteur, Date datePub, Double prix, String[] titresChapitres,
			int[] nbPagesChapitre) {
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		// recharge l'�diteur persist�
		Editeur edPersist = em.find(Editeur.class, e.getId());
		Livre l = new Livre();
		
		edPersist.getLivres().add(l);
		l.setEditeur(edPersist);
		
		l.setAuteur(auteur);
		l.setTitre(titre);
		l.setDatePublication(datePub);
		l.setPrix(prix);
		l.setNoISBN(isbn);
		
		for (int i = 0; i < titresChapitres.length ; i++) {
			Chapitre ch = new Chapitre();
			ch.setNo(i + 1);
			ch.setNbPages(nbPagesChapitre[i]);
			ch.setTitre(titresChapitres[i]);
			ch.setLivre(l);
			l.getChapitres().add(ch);
		}
		tx.commit();
		return l;
	}

	@Override
	public List<Livre> listeLivres(Editeur e) {
		Editeur edPersist = em.find(Editeur.class, e.getId());
		return new ArrayList<Livre>(edPersist.getLivres());
	}

	@Override
	public List<Editeur> listeEditeurs() {
		TypedQuery<Editeur> q1 = em.createNamedQuery("Editeur.FindAll", Editeur.class);
		List<Editeur> l = q1.getResultList();
		return l;
	}

}

package service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import metier.Chapitre;
import metier.Editeur;
import metier.Livre;

public class GestionEdition implements IEdition{
	//List<Editeur> listeEditeurs = new ArrayList<Editeur>();
	
	EntityManager em;
	
	public GestionEdition(EntityManager em2) {
		this.em = em2;
	}

	@Override
	public Editeur creeEditeur(String nom, String adresse) {
		
		em.getTransaction().begin();
		Editeur ed = new Editeur();
		ed.setNom(nom);
		ed.setAdresse(adresse);
		em.persist(ed);
		em.getTransaction().commit();
		//listeEditeurs.add(ed);
		return ed;
	}

	@Override
	public Livre publieLivre(Editeur e, String titre, String isbn,
			String auteur, Date datePub, Double prix, String[] titresChapitres,
			int[] nbPagesChapitre) {
		
		em.getTransaction().begin();
		Editeur edPersist = em.find(Editeur.class, e.getId());
		Livre l = new Livre();
		
		
		edPersist.getLivres().add(l);
		l.setEditeur(e);
				
		l.setAuteur(auteur);
		l.setTitre(titre);
		l.setDatePublication(datePub);
		l.setPrix(prix);
		l.setNoISBN(isbn);
		
		for (int i = 0; i < titresChapitres.length ; i++) {
			Chapitre ch = new Chapitre();
			// init les propriétés
			ch.setNo(i + 1);
			ch.setNbPages(nbPagesChapitre[i]);
			ch.setTitre(titresChapitres[i]);
			// connexion
			ch.setLivre(l);
			l.getChapitres().add(ch);
		}
		em.persist(l);
		em.getTransaction().commit();
		return l;
	}

	@Override
	public List<Livre> listeLivres(Editeur e) {
		Editeur edPersist = em.find(Editeur.class, e.getId());
		return new ArrayList<Livre>(edPersist.getLivres());
	}

	@Override
	public List<Editeur> listeEditeurs() {
		Query ql = em.createQuery("SELECT e FROM Editeur e", Editeur.class);
		List<Editeur> l = ql.getResultList();
		return l;
	}

}

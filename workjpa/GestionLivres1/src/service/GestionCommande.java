package service;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import metier.Client;
import metier.Commande;

public class GestionCommande implements ICommandes {
	//List<Client> listeClients = new ArrayList<Client>();

	EntityManager em;
	
	
	
	public GestionCommande(EntityManager em1) {
		this.em = em1;
	}

	@Override
	public Client creeClient(String nom, String prenom, String adresse,
			String email) {
		
		em.getTransaction().begin();
		
		Client clt = new Client();
		clt.setNom(nom);
		clt.setPrenom(prenom);
		clt.setAdresse(adresse);
		clt.setEmail(email);
		//listeClients.add(clt);
		
		em.persist(clt);
		em.getTransaction().commit();
		
		return clt;
	}

	@Override
	public Commande creeCommande(Client clt, Date dateCommande, Double montant) {
		
		em.getTransaction().begin();
		
		Commande cmd = new Commande();
		cmd.setDateCommande(dateCommande);
		cmd.setMontant(montant);
		cmd.setClient(clt);
		clt.getCommandes().add(cmd);
		
		em.persist(cmd);
		em.getTransaction().commit();
		
		return cmd;
	}

	@Override
	public List<Commande> listeCommandes(Client clt) {
		return clt.getCommandes();
	}

	@Override
	public List<Client> listeClients() {
		TypedQuery<Client> ql = em.createQuery("SELECT c FROM Client c", Client.class);
		List<Client> l = ql.getResultList(); 
		return l;
	}



}

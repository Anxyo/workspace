package service;

import java.util.Date;
import java.util.List;

import metier.Client;
import metier.Commande;

public interface ICommandes {
	// cr�er un Client
	Client creeClient(String nom, String prenom, String adresse, String email);
	// ajouter une commande
	Commande creeCommande(Client clt, Date dateCommande, Double montant);
	// lister les commandes d'un client
	List<Commande> listeCommandes(Client clt);
	// lister les clients
	List<Client> listeClients();
}

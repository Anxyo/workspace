package service;

import java.util.Date;
import java.util.List;

import metier.Editeur;
import metier.Livre;

public interface IEdition {
	// cr�ation d'un �diteur
	// Etapes : 
	// Cree un nouvel objet Editeur
	// affecter les propri�t�s
	// m�moriser l'�diteur
	// retourner l'objet cr��
	Editeur creeEditeur(String nom, String adresse);
	
	/**
	 * Etapes :
	 * - Cr�er un nouveau Livre
	 * - renseigner les propri�t�s
	 * - le connecter � l'Editeur transmis
	 * - faire une boucle de cr�ation/initialisation des chapitres
	 * - retourner le nouveau Livre
	 */
	Livre publieLivre(	Editeur e, String titre, String isbn, 
						String auteur, Date datePub, Double prix, 
						String [] titresChapitres, int [] nbPagesChapitre);
	
	/**
	 * Retourne la collection 'livres' des livres d'un Editeur.
	 * Impose de convertir un Set<Livre> en un List<Livre>.
	 */
	List<Livre> listeLivres(Editeur e);
	
	/**
	 * Retourne la liste des Editeurs cr��s.
	 */
	List<Editeur> listeEditeurs();
}

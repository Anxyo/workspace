package presentation;

import static java.lang.System.out;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import metier.Client;
import metier.Editeur;
import service.GestionCommande;
import service.GestionEdition;
import service.ICommandes;
import service.IEdition;
public class Principale {


	public static void main(String[] args) {
		
		//Objet cmplexe, lourd et threadSafe
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("EditeurLivre");
		//objet l�ger et non threadSafe
		EntityManager em1 = emf.createEntityManager();
		EntityManager em2 = emf.createEntityManager();
		
		IEdition ie = new GestionEdition(em2);
		Editeur ed = ie.creeEditeur("Gallimard", "12 rue de la Lune");
		ie.publieLivre(	 ed, 
						"L'�tranger", 
						"0321127420",
						"Albert Camus" , 
						new Date(System.currentTimeMillis()),
						20.50, 
						new String[]{"Chapitre 1","Chapitre 2","Chapitre 3"}, 
						new int[]{34,42,55});
		ie.publieLivre(ed, "La peste", "0321127450","Albert Camus" , new Date(System.currentTimeMillis()),
				22.50, new String[]{"Chapitre 1","Chapitre 2","Chapitre 3"}, new int[]{34,53,75});
		out.println(ed);
		
		
		ICommandes ic = new GestionCommande(em1);
		Client clt = ic.creeClient("Lamy", "Marc", "12 rue des amis", "blamy@gmail.com");
		ic.creeCommande(clt, new Date(System.currentTimeMillis()), 34.50);
		ic.creeCommande(clt, new Date(System.currentTimeMillis()), 43.0);
		out.println(clt);
		
		
		
		em1.close();
		em2.close();
		emf.close();
	}

}

package services;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URI;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import metier.Client;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


@Path("/clients")
public class RessourceClient {
	private Map<Integer, Client> clients = new ConcurrentHashMap<Integer, Client>();
	private AtomicInteger idCourant = new AtomicInteger();

	public RessourceClient() {
		// ajoute un client permettant de tester tout de suite le GET
		Client client = new Client();
		client.setId(19);
		client.setPrenom("Max");
		client.setNom("LaMenace");
		client.setAdresse("12 rue du Bois");
		client.setVille("Nantes");
		client.setZip("44000");
		clients.put(client.getId(), client);
	}

	// M�thodes publi�es
	// //////////////////
	/**
	 * Utilisation des donn�es d'un formulaire.
	 * @param prenom
	 * @param nom
	 * @param adresse
	 * @param ville
	 * @param zip
	 * @return
	 */
	@POST
	@Produces("text/html")
	public Response creeClient( @FormParam("prenom") String prenom,
            					@FormParam("nom") String nom,
            					@FormParam("adresse") String adresse,
            					@FormParam("ville") String ville,
            					@FormParam("zip") String zip) {
		Client client = new Client();
		client.setId(idCourant.incrementAndGet());
		client.setPrenom(prenom);
		client.setNom(nom);
		client.setAdresse(adresse);
		client.setVille(ville);
		client.setZip(zip);
		clients.put(client.getId(), client);
		System.out.println("Client cr��, id = " + client.getId());
		String html = "Client cr&eacute;e : <a href='clients/" + client.getId() +"'>" 
					+ client.getPrenom() + " " + client.getNom() + "</a>";
		String maintenant = DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date());
		return Response.created(URI.create("/clients/" + client.getId()))
				.entity(html)
				.cookie(new NewCookie("heure-passage",maintenant))
				.build();
	}

	// Obtention d'un client, � partir de son id
	@GET
	@Path("/{id}")
	@Produces("text/html")
	public Response getClient(	@PathParam("id") int id,
			 					@CookieParam("heure-passage") String heurePassage) {
		Client client = clients.get(id);
		if (client == null) {
			// d�clenche une exception packageant une 'ressource non trouv�e'
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		// g�n�re une r�ponse
		String maintenant = DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date());
		String reponse = "<!DOCTYPE html><html><head><meta charset='ISO-8859-1'><title>Infos client</title></head><body>"; 
		reponse += "Passage pr&eacute;c&eacute;dent : " + heurePassage;
				reponse += "<br>Client : " + client.getPrenom() + " " + client.getNom() + "<br><a href='/Rest4/index.html'>formulaire</a></body></html>";
		return Response.ok(reponse)
				.cookie(new NewCookie("heure-passage",maintenant))
				.build();
	}



	// M�thodes utilitaires
	// /////////////////////
	// D'abord deux m�thodes de s�rialisation/d�s�rialisation d'un client en XML
	// Bien que l'utilisation de JAXB soit certainement plus judicieuse, on
	// utilise
	// d'abord la 'force brute'.
	/**
	 * Extrait un objet client, � partir d'un bloc XML re�u dans un flux
	 * d'entr�e
	 * 
	 * @param is le flux d'entr�e
	 * @return le client d�s�rialis�
	 */
	private Client extraitClient(InputStream is) {
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = builder.parse(is);
			Element racine = doc.getDocumentElement();
			Client client = new Client();
			// id est un attribut de l'�l�ment racine
			if ((racine.getAttribute("id") != null)
					&& !racine.getAttribute("id").trim().equals("")) {
				client.setId(Integer.valueOf(racine.getAttribute("id")));
			}
			NodeList noeuds = racine.getChildNodes();
			for (int i = 0; i < noeuds.getLength(); i++) {
				if (noeuds.item(i) instanceof Element) { // pour omettre les
															// Nodes de white
															// space
					Element element = (Element) noeuds.item(i);
					if (element.getTagName().equals("prenom")) {
						client.setPrenom(element.getTextContent());
					} else if (element.getTagName().equals("nom")) {
						client.setNom(element.getTextContent());
					} else if (element.getTagName().equals("adresse")) {
						client.setAdresse(element.getTextContent());
					} else if (element.getTagName().equals("ville")) {
						client.setVille(element.getTextContent());
					} else if (element.getTagName().equals("zip")) {
						client.setZip(element.getTextContent());
					}
				}
			}
			return client;
		} catch (Exception e) {
			throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
		}
	}

	/**
	 * Ecriture d'un bloc XML repr�sentant un client, dans un flux de sortie.
	 * 
	 * @param os Le flux utilis�
	 * @param client le client � �crire
	 */
	private void ecritClient(OutputStream os, Client client) {
		PrintStream writer = new PrintStream(os);
		writer.println("<client id=\"" + client.getId() + "\">");
		writer.println(" <prenom>" + client.getPrenom() + "</prenom>");
		writer.println(" <nom>" + client.getNom() + "</nom>");
		writer.println(" <adresse>" + client.getAdresse() + "</adresse>");
		writer.println(" <ville>" + client.getVille() + "</ville>");
		writer.println(" <zip>" + client.getZip() + "</zip>");
		writer.println("</client>");
	}
}

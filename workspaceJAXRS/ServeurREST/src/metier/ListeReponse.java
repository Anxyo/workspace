package metier;

import java.util.ArrayList;
import java.util.List;

public class ListeReponse {
	int statut;
	String message;
	List<Personne> data;
	
	public ListeReponse() {
		data = new ArrayList<Personne>();
	}

	public int getStatut() {
		return statut;
	}

	public void setStatut(int statut) {
		this.statut = statut;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Personne> getData() {
		return data;
	}
	
}

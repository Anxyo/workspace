package metier;

public class Reponse {
	int statut;
	String message;
	Personne data;


	public int getStatut() {
		return statut;
	}

	public void setStatut(int statut) {
		this.statut = statut;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Personne getData() {
		return data;
	}

	public void setData(Personne data) {
		this.data = data;
	}
	
}

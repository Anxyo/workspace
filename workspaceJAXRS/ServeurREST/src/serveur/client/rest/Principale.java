package serveur.client.rest;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/**
 
 * @author JM
 *
 */
public class Principale {
	private final static int port = 8888;
	private final static String host = "http://localhost/";
	 
	public static void main(String[] args) {
		URI uri = UriBuilder.fromUri(host).port(port).build();
		ResourceConfig config = new ResourceConfig(ServiceREST.class);
		com.sun.net.httpserver.HttpServer serveur = JdkHttpServerFactory.createHttpServer(uri, config);
	}
}

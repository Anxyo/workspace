package serveur.client.rest;

import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import metier.ListeReponse;
import metier.Personne;
import metier.Reponse;

@Path("/restjava")
@Singleton
public class ServiceREST {
	// notre stock de personnes...
	private Map<String, Personne> modele = new ConcurrentHashMap<String, Personne>();
	
	public ServiceREST() {
		// ajoute deux personnes au mod�le,
		// devrait normalement aller lire le SGBD...
		Personne p = new Personne("Dupont", "Pierre", "12 rue du Pont", "45000", "Orl�ans",45);
		modele.put(p.getNom()+p.getPrenom(),p);
		p =   new Personne("Durand", "Paul", "12 rue du Rang", "45000", "Orl�ans",40);
		modele.put(p.getNom()+p.getPrenom(),p);
	}
	  // retourne toutes les personnes
	  @GET
	  @Path("/personnes")
	  @Produces(MediaType.APPLICATION_JSON)
	  public ListeReponse getPersonnes() {
		ListeReponse lr = new ListeReponse();
		for (Personne p : modele.values()) {
			lr.getData().add(p);
		}
		lr.setStatut(200);
	    return lr;
	  }

	  // retourne une seule personne
	  @GET
	  @Path("/personne")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Reponse getPersone(@QueryParam("nom") String nom, @QueryParam("prenom") String prenom) {
		  Personne p = modele.get(nom + prenom);
		  Reponse r = new Reponse();
		  r.setData(p);
		  if (p == null) {
			  r.setStatut(404);
			  r.setMessage("Personne non trouv�e");
		  } else {
			  r.setStatut(200);
			  r.setMessage("Personne trouv�e");
		  }
		  return r;
	  }	
	  
	  
	  // ajoute une personne en r�cup�rant les donn�es d'un formulaire HTML (type mime 'application/x-www-form-urlencoded')
	  // retourne un code 404 si le parsing/conversion de type n'est pas possible.
	  @POST
	  @Consumes("application/x-www-form-urlencoded")
	  public Response inserePersonne(	@FormParam("nom") String nom, 
			  						@FormParam("prenom") String prenom, 
			  						@FormParam("adresse") String adresse, 
			  						@FormParam("age") int age, 
			  						@FormParam("codePostal") String codePostal, 
			  						@FormParam("ville") String ville) {
		  Personne p = new Personne(nom,prenom,adresse,codePostal, ville, age);
		  modele.put(p.getNom()+p.getPrenom(), p);
		  return Response.created(URI.create("/personne?nom=" + p.getNom() + "&prenom=" + p.getPrenom()))
					.build();
	  }
}

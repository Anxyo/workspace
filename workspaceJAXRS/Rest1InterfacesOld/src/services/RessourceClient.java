package services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import metier.Client;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/*
 * Cette couche de services peut �tre :
 * - un singleton, potentiellement travers� par plusieurs threads.
 * - une instance fabriqu�ee � chaque requete, et d�truite � sa fin.
 * L'impl�mentation choisie ici est celle d'un singleton, ce qui impose une gestion 
 * 'thread safe'.
 */


public class RessourceClient implements IRessourceClient {
	private Map<Integer, Client> clients = new ConcurrentHashMap<Integer, Client>();
	private AtomicInteger idCourant = new AtomicInteger();
	
	public RessourceClient(){
		Client client = new Client();
		client.setId(19);
		client.setPrenom("Max");
		client.setNom("LaMenace");
		client.setAdresse("12 rue du Bois");
		client.setVille("Nantes");
		client.setZip("44000");
		clients.put(client.getId(), client);
	}
	

	public Response creeClient(InputStream is) {
	Client client = readClient(is);
	client.setId(idCourant.incrementAndGet());
	clients.put(client.getId(), client);
	System.out.println("Client cr��, id = " + client.getId());
	return Response.created(URI.create("/clients/" + client.getId())).build();
	}

	public StreamingOutput getClient(@PathParam("id") int id) {
	final Client client = clients.get(id);
	if (client == null) {
		// d�clenche une exception packageant une 'ressource non trouv�e'
		throw new WebApplicationException(Response.Status.NOT_FOUND);
	}
	// retourne une repr�sentation XML du client trouv�, g�n�r�e la m�thode
	// utilitaire ecritXMLClient()
	return new StreamingOutput() {
		public void write(OutputStream outputStream)
					throws IOException, WebApplicationException {
							ecritXMLClient(outputStream, client);
					}
		};
	}
	
	// M�thodes utilitaires
	///////////////////////
	// D'abord deux m�thodes de s�rialisation/d�s�rialisation d'un client en XML
	// Bien que l'utilisation de JAXB soit certainement plus judicieuse, on utilise 
	// d'abord la 'force brute'. 
	/**
	 * Extrait un objet client, � partir d'un bloc XML re�u dans un flux d'entr�e
	 * @param is le flux d'entr�e
	 * @return le client d�s�rialis�
	 */
	private Client readClient(InputStream is) {
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.parse(is);
			Element racine = doc.getDocumentElement();
			Client client = new Client();
			// id est un attribut de l'�l�ment racine
			if ((racine.getAttribute("id") != null)&& !racine.getAttribute("id").trim().equals("")) {
				client.setId(Integer.valueOf(racine.getAttribute("id")));
			}
			NodeList noeuds = racine.getChildNodes();
			for (int i = 0; i < noeuds.getLength(); i++) {
				if (noeuds.item(i) instanceof Element){   	// pour omettre les Nodes de white space
					Element element = (Element) noeuds.item(i);
					if (element.getTagName().equals("prenom")) {
						client.setPrenom(element.getTextContent());
					}
					else if (element.getTagName().equals("nom")) {
						client.setNom(element.getTextContent());
					}
					else if (element.getTagName().equals("adresse")) {
						client.setAdresse(element.getTextContent());
					}
					else if (element.getTagName().equals("ville")) {
						client.setVille(element.getTextContent());
					}
					else if (element.getTagName().equals("zip")) {
						client.setZip(element.getTextContent());
					}
				}
			}
			return client;
		}
		catch (Exception e) {
			throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
		}
	}
	
	/**
	 * Ecriture d'un bloc XML repr�sentant un client, dans un flux de sortie. 
	 * 
	 * @param os Le flux utilis�
	 * @param client le client � �crire
	 */
	private void ecritXMLClient(OutputStream os, Client client) {
		PrintStream writer = new PrintStream(os);
		writer.println("<client id=\"" + client.getId() + "\">");
		writer.println(" <prenom>" + client.getPrenom()	+ "</prenom>");
		writer.println(" <nom>" + client.getNom() + "</nom>");
		writer.println(" <adresse>" + client.getAdresse() + "</adresse>");
		writer.println(" <ville>" + client.getVille() + "</ville>");
		writer.println(" <zip>" + client.getZip() + "</zip>");
		writer.println("</client>");
	}
}

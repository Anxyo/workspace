package mini.jersey;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;
import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Cet exemple montre le m�me exemple minimal publi� en dehors d'un container de servlets,
 * dans ce cas avec le serveur Http inclus dans le JDK.
 * Le code est encore plus simple, la classe de configuration a disparue et c'est nous qui 
 * devons d�signer explicitement les ressources � publier.
 * Cette approche permet de fabriquer des applications locales publiant des services REST.
 * 
 *  Si on souhaitait une application montant 's�rieusement' en charge, on pr�fera utiliser
 *  grizzly ou un autre container 'embarqu�' dans l'application locale (voir la doc).
 *   
 * @author JM
 *
 */
public class Principale {
	private final static int port = 8888;
	private final static String host = "http://localhost/";
	 
	public static void main(String[] args) {
		URI uri = UriBuilder.fromUri(host).port(port).build();
		ResourceConfig config = new ResourceConfig(ServiceMini.class);
		// Warning : ce type ne fait pas partie de l'API 'officielle' du JDK !
		JdkHttpServerFactory.createHttpServer(uri, config);

	}
}

package mini.jersey;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;


@ApplicationPath("rest")
public class AppliMini extends ResourceConfig {
	
	    public AppliMini() {
	        packages("mini.jersey");
	    }
}


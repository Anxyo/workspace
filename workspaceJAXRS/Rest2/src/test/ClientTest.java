package test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.Response;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe de tests JUnit utilisant l'API cliente de JAX-RS, vue plus tard. 
 *
 */
public class ClientTest {
	private static Client client;
	
	@BeforeClass
	public static void init() {
		// une seule instance ('heavyweight' !) est r�utilis�e par chacun des tests
		client = ClientBuilder.newClient();
	}

	@AfterClass
	public static void close() {
		// fermeture connexion r�seau (remise dans le pool de la cnx)
		client.close();
	}

	@Test
	public void testGetClientIdf() {
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest2/services/clients/idf/19";
		Builder builder = client.target(urlCible).request();
		assertTrue(builder.get(String.class).contains("LaMenace"));
	}

	@Test
	public void testCreeClientIdf() {
		StringBuilder sb = new StringBuilder();
		sb.append("<client>");
		sb.append("<prenom>Pierre</prenom>");
		sb.append("<nom>Dupont-Lajoie</nom>");
		sb.append("<adresse>23 rue Robert Lindet</adresse>");
		sb.append("<ville>Paris</ville>");
		sb.append("<zip>75015</zip>");
		sb.append("</client>");
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest2/services/clients/idf/";
		Builder builder = client.target(urlCible).request();
		Response reponse = builder.post(Entity.xml(sb.toString()));
		assertEquals(201,reponse.getStatus());	// cr�� ?
	}

	@Test
	public void testGetClientPaca() {
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest2/services/clients/paca/Tim-BernersLee";
		Builder builder = client.target(urlCible).request();
		assertTrue(builder.get(String.class).contains("BernersLee"));
	}

	@Test
	public void testCreeClientPaca() {
		StringBuilder sb = new StringBuilder();
		sb.append("<client>");
		sb.append("<prenom>Jim</prenom>");
		sb.append("<nom>Clark</nom>");
		sb.append("<adresse>15 square du Petit Soldat</adresse>");
		sb.append("<ville>Paris</ville>");
		sb.append("<zip>75009</zip>");
		sb.append("</client>");
		// le builder pour construire la requ�te;
		String urlCible = "http://localhost:8080/Rest2/services/clients/paca/";
		Builder builder = client.target(urlCible).request();
		Response reponse = builder.post(Entity.xml(sb.toString()));
		assertEquals(201,reponse.getStatus());	// cr�� ?
	}

}

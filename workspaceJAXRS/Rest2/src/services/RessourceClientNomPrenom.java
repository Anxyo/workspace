package services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import metier.Client;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class RessourceClientNomPrenom {
	private Map<String, Client> clients =
	           new ConcurrentHashMap<String, Client>();

	public RessourceClientNomPrenom() {
		// ajoute un client permettant de tester tout de suite le GET
				Client client = new Client();
				client.setId(32);
				client.setPrenom("Tim");
				client.setNom("BernersLee");
				client.setAdresse("12 rue de la Calanque");
				client.setVille("Marseilles");
				client.setZip("13005");
				clients.put( client.getPrenom() + "-" + client.getNom(), client);
	}
	
	
	   @GET
	   @Path("{prenom}-{nom}")
	   @Produces("application/xml")
	   public StreamingOutput getClient(@PathParam("prenom") String prenom,
	                                      @PathParam("nom") String nom)
	   {
	      final Client client = clients.get(prenom + "-" + nom);
	      if (client == null)
	      {
	         throw new WebApplicationException(Response.Status.NOT_FOUND);
	      }
	      return new StreamingOutput()
	      {
	         public void write(OutputStream outputStream) throws IOException, WebApplicationException
	         {
	            ecritClient(outputStream, client);
	         }
	      };
	   }

	   @PUT
	   @Path("{prenom}-{nom}")
	   @Consumes("application/xml")
	   public void majClient(@PathParam("prenom") String prenom,
	                              @PathParam("nom") String nom,
	                              InputStream is)
	   {
	      Client maj = extraitClient(is);
	      Client actuel = clients.get(prenom + "-" + nom);
	      if (actuel == null) 
	    	  throw new WebApplicationException(Response.Status.NOT_FOUND);

	      actuel.setPrenom(maj.getPrenom());
	      actuel.setNom(maj.getNom());
	      actuel.setAdresse(maj.getAdresse());
	      actuel.setVille(maj.getVille());
	      actuel.setZip(maj.getZip());
	   }

	   @POST
	   @Consumes("application/xml")
	   public Response creeClient(InputStream is)
	   {
	      Client client = extraitClient(is);
	      String index = client.getPrenom() + "-" + client.getNom();
	      clients.put(index, client);
	      System.out.println("Client cree : " + index);
	      return Response.created(URI.create("/clients/paca/" + index)).build();

	   }


	   protected void ecritClient(OutputStream os, Client client) throws IOException
	   {
	      PrintStream writer = new PrintStream(os);
	      writer.println("<client>");
	      writer.println("   <prenom>" + client.getPrenom() + "</prenom>");
	      writer.println("   <nom>" + client.getNom() + "</nom>");
	      writer.println("   <adresse>" + client.getAdresse() + "</adresse>");
	      writer.println("   <ville>" + client.getVille() + "</ville>");
	      writer.println("   <zip>" + client.getZip() + "</zip>");
	      writer.println("</client>");
	   }

	   protected Client extraitClient(InputStream is)
	   {
		   try {
				DocumentBuilder builder = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				Document doc = builder.parse(is);
				Element racine = doc.getDocumentElement();
				Client client = new Client();
				// id est un attribut de l'�l�ment racine
				if ((racine.getAttribute("id") != null)
						&& !racine.getAttribute("id").trim().equals("")) {
					client.setId(Integer.valueOf(racine.getAttribute("id")));
				}
				NodeList noeuds = racine.getChildNodes();
				for (int i = 0; i < noeuds.getLength(); i++) {
					if (noeuds.item(i) instanceof Element) { // pour omettre les
																// Nodes de white
																// space
						Element element = (Element) noeuds.item(i);
						if (element.getTagName().equals("prenom")) {
							client.setPrenom(element.getTextContent());
						} else if (element.getTagName().equals("nom")) {
							client.setNom(element.getTextContent());
						} else if (element.getTagName().equals("adresse")) {
							client.setAdresse(element.getTextContent());
						} else if (element.getTagName().equals("ville")) {
							client.setVille(element.getTextContent());
						} else if (element.getTagName().equals("zip")) {
							client.setZip(element.getTextContent());
						}
					}
				}
				return client;
			} catch (Exception e) {
				throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
			}
	   }
}

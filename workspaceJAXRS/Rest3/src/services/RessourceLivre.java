package services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.UriInfo;

@Path("/livres")
public class RessourceLivre {
		
	   @GET
	   @Path("/matrix/{auteur}/{annee}/{titre}")
	   @Produces("text/plain")
	   public String getFromMatrixParam(@PathParam("auteur") String auteur,
	                                    @PathParam("annee") String annee,
	                                    @PathParam("titre") String titre,
	                                    @MatrixParam("genre") String genre)	// avec la version actuelle de Jersey 
	                                    									// les @MatrixParameters semblent ne
	                                    									// fonctionner qu'avec le dernier segment
	                                    									// de l'URI (contrairement de RestEasy de
	                                    									// de JBoss)                                   
	   {
	      return "getFromMatrixParam() : Livre, genre : " + genre + 
	    		  ", titre : " + titre + 
	    		   ", publication : " + annee + 
	    		  ", auteur : " + auteur; 
	    		  //", chemin uri : " + titre.getPath();
	   }

	   @GET
	   @Path("/segment/{auteur}/{titre}/{annee}")
	   @Produces("text/plain")
	   public String getFromPathSegment(@PathParam("auteur") String auteur,
	                                    @PathParam("titre") PathSegment livre,
	                                    @PathParam("annee") String annee)
	   {
	      // extraction par code du 1er MatrixParameter : on le trouve qu'il 
		  // soit dans le dernier segment ou pas (dans l'avant dernier, ici)
		  String genre = livre.getMatrixParameters().getFirst("genre");
	      return "getFromPathSegment() : Livre, genre : " + genre + 
	    		  ", publication : " + annee + 
	    		  ", auteur : " + auteur + 
	    		  ", chemin uri : " + livre.getPath();
	   }

	   @GET
	   @Path("/segments/{auteur}/{titre : .+}/annee/{annee}")
	   @Produces("text/plain")
	   public String getFromMultipleSegments(@PathParam("auteur") String auteur,
	                                         @PathParam("titre") List<PathSegment> titre,
	                                         @PathParam("annee") String annee)
	   {
	      String aAfficher ="getFromMultipleSegments() : Livre, annee : " + annee + ", auteur : " + auteur + ", titre(s) : ";
	      // retrouve tous les sous �l�ments du segment de titre
	      for (PathSegment segment : titre)
	      {
	         aAfficher += "\n\t- " + segment.getPath();
	      }
	      return aAfficher;
	   }

	   @GET
	   @Path("/uriinfo/{auteur}/{titre}/{annee}")
	   @Produces("text/plain")
	   public String getFromUriInfo(@Context UriInfo info)
	   {
	      String auteur = info.getPathParameters().getFirst("auteur");
	      String anneePublication = info.getPathParameters().getFirst("annee");
	      PathSegment titre = info.getPathSegments().get(3);
	      String genre = titre.getMatrixParameters().getFirst("genre");
	      return "getFromUriInfo() : Livre, genre : " + genre + 
	    		  ", publication : " + anneePublication + 
	    		  ", auteur : " + auteur + 
	    		  ", titre : " + titre.getPath();
	   }
}

package client.swing;

import org.jdesktop.application.SingleFrameApplication;

/**
 * The main class of the application.
 */
public class Application extends SingleFrameApplication {

    /**
     * Cr�e et affiche la fen�tre principale au d�marrage
     */
    @Override protected void startup() {
        show(new VuePrincipale(this));
    }

    /**
     * Cette m�thode sert � initiliser la fenetre sp�cifique en lui injectant des ressources.
     * Comme elle est pleinement initilis�e depuis le builer Netbeans, elle n'est pas n�cessaire.
     */
    @Override protected void configureWindow(java.awt.Window root) {
    }

    /**
     * Un getter static pour l'instance de l'application
     * @return l'instance de l'Application
     */
    public static Application getApplication() {
        return Application.getInstance(Application.class);
    }

    /**
     * m�thode principale
     */
    public static void main(String[] args) {
        launch(Application.class, args);
    }
}

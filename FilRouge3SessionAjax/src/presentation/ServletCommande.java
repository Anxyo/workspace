package presentation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import metier.Client;
import metier.Panier;
import service.Facade;

/**
 * Servlet implementation class ServletCommande
 */
@WebServlet("/Commande")
public class ServletCommande extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private static final String INSCR = "btnInscription";
	private static final String CNX = "btnConnexion";
	private static final String VALIDER = "valider_achat";
	private static final String RETOUR = "btnRetourCata";
	
	
	
	
	
  
    public ServletCommande() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer position = 0;
		// retrouve la session de l'utilisateur
		HttpSession session = request.getSession(true);
		Panier panier = (Panier)session.getAttribute("panier"); 
		Facade facade = new Facade();
		facade.setPanier(panier);
		
		//request.setAttribute("panier", panier);
		
		if (request.getParameter(VALIDER) != null) {
			request.getRequestDispatcher("/catalogue/connexion.jsp").forward(request, response);
		}
		else if (request.getParameter(RETOUR) != null) {
			request.getRequestDispatcher("/catalogue/panier.jsp").forward(request, response);
		}
		
		
		
		else if (request.getParameter(CNX) != null) {
			String user = request.getParameter("nomCnx");
			String userPwd = request.getParameter("pwdCnx");
			
			Client clt = facade.rechercheClient(user, userPwd);
			System.out.println(user + ' ' + userPwd);
			
			
		}
		else if (request.getParameter(INSCR) != null) {
			
			String userNom = request.getParameter("nomInsc");
			String userPrenom = request.getParameter("prenomInsc");
			String userAdresse = request.getParameter("adresseInsc");
			String userPwdInsc = request.getParameter("pwdInsc");
			
			Client clt = facade.creeClient(userPwdInsc, userNom, userPrenom, userAdresse);
			System.out.println(userPwdInsc + ' ' + userNom + ' ' + userPrenom + ' ' + userAdresse);	
		}
		
		request.getRequestDispatcher("/catalogue/commande.jsp").forward(request, response);
		
		
		
	}

}

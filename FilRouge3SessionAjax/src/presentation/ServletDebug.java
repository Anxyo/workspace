package presentation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/ServletDebug")
public class ServletDebug extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public ServletDebug() {
        super();
    
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// suppression des donn�es de session, si elles existent
		HttpSession session = request.getSession();
		session.removeAttribute("panier");
		session.removeAttribute("pwd");
		session.removeAttribute("uid");
		session.invalidate();
		System.out.println("Session supprim�e");
		System.out.println("Repositiory vid�");
		response.getWriter().println("Donn�es supprim�es. Clickez sur back !");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

package presentation;

import metier.Produit;

/**
 * Objet DTO contenant toutes les donn�es n�cessaires � l'afficchage du
 * catalogue. Envoy� en JSON.
 * 
 * @author JM
 *
 */
public class CatalogueDTO {
	private String nom;
	private String description;
	private double prix;
	private String urlImage;

	// constructeurs
	public CatalogueDTO() {
	}

	public CatalogueDTO(Produit produitCourant) {
		this.nom = produitCourant.getNom();
		this.description = produitCourant.getDescription();
		this.prix = produitCourant.getPrix();
		this.urlImage = produitCourant.getUrlImage();
	}

	// accesseurs
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}
	
	// m�thodes utilitaires
	@Override
	public String toString() {
		return "CatalogueDTO [nom=" + nom + ", description=" + description + ", prix=" + prix + ", urlImage=" + urlImage
				+ "]";
	}
}

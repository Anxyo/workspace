package persistance;

import metier.Commande;
import metier.DetailCommande;

public class CommandeDAO {

	public void enregistreCommande(Commande cmd) {
		//attribution des Id
		cmd.setId(Repository.getInstance().getCompteurCommandes().getAndIncrement());
		
		for(DetailCommande dc : cmd.getDetails()) {
			dc.setId(Repository.getInstance().getCompteurDetailCommande().getAndIncrement());
		}
		//enregistrement
		Repository.getInstance().getCommandes().put(cmd.getId(), cmd);
	}
}

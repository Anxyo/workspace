package persistance;

import java.util.List;

import metier.Produit;

public class ProduitDAO {
	
	//charger les produits depuis la table produits de la base de donn�es
	public List<Produit> chargeProduits() {
		return Repository.getInstance().getProduits();
	}
}

package persistance;

import metier.Client;

public class ClientDAO {

	
	public Client getClient(String nom, String motPasse) {
		return Repository.getInstance().getClients().get(nom + motPasse);
	}
	
	public void enregistreClient(Client client) {
		
		client.setId(Repository.getInstance().getCompteurClients().getAndIncrement());
		Repository.getInstance().getClients().put(client.getNom()+client.getMotPasse(), client);
	}
}

package metier;

import java.util.List;
import java.util.Vector;

import persistance.ProduitDAO;
/**
 * Repr�sente le catalogue des produits propos�s au visiteur.
 * Un seul objet de ce type devrait exister ("singleton").
 * Cet objet unique sera en lecture seule, une fois initialis�.
 * 
 * Notes :
 * Pour plus de d�tails sur le pattern singleton regarder :
 * 		https://fr.wikipedia.org/wiki/Singleton_(patron_de_conception)
 * @author Administrateur
 *
 */
public class Catalogue {
	private List<Produit> produits;
	
	///////////////////////// Impl�mentation du singleton //////////////////////////
	private static class Container {
		private static Catalogue leCatalogue = new Catalogue(); 
	}
	
	// le constructeur private, effectuant l'initialisation de l'unique objet
	private Catalogue() {
		produits = new Vector<Produit>();
		initCatalogue();
	}
	
	// pour acc�der � l'instance unique
	public static Catalogue getInstance() {
		return Container.leCatalogue;
	}

	///////////////////////////////// M�thodes d'instance ////////////////////////////
	/**
	 * Retourne la liste des produits du catalogue
	 * @return
	 */
	public List<Produit> getProduits() {
		return produits;
	}
	
	/**
	 * Ajoute un produit au catalogue
	 * @param p le produit � ajouter
	 */
	public void ajoutProduit(Produit p) {
		produits.add(p);
	}
	/**
	 * Retourne le nombre de produits au catalogue
	 * @return
	 */
	public int getNbProduits() {
		return produits.size();
	}
	
	// initialise le catalogue avec des produits
	private void initCatalogue() {
		ProduitDAO dao = new ProduitDAO();
		List<Produit> liste = dao.chargeProduits();
		for (Produit p : liste) {
			this.produits.add(p);
		}
	}
}

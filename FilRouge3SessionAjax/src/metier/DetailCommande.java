package metier;
/**
 * Repr�sente une ligne de d�tail dans une commande.
 * Reprend la structure d'une ligne de d�tail du panier,
 * avec, en plus, une r�f�rence vers la commande associ�e.
 * @author Administrateur
 *
 */
public class DetailCommande {
	private Integer id;
	private Commande commande; 
	private Produit produit;
	private int quantite;
	private double sousTotal;
	
	// cr�e un DetailCommande � partir d'un DetailPanier
	public DetailCommande(Commande commande, DetailPanier dp) {
		this.commande = commande;
		produit = dp.getProduit();
		quantite = dp.getQuantite();
		sousTotal = dp.getSousTotal();
	}
	
	// accesseurs	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Commande getCommande() {
		return commande;
	}
	public Produit getProduit() {
		return produit;
	}
	public int getQuantite() {
		return quantite;
	}
	public double getSousTotal() {
		return sousTotal;
	}

	@Override
	public String toString() {
		return "Produit : " + produit.getNom() + " " + produit.getDescription() +
				", qut� : " + quantite + ", sous total : " + sousTotal;
	}
	
}

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Panier</title>
<link rel="stylesheet" href="css/styles.css">
<style>

   h1 {
   		float: left;
   }
   
   article, nav {
	  	border: solid 3px darkgrey; 
   		border-radius: 10px; 
   		width: 80%;
   		padding: 10px 10px 10px 10px;
   }
   nav {
   		margin-top: 20px;
   		height: 60px;
   }
   
   th {
   	border-bottom: 1px solid darkgrey;
   }
   td {
   	padding-left: 10px;
   	padding-right: 10px;
   }
   table {
   	margin-bottom: 20px;
   }
</style>
</head>
<body bgcolor="white">
<header>
	<img src="images/logo5.png"
		style="float: left; border-radius: 30px;margin-left: 30px;display: inline-block;" width="75%" height="100px">
	<h1>&nbsp;&nbsp;Shop</h1>
	<br style="clear: both;">
	<br>
	<hr >
</header>
<article>
<h2>Votre panier</h2>
	<c:choose> 
		<c:when test = '${empty panier or panier.options.size() == 0}'>  
			Votre panier est vide !
		</c:when>
		<c:otherwise>
			Le montant total de votre panier est de ${panier.total} euros, et il contient les articles suivants :<br><br>
			<form action="Catalogue" method="post">
				<table>
					<tr><th>Nom</th><th>Description</th><th>Quantit�</th><th>Total</th></tr>
					<c:forEach items="${panier.options}" var="detailPanier" varStatus="position">
						<tr><td>${detailPanier.produit.nom}</td><td>${detailPanier.produit.description}</td>
							<td><input type="number" size="2" min="0" max="99" name="qute_${position.index}" value="${detailPanier.quantite}"></td>
							<td>${detailPanier.sousTotal}</td></tr>
					</c:forEach>
				</table>
				<input type="submit" value="Mettre � jour" name="edit_panier" style='width: 150px;'>
			</form>
		</c:otherwise>
	</c:choose>
</article>
<nav>
	<br>
	<form action='Catalogue?position=${position}' method="post">
		<input type="submit" value="Continuer mes achats" name="continuer_achat" style='width: 150px;float: left;'>
	</form>
	<form action='Commande' method='post'>
		<input type="submit" value="valider ma commande" name="valider_achat" style='width: 150px;float: left;'>
	</form>
</nav>
</body>
</html>
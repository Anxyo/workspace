<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Connexion</title>
<script src='script/jquery.js'></script>
<script>

$(function(){
	
	$('#rdconnexion').click(function(){
		$('#entreeinscription').hide();
		$('#entreeconnexion').show();
	});
	
	$('#rdinscription').click(function(){
		$('#entreeconnexion').hide();
		$('#entreeinscription').show();
	});
	
	
	
});

</script>

</head>
<body>
<h1>Connexion / inscription</h1>

<nav>
	<input type='radio' id='rdconnexion' name='Connexion'><label for='connectemoi'>Connexion</label>
	<input type='radio' id='rdinscription' name='Connexion'><label for='inscritmoi'>Inscription</label>
</nav>


<form action='Commande' method='post'>	
<div id='entreeconnexion' hidden="hidden">
	<input type='text' name='nomCnx' placeholder='Entrez votre nom'>
	<input type="password" name='pwdCnx' placeholder='Entrez votre mot de passe'>
	<button type='submit' name='btnConnexion'>Connexion</button></div>
</form>

<form action='Commande' method='post'>	
<div id='entreeinscription' hidden="hidden">
	<input type='text' name='nomInsc' placeholder='Entrez votre nom'>
	<input type='text' name='prenomInsc' placeholder='Entrez votre prenom'>
	<input type='text' name='adresseInsc' placeholder='Entrez votre adresse'>
	<input type="password" name='pwdInsc' placeholder='Entrez votre mot de passe'>
	<button type='submit' name='btnInscription'>Inscription</button>
</div>
</form>


</body>
</html>